/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Detector/Muon/TileID.h"
#include "GaudiKernel/MsgStream.h"
#include "MuonCardiacIDPack.h"

class MuonCardiacChID final {
public:
  MuonCardiacChID() = default;
  explicit MuonCardiacChID( unsigned int ID ) : m_channelID( ID ){};
  MuonCardiacChID& setStation( unsigned int value ) {
    return set<MuonCardiacIDPack::shiftStation, MuonCardiacIDPack::maskStation>( value );
  }
  MuonCardiacChID& setRegion( unsigned int value ) {
    return set<MuonCardiacIDPack::shiftRegion, MuonCardiacIDPack::maskRegion>( value );
  }
  MuonCardiacChID& setQuadrant( unsigned int value ) {
    return set<MuonCardiacIDPack::shiftQuadrant, MuonCardiacIDPack::maskQuadrant>( value );
  }
  MuonCardiacChID& setChamber( unsigned int value ) {
    return set<MuonCardiacIDPack::shiftChamber, MuonCardiacIDPack::maskChamber>( value );
  }
  MuonCardiacChID& setChIDX( unsigned int value ) {
    return set<MuonCardiacIDPack::shiftPhChIDX, MuonCardiacIDPack::maskPhChIDX>( value );
  }
  MuonCardiacChID& setChIDY( unsigned int value ) {
    return set<MuonCardiacIDPack::shiftPhChIDY, MuonCardiacIDPack::maskPhChIDY>( value );
  }
  MuonCardiacChID& setReadout( unsigned int value ) {
    return set<MuonCardiacIDPack::shiftReadout, MuonCardiacIDPack::maskReadout>( value );
  }

  [[nodiscard]] unsigned int getID() const { return m_channelID; }
  [[nodiscard]] unsigned int getStation() const {
    return get<MuonCardiacIDPack::shiftStation, MuonCardiacIDPack::maskStation>();
  }
  [[nodiscard]] unsigned int getRegion() const {
    return get<MuonCardiacIDPack::shiftRegion, MuonCardiacIDPack::maskRegion>();
  }
  [[nodiscard]] unsigned int getQuadrant() const {
    return get<MuonCardiacIDPack::shiftQuadrant, MuonCardiacIDPack::maskQuadrant>();
  }
  [[nodiscard]] unsigned int getChamber() const {
    return get<MuonCardiacIDPack::shiftChamber, MuonCardiacIDPack::maskChamber>();
  }
  [[nodiscard]] unsigned int getChIDX() const {
    return get<MuonCardiacIDPack::shiftPhChIDX, MuonCardiacIDPack::maskPhChIDX>();
  }
  [[nodiscard]] unsigned int getChIDY() const {
    return get<MuonCardiacIDPack::shiftPhChIDY, MuonCardiacIDPack::maskPhChIDY>();
  }
  [[nodiscard]] unsigned int getReadout() const {
    return get<MuonCardiacIDPack::shiftReadout, MuonCardiacIDPack::maskReadout>();
  }

  /// printout to std::ostream
  std::ostream& printOut( std::ostream& ) const;
  /// printout to MsgStream
  MsgStream& printOut( MsgStream& ) const;

  friend bool operator==( const MuonCardiacChID& lhs, const MuonCardiacChID& rhs ) {
    return lhs.m_channelID == rhs.m_channelID;
  }
  // output to std::ostream
  friend std::ostream& operator<<( std::ostream& os, const MuonCardiacChID& id ) { return id.printOut( os ); }

  friend MsgStream& operator<<( MsgStream& os, const MuonCardiacChID& id ) { return id.printOut( os ); }

private:
  template <unsigned int shift, unsigned int mask>
  MuonCardiacChID& set( unsigned int value ) {
    MuonCardiacIDPack::setBit( m_channelID, shift, mask, value );
    return *this;
  }
  template <unsigned int shift, unsigned int mask>
  unsigned int get() const {
    return MuonCardiacIDPack::getBit( m_channelID, shift, mask );
  }
  unsigned int m_channelID{0};
};

inline MsgStream& MuonCardiacChID::printOut( MsgStream& os ) const {
  os << "[";
  return os << getStation() << "," << getRegion() << "," << getQuadrant() << "," << getChamber() << "," << getChIDX()
            << "," << getChIDY() << "," << getReadout() << "]";
}

inline std::ostream& MuonCardiacChID::printOut( std::ostream& os ) const {
  os << "[";
  return os << getStation() << "," << getRegion() << "," << getQuadrant() << "," << getChamber() << "," << getChIDX()
            << "," << getChIDY() << "," << getReadout() << "]";
}
