/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/KeyedObject.h"
#include "MuonHitTraceBack.h"
#include "MuonPhChID.h"
#include <utility>

class MuonPhyChannelInput : public KeyedObject<int> {
public:
  MuonPhyChannelInput( MuonPhChID phID, MuonHitTraceBack value ) : m_ID( phID ), m_hitTraceBack( std::move( value ) ){};

  [[nodiscard]] MuonPhChID&       phChID() { return m_ID; }
  [[nodiscard]] const MuonPhChID& phChID() const { return m_ID; }

  MuonPhyChannelInput& setPhChID( MuonPhChID& value ) {
    m_ID = value;
    return *this;
  }
  [[nodiscard]] MuonHitTraceBack&       getHitTraceBack() { return m_hitTraceBack; }
  [[nodiscard]] const MuonHitTraceBack& getHitTraceBack() const { return m_hitTraceBack; }

private:
  MuonPhChID       m_ID;
  MuonHitTraceBack m_hitTraceBack;
};

using MuonPhyChannelInputs = KeyedContainer<MuonPhyChannelInput, Containers::HashMap>;
