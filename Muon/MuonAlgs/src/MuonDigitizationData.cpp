/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MuonDigitizationData.h"
#include "MuonCardiacChannelOutput.h"
#include "MuonPhyChannelInput.h"
#include "MuonPhysicalChannel.h"
#include "MuonPhysicalChannelOutput.h"
#include <string>

template <class T>
MuonDigitizationData<T>::MuonDigitizationData( std::string path, MsgStream* log )
    : m_path{std::move( path )}, m_log{log} {
  for ( auto& i : m_partition ) i = new KeyedContainer<T>;
}

template <class T>
MuonDigitizationData<T>::~MuonDigitizationData() {
  if ( m_registered == 0 ) {
    *m_log << MSG::DEBUG << " the containers for " << System::typeinfoName( typeid( T ) )
           << " have not been registered in the TES => deleting containers " << endmsg;
    for ( auto& i : m_partition ) delete i;
  } else if ( m_registered > 0 ) {
    *m_log << MSG::DEBUG << " the containers have been registed in the TES " << endmsg;
  } else if ( m_registered < 0 ) {
    *m_log << MSG::DEBUG << " the containers have not been registered in the TES"
           << " for some error." << endmsg;
  };
}

template <class T>
StatusCode MuonDigitizationData<T>::registerPartitions( IDataProviderSvc* eventSvc, int partitions,
                                                        const DeMuonDetector& muonDet ) {
  for ( int i = 0; i != partitions; ++i ) {
    const int ista = i / 4;
    const int ireg = i - ista * 4 + 1;
    auto      path = m_path + "/" + muonDet.getStationName( ista ) + "/R" + std::to_string( ireg ) + "/" + m_path;

    StatusCode result = eventSvc->registerObject( path, m_partition[i] );
    if ( result.isFailure() ) {
      *m_log << MSG::INFO << i << " non funziona " << m_partition[i]->size() << endmsg;
      m_registered = -1;
    }
  }
  m_registered = 1;
  return StatusCode::SUCCESS;
}

template <class T>
T* MuonDigitizationData<T>::addMuonObject( int i, std::unique_ptr<T> p0 ) {
  auto ret = p0.get();
  m_partition[i]->add( p0.release() );
  return ret;
}

template <class T>
void MuonDigitizationData<T>::eraseMuonObject( int i, T* p0 ) {
  m_partition[i]->remove( p0 );
  delete p0;
}

template class MuonDigitizationData<MuonPhyChannelInput>;
template class MuonDigitizationData<MuonPhysicalChannel>;
template class MuonDigitizationData<MuonPhysicalChannelOutput>;
template class MuonDigitizationData<MuonCardiacChannelOutput>;
