/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

namespace MuonHitOriginPack {

  inline constexpr unsigned int       bitHitOrigin = 3;
  inline constexpr const unsigned int bitBX        = 3;

  inline constexpr unsigned int shiftHitOrigin = 0;
  inline constexpr unsigned int shiftBX        = shiftHitOrigin + bitHitOrigin;

  inline constexpr unsigned int maskHitOrigin = ( ( ( (unsigned int)1 ) << bitHitOrigin ) - 1 ) << shiftHitOrigin;
  inline constexpr unsigned int maskBX        = ( ( ( (unsigned int)1 ) << bitBX ) - 1 ) << shiftBX;

  inline constexpr unsigned int getBit( unsigned int packedValue, unsigned int shift, unsigned int mask ) {
    return ( ( packedValue & mask ) >> shift );
  }

  inline constexpr void setBit( unsigned int& origValue, unsigned int shift, unsigned int mask,
                                unsigned int bitValue ) {
    origValue = ( ( ( bitValue << shift ) & mask ) | ( origValue & ~mask ) );
  }

} // namespace MuonHitOriginPack
