/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

namespace MuonPhChIDPack {

  inline constexpr unsigned int bitGap      = 1;
  inline constexpr unsigned int bitReadout  = 1;
  inline constexpr unsigned int bitFrontEnd = 1;
  inline constexpr unsigned int bitPhChIDX  = 8;
  inline constexpr unsigned int bitPhChIDY  = 5;
  inline constexpr unsigned int bitChamber  = 8;
  inline constexpr unsigned int bitQuadrant = 2;
  inline constexpr unsigned int bitRegion   = 2;
  inline constexpr unsigned int bitStation  = 3;

  inline constexpr unsigned int shiftGap      = 0;
  inline constexpr unsigned int shiftReadout  = shiftGap + bitGap;
  inline constexpr unsigned int shiftFrontEnd = shiftReadout + bitReadout;
  inline constexpr unsigned int shiftPhChIDX  = shiftFrontEnd + bitFrontEnd;
  inline constexpr unsigned int shiftPhChIDY  = shiftPhChIDX + bitPhChIDX;
  inline constexpr unsigned int shiftChamber  = shiftPhChIDY + bitPhChIDY;
  inline constexpr unsigned int shiftQuadrant = shiftChamber + bitChamber;
  inline constexpr unsigned int shiftRegion   = shiftQuadrant + bitQuadrant;
  inline constexpr unsigned int shiftStation  = shiftRegion + bitRegion;

  inline constexpr unsigned int maskStation  = ( ( ( (unsigned int)1 ) << bitStation ) - 1 ) << shiftStation;
  inline constexpr unsigned int maskRegion   = ( ( ( (unsigned int)1 ) << bitRegion ) - 1 ) << shiftRegion;
  inline constexpr unsigned int maskQuadrant = ( ( ( (unsigned int)1 ) << bitQuadrant ) - 1 ) << shiftQuadrant;
  inline constexpr unsigned int maskChamber  = ( ( ( (unsigned int)1 ) << bitChamber ) - 1 ) << shiftChamber;
  inline constexpr unsigned int maskPhChIDX  = ( ( ( (unsigned int)1 ) << bitPhChIDX ) - 1 ) << shiftPhChIDX;
  inline constexpr unsigned int maskPhChIDY  = ( ( ( (unsigned int)1 ) << bitPhChIDY ) - 1 ) << shiftPhChIDY;
  inline constexpr unsigned int maskFrontEnd = ( ( ( (unsigned int)1 ) << bitFrontEnd ) - 1 ) << shiftFrontEnd;
  inline constexpr unsigned int maskReadout  = ( ( ( (unsigned int)1 ) << bitReadout ) - 1 ) << shiftReadout;
  inline constexpr unsigned int maskGap      = ( ( ( (unsigned int)1 ) << bitGap ) - 1 ) << shiftGap;

  inline constexpr unsigned int getBit( unsigned int packedValue, unsigned int shift, unsigned int mask ) {
    return ( ( packedValue & mask ) >> shift );
  }

  inline constexpr void setBit( unsigned int& origValue, unsigned int shift, unsigned int mask,
                                unsigned int bitValue ) {
    origValue = ( ( ( bitValue << shift ) & mask ) | ( origValue & ~mask ) );
  }

} // namespace MuonPhChIDPack
