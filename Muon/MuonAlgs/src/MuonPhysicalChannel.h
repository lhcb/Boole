/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/KeyedObject.h"
#include "MuonHitTraceBack.h"
#include "MuonPhChID.h"
#include "MuonPhysicalChannelResponse.h"
#include <algorithm>
#include <vector>

class MuonPhysicalChannel : public KeyedObject<int> {

public:
  MuonPhysicalChannel( double time, double bx ) {
    m_timeGate = time;
    m_timeBX   = bx;
  }

  MuonPhysicalChannel( MuonPhChID FETile, double time, double bx ) : m_ID( FETile ) {
    m_timeGate = time;
    m_timeBX   = bx;
  }

  /// Retrieve Stored GEANT hits connected to the ph. (const)
  [[nodiscard]] const std::vector<MuonHitTraceBack>& hitsTraceBack() const { return m_Hits; }

  /// Retrieve Stored GEANT hits connected to the ph (non-const)
  [[nodiscard]] std::vector<MuonHitTraceBack>& hitsTraceBack() { return m_Hits; }
  [[nodiscard]] MuonPhChID&                    phChID() { return m_ID; }
  [[nodiscard]] const MuonPhChID&              phChID() const { return m_ID; }
  void                                         setPhChID( MuonPhChID& value ) { m_ID = value; }
  void                                         applyTimeJitter();
  void                                         applyGeoInefficiency();
  void                                         applyChamberInefficiency();
  void                                         applyXTalk( std::array<int, 2> phChInX, std::array<int, 2> phChInY,
                                                           std::vector<std::unique_ptr<MuonPhysicalChannel>>& hitDueToXTalk );
  void                                         addToPC( const MuonPhysicalChannel& );
  void                                         sortInTimeHits();
  void                                         applyTimeAdjustment();
  void                                         applyDeadtime( int BX );
  [[nodiscard]] MuonPhysicalChannelResponse*   getResponse() { return response; }
  void                                         setResponse( MuonPhysicalChannelResponse* value ) { response = value; }

  [[nodiscard]] std::unique_ptr<MuonPhysicalChannel> createXTalkChannel( MuonPhChID FETile, MuonHitTraceBack* iter,
                                                                         unsigned int newChannel, int axis );
  void fireXTalkChannels( int axis, int numberOfXTalkHits, MuonHitTraceBack* iter, unsigned int indexOfChannel,
                          int direction, int maxChannel,
                          std::vector<std::unique_ptr<MuonPhysicalChannel>>& hitDueToXTalk );

private:
  MuonPhChID                    m_ID;
  std::vector<MuonHitTraceBack> m_Hits;
  MuonPhysicalChannelResponse*  response = nullptr;
  static double                 m_timeGate;
  static double                 m_timeBX;
};

using MuonPhysicalChannels = KeyedContainer<MuonPhysicalChannel, Containers::HashMap>;
