/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "MuonDet/MuonReadoutCond.h"

#include "GaudiKernel/RndmGenerators.h"

class MuonPhysicalChannelResponse final {
public:
  // constructor
  MuonPhysicalChannelResponse( Rndm::Numbers* flat, Rndm::Numbers* gauss, Rndm::Numbers* genericPDF,
                               Rndm::Numbers* electronicNoise, double min, double max,
                               const MuonReadoutCond* detectorCondition, int readoutIndex );
  double extractDeadtime();
  bool   surviveInefficiency();
  double extractTimeAdjustmentImprecision();
  double extractTimeJitter();
  int    extractXTalkX( double distanceFromBoundary );
  int    extractXTalkY( double distanceFromBoundary );
  int    electronicNoise();

private:
  Rndm::Numbers*         m_flat{};
  Rndm::Numbers*         m_gauss{};
  Rndm::Numbers*         m_timeJitter{};
  Rndm::Numbers*         m_electronicNoise{};
  int                    m_readoutIndex{};
  double                 m_minOfTimeJitter{};
  double                 m_maxOfTimeJitter{};
  double                 m_meadDeadtime{};
  double                 m_rmsDeadtime{};
  double                 m_chamberEfficiency{};
  double                 m_timeAdjustmentImprecision{};
  const MuonReadoutCond* m_detectorResponse{};
};
