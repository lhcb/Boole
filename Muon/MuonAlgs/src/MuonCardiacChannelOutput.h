/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/MCMuonDigitInfo.h"
#include "GaudiKernel/KeyedObject.h"
#include "MuonCardiacChannelOutput.h"
#include "MuonCardiacTraceBack.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonHitTraceBack.h"
#include "MuonPhysicalChannelOutput.h"
#include <algorithm>
#include <vector>

class MuonCardiacChannelOutput : public KeyedObject<int> {
public:
  MuonCardiacChannelOutput() = default;
  explicit MuonCardiacChannelOutput( LHCb::Detector::Muon::TileID origID ) : m_ID( origID ){};

  void                         setChID( LHCb::Detector::Muon::TileID value ) { m_ID = value; }
  LHCb::Detector::Muon::TileID chID() const { return m_ID; }

  void addPhyChannel( MuonPhysicalChannelOutput* value );

  void                         setMCMuonDigitInfo( LHCb::MCMuonDigitInfo value );
  LHCb::MCMuonDigitInfo&       chInfo() { return m_phChInfo; }
  const LHCb::MCMuonDigitInfo& chInfo() const { return m_phChInfo; }

  void   setFiringTime( double value ) { m_FiringTime = value; }
  void   setFiringTime();
  double firingTime() const { return m_FiringTime; }
  bool   hasFired() const { return m_fired; };

  void processForDeadTime( float value, float timeGate );

  std::vector<MuonHitTraceBack*>&       hitsTraceBack() { return m_Hits; };
  const std::vector<MuonHitTraceBack*>& hitsTraceBack() const { return m_Hits; };
  void                                  fillhitsTraceBack();

  void                       setChInfoFiring( MuonCardiacTraceBack pointer );
  MuonPhysicalChannelOutput* getPhCh( MuonCardiacTraceBack pointer );
  void                       UpdateMCInfo();

private:
  LHCb::Detector::Muon::TileID                        m_ID;
  std::vector<MuonPhysicalChannelOutput*>             m_phChannel;
  std::vector<std::pair<float, MuonCardiacTraceBack>> m_timeList;
  LHCb::MCMuonDigitInfo                               m_phChInfo;
  double                                              m_FiringTime{};
  std::vector<MuonHitTraceBack*>                      m_Hits;
  bool                                                m_fired{};
};

inline void MuonCardiacChannelOutput::setMCMuonDigitInfo( LHCb::MCMuonDigitInfo value ) { m_phChInfo = value; }

using MuonCardiacChannelOutputs = KeyedContainer<MuonCardiacChannelOutput, Containers::HashMap>;
