/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MuonPhysicalChannelOutput.h"
#include "MuonDet/DeMuonDetector.h"

boost::container::static_vector<LHCb::Detector::Muon::TileID, 2>
MuonPhysicalChannelOutput::calculateTileID( const DeMuonDetector& muonDetector ) const {

  const unsigned int station  = phChID().getStation();
  const unsigned int region   = phChID().getRegion();
  const unsigned int chamber  = phChID().getChamber();
  const unsigned int readout  = phChID().getReadout();
  const unsigned int quadrant = phChID().getQuadrant();
  const unsigned int idX      = phChID().getPhChIDX();
  const unsigned int idY      = phChID().getPhChIDY();

  boost::container::static_vector<LHCb::Detector::Muon::TileID, 2> phChTileID;

  // loop over FE channel readout
  //

  for ( int readoutNumber = 0; readoutNumber < (int)muonDetector.getLogMapInRegion( station, region );
        ++readoutNumber ) {
    //
    // check if current readout coincides with one of the LogMap readouts
    //
    if ( muonDetector.getLogMapRType( readoutNumber, station, region ) != readout ) continue;

    // define order of FE channel according to the MuonTileID
    // conventions: from 0,0 left,bottom to radial coordinates
    //
    unsigned int numberOfPCX = 0;
    unsigned int numberOfPCY = 0;
    for ( int countReadout = 0; countReadout < muonDetector.readoutInRegion( station, region ); ++countReadout ) {
      if ( muonDetector.getLogMapRType( readoutNumber, station, region ) ==
           muonDetector.getReadoutType( countReadout, station, region ) ) {
        numberOfPCX = muonDetector.getPhChannelNX( countReadout, station, region );
        numberOfPCY = muonDetector.getPhChannelNY( countReadout, station, region );
      }
    }
    //
    // FE ch address relative to the chamber transformed
    // in quadrant reference system
    //
    const auto [newidX, newidY] = [&]() -> std::array<unsigned int, 2> {
      switch ( quadrant ) {
      case 0:
        return {idX, idY};
      case 3:
        return {numberOfPCX - idX - 1, idY};
      case 2:
        return {numberOfPCX - idX - 1, numberOfPCY - idY - 1};
      case 1:
        return {idX, numberOfPCY - idY - 1};
      default:
        return {0, 0};
      }
    }();
    //
    // FE ch address in the whole quadrant
    //
    LHCb::Detector::Muon::TileID chaTile   = muonDetector.Chamber2Tile( chamber, station, region );
    const unsigned int           idXGlobal = newidX + chaTile.nX() * numberOfPCX;
    const unsigned int           idYGlobal = newidY + chaTile.nY() * numberOfPCY;
    //
    //  compute Logical Channel address now
    //
    const unsigned int idLogX = idXGlobal / muonDetector.getLogMapMergex( readoutNumber, station, region );
    const unsigned int idLogY = idYGlobal / muonDetector.getLogMapMergey( readoutNumber, station, region );
    //
    // create the tile of the phys chan
    //
    const LHCb::Detector::Muon::Layout layout{muonDetector.getLayoutX( readoutNumber, station, region ),
                                              muonDetector.getLayoutY( readoutNumber, station, region )};

    phChTileID.emplace_back( station, layout, region, quadrant, idLogX, idLogY );
  }
  return phChTileID;
}

MuonPhysicalChannelOutput& MuonPhysicalChannelOutput::fillTimeList() {
  for ( const auto& hit : hitsTraceBack() ) {
    if ( hit.getMCMuonHistory().isHitFiring() ) m_timeList.push_back( hit.hitArrivalTime() );
  }
  return *this;
}
