/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MuonDetectorResponse.h"
#include "MuonChamberResponse.h"
#include "MuonPhChID.h"
#include "MuonPhysicalChannelResponse.h"
#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/SmartDataPtr.h>
#include <MuonDet/DeMuonDetector.h>
#include <MuonDet/MuonNamespace.h>
#include <MuonDet/MuonReadoutCond.h>
#include <cmath>
#include <fmt/format.h>
#include <vector>

MuonDetectorResponse::MuonDetectorResponse( const DeMuonDetector& det, SmartIF<IRndmGenSvc> randSvc,
                                            IMessageSvc* msgSvc )
    : m_muonDetector( det ) {
  MsgStream log( msgSvc, "MuonDetectorResponse" );

  StatusCode sc = m_gaussDist.initialize( randSvc, Rndm::Gauss( 0.0, 1.0 ) );
  if ( sc.isFailure() ) log << MSG::DEBUG << "error ini gauss dist " << endmsg;

  sc = m_flatDist.initialize( randSvc, Rndm::Flat( 0.0, 1.0 ) );
  if ( sc.isFailure() ) log << MSG::DEBUG << "error ini flat dist " << endmsg;

  auto regionsNumber = m_muonDetector->regions();

  for ( int indexRegion = 0; indexRegion != regionsNumber; ++indexRegion ) {
    std::string regionName =
        fmt::format( "{}R{}", m_muonDetector->getStationName( indexRegion / 4 ), ( indexRegion % 4 ) + 1 );

    const int station = indexRegion / 4;
    const int region  = indexRegion - station * 4;

    auto&        muReadout     = m_muonDetector->getReadoutCond( station, region );
    const double newMean       = muReadout.chamberNoise( 0 ) / 100; // tranform from cm^2 to mm^2
    auto         poissonDist   = std::make_unique<Rndm::Numbers>();
    const double areaOfChamber = m_muonDetector->areaChamber( station, region );
    const int    numberOfGaps  = m_muonDetector->gapsInRegion( station, region );

    const double totalArea             = areaOfChamber * numberOfGaps;
    const double meanOfNoisePerChamber = newMean * totalArea * 25 * 1.0E-9;
    log << MSG::DEBUG << "region name " << m_muonDetector->getRegionName( station, region ) << " noise level "
        << meanOfNoisePerChamber << endmsg;
    sc = poissonDist->initialize( randSvc, Rndm::Poisson( meanOfNoisePerChamber ) );
    if ( sc.isFailure() ) log << MSG::DEBUG << " not able to ini poisson dist " << endmsg;

    responseChamber[indexRegion] = std::make_unique<MuonChamberResponse>( std::move( poissonDist ), newMean );

    for ( int readout = 0; readout != m_muonDetector->readoutInRegion( station, region ); ++readout ) {
      double      min, max;
      const auto& timeJitterPDF = muReadout.timeJitter( min, max, readout );
      log << MSG::VERBOSE << " time jitter " << min << " " << max << endmsg;

      auto p_time = std::make_unique<Rndm::Numbers>();
      sc          = p_time->initialize( randSvc, Rndm::DefinedPdf( timeJitterPDF, 0 ) );
      if ( sc.isFailure() ) log << MSG::DEBUG << " not able to ini time jitter " << endmsg;

      for ( double itPDF : timeJitterPDF ) { log << MSG::VERBOSE << " time jitter " << itPDF << endmsg; }
      auto         p_electronicNoise = std::make_unique<Rndm::Numbers>();
      const double noiseCounts       = muReadout.electronicsNoise( readout ) *
                                 m_muonDetector->getPhChannelNX( readout, station, region ) *
                                 m_muonDetector->getPhChannelNY( readout, station, region ) * 25 * 1.0E-9;

      log << MSG::VERBOSE << "noisecounts " << noiseCounts << endmsg;
      sc = p_electronicNoise->initialize( randSvc, Rndm::Poisson( noiseCounts ) );
      if ( sc.isFailure() ) log << MSG::DEBUG << " not able to ini ele noise dist " << endmsg;

      // Note: passing a pointer to ReadoutCond here is safe enough as the channel response objects
      // will be created again when a new condition slice is created (we live in a derived condition)
      auto response = std::make_unique<MuonPhysicalChannelResponse>(
          &m_flatDist, &m_gaussDist, p_time.get(), p_electronicNoise.get(), min, max, &muReadout, readout );
      m_electronicNoise.push_back( std::move( p_electronicNoise ) );
      m_timeJitter.push_back( std::move( p_time ) );
      log << MSG::DEBUG << "inserting " << readout << " " << indexRegion << endmsg;
      responseVector[readout][indexRegion] = std::move( response );
      log << MSG::DEBUG << "inserted " << endmsg;
    }
  }
}

MuonPhysicalChannelResponse* MuonDetectorResponse::getResponse( MuonPhChID phChID ) const {
  const int          station     = phChID.getStation();
  const int          region      = phChID.getRegion();
  const int          part        = station * 4 + region;
  const unsigned int readoutType = phChID.getReadout();
  int                readout     = -1;
  for ( int ireadout = 0; ireadout != m_muonDetector->readoutInRegion( station, region ); ++ireadout ) {
    if ( readoutType == m_muonDetector->getReadoutType( ireadout, station, region ) ) { readout = ireadout; }
  }
  return ( readout >= 0 ) ? responseVector[readout][part].get() : nullptr;
}
