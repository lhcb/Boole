/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "MuonHitTraceBack.h"
#include "MuonPhysicalChannelOutput.h"

/** @class MuonCardiacTraceBack MuonCardiacTraceBack.h
 *
 *
 *  @author Alessia Satta
 *  @date   2006-12-11
 */
class MuonCardiacTraceBack final {
public:
  explicit MuonCardiacTraceBack( MuonHitTraceBack* value ) : m_traceBack{value} {}
  void              setDeadTime() { m_alive = false; }
  bool              getDeadTime() const { return !m_alive; }
  MuonHitTraceBack* getHitTrace() { return m_traceBack; }

private:
  MuonHitTraceBack* m_traceBack{nullptr};
  bool              m_alive{true};
};
