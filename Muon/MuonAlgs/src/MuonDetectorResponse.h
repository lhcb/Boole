/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <DetDesc/GenericConditionAccessorHolder.h>
#include <GaudiKernel/RndmGenerators.h>
#include <MuonDet/DeMuonDetector.h>
#include <array>
#include <memory>
#include <vector>

class IRndmGenSvc;
class IDataProviderSvc;
class IMessageSvc;
class MuonPhysicalChannelResponse;
class MuonPhChID;
class MuonChamberResponse;

class MuonDetectorResponse final {
public:
  // constructor
  MuonDetectorResponse( const DeMuonDetector& det, SmartIF<IRndmGenSvc> randSvc, IMessageSvc* msgSvc );
  // non copiable and non movable because MuonChamberResponse instances hold pointers to our
  // data members
  MuonDetectorResponse( const MuonDetectorResponse& ) = delete;
  MuonDetectorResponse( MuonDetectorResponse&& )      = delete;
  MuonDetectorResponse& operator=( const MuonDetectorResponse& ) = delete;
  MuonDetectorResponse& operator=( MuonDetectorResponse&& ) = delete;

  MuonPhysicalChannelResponse* getResponse( MuonPhChID phChID ) const;
  MuonPhysicalChannelResponse* getResponse( int part, int readout ) const {
    return responseVector[readout][part].get();
  }
  MuonChamberResponse* getChamberResponse( int part ) const { return responseChamber[part].get(); }

private:
  Rndm::Numbers                                                               m_gaussDist;
  Rndm::Numbers                                                               m_flatDist;
  std::vector<std::unique_ptr<Rndm::Numbers>>                                 m_electronicNoise;
  std::vector<std::unique_ptr<Rndm::Numbers>>                                 m_timeJitter;
  std::array<std::array<std::unique_ptr<MuonPhysicalChannelResponse>, 20>, 2> responseVector;
  std::array<std::unique_ptr<MuonChamberResponse>, 20>                        responseChamber{};
  LHCb::DetDesc::DetElementRef<DeMuonDetector>                                m_muonDetector;
};
