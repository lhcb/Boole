/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MuonPhysicalChannel.h"
#include "MuonHitTraceBack.h"

double MuonPhysicalChannel::m_timeGate;
double MuonPhysicalChannel::m_timeBX;

void MuonPhysicalChannel::applyTimeJitter() {
  for ( auto& m_Hit : m_Hits ) {
    if ( ( !m_Hit.getMCMuonHitOrigin().getXTalkNature() ) &&
         ( !m_Hit.getMCMuonHitOrigin().getElectronicNoiseNature() ) ) {
      const double originalTime = m_Hit.hitArrivalTime();
      const double deltaT       = response->extractTimeJitter();
      m_Hit.setHitArrivalTime( originalTime + deltaT );
      if ( m_Hit.getMCMuonHitOrigin().getBX() == LHCb::MuonBXFlag::CURRENT ) {
        if ( originalTime > 0.0 && originalTime < m_timeGate ) {
          if ( m_Hit.hitArrivalTime() < 0. || m_Hit.hitArrivalTime() > m_timeGate ) {
            m_Hit.getMCMuonHistory().setHitOutByTimeJitter( (unsigned int)1 );
          }
        }
      }
    }
  }
}

void MuonPhysicalChannel::applyGeoInefficiency() {
  /* MC 2013-06-26: commenting out, since this code has no effect.
   * See Coverity defect 17443
  bool drop=false;
  for (auto iter= m_Hits.begin();iter!= m_Hits.end();iter++){
    if((!(*iter).getMCMuonHitOrigin().getXTalkNature())&&
       (!(*iter).getMCMuonHitOrigin().getElectronicNoiseNature())){
      if(drop){
        (*iter).getMCMuonHistory().
          setGeometricalNotAcceptedHitHistory((unsigned int)1);
      }
    }
  }
  */
}

void MuonPhysicalChannel::applyChamberInefficiency() {
  for ( auto& m_Hit : m_Hits ) {
    if ( ( !m_Hit.getMCMuonHitOrigin().getXTalkNature() ) &&
         ( !m_Hit.getMCMuonHitOrigin().getElectronicNoiseNature() ) &&
         ( !m_Hit.getMCMuonHitOrigin().getChamberNoiseNature() ) ) {
      bool keep = response->surviveInefficiency();
      if ( !keep ) { m_Hit.getMCMuonHistory().setKilledByChamberInefficiencyHit( (unsigned int)1 ); }
    }
  };
}

std::unique_ptr<MuonPhysicalChannel> MuonPhysicalChannel::createXTalkChannel( MuonPhChID FETile, MuonHitTraceBack* iter,
                                                                              unsigned int newChannel, int axis ) {
  auto newPhCh = std::make_unique<MuonPhysicalChannel>( FETile, m_timeGate, m_timeBX );
  if ( axis == 0 ) {
    newPhCh->phChID().setPhChIDX( newChannel );
  } else if ( axis == 1 ) {
    newPhCh->phChID().setPhChIDY( newChannel );
  }
  MuonHitTraceBack& tb = newPhCh->hitsTraceBack().emplace_back();
  tb.setHitArrivalTime( iter->hitArrivalTime() );
  tb.getMCMuonHitOrigin().setHitNature( LHCb::MuonOriginFlag::XTALK ).setBX( iter->getMCMuonHitOrigin().getBX() );
  tb.getMCMuonHistory().setNatureOfHit( LHCb::MuonOriginFlag::XTALK ).setBXOfHit( iter->getMCMuonHistory().BX() );
  return newPhCh;
}

void MuonPhysicalChannel::fireXTalkChannels( int axis, int numberOfXTalkHits, MuonHitTraceBack* iter,
                                             unsigned int indexOfChannel, int direction, int maxChannel,
                                             std::vector<std::unique_ptr<MuonPhysicalChannel>>& hitDueToXTalk ) {
  auto addHit = [&]( int offset ) {
    if ( offset == 0 ) return;
    int newChannel = indexOfChannel + offset;
    if ( newChannel < 0 || newChannel >= maxChannel ) return;
    hitDueToXTalk.push_back( createXTalkChannel( phChID(), iter, newChannel, axis ) );
  };

  if ( numberOfXTalkHits % 2 == 1 ) {
    // FIXME: this will generate (if numberOfXTalkHits>=3) two X-talk hits in indexOfChannel+direction...
    addHit( direction );
    --numberOfXTalkHits;
  }
  for ( int i = -numberOfXTalkHits / 2; i <= numberOfXTalkHits / 2; ++i ) addHit( i );
}

void MuonPhysicalChannel::applyXTalk( std::array<int, 2> phChInX, std::array<int, 2> phChInY,
                                      std::vector<std::unique_ptr<MuonPhysicalChannel>>& hitDueToXTalk ) {
  const unsigned int nx          = phChID().getPhChIDX();
  const unsigned int ny          = phChID().getPhChIDY();
  const int          readoutType = phChID().getReadout();

  for ( auto& m_Hit : m_Hits ) {
    if ( !m_Hit.getMCMuonHitOrigin().getXTalkNature() && !m_Hit.getMCMuonHitOrigin().getElectronicNoiseNature() &&
         // reject hit that has not produced any signal by its own
         !m_Hit.getMCMuonHistory().isKilledByChamberInefficiency() &&
         !m_Hit.getMCMuonHistory().isHitOutGeoAccemtance() ) {
      const auto [xLeft, yDown, xRight, yUp] = m_Hit.getCordinate();
      const auto [x, directionx]             = ( xLeft < xRight ? std::pair{xLeft, -1} : std::pair{xRight, +1} );
      const auto [y, directiony]             = ( yDown < yUp ? std::pair{yDown, -1} : std::pair{yUp, +1} );
      /// here the XTalk probability is evaluated so numberOfXTalkHits
      // modified
      if ( const int numberOfXTalkHitsX = response->extractXTalkX( x ) - 1; numberOfXTalkHitsX )
        fireXTalkChannels( 0, numberOfXTalkHitsX, &m_Hit, nx, directionx, phChInX[readoutType], hitDueToXTalk );
      if ( const int numberOfXTalkHitsY = response->extractXTalkY( y ) - 1; numberOfXTalkHitsY )
        fireXTalkChannels( 1, numberOfXTalkHitsY, &m_Hit, ny, directiony, phChInY[readoutType], hitDueToXTalk );
    }
  }
}

void MuonPhysicalChannel::addToPC( const MuonPhysicalChannel& PCToAdd ) {
  m_Hits.insert( m_Hits.end(), PCToAdd.hitsTraceBack().begin(), PCToAdd.hitsTraceBack().end() );
}

void MuonPhysicalChannel::sortInTimeHits() {
  std::stable_sort( m_Hits.begin(), m_Hits.end(),
                    []( const auto& lhs, const auto& rhs ) { return lhs.hitArrivalTime() < rhs.hitArrivalTime(); } );
}

void MuonPhysicalChannel::applyTimeAdjustment() {
  // extract time adjustment deltaT
  const double deltaT = response->extractTimeAdjustmentImprecision();
  for ( auto& m_Hit : m_Hits ) {
    const double originalTime = m_Hit.hitArrivalTime();
    // modify the time of the hit
    m_Hit.setHitArrivalTime( originalTime + deltaT );
    // set the bit flag if needed
    if ( m_Hit.getMCMuonHitOrigin().getBX() != LHCb::MuonBXFlag::CURRENT ) continue;
    if ( m_Hit.hitArrivalTime() > 0 && m_Hit.hitArrivalTime() < m_timeGate &&
         m_Hit.getMCMuonHistory().hasTimeJittered() ) {
      m_Hit.getMCMuonHistory().setHitInByTimeAdjustment( 1u );
    }
    if ( originalTime > 0.0 && originalTime < m_timeGate &&
         ( m_Hit.hitArrivalTime() < 0. || m_Hit.hitArrivalTime() > m_timeGate ) ) {
      m_Hit.getMCMuonHistory().setHitOutByTimeAdjustment( 1u );
    }
  }
}

void MuonPhysicalChannel::applyDeadtime( int BX ) {
  // extract the start conditions
  double timeOfStart = 0;
  // corrected for the BX(negative)
  timeOfStart = -timeOfStart - BX * m_timeBX;
  timeOfStart = -500;

  double timeOfStartOfDeadtime = timeOfStart;
  double lenghtOfDeadtime      = 0;

  for ( auto& m_Hit : m_Hits ) {
    // check that he hit is able to fire the channel
    if ( !m_Hit.getMCMuonHistory().isHitOutGeoAccemtance() &&
         !m_Hit.getMCMuonHistory().isKilledByChamberInefficiency() ) {
      const double timeOfHit = m_Hit.hitArrivalTime();
      if ( timeOfHit < timeOfStartOfDeadtime + lenghtOfDeadtime ) {
        // hit in deadtime
        m_Hit.getMCMuonHistory().setHitInDeadtime( 1 );
      } else {
        // hit (re)start the deadtime
        timeOfStartOfDeadtime = timeOfHit;
        lenghtOfDeadtime      = response->extractDeadtime();
      }
    }
  }
}
