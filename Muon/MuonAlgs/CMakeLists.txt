###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Muon/MuonAlgs
-------------
#]=======================================================================]

gaudi_add_module(MuonAlgs
    SOURCES
        src/MuonCardiacChannelOutput.cpp
        src/MuonDetectorResponse.cpp
        src/MuonDigitization.cpp
        src/MuonDigitizationData.cpp
        src/MuonPhysicalChannel.cpp
        src/MuonPhysicalChannelOutput.cpp
        src/MuonPhysicalChannelResponse.cpp
    LINK
        Boost::container
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::DetDescLib
        LHCb::DigiEvent
        LHCb::LHCbKernel
        LHCb::MCEvent
        LHCb::MuonDetLib
)
