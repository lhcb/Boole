/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Core/FloatComparison.h"
#include "MuBgDistribution.h"
#include "ParticleInfo.h"
#include <AIDA/IAxis.h>
#include <AIDA/IHistogram1D.h>
#include <AIDA/IHistogram2D.h>
#include <AIDA/IHistogramFactory.h>
#include <DetDesc/GenericConditionAccessorHolder.h>
#include <DetDesc/IGeometryInfo.h>
#include <Detector/Muon/TileID.h>
#include <Event/GenCollision.h>
#include <Event/GenHeader.h>
#include <Event/MCHit.h>
#include <GaudiAlg/GaudiAlgorithm.h>
#include <GaudiKernel/Algorithm.h>
#include <GaudiKernel/IAlgManager.h>
#include <GaudiKernel/IAlgorithm.h>
#include <GaudiKernel/IProperty.h>
#include <GaudiKernel/PhysicalConstants.h>
#include <GaudiKernel/Plane3DTypes.h>
#include <GaudiKernel/Point3DTypes.h>
#include <GaudiKernel/RndmGenerators.h>
#include <GaudiKernel/SmartDataPtr.h>
#include <GaudiKernel/Transform3DTypes.h>
#include <MuonDet/DeMuonChamber.h>
#include <MuonDet/DeMuonDetector.h>
#include <MuonDet/MuonNamespace.h>
#include <algorithm>
#include <array>
#include <cmath>
#include <cstdint>
#include <fmt/core.h>
#include <string>
#include <utility>

#if __has_include( <Gaudi/PropertyFmt.h> )
#  include <Gaudi/PropertyFmt.h>
#endif

#ifdef USE_DD4HEP
#  include <DD4hep/GrammarUnparsed.h>
#endif

/**
 *  @author Alessia Satta
 *  @date   2003-02-18
 */

class MuonBackground : public LHCb::DetDesc::ConditionAccessorHolder<GaudiAlgorithm> {
  struct Cache {
    Cache()          = default;
    Cache( Cache&& ) = default;

    Cache& operator=( Cache&& ) = default;

    int maxDimension() const { return gaps * stationsNumber; }
    int chamberOffset( int station, int region ) const { return chamberInRegion[station * 4 + region]; }

    DetElementRef<DeMuonDetector> det;
    int                           stationsNumber = 0;
    std::vector<std::string>      stationsNames;
    unsigned int                  gaps = 0;
    std::array<int, 20>           chamberInRegion;

    struct Backgrounds {
      std::unique_ptr<MuBgDistribution> correlation;
      std::unique_ptr<MuBgDistribution> radial;
      std::unique_ptr<MuBgDistribution> phiglobvsradial;
      std::unique_ptr<MuBgDistribution> thetalocvsradial;
      std::unique_ptr<MuBgDistribution> philocvsradial;
      std::unique_ptr<MuBgDistribution> logtimevsradial;
      std::unique_ptr<MuBgDistribution> lintimevsradial;
      std::unique_ptr<MuBgDistribution> hitgap;
    };
    std::vector<Backgrounds> bg;
  };

public:
  MuonBackground( const std::string& name, ISvcLocator* svcloc )
      : LHCb::DetDesc::ConditionAccessorHolder<GaudiAlgorithm>( name, svcloc )
      , m_cache{this, fmt::format( "MuonBackground-Cache-{}", name )} {}

  StatusCode   initialize() override; ///< Algorithm initialization
  StatusCode   execute() override;    ///< Algorithm execution
  StatusCode   finalize() override;   ///< Algorithm finalization
  unsigned int calculateNumberOfCollision( int ispill ) const;
  inline int   mapHistoCode( int icode, int station, int multy ) const {
    return m_histogramsUpCode + icode * 10000 + ( station + 2 ) * 1000 + ( multy + 1 ) * 100;
  }

  StatusCode calculateStartingNumberOfHits( const Cache& c, int ispill, std::vector<std::vector<int>>& results,
                                            const unsigned int collisions ) const;
  StatusCode initializeParametrization( Cache& c ) const;

  // inline std::vector<float>* getVectorOfGapPosition( int station ) { return &containerOfFirstGapPosition[station]; };
  StatusCode initializeRNDDistribution1D( const IHistogram1D*                          histoPointer,
                                          std::vector<std::unique_ptr<Rndm::Numbers>>& distributions,
                                          std::vector<bool>& flags, double& xmin, double& xmax ) const;
  StatusCode initializeRNDDistribution2D( const IHistogram2D*                          histoPointer,
                                          std::vector<std::unique_ptr<Rndm::Numbers>>& distributions,
                                          std::vector<bool>& flags, double& xmin, double& xmax, int& nbinx,
                                          double& ymin, double& ymax ) const;
  StatusCode createHit( const Cache& c, LHCb::MCHits* hitsContainer, int station, int multi, int ispill ) const;
  StatusCode correctInterceptPosition( float xlow, float xup, float ylow, float yup, float zlow, float zup, float xave,
                                       float yave, float zave, float xslope, float yslope, float& xentry, float& xexit,
                                       float& yentry, float& yexit, float& zentry, float& zexit ) const;
  int        howManyHit( float floatHit ) const;
  unsigned int numberOfCollision( const LHCb::MCVertex* vertex, std::vector<const LHCb::MCVertex*>& vtxList ) const;

private:
  ConditionAccessor<Cache> m_cache{this, "CacheLocation", ""};
  Cache                    makeCache( const DeMuonDetector& det ) const;

  std::optional<std::tuple<DetElementRef<DeMuonChamber>, float, float, float>>
  getRadialPosition( const Cache& c, int index, int station ) const;

  int                          m_type;
  Gaudi::Property<std::string> m_typeOfBackground{this, "BackgroundType"};

  Gaudi::Property<std::vector<double>> m_safetyFactor{this, "SafetyFactors"};
  std::vector<int>                     m_numberOfGaps;
  Gaudi::Property<bool>                m_histos{this, "DebugHistos", false};

  Gaudi::Property<float>               m_luminosity{this, "Luminosity", 2.0};
  Gaudi::Property<std::vector<double>> m_flatSpilloverHit{this, "AverageFlatHits"};
  Gaudi::Property<int>                 m_numberOfFlatSpill{this, "FlatSpillNumber", 1};

  std::vector<std::vector<float>> containerOfFirstGapPosition;
  Gaudi::Property<std::string>    m_containerName{this, "ContainerName"};
  // declare the offset to reconstruct each distribution
  // starting form histogram
  // the template is 100000+par1*10000+station*1000+multiplicity*100
  Gaudi::Property<int>                      m_histogramsUpCode{this, "HistogramsUpCode"};
  Gaudi::Property<float>                    m_BXTime{this, "BXTime", 25.0};
  Gaudi::Property<float>                    m_unitLength{this, "RadialUnit", 10.0};
  int                                       m_readSpilloverEvents = 0;
  Gaudi::Property<std::string>              m_histoFile{this, "HistogramsFile", "InFile"};
  Gaudi::Property<std::vector<std::string>> m_histoName{this, "HistogramsMeaning"};
  Gaudi::Property<std::vector<int>>         m_histogramsMapNumber{this, "HistogramsNumber"};
  Gaudi::Property<std::vector<int>>         m_histogramsDimension{this, "HistogramsDimension"};
  double                                    m_luminosityFactor;

  // only to test the histos
  IHistogram1D*                  m_pointer1D[16];
  IHistogram2D*                  m_pointer2D[16];
  std::unique_ptr<Rndm::Numbers> m_flatDistribution;

  std::string m_persType;
  bool        m_alreadyIni = false;
  // Use as default Run1/Run2 number of NBXFullFull from Low Energy Threshold MC
  // Can be changed in Muon/MuonBackground/python/MuonBackground/__init__.py
  Gaudi::Property<int>   m_BXFillFill{this, "NBXFullFull", 1296};
  Gaudi::Property<float> m_LumiLETMC{this, "LumiLETMC", 2.0e+32};
};

DECLARE_COMPONENT( MuonBackground )

namespace {
  std::array<std::string_view, 5> spill = {"", "/Prev", "/PrevPrev", "/Next", "/NextNext"};
}

enum BackgroundType { LowEnergy = 0, FlatSpillover };

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode MuonBackground::initialize() {
  StatusCode sc = ConditionAccessorHolder<GaudiAlgorithm>::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;                                       // error printed already by  GaudiAlgorithm

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialise" << endmsg;

  // Get the application manager. Used to find the histogram persistency type
  // and to get number of spillovers from SpillOverAlg
  SmartIF<IProperty> algmgr = svc<IProperty>( "ApplicationMgr" );
  if ( !algmgr ) { return Error( "Failed to locate IProperty i/f of AppMgr" ); }

  StringProperty persType;
  persType.assign( algmgr->getProperty( "HistogramPersistency" ) );
  m_persType = persType.value();
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Histogram persistency type is " << m_persType << endmsg;

  addConditionDerivation( {DeMuonLocation::Default}, m_cache.key(),
                          [&]( const DeMuonDetector& det ) { return makeCache( det ); } );

  m_flatDistribution = std::make_unique<Rndm::Numbers>( randSvc(), Rndm::Flat( 0.0, 1.0 ) );
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << " type input " << m_typeOfBackground << endmsg;
    debug() << " safety factor " << m_safetyFactor[0] << " " << m_safetyFactor[1] << " " << m_safetyFactor[2] << " "
            << m_safetyFactor[3] << endmsg;
  }
  if ( m_typeOfBackground == "LowEnergy" ) {
    m_type = LowEnergy;
  } else if ( m_typeOfBackground == "FlatSpillover" ) {
    m_type = FlatSpillover;
  }
  if ( msgLevel( MSG::DEBUG ) ) debug() << " type " << m_type << endmsg;

  if ( m_type == LowEnergy ) {
    if ( m_histos ) {
      for ( int y = 0; y < 4; y++ ) {
        for ( int kk = 0; kk < 4; kk++ ) {
          int hh;
          hh = y * 4 + kk;
          char label[11];
          snprintf( label, sizeof( label ), hh < 9 ? "%1i" : "%2i", hh + 1 );

          std::string ap     = "STAT/";
          std::string label2 = ap + label;

          m_pointer1D[hh] = histoSvc()->book( label, "HT multiplicity", 50, 0., 50. );
          m_pointer2D[hh] = histoSvc()->book( label2, "ADD bk vs HT multiplicity", 50, 0., 50., 50, 0., 50. );
        }
      }
    }
    info() << "The low energy background is run by default for main events plus (if any) spillover events (max 4)"
           << endmsg;
    m_readSpilloverEvents = 4;

  } else if ( m_type == FlatSpillover ) {
    m_readSpilloverEvents = 0;
    m_luminosityFactor    = m_luminosity / 2.0;
  }

  return StatusCode::SUCCESS;
}

MuonBackground::Cache MuonBackground::makeCache( const DeMuonDetector& det ) const {
  Cache c;
  c.det = det;
  // FIXME: methods const-ness of DeMuonDetector is a bit fuzzy
  DeMuonDetector& mut_det = const_cast<DeMuonDetector&>( det );

  c.stationsNumber = det.stations();
  c.stationsNames.reserve( c.stationsNumber );
  for ( int i = 0; i < c.stationsNumber; ++i ) {
    c.stationsNames.emplace_back( mut_det.getStationName( i ) );
    if ( msgLevel( MSG::DEBUG ) ) debug() << " station " << i << ' ' << c.stationsNames.back() << endmsg;
  }

  // FIXME: these two loop can/should be merged into one loop
  for ( int i = 0; i < det.regions(); ++i ) {
    const int station = i / 4;
    const int region  = i % 4;
    c.gaps            = std::max<unsigned int>( c.gaps, det.gapsInRegion( station, region ) );
  }

  int chamber = 0;
  for ( int station = 0; station < c.stationsNumber; ++station ) {
    for ( int region = 0; region < 4; ++region ) {
      c.chamberInRegion[station * 4 + region] = chamber;

      chamber += det.chamberInRegion( station, region );
    }
  }

  initializeParametrization( c ).orThrow( "failed to initialize parametrization", name() );

  return c;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode MuonBackground::execute() {
  const Cache& c = m_cache.get( getConditionContext( Gaudi::Hive::currentContext() ) );

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << m_readSpilloverEvents << endmsg;
  StatusCode sc;

  for ( int ispill = 0; ispill <= m_readSpilloverEvents; ispill++ ) {
    auto collisions = calculateNumberOfCollision( ispill );
    if ( !collisions ) continue;
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "==> collision " << collisions << endmsg;
    LHCb::MCHits*                 hitsContainer = new LHCb::MCHits();
    std::vector<std::vector<int>> results{collisions};

    if ( m_type == LowEnergy ) {
      sc = calculateStartingNumberOfHits( c, ispill, results, collisions );
      if ( sc.isFailure() ) return sc;
      for ( unsigned int coll = 0; coll < collisions; coll++ ) {
        for ( int station = 0; station < c.stationsNumber; station++ ) {
          for ( unsigned int multi = 0; multi < c.gaps; multi++ ) {
            int index        = station * c.gaps + multi;
            int startingHits = results[coll][index];
            if ( msgLevel( MSG::VERBOSE ) ) verbose() << "station safe start end hits " << startingHits << endmsg;

            // extract number of hits to be added
            int   hitToAdd = 0;
            float floatHit = 0;

            // debug()<<"adding "<<	index <<" "<<startingHits<<endmsg;
            int yy = (int)( c.bg[index].correlation )->getRND( startingHits + 0.5F );

            floatHit = float( m_safetyFactor[station] * ( yy ) );
            hitToAdd = howManyHit( floatHit );
            // debug()<<"station safe start end hits "<<
            //  station<< " "<<m_safetyFactor[station]<< " "<<
            //  yy<< " "<< hitToAdd<<" "<<startingHits<<endmsg;

            if ( msgLevel( MSG::VERBOSE ) )
              verbose() << "adding " << hitToAdd << " to orginal " << startingHits << " in station " << station
                        << " for multiplicity " << multi << " and collisions/spill " << coll << " " << ispill << endmsg;

            for ( int hitID = 0; hitID < hitToAdd; hitID++ ) {
              StatusCode asc = createHit( c, hitsContainer, station, multi, ispill );
              if ( asc.isFailure() ) { warning() << " error in creating hit " << endmsg; }
            }
            if ( m_histos ) {
              m_pointer1D[index]->fill( startingHits + 0.00001, 1.0 );
              m_pointer2D[index]->fill( startingHits + 0.00001, hitToAdd + 0.00001, 1.0 );
            }
          }
        }
      }
    } else if ( m_type == FlatSpillover ) {
      // value of nu in the evnts
      if ( !m_alreadyIni ) {
        if ( msgLevel( MSG::DEBUG ) )
          debug() << " Initializing the luminosity for flast spillover simulation " << endmsg;
        //        LHCb::BeamParameters* beam=get<LHCb::BeamParameters>(LHCb::BeamParametersLocation::Default);
        LHCb::GenHeader* beam = get<LHCb::GenHeader>( LHCb::GenHeaderLocation::Default );
        if ( beam ) {
          //          info()<<" beam nu "<<beam->nu()<<" "
          if ( msgLevel( MSG::VERBOSE ) )
            verbose() << "luminosity " << beam->luminosity() << " " << m_BXFillFill << endmsg;
          m_luminosityFactor =
              ( beam->luminosity() * m_BXFillFill ) / ( m_LumiLETMC / ( Gaudi::Units::cm2 * Gaudi::Units::s ) );
          info() << " luminosity factor " << m_luminosityFactor << " compared to default " << m_LumiLETMC
                 << " cm-2 s-1 " << endmsg;
          m_alreadyIni = true;
        } else {
          info() << "could not access beam parameter information " << endmsg;
        }
      }
      // for flat spillover the spill events have no meaning....
      // thus take care of deleting allocated menory
      if ( ispill > 0 ) {
        delete hitsContainer;
        break;
      }

      for ( int station = 0; station < c.stationsNumber; station++ ) {
        for ( unsigned int multi = 0; multi < c.gaps; multi++ ) {
          int   index    = station * c.gaps + multi;
          float floatHit = float( m_flatSpilloverHit[index] * m_luminosityFactor * m_safetyFactor[station] );
          for ( int fspill = 0; fspill <= m_numberOfFlatSpill; fspill++ ) {
            int hitToAdd = 0;
            hitToAdd     = howManyHit( floatHit );
            if ( msgLevel( MSG::VERBOSE ) )
              verbose() << "adding " << hitToAdd << " in station " << station << " for multiplicity " << multi
                        << " and spill" << fspill << endmsg;
            for ( int hitID = 0; hitID < hitToAdd; hitID++ ) {
              StatusCode asc = createHit( c, hitsContainer, station, multi, fspill );
              if ( asc.isFailure() && msgLevel( MSG::DEBUG ) ) debug() << "failing hit creation " << endmsg;
            }
          }
        }
      }
    }
    std::string path = fmt::format( "/Event{}/MC/Muon/{}", spill[ispill], m_containerName );
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << " starting saveing the container " << path << endmsg;
      debug() << " number of total hit added " << hitsContainer->size() << endmsg;
    }
    sc = eventSvc()->registerObject( path, hitsContainer );
    if ( sc.isFailure() ) debug() << " error registering object " << endmsg;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode MuonBackground::finalize() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Finalize" << endmsg;
  m_flatDistribution.reset();
  return ConditionAccessorHolder<GaudiAlgorithm>::finalize();
}

//=============================================================================

StatusCode MuonBackground::initializeParametrization( Cache& c ) const {
  int numName = m_histoName.size();
  int numCode = m_histogramsMapNumber.size();
  if ( numName != numCode ) return StatusCode::FAILURE;

  int code = 0;
  //  int gap;
  std::string name;
  // Add an "h" to histogram identifiers translated by h2root
  std::string path_fmt = m_persType == "ROOT" ? "{}/h{}" : "{}/{}";

  c.bg.resize( c.maxDimension() );
  // the first station is without background!!!!!
  for ( int station = 0; station < c.stationsNumber; station++ ) {
    //    gap=m_numberOfGaps[station*m_cache.gaps];
    for ( unsigned int mult = 0; mult < c.gaps; mult++ ) {
      int index = station * c.gaps + mult;
      for ( int i = 0; i < numCode; i++ ) {
        code   = m_histogramsMapNumber[i];
        name   = m_histoName[i];
        int tt = mapHistoCode( code, station, mult );
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << code << " " << station << " " << mult << " " << tt << endmsg;
        std::string path = fmt::format( fmt::runtime( path_fmt ), m_histoFile.value(), tt );
        if ( m_histogramsDimension[i] == 1 ) {
          SmartDataPtr<IHistogram1D> histo1d( histoSvc(), path );
          if ( histo1d ) {
            std::vector<std::unique_ptr<Rndm::Numbers>> distributions;
            double                                      xmin, xmax;
            std::vector<bool>                           flags;
            StatusCode asc = initializeRNDDistribution1D( histo1d, distributions, flags, xmin, xmax );
            if ( asc.isFailure() ) return asc;

            auto mubg =
                std::make_unique<MuBgDistribution>( std::move( distributions ), flags, float( xmin ), float( xmax ) );
            if ( m_histoName[i] == "correlation" ) {
              c.bg[index].correlation = std::move( mubg );
            } else if ( m_histoName[i] == "r" ) {
              c.bg[index].radial = std::move( mubg );
            } else if ( m_histoName[i] == "phi" ) {
              c.bg[index].phiglobvsradial = std::move( mubg );
            } else if ( m_histoName[i] == "thetaloc" ) {
              c.bg[index].thetalocvsradial = std::move( mubg );
            } else if ( m_histoName[i] == "philoc" ) {
              c.bg[index].philocvsradial = std::move( mubg );
            } else if ( m_histoName[i] == "logtime" ) {
              c.bg[index].logtimevsradial = std::move( mubg );
            } else if ( m_histoName[i] == "lintime" ) {
              c.bg[index].lintimevsradial = std::move( mubg );
            } else if ( m_histoName[i] == "hitgap" ) {
              c.bg[index].hitgap = std::move( mubg );
            }
          } else {
            warning() << "not found the 1D  histo " << path << endmsg;
          }
        } else if ( m_histogramsDimension[i] == 2 ) {
          SmartDataPtr<IHistogram2D> histo2d( histoSvc(), path );
          if ( histo2d ) {
            //   histoPointer=histoin2;
            if ( msgLevel( MSG::VERBOSE ) ) debug() << "found the 2D histo " << path << endmsg;
            std::vector<std::unique_ptr<Rndm::Numbers>> distributions;
            double                                      xmin, xmax, ymin, ymax;
            int                                         nbinx;
            std::vector<bool>                           flags;
            StatusCode                                  asc =
                initializeRNDDistribution2D( histo2d, distributions, flags, xmin, xmax, nbinx, ymin, ymax );
            if ( asc.isFailure() ) return asc;

            auto mubg = std::make_unique<MuBgDistribution>( std::move( distributions ), flags, float( xmin ),
                                                            float( xmax ), nbinx, float( ymin ), float( ymax ) );
            if ( m_histoName[i] == "correlation" ) {
              c.bg[index].correlation = std::move( mubg );
            } else if ( m_histoName[i] == "r" ) {
              c.bg[index].radial = std::move( mubg );
            } else if ( m_histoName[i] == "phi" ) {
              c.bg[index].phiglobvsradial = std::move( mubg );
            } else if ( m_histoName[i] == "thetaloc" ) {
              c.bg[index].thetalocvsradial = std::move( mubg );
            } else if ( m_histoName[i] == "philoc" ) {
              c.bg[index].philocvsradial = std::move( mubg );
            } else if ( m_histoName[i] == "logtime" ) {
              c.bg[index].logtimevsradial = std::move( mubg );
            } else if ( m_histoName[i] == "lintime" ) {
              c.bg[index].lintimevsradial = std::move( mubg );
            } else if ( m_histoName[i] == "hitgap" ) {
              c.bg[index].hitgap = std::move( mubg );
            }
          } else {
            warning() << "not found the 2D-histo " << path << endmsg;
          }
        }
      }
    }
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
unsigned int MuonBackground::calculateNumberOfCollision( int ispill ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> calculateNumberOfCollsion " << endmsg;
  unsigned int collision         = 0;
  std::string  collisionLocation = fmt::format( "/Event{}/{}", spill[ispill], LHCb::GenCollisionLocation::Default );
  if ( msgLevel( MSG::VERBOSE ) ) {
    verbose() << "string " << collisionLocation << endmsg;
    verbose() << "spill " << ispill << endmsg;
  }

  SmartDataPtr<LHCb::GenCollisions> collisionsPointer( eventSvc(), collisionLocation );
  if ( collisionsPointer ) {
    collision = collisionsPointer->size();
    if ( msgLevel( MSG::VERBOSE ) ) {
      for ( const auto& c : *collisionsPointer ) { verbose() << " --- collision number " << c->key() << endmsg; }
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << " --- total collision number " << collision << endmsg;
  return collision;
}

StatusCode MuonBackground::calculateStartingNumberOfHits( const Cache& c, int ispill,
                                                          std::vector<std::vector<int>>& results,
                                                          const unsigned int             collisions ) const {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> calculateStartingNumberOfHit " << endmsg;
  int gap, chamber, index, station, region;
  int preGap, preChamber, preIndex;

  SmartDataPtr<LHCb::MCParticles> particlePointer(
      eventSvc(), fmt::format( "/Event{}/{}", spill[ispill], LHCb::MCParticleLocation::Default ) );
  //  LHCb::MCParticles::iterator itParticle;
  // for(itParticle=particlePointer->begin();
  //    itParticle<particlePointer->end();itParticle++){
  // }
  int numberOfParticles = 0;
  if ( particlePointer ) {
    std::vector<const LHCb::MCVertex*> vtxList;

    numberOfParticles = particlePointer->size();
    if ( msgLevel( MSG::DEBUG ) ) debug() << " --- original number of particles " << numberOfParticles << endmsg;
    // create an dimension appropriate vector
    std::map<int, std::unique_ptr<ParticleInfo>> particleInfo;
    // loop un hits

    std::string                path = fmt::format( "/Event{}/{}", spill[ispill], LHCb::MCHitLocation::Muon );
    SmartDataPtr<LHCb::MCHits> hitPointer( eventSvc(), path );
    if ( msgLevel( MSG::VERBOSE ) ) {
      verbose() << " container in path " << path << " " << endmsg;
      if ( hitPointer != 0 )
        verbose() << "found " << endmsg;
      else
        verbose() << " not found " << endmsg;
    }

    preGap     = -1;
    preIndex   = -1;
    preChamber = -1;
    if ( hitPointer != 0 ) {
      for ( auto& hit : *hitPointer ) {
        int det = hit->sensDetID();
        if ( det < 0 ) continue;

        station = c.det->stationID( det );
        region  = c.det->regionID( det );
        gap     = c.det->gapID( det );

        chamber = c.det->chamberID( det ) + c.chamberOffset( station, region );
        index   = hit->mcParticle()->key();
        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << " index, chamber, gap " << index << " " << chamber << " " << gap << endmsg;
          verbose() << " index " << index << " in position " << hit->entry().x() << " " << hit->entry().y() << " "
                    << hit->entry().z() << " "
                    << " out position  " << hit->exit().x() << " " << hit->exit().y() << " " << hit->exit().z()
                    << endmsg;
        }
        const LHCb::MCVertex* pointVertex = hit->mcParticle()->primaryVertex();
        unsigned int          collNumber  = numberOfCollision( pointVertex, vtxList );
        if ( msgLevel( MSG::DEBUG ) ) debug() << " collNumber " << collNumber << endmsg;
        if ( collNumber >= collisions ) {
          error() << " problem with input data inconsistency in collisions number " << endmsg;
          return StatusCode::FAILURE;
        }

        if ( particleInfo.count( index ) > 0 ) {

          if ( chamber != preChamber || gap != preGap || index != preIndex )
            particleInfo[index]->setHitIn( station, gap, chamber );

        } else {
          particleInfo[index] = std::make_unique<ParticleInfo>( collNumber, c.stationsNumber, c.gaps );
          particleInfo[index]->setHitIn( station, gap, chamber );
        }
        preGap     = gap;
        preIndex   = index;
        preChamber = chamber;
      }
    }

    std::for_each( begin( results ), end( results ), [&]( auto& entry ) { entry.resize( c.maxDimension() ); } );

    // all hits in the event have been added
    // then count the multiplicity per station  and track lenght   and delete
    for ( auto& [_, pinfo] : particleInfo ) {
      if ( pinfo ) {
        auto particleResult = pinfo->multiplicityResults();
        auto partCollision  = pinfo->getCollision();
        std::transform( begin( particleResult ), end( particleResult ), begin( results[partCollision] ),
                        begin( results[partCollision] ), std::plus{} );
      }
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << " --- end of routine " << endmsg;
  return StatusCode::SUCCESS;
}

StatusCode MuonBackground::initializeRNDDistribution1D( const IHistogram1D*                          histoPointer,
                                                        std::vector<std::unique_ptr<Rndm::Numbers>>& distributions,
                                                        std::vector<bool>& flags, double& xmin, double& xmax ) const {

  //  const IAxis* axis=&(histoPointer->axis());
  const IAxis& axis = ( histoPointer->axis() );
  int          nbin = axis.bins();
  xmin              = axis.lowerEdge();
  xmax              = axis.upperEdge();
  std::vector<double> content( nbin );
  int                 total = 0;

  for ( int i = 0; i < nbin; i++ ) {
    double tmp = histoPointer->binHeight( i );
    if ( tmp < 0 ) {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " negative value for histogram " << histoPointer->title() << " " << tmp << endmsg;
      tmp = 0;
    }
    content[i] = tmp;
    total      = total + (int)histoPointer->binHeight( i );
  }
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "total " << total << endmsg;
  if ( total <= 0 ) {
    flags.push_back( false );
    distributions.push_back( {} );
  } else {
    flags.push_back( true );
    distributions.push_back( std::make_unique<Rndm::Numbers>( randSvc(), Rndm::DefinedPdf( content, 0 ) ) );
  }
  return StatusCode::SUCCESS;
}

StatusCode MuonBackground::initializeRNDDistribution2D( const IHistogram2D*                          histoPointer,
                                                        std::vector<std::unique_ptr<Rndm::Numbers>>& distributions,
                                                        std::vector<bool>& flags, double& xmin, double& xmax,
                                                        int& nbinx, double& ymin, double& ymax ) const {
  const IAxis& axisx = ( histoPointer->xAxis() );
  nbinx              = axisx.bins();
  xmin               = axisx.lowerEdge();
  xmax               = axisx.upperEdge();
  const IAxis& axisy = ( histoPointer->yAxis() );
  //  int nbiny=axisy.bins();
  ymin = axisy.lowerEdge();
  ymax = axisy.upperEdge();
  for ( int xbin = 0; xbin < nbinx; xbin++ ) {
    // create slice parallel to x axis
    IHistogram1D* ySlice  = histoSvc()->histogramFactory()->sliceY( "MuBG/1", *histoPointer, xbin );
    int           entries = ySlice->entries();
    if ( msgLevel( MSG::VERBOSE ) && entries <= 0 ) { verbose() << " zero entries" << endmsg; }
    StatusCode sc = initializeRNDDistribution1D( ySlice, distributions, flags, ymin, ymax );
    if ( sc.isFailure() ) return sc;
    sc = histoSvc()->unregisterObject( ySlice );
    if ( sc.isFailure() ) { warning() << " problem in releasing the histo " << endmsg; }

    delete ySlice;
  }
  return StatusCode::SUCCESS;
}

// if successful, returns tuple chamber, r, xpos, ypos
std::optional<std::tuple<MuonBackground::DetElementRef<DeMuonChamber>, float, float, float>>
MuonBackground::getRadialPosition( const Cache& c, int index, int station ) const {
  for ( int tryR = 0; tryR < 10; tryR++ ) {
    float r = ( c.bg[index].radial )->getRND();
    // 2) extract the gloabl phi
    for ( int tryPhi = 0; tryPhi < 10; tryPhi++ ) {
      double globalPhi = ( ( c.bg[index].phiglobvsradial )->getRND( r ) ) * Gaudi::Units::pi / 180.0;
      // 3) test whether the r,phi is inside the sensitive chamber volume
      //  transform r and phi in x,y
      float xpos = float( r * cos( globalPhi ) * Gaudi::Units::mm * m_unitLength );
      float ypos = float( r * sin( globalPhi ) * Gaudi::Units::mm * m_unitLength );

      try {
        DetElementRef<DeMuonChamber> chamber = c.det->pos2StChamber( xpos, ypos, station );
        // check n ot only that hit is inside chamber but also gap...
        if ( chamber->checkHitAndGapInChamber( xpos, ypos ) ) {
          // remember this is the chamber number inside a region....
          if ( msgLevel( MSG::DEBUG ) )
            debug() << " chamber number " << chamber->chamberNumber() << " station number " << chamber->stationNumber()
                    << " region number " << chamber->regionNumber() << " position " << xpos << " " << ypos << " "
                    << endmsg;
          return std::tuple<DetElementRef<DeMuonChamber>, float, float, float>{chamber, r, xpos, ypos};
        } else {
          if ( msgLevel( MSG::DEBUG ) ) debug() << " no gap found for muon hit" << endmsg;
        }
      } catch ( std::exception& e ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << " no chamber found for muon hit : " << e.what() << endmsg;
      }
    }
  }
  return {};
}

StatusCode MuonBackground::createHit( const Cache& c, LHCb::MCHits* hitsContainer, int station, int multi,
                                      int ispill ) const {
  int index = station * c.gaps + multi;

  // 1) extract the radial position
  auto pos = getRadialPosition( c, index, station );

  if ( !pos ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << " could not find a r.phi combination " << endmsg;
    return StatusCode::SUCCESS;
  }

  auto& [pChamber, r, xpos, ypos] = *pos;
  // define the chamber index in the total reference...
  // extract the hit gaps
  int              allgap = (int)( c.bg[index].hitgap )->getRND();
  int              max    = 8;
  std::vector<int> gapHitTmp;
  if ( msgLevel( MSG::DEBUG ) )
    debug() << " gap extracted " << allgap << " multiplicity " << multi << " " << index << " " << c.gaps << endmsg;
  for ( unsigned int i = 0; i < c.gaps; i++ ) {
    int gap = allgap / max;
    if ( gap > 1 ) gap = 1;
    if ( gap == 1 ) gapHitTmp.push_back( c.gaps - i - 1 );
    allgap = allgap - gap * max;
    // verbose()<<"allgap "<<gap<<" "<<gapHitTmp.back()<<" "<<
    //  max<<endmsg;
    max = max / 2;
  }
  if ( gapHitTmp.size() != (unsigned int)multi + 1 ) {
    warning() << "problem in extraction the gaps exit " << endmsg;
    return StatusCode::SUCCESS;
  }
  std::vector<int>                   gapHit;
  std::vector<int>::reverse_iterator it;
  for ( it = gapHitTmp.rbegin(); it < gapHitTmp.rend(); it++ ) {
    gapHit.push_back( ( *it ) );
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << " gap back" << gapHit.back() << endmsg;
  }
  int   firstGap         = gapHit[0];
  int   lastGap          = gapHit[gapHit.size() - 1];
  int   tryPhiLoc        = 0;
  int   maxTryPhiLoc     = 20;
  bool  allHitsInsideCha = false;
  bool  hitsToAdd        = false;
  float xSlope, ySlope;
  for ( tryPhiLoc = 0; tryPhiLoc < maxTryPhiLoc && !allHitsInsideCha; tryPhiLoc++ ) {
    allHitsInsideCha = true;
    hitsToAdd        = false;
    double phiLoc    = ( ( c.bg[index].philocvsradial )->getRND( r ) ) * Gaudi::Units::pi / 180.0;
    double thetaLoc  = ( ( c.bg[index].thetalocvsradial )->getRND( r ) ) * Gaudi::Units::pi / 180.0;
    // define the more confortable slope in x-y direction
    if ( !LHCb::essentiallyZero( cos( thetaLoc ) ) ) {
      xSlope = float( sin( thetaLoc ) * cos( phiLoc ) / cos( thetaLoc ) );
      ySlope = float( sin( thetaLoc ) * sin( phiLoc ) / cos( thetaLoc ) );
    } else {
      xSlope = 1.0F;
      ySlope = 1.0F;
    }
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << " local slope " << xSlope << " " << ySlope << endmsg;
    // define the z of the average gaps position
    float averageZ = pChamber->calculateAverageGap( firstGap, lastGap, xpos, ypos );
    for ( int igap = 0; igap <= multi; igap++ ) {
      int             gapNumber = gapHit[igap];
      Gaudi::XYZPoint entryGlobal;
      Gaudi::XYZPoint exitGlobal;
      bool            success =
          pChamber->calculateHitPosInGap( gapNumber, xpos, ypos, xSlope, ySlope, averageZ, entryGlobal, exitGlobal );
      if ( success ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "found hit in chamber  " << endmsg;
      } else {
        allHitsInsideCha = false;
      }
    }
    if ( allHitsInsideCha ) hitsToAdd = true;
  }
  if ( hitsToAdd ) {
    float scale    = log( 10.0F );
    float time     = exp( scale * ( c.bg[index].logtimevsradial )->getRND( r ) );
    float timeBest = 0;
    // patch to extract the timein linear mode if needed other
    // wise just transform inlinear fromlog scale
    if ( time <= c.bg[index].lintimevsradial->getMax() ) {
      timeBest = c.bg[index].lintimevsradial->getRND( r );
    } else {
      timeBest = time;
    }
    int   _firstGap = gapHit[0];
    int   _lastGap  = gapHit[gapHit.size() - 1];
    float averageZ  = pChamber->calculateAverageGap( _firstGap, _lastGap, xpos, ypos );
    for ( int igap = 0; igap <= multi; igap++ ) {
      bool            correct   = true;
      int             gapNumber = gapHit[igap];
      Gaudi::XYZPoint entryGlobal;
      Gaudi::XYZPoint exitGlobal;

      bool _sc =
          pChamber->calculateHitPosInGap( gapNumber, xpos, ypos, xSlope, ySlope, averageZ, entryGlobal, exitGlobal );
      if ( !_sc ) return StatusCode::FAILURE;
      double       x    = ( entryGlobal.x() + exitGlobal.x() ) / 2.0;
      double       y    = ( entryGlobal.y() + exitGlobal.y() ) / 2.0;
      double       z    = ( entryGlobal.z() + exitGlobal.z() ) / 2.0;
      LHCb::MCHit* pHit = new LHCb::MCHit();
      pHit->setEntry( entryGlobal );
      pHit->setDisplacement( exitGlobal - entryGlobal );
      double tofOfLight = ( sqrt( x * x + y * y + z * z ) ) / 300.0;
      if ( m_type == FlatSpillover ) {
        float shiftOfTOF = -ispill * m_BXTime;
        pHit->setTime( timeBest + tofOfLight + shiftOfTOF );
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "time " << timeBest + shiftOfTOF << " spill " << ispill << endmsg;
      } else {
        pHit->setTime( timeBest + tofOfLight );
      }
      if ( msgLevel( MSG::DEBUG ) ) debug() << " mid point " << x << " " << y << " " << z << endmsg;

      int sen = c.det->sensitiveVolumeID( Gaudi::XYZPoint( x, y, z ) );
      if ( msgLevel( MSG::DEBUG ) ) debug() << " the volume ID is " << sen << endmsg;
      pHit->setSensDetID( sen );
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "gap, time, position " << gapNumber << " " << pChamber->chamberNumber() << " "
                << timeBest + tofOfLight << " " << x << " " << y << " " << endmsg;
      if ( correct ) {
        ( hitsContainer )->push_back( pHit );
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << " hits has been inserted " << endmsg;
          debug() << " multiplicity " << multi << endmsg;
          debug() << "entry position " << pHit->entry().x() << " " << pHit->entry().y() << " " << pHit->entry().z()
                  << endmsg;
          debug() << "exit position " << pHit->exit().x() << " " << pHit->exit().y() << " " << pHit->exit().z()
                  << endmsg;
        }
      }
    }
  } else {
    if ( msgLevel( MSG::DEBUG ) )
      debug() << " impossible to add the requested hits in station " << station << " and multiplicity " << multi
              << endmsg;
  }
  return StatusCode::SUCCESS;
}

int MuonBackground::howManyHit( float floatHit ) const {
  int   intHit  = 0;
  float partHit = 0;
  intHit        = (int)floatHit;
  partHit       = floatHit - intHit;

  if ( ( *m_flatDistribution )() < partHit ) {
    return intHit + 1;
  } else {
    return intHit;
  }
}

unsigned int MuonBackground::numberOfCollision( const LHCb::MCVertex*               vertex,
                                                std::vector<const LHCb::MCVertex*>& vtxList ) const {
  unsigned int collision = 0;
  for ( auto v : vtxList ) {
    if ( v == vertex ) return collision;
    ++collision;
  }
  vtxList.push_back( vertex );
  return collision;
}
