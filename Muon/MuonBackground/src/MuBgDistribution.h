/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <GaudiKernel/RndmGenerators.h>
#include <memory>
#include <utility>
#include <vector>

/** @class MuBgDistribution MuBgDistribution.h
 *
 *
 *  @author Alessia Satta
 *  @date   2003-03-03
 */
class MuBgDistribution final {
public:
  /// Standard constructor
  MuBgDistribution( std::vector<std::unique_ptr<Rndm::Numbers>> distributions, std::vector<bool> flags, float xmin,
                    float xmax )
      : m_distributions{std::move( distributions )}
      , m_flags{std::move( flags )}
      , m_nbinx{0}
      , m_xmin{xmin}
      , m_xmax{xmax}
      , m_ymin{0}
      , m_ymax{0}
      , m_dimension{1} {}

  MuBgDistribution( std::vector<std::unique_ptr<Rndm::Numbers>> distributions, std::vector<bool> flags, float xmin,
                    float xmax, int nbinx, float ymin, float ymax )
      : m_distributions{std::move( distributions )}
      , m_flags{std::move( flags )}
      , m_nbinx{nbinx}
      , m_xmin{xmin}
      , m_xmax{xmax}
      , m_ymin{ymin}
      , m_ymax{ymax}
      , m_dimension{2} {}

  inline float getMin() const { return ( m_dimension == 2 ) ? m_ymin : m_xmin; }
  inline float getMax() const { return ( m_dimension == 2 ) ? m_ymax : m_xmax; }

  float getRND();
  float getRND( float x );

private:
  std::vector<std::unique_ptr<Rndm::Numbers>> m_distributions;
  std::vector<bool>                           m_flags;

  int   m_nbinx;
  float m_xmin, m_xmax, m_ymin, m_ymax;
  int   m_dimension;
};
