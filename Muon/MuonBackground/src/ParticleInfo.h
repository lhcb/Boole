/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Event/MCParticle.h>
#include <cstdint>

/** @class ParticleInfo ParticleInfo.h
 *
 *
 *  @author Alessia Satta
 *  @date   2003-02-19
 */
class ParticleInfo final {
public:
  /// Standard constructor
  ParticleInfo( int collision, std::size_t stations = 0, std::size_t gaps = 0 )
      : m_collision{collision}, m_stationsNumber{stations}, m_gapsNumber{gaps}, m_storage{stations * gaps} {}

  void setHitIn( std::size_t station, std::size_t gap, std::uint32_t chamber ) {
    m_storage[index( station, gap )].push_back( chamber );
  }
  std::vector<std::uint32_t> multiplicityResults() const;
  int                        getCollision() const { return m_collision; }

private:
  std::size_t index( std::size_t station, std::size_t gap ) const { return station * m_stationsNumber + gap; }

  int         m_collision;
  std::size_t m_stationsNumber;
  std::size_t m_gapsNumber;

  std::vector<std::vector<std::uint32_t>> m_storage;
};
