/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "ParticleInfo.h"
#include <iostream>

std::vector<std::uint32_t> ParticleInfo::multiplicityResults() const {
  std::vector<std::uint32_t> result( m_storage.size() );

  auto storage = m_storage; // make a copy because the scan is destructive
  for ( std::size_t station = 0; station < m_stationsNumber; ++station ) {
    for ( std::size_t gap = 0; gap < m_gapsNumber; ++gap ) {
      auto& chambers = storage[index( station, gap )];
      for ( auto chamber = rbegin( chambers ); chamber != rend( chambers ); ++chamber ) {
        std::uint32_t multiplicity = 0;
        for ( std::size_t secondGap = gap + 1; secondGap < m_gapsNumber; ++secondGap ) {
          auto& others   = storage[index( station, secondGap )];
          auto  foundHit = std::find( others.begin(), others.end(), *chamber );
          if ( foundHit != others.end() ) {
            multiplicity++;
            others.erase( foundHit );
            auto deleteHit = std::find( others.begin(), others.end(), *chamber );
            while ( deleteHit != others.end() ) {
              others.erase( deleteHit );
              deleteHit = std::find( others.begin(), others.end(), *chamber );
            }
          }
        }
        ++result[station * m_gapsNumber + multiplicity];
      }
    }
  }
  return result;
}
