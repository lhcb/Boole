/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuBgDistribution.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuBgDistribution
//
// 2003-03-03 : Alessia Satta
//-----------------------------------------------------------------------------

//=============================================================================
float MuBgDistribution::getRND() {
  if ( m_flags[0] ) {
    double nd = ( *( m_distributions[0] ) )();
    return float( nd * ( m_xmax - m_xmin ) + m_xmin );
  }
  return 0;
}

float MuBgDistribution::getRND( float x ) {
  float        lenght = ( m_xmax - m_xmin ) / m_nbinx;
  unsigned int index  = (int)( ( x - m_xmin ) / lenght );
  if ( index < m_flags.size() ) {
    if ( m_flags[index] ) {
      double nd = ( *( m_distributions[index] ) )();
      return float( nd * ( m_ymax - m_ymin ) + m_ymin );
    }
  }
  return 0;
}
