###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def makeLowEnergyBG(suffix):
    from Configurables import MuonBackground
    from Configurables import HistogramDataSvc

    alg = MuonBackground(
        "MuonLowEnergy",
        HistogramsFile="LowEnergy",
        HistogramsUpCode=100000,
        ContainerName="BackgroundHits",
        SafetyFactors=[1.1, 1.1, 1.1, 1.1],
        HistogramsNumber=[-1, 1, 2, 3, 4, 5, 6, 7],
        HistogramsDimension=[2, 1, 2, 2, 2, 2, 2, 1],
        HistogramsMeaning=[
            "correlation", "r", "phi", "thetaloc", "philoc", "lintime",
            "logtime", "hitgap"
        ],
        BackgroundType="LowEnergy",
    )

    HistogramDataSvc().Input.append(
        f"LowEnergy DATAFILE='$PARAMFILESROOT/data/muonlow-{suffix}.root' TYP='ROOT'"
    )

    if suffix.startswith("G4"):
        alg.RadialUnit = 1.0

    return alg


def makeFlatSpilloverBG(suffix):
    from Configurables import MuonBackground
    from Configurables import HistogramDataSvc
    alg = MuonBackground(
        "MuonFlatSpillover",
        HistogramsFile="MuonFlatSpillover",
        HistogramsUpCode=100000,
        ContainerName="FlatSpilloverHits",
        SafetyFactors=[1, 1, 1, 1],
        HistogramsNumber=[1, 2, 3, 4, 5, 6, 7],
        HistogramsDimension=[1, 2, 2, 2, 2, 2, 1],
        HistogramsMeaning=[
            "r", "phi", "thetaloc", "philoc", "lintime", "logtime", "hitgap"
        ],
        BackgroundType="FlatSpillover",
        FlatSpillNumber=8,
    )

    HistogramDataSvc().Input.append(
        f"MuonFlatSpillover DATAFILE='$PARAMFILESROOT/data/muonflat-{suffix}.root' TYP='ROOT'"
    )

    if suffix.startswith("GCALOR"):
        alg.AverageFlatHits = [
            0.48, 0.1, 0.03, 0.023, 0.16, 0.03, 0.01, 0.009, 0.14, 0.03, 0.01,
            0.006, 0.26, 0.05, 0.02, 0.01
        ]
    elif suffix.startswith("G4"):
        # AverageFlatHits:  mean values of occupancy distributions for 1/2/3/4-gaps hits with times > 400 ns for Low Threshold MC
        if suffix.endswith("Run3-v2.0"):
            # Run3 numbers
            alg.AverageFlatHits = [
                1.25, 0.27, 0.08, 0.01, 0.04, 0.003, 0.0002, 0.000002, 0.03,
                0.002, 0.0002, 0.000003, 0.3, 0.05, 0.01, 0.003
            ]
            alg.NBXFullFull = 2395
            alg.LumiLETMC = 2.0e+33
        else:
            # # Run1/Run2 numbers
            alg.AverageFlatHits = [
                1.42, 0.24, 0.06, 0.02, 0.57, 0.09, 0.02, 0.005, 0.34, 0.06,
                0.01, 0.003, 1.61, 0.26, 0.06, 0.01
            ]
        alg.RadialUnit = 1.0
    else:
        raise RuntimeError(
            "Muon Low Energy background file suffix must start with G4 or GCALOR"
        )

    return alg
