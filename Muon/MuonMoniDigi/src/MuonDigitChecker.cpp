/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Event/MCHeader.h>
#include <Event/MCHit.h>
#include <Event/MCMuonDigit.h>
#include <Event/MuonDigit.h>
#include <Gaudi/Accumulators.h>
#include <GaudiAlg/GaudiTupleAlg.h>
#include <GaudiAlg/Tuple.h>
#include <GaudiAlg/TupleObj.h>
#include <GaudiKernel/GaudiException.h>
#include <LHCbAlgs/Consumer.h>
#include <MuonDet/DeMuonDetector.h>
#include <MuonDet/MuonNamespace.h>
#include <array>
#include <cstdint>
#include <fmt/format.h>
#include <limits>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>

namespace {
  template <typename Counter, std::size_t N, typename Owner, typename NameFunction, int... I>
  auto make_counter_array_impl( Owner* owner, NameFunction f, std::integer_sequence<int, I...> ) {
    return std::array<Counter, N>{{{owner, f( I )}...}};
  }

  template <typename Counter, std::size_t N, typename Owner, typename NameFunction>
  auto make_counter_array( Owner* owner, NameFunction f ) {
    return make_counter_array_impl<Counter, N>( owner, f, std::make_integer_sequence<int, N>{} );
  }

  // FIXME: this strings are never used in the code, but I keep them here for reference as they were used at
  //        some point in the past
  // constexpr std::array<const char*, 4> hits_sources{"Hits", "ChamberNoiseHits", "FlatSpilloverHits",
  // "BackgroundHits"};
  constexpr std::array<const char*, 6> digi_sources{"Geant",         "FlatSpillOver", "Low Energy Background",
                                                    "Chamber Noise", "Crosstalk",     "Electronic Noise"};
} // namespace

/**
 *  This File contains the monitor for muon Digi system
 *
 *  @author A Sarti
 *  @date   2005-05-20
 */
class MuonDigitChecker
    : public LHCb::Algorithm::Consumer<void( const LHCb::MCHeader&, const LHCb::MCHits&, const LHCb::MuonDigits&,
                                             const LHCb::MCMuonDigits&, const DeMuonDetector& ),
                                       LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeMuonDetector>> {
public:
  MuonDigitChecker( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  void       operator()( const LHCb::MCHeader&, const LHCb::MCHits&, const LHCb::MuonDigits&, const LHCb::MCMuonDigits&,
                   const DeMuonDetector& muon ) const override;
  StatusCode finalize() override;

private:
  mutable std::array<Gaudi::Accumulators::AveragingCounter<>, 5 * 4> m_avgHits =
      make_counter_array<Gaudi::Accumulators::AveragingCounter<>, 5 * 4>(
          this, []( auto n ) { return fmt::format( "M{}R{}/Hits", n / 4 + 1, n % 4 + 1 ); } );
  mutable std::array<Gaudi::Accumulators::AveragingCounter<>, 5 * 4 * 6> m_avgDHits =
      make_counter_array<Gaudi::Accumulators::AveragingCounter<>, 5 * 4 * 6>( this, []( auto n ) {
        return fmt::format( "M{}R{}/{}", n / 6 / 4 + 1, ( n / 6 ) % 4 + 1, digi_sources[n % 6] );
      } );

  //
  // To activate ExpertMode
  // add following lines in your Boole python file:
  //
  // from Configurables import MuonDigitChecker
  // MuonDigitChecker().ExpertMode = True
  // from Configurables import NTupleSvc
  // NTupleSvc().Output = ["FILE1 DATAFILE='Boole-Muon-Expert.root' TYP='ROOT' OPT='NEW'"]
  //
  Gaudi::Property<bool> m_hitMonitor{this, "hitMonitor", true};
  Gaudi::Property<bool> m_expertMode{this, "ExpertMode", false};
  Gaudi::Property<bool> m_verboseMode{this, "VerboseMode", false};

  std::size_t m_stationIdOffset{0};
};

DECLARE_COMPONENT( MuonDigitChecker )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MuonDigitChecker::MuonDigitChecker( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"MCHeader", LHCb::MCHeaderLocation::Default}, KeyValue{"MCHits", LHCb::MCHitLocation::Muon},
                 KeyValue{"MuonDigits", LHCb::MuonDigitLocation::MuonDigit},
                 KeyValue{"MCMuonDigits", LHCb::MCMuonDigitLocation::MCMuonDigit},
                 KeyValue{"Muon", DeMuonLocation::Default}} ) {
  setHistoTopDir( "MUON/" );
  setProperty( "NTupleTopDir", "MUON/" ).orThrow( "failed to set property NTupleTopDir", name );
}

StatusCode MuonDigitChecker::initialize() {
  return Consumer::initialize().andThen( [&]() {
    // Add conditions callback to get the names of Muon stations.
    // We keep the names in a data member because we need them at finalize.
    addConditionDerivation( {inputLocation<DeMuonDetector>()}, name() + "GotStationNames",
                            [&]( const DeMuonDetector& det ) {
                              struct GotStationNames {};
                              // check if we are in the with or without M1 case
                              m_stationIdOffset = ( det.getStationName( 0 ) == "M1" ) ? 0 : 1;
                              return GotStationNames{};
                            } );
  } );
}

//=============================================================================
// Main execution
//=============================================================================
void MuonDigitChecker::operator()( const LHCb::MCHeader& evt, const LHCb::MCHits& hits, const LHCb::MuonDigits& digits,
                                   const LHCb::MCMuonDigits& mcdigits, const DeMuonDetector& muon ) const {
  std::array<int, std::tuple_size<decltype( m_avgHits )>::value>  tnhit  = {0};
  std::array<int, std::tuple_size<decltype( m_avgDHits )>::value> tnDhit = {0};

  auto evtNr = evt.evtNumber();

  std::vector<float> sta, reg, cha, x, y, z, vtime, id;
  std::vector<float> px, py, pz, E, xv, yv, zv, tv, mom;
  std::vector<float> ple, hen, dix, dxz, dyz;

  std::vector<float> digit_s, digit_r, digit_x, digit_y;
  std::vector<float> digit_z, digit_dx, digit_dy, digit_dz, digit_time;
  std::vector<float> digit_origin, digit_bx, digit_firing, digit_multi;

  // loop over Muon Hits only if required
  if ( m_hitMonitor ) {
    int MyDetID;
    // Loop over Muon Hits of given type
    for ( const auto* hit : hits ) {

      MyDetID = hit->sensDetID();
      if ( MyDetID < 0 ) continue;
      // Needs to extract info from sens ID
      int station = muon.stationID( MyDetID );
      int region  = muon.regionID( MyDetID );
      int chamber = muon.chamberID( MyDetID );

      if ( msgLevel( MSG::DEBUG ) ) debug() << " region:: " << region << endmsg;

      float xpos = ( hit->entry().x() + hit->exit().x() ) / 2.0;
      float ypos = ( hit->entry().y() + hit->exit().y() ) / 2.0;
      float zpos = ( hit->entry().z() + hit->exit().z() ) / 2.0;
      float time = hit->time();

      double tof = time - sqrt( xpos * xpos + ypos * ypos + zpos * zpos ) / 300.0;
      if ( tof < 0.1 ) tof = 0.1;
      float r = sqrt( xpos * xpos + ypos * ypos );

      sta.push_back( station );
      reg.push_back( region );
      cha.push_back( chamber );

      // Temporary monitoring (need to check if already available)
      ple.push_back( hit->pathLength() );
      hen.push_back( hit->energy() );
      dix.push_back( hit->displacement().x() );
      dxz.push_back( hit->dxdz() );
      dyz.push_back( hit->dydz() );

      x.push_back( xpos );
      y.push_back( ypos );
      z.push_back( zpos );
      vtime.push_back( time );

      //
      if ( m_expertMode ) {
        int hh = station * 4 + region;
        plot( r, hh + 2000, "Hits Radial Multiplicity", 0., 6000., 200 );
        plot( tof, hh + 1000, "Hits Time multiplicity", 0., 100., 200 );
      }
      // MC truth
      const LHCb::MCParticle* particle = hit->mcParticle();
      if ( particle ) {
        if ( abs( particle->particleID().pid() ) < 100000 ) { id.push_back( particle->particleID().pid() ); }

        px.push_back( particle->momentum().px() );
        py.push_back( particle->momentum().py() );
        // Pz sign tells you the particle direction
        pz.push_back( particle->momentum().pz() );
        E.push_back( particle->momentum().e() );

        // Particle Vertex studies
        xv.push_back( particle->originVertex()->position().x() );
        yv.push_back( particle->originVertex()->position().y() );
        zv.push_back( particle->originVertex()->position().z() );
        tv.push_back( particle->originVertex()->time() );

        const LHCb::MCParticle* moth = particle->mother();
        if ( moth && ( abs( moth->particleID().pid() ) < 100000 ) ) {
          mom.push_back( moth->particleID().pid() );
        } else {
          mom.push_back( 0 );
        }
      } else {
        id.push_back( 0 );
        px.push_back( 0 );
        py.push_back( 0 );
        pz.push_back( 0 );
        E.push_back( 0 );
        xv.push_back( 0 );
        yv.push_back( 0 );
        zv.push_back( 0 );
        tv.push_back( 0 );
        mom.push_back( 0 );
      }
      ++tnhit[( station + m_stationIdOffset ) * 4 + region];
    }
    // the weird start of loop is to skip M1 if not present
    if ( m_expertMode )
      for ( std::size_t i = m_stationIdOffset * 4; i < tnhit.size(); ++i ) { m_avgHits[i] += tnhit[i]; }
  }

  // Loop on Digits (strips)

  int    Dsta, Dreg, Dcon;
  double Dfir;
  bool   Deve;

  int    occu[4] = {0};
  double r_digit = -999.;
  double tX      = -1.;

  for ( const auto* jdigit : digits ) {
    Dsta = jdigit->key().station();
    Dreg = jdigit->key().region();

    digit_s.push_back( Dsta );
    digit_r.push_back( Dreg );
    digit_time.push_back( jdigit->TimeStamp() );

    // Timestamp is one of the 8 intervals (8 digits) in which
    // the 20ns acceptance window is subdivided after beam crossing
    auto pos = muon.position( jdigit->key() );
    if ( !pos && msgLevel( MSG::DEBUG ) ) debug() << "error in tile " << endmsg;
    digit_x.push_back( pos->x() );
    digit_y.push_back( pos->y() );
    digit_z.push_back( pos->z() );

    digit_dx.push_back( pos->dX() );
    digit_dy.push_back( pos->dY() );
    digit_dz.push_back( pos->dZ() );

    // -- Prepare variables for Digits Monitoring Plots
    occu[Dsta]++;
    r_digit = sqrt( pow( pos->x(), 2 ) + pow( pos->y(), 2 ) ) / 10.; // cm
    tX      = ( jdigit->TimeStamp() * 1.6 ) / 3.;

    if ( Dsta == 0 ) {
      plot( r_digit, 3012, "Digits Radial Distribution Station M2 ", 0., 600., 100 );
      plot( tX, 3112, "Digits Time Distribution Station M2 ", 0., 10., 10 );
    }
    if ( Dsta == 1 ) {
      plot( r_digit, 3013, "Digits Radial Distribution Station M3 ", 0., 600., 100 );
      plot( tX, 3113, "Digits Time Distribution Station M3 ", 0., 10., 10 );
    }
    if ( Dsta == 2 ) {
      plot( r_digit, 3014, "Digits Radial Distribution Station M4 ", 0., 600., 100 );
      plot( tX, 3114, "Digits Time Distribution Station M4 ", 0., 10., 10 );
    }
    if ( Dsta == 3 ) {
      plot( r_digit, 3015, "Digits Radial Distribution Station M5 ", 0., 600., 100 );
      plot( tX, 3115, "Digits Time Distribution Station M5 ", 0., 10., 10 );
    }
    //---

    // Match with "true" MC digit
    LHCb::MCMuonDigit* MCmd = mcdigits.object( jdigit->key() );
    if ( !MCmd ) {
      throw GaudiException(
          fmt::format( "Could not find the match for {} in {}", jdigit->key(), inputLocation<LHCb::MCMuonDigits>() ),
          name(), StatusCode::FAILURE );
    }

    LHCb::MCMuonDigitInfo digInfo = MCmd->DigitInfo();

    // Hit orgin (codes in MuonEvent/v2r1/Event/MuonOriginFlag.h)
    // Geant                 = 0
    // FlastSpillOver        = 1
    // Low Energy Background = 2
    // Chamber Noise         = 3
    // Crosstalk             = 4
    // Electronic Noise      = 5
    Dcon = digInfo.natureOfHit();
    Deve = digInfo.doesFiringHitBelongToCurrentEvent();
    // bool Dali = digInfo.isAlive(); // not used
    Dfir = MCmd->firingTime();
    digit_firing.push_back( Dfir );
    digit_origin.push_back( Dcon );
    digit_bx.push_back( Deve );

    // Hits mupltiplicity
    digit_multi.push_back( MCmd->mcHits().size() );
    // if(Deve)
    ++tnDhit[( ( Dsta + m_stationIdOffset ) * 4 + Dreg ) * 6 + Dcon];
    // if(globalTimeOffset()>0)info()<<" qui "<<Dcon<<" "<<Dfir<<" "<<Deve<<" "<<Dsta<<" "<<Dreg<<endmsg;
  }

  // the weird start of loop is to skip M1 if not present
  for ( std::size_t i = m_stationIdOffset * 4 * 6; i < tnDhit.size(); ++i ) { m_avgDHits[i] += tnDhit[i]; }

  // --  Fill some histos with digits infos
  plot( occu[0], 3002, "Digits Multiplicity Station M2 ", 0., 350., 50 );
  plot( occu[1], 3003, "Digits Multiplicity Station M3 ", 0., 350., 50 );
  plot( occu[2], 3004, "Digits Multiplicity Station M4 ", 0., 350., 50 );
  plot( occu[3], 3005, "Digits Multiplicity Station M5 ", 0., 350., 50 );
  // --

  if ( m_expertMode ) {
    Tuple nt1 = nTuple( 41, "MCHITS", CLID_ColumnWiseTuple );

    StatusCode sc = nt1->column( "Event", evtNr, 0LL, 10000LL );
    if ( sc.isFailure() && msgLevel( MSG::DEBUG ) ) debug() << "nt error" << endmsg;

    nt1->farray( "is", sta, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "ir", reg, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "ch", cha, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "x", x, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "y", y, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "z", z, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "t", vtime, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "id", id, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "px", px, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "py", py, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "pz", pz, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "E", E, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "xv", xv, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "yv", yv, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "zv", zv, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "tv", tv, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "idm", mom, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "pl", ple, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "he", hen, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "dx", dix, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "xz", dxz, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "yz", dyz, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    sc = nt1->write();
    if ( sc.isFailure() && msgLevel( MSG::DEBUG ) ) debug() << "Failure writing nt1" << endmsg;

    Tuple nt2 = nTuple( 42, "DIGITS", CLID_ColumnWiseTuple );

    //    StatusCode sc=nt2->column("Event", digit_evt, 0LL,10000LL);
    sc = nt2->column( "Event", evtNr, 0LL, 10000LL );
    if ( sc.isFailure() ) debug() << " nt2 error " << endmsg;

    sc = nt2->farray( "is", digit_s, "Ndigits", 1000 );
    sc = nt2->farray( "ir", digit_r, "Ndigits", 1000 );
    sc = nt2->farray( "x", digit_x, "Ndigits", 1000 );
    sc = nt2->farray( "y", digit_y, "Ndigits", 1000 );
    sc = nt2->farray( "z", digit_z, "Ndigits", 1000 );
    sc = nt2->farray( "dx", digit_dx, "Ndigits", 1000 );
    sc = nt2->farray( "dy", digit_dy, "Ndigits", 1000 );
    sc = nt2->farray( "dz", digit_dz, "Ndigits", 1000 );
    sc = nt2->farray( "t", digit_time, "Ndigits", 1000 );
    sc = nt2->farray( "origin", digit_origin, "Ndigits", 1000 );
    sc = nt2->farray( "bx", digit_bx, "Ndigits", 1000 );
    sc = nt2->farray( "firing", digit_firing, "Ndigits", 1000 );
    sc = nt2->farray( "multip", digit_multi, "Ndigits", 1000 );
    sc = nt2->write();
    if ( sc.isFailure() && msgLevel( MSG::DEBUG ) ) debug() << "Failure writing nt2" << endmsg;
  }
}

//=============================================================================
//  Finalize
//=============================================================================
#include <tabulate/table.hpp>
StatusCode MuonDigitChecker::finalize() {
  using namespace tabulate;

  using Row_t = std::vector<variant<std::string, const char*, Table>>;
  Row_t stationNames;
  stationNames.push_back( "" ); // header of column for the region name
  for ( std::size_t i = m_stationIdOffset; i < 5; ++i ) { stationNames.push_back( fmt::format( "M{}", i + 1 ) ); }

  // helper to send one message line per line of the input string
  auto line_buffered_info = [this]( std::string_view msg ) {
    while ( !msg.empty() ) {
      auto newline_pos = msg.find( '\n' );
      info() << msg.substr( 0, newline_pos ) << endmsg;
      if ( newline_pos < msg.size() ) { // are at the end of the message yet
                                        // not yet: drop what we printed and the '\n'
        msg.remove_prefix( newline_pos + 1 );
      } else {
        // end of message reached: we are done
        return;
      }
    }
  };

  info() << "Muon Monitoring Tables:" << endmsg;

  if ( fullDetail() && m_expertMode ) {
    std::stringstream msg;
    msg << "- Hit Information";
    // FIXME: we should have a loop over `hits_sources`
    // but I removed it as the original code was only using "Hits"
    {
      Table source;
      source.add_row( {"TES Container: Hits"} );
      Table data;
      data.add_row( stationNames );
      for ( std::size_t r = 0; r < 4; ++r ) {
        Row_t region_row;
        region_row.reserve( stationNames.size() );
        region_row.push_back( fmt::format( "R{}", r + 1 ) );
        for ( std::size_t s = 1; s < stationNames.size(); ++s ) {
          const auto station = s - 1 + m_stationIdOffset;
          const auto region  = r;
          const auto idx     = station * 4 + region;
          region_row.push_back( fmt::format( "{:6.3f}", m_avgHits[idx].mean() ) );
        }
        data.add_row( region_row );
      }
      data.format().border_bottom( "" ).border_top( "" ).corner( "" ).font_align( FontAlign::right );
      data[0].format().font_align( FontAlign::center );
      source.add_row( {data} );
      source.format().border( "" ).corner( "" );
      source[0].format().font_style( {FontStyle::bold} );
      msg << '\n' << source;
    }
    line_buffered_info( msg.str() );
  }

  std::stringstream msg;
  msg << "- Digi Information";
  for ( auto src = digi_sources.begin(); src != digi_sources.end(); ++src ) {
    Table src_tbl;
    src_tbl.add_row( {fmt::format( "Container: {}", *src )} );
    Table data;
    data.add_row( stationNames );
    for ( std::size_t r = 0; r < 4; ++r ) {
      Row_t region_row;
      region_row.reserve( stationNames.size() );
      region_row.push_back( fmt::format( "R{}", r + 1 ) );
      for ( std::size_t s = 1; s < stationNames.size(); ++s ) {
        const auto station = s - 1 + m_stationIdOffset;
        const auto region  = r;
        const auto src_id  = src - digi_sources.begin();
        const auto idx     = ( station * 4 + region ) * 6 + src_id;
        region_row.push_back( fmt::format( "{:6.3f}", m_avgDHits[idx].mean() ) );
      }
      data.add_row( region_row );
    }
    data.format().border_bottom( "" ).border_top( "" ).corner( "" ).font_align( FontAlign::right );
    data[0].format().font_align( FontAlign::center );
    src_tbl.add_row( {data} );
    src_tbl.format().border( "" ).corner( "" );
    src_tbl[0].format().font_style( {FontStyle::bold} );
    msg << '\n' << src_tbl;
  }
  line_buffered_info( msg.str() );

  return Consumer::finalize();
}
