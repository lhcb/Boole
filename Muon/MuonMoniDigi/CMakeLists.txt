###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Muon/MuonMoniDigi
-----------------
#]=======================================================================]

gaudi_add_module(MuonMoniDigi
    SOURCES
        src/MuonDigitChecker.cpp
    LINK
        fmt::fmt
        Gaudi::GaudiAlgLib
        LHCb::DigiEvent
        LHCb::LHCbAlgsLib
        LHCb::MCEvent
        LHCb::MuonDetLib
)

# Find the header only library "tabulate" to format tables
find_path(tabulate_INCLUDE_DIR
    NAMES tabulate/table.hpp
    PATHS ${CMAKE_CURRENT_BINARY_DIR}/contrib/tabulate-1.4/include
)
if(NOT tabulate_INCLUDE_DIR)
    # if we cannot find tabulate from the system or already downloaded we download it now
    cmake_minimum_required(VERSION 3.18)  # this is to ensure we have file(ARCHIVE_EXTRACT)
    file(DOWNLOAD 
        # https://github.com/p-ranav/tabulate/archive/refs/tags/v1.4.tar.gz
        https://lhcb-repository.web.cern.ch/repository/lhcb-contrib/tabulate/tabulate-1.4.tar.gz
        ${CMAKE_CURRENT_BINARY_DIR}/contrib/tabulate-1.4.tar.gz
    )
    file(ARCHIVE_EXTRACT
        INPUT ${CMAKE_CURRENT_BINARY_DIR}/contrib/tabulate-1.4.tar.gz
        DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/contrib
        PATTERNS tabulate-1.4/include
    )
    set(tabulate_INCLUDE_DIR ${CMAKE_CURRENT_BINARY_DIR}/contrib/tabulate-1.4/include CACHE 
        PATH "" FORCE)
endif()

target_include_directories(MuonMoniDigi
    PRIVATE ${tabulate_INCLUDE_DIR}
)
