2021-09-06 Boole v43r0
===

This version uses
Lbcom [v33r1](../../../../Lbcom/-/tags/v33r1),
LHCb [v53r1](../../../../LHCb/-/tags/v53r1),
Gaudi [v36r0](../../../../Gaudi/-/tags/v36r0) and
LCG [100](http://lcginfo.cern.ch/release/100/) with ROOT 6.24.00.

This version is released on `master` branch.
Built relative to Boole [v42r0](../-/tags/v42r0), with the following changes:

### New features ~"new feature"

- ~VP | Added code to read threshold for VP pixels from SIMCOND, also HV from SIMCOND for raddamage, !331, !347 (@hcroft, @gcavalle)


### Fixes ~"bug fix" ~workaround

- Fix reference of genfsr test, !350 (@graven)
- Upstream project highlights :star:
  - ~"Event model" ~Simulation | Fix MCParticle beta, gamma and betaGamma, LHCb!3171 (@clemenci) [Boole#3]


### Enhancements ~enhancement

- ~VP | Update reference files for Boole!349 and LHCb!3163, !352 (@lbian)
- ~VP | Retina clusters and VELO SPs by default, !349 (@gbassi) :star:
- Upstream project highlights :star:
  - ~Decoding ~VP | Clustering upgrade from 3x5 to 3x3 matrix geometry, LHCb!3163 (@gbassi)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~VP ~FT | Follow changes in lhcb/LHCb!3134, !348 (@graven)
- ~Build | Rewrite CMake configuration in "modern CMake", !337 (@clemenci)
- Dropped usage of (UN)LIKELY macro, !351 (@sponce)


