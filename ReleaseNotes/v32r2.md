2017-12-15 Boole v32r2
===

This version uses projects LHCb v43r1, Lbcom v21r1, Gaudi v29r0, LCG_91
 (Root 6.10.06) and SQLDDDB v7r*, ParamFiles v8r*, FieldMap v5r*, AppConfig v3r*  

This version is a production release for upgrade simulations  

This version is released on `v32r0-patches` branch. The previous release on `v32r0-patches` branch  was Boole `v32r1`.  

### Bug fixes
- Added fix for non-existing module numbers in FT noise generation, !120, !122


