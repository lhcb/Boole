2020-10-19 Boole v41r2
===

This version uses
Lbcom [v31r2](../../../../Lbcom/-/tags/v31r2),
LHCb [v51r2](../../../../LHCb/-/tags/v51r2),
Gaudi [v34r1](../../../../Gaudi/-/tags/v34r1) and
LCG [97a](http://lcginfo.cern.ch/release/97a/) with ROOT 6.20.06.

This version is released on `master` branch.
Built relative to Boole [v41r1](../-/tags/v41r1), with the following changes:

### New features ~"new feature"

- ~RICH | RICH add realistic data formats, !289 (@jonrob)
- Upstream project highlights :star:
  - ~Decoding ~RICH | Add support for realistic RICH PMT encoding and decoding, LHCb!2664 (@jonrob)
  - ~Decoding ~Monitoring | Add decoding for SciFi NZS dataformat, LHCb!2648 (@legreeve)


### Fixes ~"bug fix" ~workaround

- Upstream project highlights :star:
  - ~Conditions | FTDet: Fix platform diffs due to floating point bug, LHCb!2679 (@sesen)


### Enhancements ~enhancement

- ~FT | Update Scifi electronics response for pacific5, !273 (@sesen) [LHCBSCIFI-150] :star:
- ~FT ~Monitoring | Move FT monitoring algorithms to MoniFT sequence, !310 (@sesen)
- ~FT ~Monitoring | Updated FT clustering monitoring algorithms, !287 (@sesen)
- Upstream project highlights :star:
  - Update of physics constants to match the current release of CLHEP, gaudi/Gaudi!1101


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Stable iteration order for Python 2/3, !305 (@cattanem)
- ~Configuration | Use log.warning instead of print, !296 (@cattanem)
- ~VP ~Conditions | Update test to latest physics constants in Gaudi, !303 (@clemenci) :star:
- ~FT | Updating Refs for !273, !300 (@sesen)
- ~FT ~Monitoring | Updating Refs for !310, !311 (@sesen)
- ~FT ~Monitoring | Updating Refs for !287, !290 (@abrearod)
- ~Muon | Adapt to new Gaudi property type, !292 (@jonrob)
- ~Calo | Remove L0 from Calo digitisation, !299 (@pkoppenb)
- Update test refs to follow gaudi/Gaudi!1116, !314 (@cattanem)
- Updating xdst-refs for LHCb!2761, !312 (@rcurrie)
- Update test references to follow !305, !307 (@cattanem)
- Cleaned up ref file of boolesys, !298 (@sponce)
- Adapted ref files to DD4hep changes, !291 (@sponce)
- Upstream project highlights :star:
  - ~Calo ~Functors ~"Event model" | Remove L0 related code, LHCb!2735 (@pkoppenb)
  - ~Monitoring | Remove HCMonitors, Lbcom!503 (@pkoppenb) [LHCBPS-1880]
