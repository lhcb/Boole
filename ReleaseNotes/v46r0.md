2024-05-15 Boole v46r0
===

This version uses
Lbcom [v36r0](../../../../Lbcom/-/tags/v36r0),
LHCb [v56r0](../../../../LHCb/-/tags/v56r0),
Detector [v2r0](../../../../Detector/-/tags/v2r0),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `master` branch.
Built relative to Boole [v45r0](/../../tags/v45r0), with the following changes:


### Fixes ~"bug fix" ~workaround

- Compatibility with Detector!474, !532 (@graven)


### Enhancements ~enhancement

- ~VP ~Simulation | Added pixel smearing in VP to match 2023 data taking values, !566 (@hcroft)


### Code cleanups, modernization and changes to tests ~modernisation ~cleanup ~testing

- ~FT | Simplify SciFi digitisation, !564 (@lohenry)
- ~RICH | MCRichDigitsToRawBufferAlg: Remove duplication in streaming format implementations, !521 (@jonrob)
- ~Build | Fixed warnings for C++20, !529 (@sponce)
- CaloSignalAlg: Replace implicit use of 'context' with dedicated isTAE property, !549 (@graven)
- Update References for: Detector!456 based on lhcb-master-mr/10413, !539 (@lhcbsoft)
- Disable tests which cannot work in DD4hep mode, !535 (@sponce)
- Reference update for LHCb!4399, !531 (@graven)
- Follow changes in LHCb!4010, !526 (@graven)
- Update References for: LHCb!4335 based on lhcb-master-mr/9875, !528 (@lhcbsoft)
- Update DetDesc refs, !573 (@edelucia)
