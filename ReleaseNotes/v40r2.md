

2018-12-20 Boole v40r2
===

This version uses Lbcom v30r2, LHCb v50r2, Gaudi v30r5 and LCG_94 with ROOT 6.14.04.

This version is a development release for all simulations  

This version is released on `master` branch. The previous release on `master` branch  was Boole `v40r1`.  

Please see Gaudi, LHCb, Lbcom release notes for detailed log of all changes   


### New features

- Add copyright statements, !177 (@clemenci) [LBCORE-1619]  

- Scifi FT cluster tuple, !171 (@sesen) [LHCBSCIFI-107]  
  A simple algorithm to study SciFi cluster resolution and efficiencies   


### Enhancements

- Change from double to float in UT noise simulation, lhcb/LHCb!1487, lhcb/Lbcom!276, !179 (@abeiter)    
  Causes tiny changes to output of tests    

- Clean up RichReadout memory management, !176 (@jonrob)   
  Addresses a minor memory leak on application exit, found by leak sanitizer.  

- Adapt to version Track object, !167 (@sstahl)   
  Just adds missing dependencies to TrackEventLib

- Convert MCHitInjector to Gaudi Functional, !165 (@mbieker) [LHCBSCIFI-144]  

- Update DDDB default global tags to follow lhcb-conddb/DDDB!16, lhcb/LHCb!1639 (@cattanem)   
  Updates particle properties to PDG 2018  
    
  dddb-20180726 for 2010  
  dddb-20180726-1 for 2011  
  dddb-20180726-2 for 2012 and 2013  
  dddb-20180726-3 for 2015, 2016, 2017 and 2018

  Includes also updates discussed in LHCBGAUSS-1224 (new description of M1 and speedup in RICH simulation)  


### Bug fixes

- RichReadout : Fix clang errors in RichPixel.h, !183 (@jonrob)   

- Fix untested StatusCode in LumiTool, !182 (@cattanem)   
  
- Protect 32-bit overflow when building rawBank word in CaloFillRawBuffer, lhcb/LHCb!1529 (@deschamp)   


### Code modernisations and cleanups

- Use dt-* branches instead of CondDB().useLatestTags, !186 (@clemenci) [LHCBPS-1813]  

- Removed unused import of CondDBAccessSvc configurable, !185 (@clemenci)   
  
- Remove unused variables, !184 (@cattanem)   
  Fixes clang warnings from Muon packages

- Remove packages list from BooleSys/CMakeLists.txt, !178 (@cattanem)   
  Packages list was needed for SVN, no longer necessary with Git

- fix gcc8 warnings, !174 (@graven)   
  
- Refactor UT code out of ST libraries, !170 (@abeiter)   
  UT classes are created from copies of ST classes. UT configuration within C++ and python code is updated to use UT libraries.

- Switch to new counters, lhcb/LHCb!1550, lhcb/Lbcom!284, !175 (@sponce)   


### Changes to tests

- Updated test-genfsr.qmt, !187 (@dfazzini)   
  Updated the test adding a check for each value stored in the genFSR  (counters, cross-sections and efficiencies)

