/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "RadDamage.h"
#include <DetDesc/DetectorElement.h>
#include <Detector/VP/VPChannelID.h>
#ifdef USE_DD4HEP
#  include <Detector/VP/VPConstants.h>
#  include <Detector/VP/VPVolumeID.h>
#endif
#include <Event/GenHeader.h>
#include <Event/MCHit.h>
#include <Event/MCVPDigit.h>
#include <GaudiAlg/GaudiTupleAlg.h>
#include <GaudiKernel/PhysicalConstants.h>
#include <GaudiKernel/RndmGenerators.h>
#include <Kernel/VPDataFunctor.h>
#include <LHCbAlgs/Transformer.h>
#include <LHCbMath/LHCbMath.h>
#include <VPDet/DeVP.h>
#include <map>
#include <vector>

#ifdef USE_DD4HEP
#  include <yaml-cpp/yaml.h>
#endif

namespace VPDepositCreatorConditions {
  struct ConditionsCache {
    ConditionsCache( std::array<double, 52> diffCoeff, std::array<std::array<double, 4>, 52> dTP )
        : diffusionCoefficient( std::move( diffCoeff ) ), deadTimeParam( std::move( dTP ) ) {}
    ConditionsCache()                         = default;
    ConditionsCache( const ConditionsCache& ) = default;
    ConditionsCache& operator=( const ConditionsCache& ) = default;
    // note 52 may fail if the number of modules changes, optimised for now
    std::array<double, 52>                diffusionCoefficient;
    std::array<std::array<double, 4>, 52> deadTimeParam;
  };
} // namespace VPDepositCreatorConditions

/**
 *  Using MCHits as input, this Functional::Transformer simulates the spatial ionisation
 *  pattern as well as the drift and diffusion of charge carriers in the sensor
 *  and produces an MCVPDigit for each pixel with a charge deposit.
 *
 *  @author Marcin Kucharczyk
 *  @date   20/09/09
 */
class VPDepositCreator
    : public LHCb::Algorithm::Transformer<
          LHCb::MCVPDigits( const LHCb::MCHits& mcPrevPrev, const LHCb::MCHits& mcPrev, const LHCb::MCHits& mcMain,
                            const LHCb::MCHits& mcNext, const LHCb::GenHeader& genHeader, const DeVP&,
                            const VPDepositCreatorConditions::ConditionsCache& ),
          LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeVP, VPDepositCreatorConditions::ConditionsCache>> {

public:
  VPDepositCreator( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode       initialize() override; ///< Algorithm initialization
  LHCb::MCVPDigits operator()( const LHCb::MCHits& mcPrevPrev, const LHCb::MCHits& mcPrev, const LHCb::MCHits& mcMain,
                               const LHCb::MCHits& mcNext, const LHCb::GenHeader& genHeader, const DeVP&,
                               const VPDepositCreatorConditions::ConditionsCache& ) const override;

private:
  /// Convert G4 energy deposit to charge deposits on pixels
  void createDeposits( const LHCb::MCHit&, LHCb::MCVPDigits&, const DeVP&, const double,
                       const VPDepositCreatorConditions::ConditionsCache& ) const;

  /// Draw random number from 1/q^2 distribution
  double randomTail( const double qmin, const double qmax ) const;

  /// Remove pixels randomly to simulate the deadtime in the VeloPix chips
  void deadTimeSim( LHCb::MCVPDigits& digits, const DeVP& det, const double deadTimeScaled,
                    const VPDepositCreatorConditions::ConditionsCache& conditions ) const;

  /// Associated time offset of input containers (ns)
  Gaudi::Property<std::vector<float>> m_hitTimeOffset{
      this,
      "HitTimeOffsets",
      {-50. * Gaudi::Units::ns, -25.0 * Gaudi::Units::ns, 0. * Gaudi::Units::ns, 25. * Gaudi::Units::ns}};

  /// Convert Prev/Prev/VP/MCHits (defaults to false)
  Gaudi::Property<bool> m_SimPrevPrev{this, "SimPrevPrev", false};

  /// Convert Prev/VP/MCHits (defaults to true)
  Gaudi::Property<bool> m_SimPrev{this, "SimPrev", true};

  /// Convert Next/VP/MCHits (defaults to false, only if time alignment is wrong is needed)
  Gaudi::Property<bool> m_SimNext{this, "SimNext", false};

  /// Distance between points on hit trajectory
  Gaudi::Property<double> m_stepSize{this, "StepSize", 5 * Gaudi::Units::micrometer};

  /// Max. number of points on hit trajectory
  Gaudi::Property<unsigned int> m_nMaxSteps{this, "MaxNumSteps", 150};

  /// Number of electrons per micron (MPV)
  Gaudi::Property<double> m_chargeUniform{this, "ChargeUniform", 70.0};

  /// Conversion factor between energy deposit and number of electrons
  Gaudi::Property<double> m_eVPerElectron{this, "eVPerElectron", 3.6};

  /// Lower limit of 1/q^2 distribution
  Gaudi::Property<double> m_minChargeTail{this, "MinChargeTail", 10.0};

  /// Temperature of the sensor [K]
  Gaudi::Property<double> m_temperature{this, "Temperature", 253.15 * Gaudi::Units::kelvin};

  /// Diffusion scale factor, m_diffusionScaleFactor * sqrt(kT*thickness*dz/HV)
  Gaudi::Property<double> m_diffusionScaleFactor{this, "DiffusionScaleFactor", 1.5};

  /// Discrimination threshold in electrons, for time walk
  Gaudi::Property<double> m_threshold{this, "ChargeThreshold", 1000.0};

  /// Flag to simulate radiation damage or not
  Gaudi::Property<bool> m_irradiated{this, "Irradiated", false};

  /// Integrated luminosity in fb-1
  Gaudi::Property<double> m_dataTaken{this, "DataTaken", 0.};

  /// Flag to activate monitoring histograms or not
  Gaudi::Property<bool> m_monitoring{this, "Monitoring", true};

  /// Flag to force luminosity from options (True) or GenHeader (False)
  Gaudi::Property<bool> m_fixLumi{this, "FixLumi", false};

  // From Beam7000GeV-mu100-nu7.6-HorExtAngle.py
  // This is the L per bunch, corresponding to
  //   L = 2 x 10^33 cm-2 s-1 with 2400 colliding bunches
  // It correspond to nu(total) = 7.6 and we assume mu(visible)=0.699*nu
  // Gauss().Luminosity = 0.8338*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
  // Gauss().TotalCrossSection = 102.5*SystemOfUnits.millibarn

  /// Effective luminosity for deadtime simulation
  Gaudi::Property<double> m_lumi{this, "Luminosity", 0.8338e30 / ( Gaudi::Units::cm2 * Gaudi::Units::s )};

  /// Deadtime (in bunch crossings) for the velopix: 400/25 by default
  Gaudi::Property<double> m_deadtime{this, "DeadTime", 400. / 25.};

  /// Conversion factor for distance in mm to time in ns
  static constexpr double s_timeFromDist = Gaudi::Units::ns * ( Gaudi::Units::mm / Gaudi::Units::c_light );

  /// Occupancy v radius fitted parameters by module for p0 (default when condition missing)
  static constexpr std::array<double, 52> s_defaultVals = {
      2.75041e-03, 2.77865e-03, 2.80689e-03, 2.83513e-03, 2.86337e-03, 2.89161e-03, 2.91985e-03, 2.94809e-03,
      3.33328e-03, 3.62045e-03, 3.90762e-03, 4.19479e-03, 4.48196e-03, 4.76913e-03, 5.05630e-03, 5.34347e-03,
      4.71857e-03, 4.56792e-03, 4.41727e-03, 4.26662e-03, 4.11597e-03, 3.96532e-03, 3.81467e-03, 3.66402e-03,
      3.51337e-03, 3.36272e-03, 3.35648e-03, 3.29266e-03, 3.22885e-03, 3.16503e-03, 3.10121e-03, 3.03740e-03,
      2.97358e-03, 2.90976e-03, 2.84595e-03, 2.78213e-03, 2.71832e-03, 2.65450e-03, 2.59068e-03, 2.52687e-03,
      2.46305e-03, 2.39923e-03, 1.77097e-03, 1.75876e-03, 1.74656e-03, 1.73435e-03, 1.72214e-03, 1.70993e-03,
      1.69773e-03, 1.68552e-03, 1.67331e-03, 1.66111e-03};

  /// fraction occupied at radius r = vals[module]*p0 * ( p1*exp(r*p2) + (1-p1)*exp(r*p3) )
  static constexpr std::array<double, 4> s_defaultDeadParam = {1., 0.9048, -0.2616, -0.0505};

  /// Total number of VPDeposits created
  mutable Gaudi::Accumulators::StatCounter<> m_numVPDepositsPP   = {this, "1 VPDeposits from Prev/Prev"};
  mutable Gaudi::Accumulators::StatCounter<> m_numVPDepositsP    = {this, "2 VPDeposits from Prev"};
  mutable Gaudi::Accumulators::StatCounter<> m_numVPDepositsMain = {this, "3 VPDeposits from Main"};
  mutable Gaudi::Accumulators::StatCounter<> m_numVPDepositsN    = {this, "4 VPDeposits from Next"};

  /// Number of VPDigits set to -9999 due to the deadtime simulation
  mutable Gaudi::Accumulators::StatCounter<> m_numVPDeadtimeKilled = {this, "5 Deadtime removed VPDeposits"};

  mutable Rndm::Numbers m_gauss;
  mutable Rndm::Numbers m_uniform;
};

DECLARE_COMPONENT( VPDepositCreator )

VPDepositCreator::VPDepositCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {KeyValue{"InputLocationPrevPrev", "/Event/PrevPrev/" + LHCb::MCHitLocation::VP},
                   KeyValue{"InputLocationPrev", "/Event/Prev/" + LHCb::MCHitLocation::VP},
                   KeyValue{"InputLocationMain", LHCb::MCHitLocation::VP},
                   KeyValue{"InputLocationNext", "/Event/Next/" + LHCb::MCHitLocation::VP},
                   KeyValue{"InputLocationGenHeader", LHCb::GenHeaderLocation::Default},
                   KeyValue{"DeVPLocation", DeVPLocation::Default},
                   KeyValue{"ConditionsCache", "AlgorithmSpecific-" + name + "-ConditionsCache"}},
                  {"OutputLocation", LHCb::MCVPDigitLocation::Default}} {}

StatusCode VPDepositCreator::initialize() {
  return Transformer::initialize()
      .andThen( [&] {
        if ( !m_irradiated ) {
          info() << "VP simulation assumes no radiation damage effects" << endmsg;
        } else {
          info() << "VP simulation assumes " << m_dataTaken << " fb^-1 radiation damage effects" << endmsg;
        }
        if ( m_hitTimeOffset.size() != 4 ) {
          error() << "Must have four HitTimeOffsets";
          return StatusCode::FAILURE;
        } else {
          return StatusCode::SUCCESS;
        }
      } )
      .andThen( [&] { return m_gauss.initialize( randSvc(), Rndm::Gauss( 0.0, 1.0 ) ); } )
      .andThen( [&] { return m_uniform.initialize( randSvc(), Rndm::Flat( 0.0, 1.0 ) ); } )
      .andThen( [&] {
        addConditionDerivation(
#ifdef USE_DD4HEP
            {inputLocation<DeVP>(), "/world/BeforeMagnetRegion/VP:SimCond"},
            inputLocation<VPDepositCreatorConditions::ConditionsCache>(),
            [=, this]( DeVP const& det, YAML::Node const& vpSimCond ) -> VPDepositCreatorConditions::ConditionsCache {
#else
            {inputLocation<DeVP>()}, inputLocation<VPDepositCreatorConditions::ConditionsCache>(),
            [=, this]( DeVP const& det ) -> VPDepositCreatorConditions::ConditionsCache {
#endif
              // Calculate the diffusion coefficient.
              const double           kt = 2. * m_temperature * Gaudi::Units::k_Boltzmann / Gaudi::Units::eV;
              std::array<double, 52> tmpDiffusionCoefficient{};
              det.runOnAllSensors( [&]( const DeVPSensor& sensor ) {
                tmpDiffusionCoefficient[sensor.module()] =
                    m_diffusionScaleFactor * sqrt( kt * sensor.siliconThickness() / ( sensor.sensorHV() ) );
              } );
#ifdef USE_DD4HEP
              return {tmpDiffusionCoefficient,
                      vpSimCond["VPDeadTimeParam"].as<std::array<std::array<double, 4>, 52>>()};
#else
              std::array<std::array<double, 4>, 52> tmpDeadTimeParam{};
              det.runOnAllSensors( [&]( const DeVPSensor& sensor ) {
                auto        module = sensor.module();
                std::string conditionName =
                    format( "Conditions/Calibration/VP/VPDeadTimeParam/Module%02i-DeadTime", module );
                auto cond = get<Condition>( detSvc(), conditionName );
                std::copy_n( cond->param<std::vector<double>>( "FunctionParam" ).begin(), 4,
                             tmpDeadTimeParam[module].begin() );
              } );
              return {tmpDiffusionCoefficient, tmpDeadTimeParam};
#endif
            } );
        setHistoTopDir( "VP/" );
      } );
}

LHCb::MCVPDigits VPDepositCreator::operator()( const LHCb::MCHits& mcPrevPrev, const LHCb::MCHits& mcPrev,
                                               const LHCb::MCHits& mcMain, const LHCb::MCHits& mcNext,
                                               const LHCb::GenHeader& genHeader, const DeVP& det,
                                               const VPDepositCreatorConditions::ConditionsCache& conditions ) const {
  // Create a container for the MC digits and transfer ownership to the TES.
  LHCb::MCVPDigits digits;

  // loop over PrevPrev, Prev and main to make MCVPDigits
  // If required use PrevPrev (default not to do this)
  if ( m_SimPrevPrev ) {
    for ( const auto& hit : mcPrevPrev ) createDeposits( *hit, digits, det, m_hitTimeOffset[0], conditions );
  }
  m_numVPDepositsPP += digits.size();
  unsigned int lastSize = digits.size(); // previous size of digits

  // Now Prev (default to process this container)
  if ( m_SimPrev ) {
    for ( const auto& hit : mcPrev ) createDeposits( *hit, digits, det, m_hitTimeOffset[1], conditions );
  }
  m_numVPDepositsP += digits.size() - lastSize;
  lastSize = digits.size(); // previous size of digits

  // Always do main
  for ( const auto& hit : mcMain ) createDeposits( *hit, digits, det, m_hitTimeOffset[2], conditions );
  m_numVPDepositsMain += digits.size() - lastSize;
  lastSize = digits.size(); // previous size of digits

  // Now Prev (default to process this container)
  if ( m_SimNext ) {
    for ( const auto& hit : mcNext ) createDeposits( *hit, digits, det, m_hitTimeOffset[3], conditions );
  }
  m_numVPDepositsN += digits.size() - lastSize;
  lastSize = digits.size(); // previous size of digits

  // for dead pixel purposes we need the simulated instananious luminosity
  auto eventLumi = m_lumi;
  if ( !m_fixLumi ) {
    eventLumi = genHeader.luminosity();

    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "Using event by event lumi " << eventLumi << " for dead time pixel removal fractions" << endmsg;
  }

  // tuning MC was Lumi = 0.8338e30, so scale to that
  double deadTimeScaled = m_deadtime * ( eventLumi / ( 0.8338e30 / ( Gaudi::Units::cm2 * Gaudi::Units::s ) ) );

  // remove digits randomly due to luminosity induced deadtime
  deadTimeSim( digits, det, deadTimeScaled, conditions );

  // Sort the MC digits by channel ID.
  std::sort( digits.begin(), digits.end(), VPDataFunctor::Less_by_Channel<const LHCb::MCVPDigit*>() );
  return digits;
}

//===========================================================================
// Create deposits
//============================================================================
void VPDepositCreator::createDeposits( const LHCb::MCHit& hit, LHCb::MCVPDigits& digits, const DeVP& det,
                                       const double                                       timeOff,
                                       const VPDepositCreatorConditions::ConditionsCache& conditions ) const {

  // Calculate the total number of electrons based on the G4 energy deposit.
  const double charge = ( hit.energy() / Gaudi::Units::eV ) / m_eVPerElectron;
  // Skip very small deposits.
  if ( charge < 100. ) return;
  const double path = hit.pathLength();
  // Skip very small path lengths.
  if ( path < 1.e-6 ) {
    warning() << "Path length in active silicon: " << path << endmsg;
    return;
  }
  // Calculate the number of points to distribute the deposited charge on.
  unsigned int nPoints = int( ceil( path / m_stepSize ) );
  if ( nPoints > m_nMaxSteps ) nPoints = m_nMaxSteps;
  // Calculate the charge on each point.
  std::vector<double> charges( nPoints, 0. );
  double              sum   = 0.;
  const double        mpv   = m_chargeUniform * ( path / Gaudi::Units::micrometer ) / nPoints;
  const double        sigma = sqrt( fabs( mpv ) );
  for ( unsigned int i = 0; i < nPoints; ++i ) {
    const double q = mpv + m_gauss() * sigma;
    if ( q > 0. ) {
      charges[i] = q;
      sum += q;
    }
  }
  while ( charge > sum + m_minChargeTail ) {
    // Add additional charge sampled from an 1 / n^2 distribution.
    const double       q = randomTail( m_minChargeTail, charge - sum );
    const unsigned int i = int( LHCb::Math::round( m_uniform() * ( nPoints - 1 ) ) );
    charges[i] += q;
    sum += q;
  }

  // Get the sensor that was hit and its thickness.
  auto sensDetId = static_cast<unsigned int>( hit.sensDetID() );
#ifdef USE_DD4HEP
  auto sensorId = ( sensDetId < VP::NSensors ) ? LHCb::Detector::VPChannelID::SensorID( sensDetId )
                                               : LHCb::Detector::VPVolumeID( sensDetId ).sensor();
#else
  auto sensorId = LHCb::Detector::VPChannelID::SensorID( sensDetId );
#endif
  const DeVPSensor& sensor    = det.sensor( sensorId );
  const double      thickness = sensor.siliconThickness();

  // for fluence calc. assume radiation was delivered in the module not global frame
  // as the modules should have been centered on the beam each fill
  // Tree is LHCb->VP->Half->ModuleWithSupport->Module->Ladder->Sensor, so up two levels

  const auto& module = det.module( sensorId );
#ifdef USE_DD4HEP
  const auto& localPos = module.toLocal( hit.midPoint() );
#else
  const auto& localPos = module->toLocal( hit.midPoint() );
#endif
  double activeDepth = thickness;
  if ( m_irradiated.value() ) {
    // radius from local and z from global
    const double f = RadDamage::fluence( localPos.rho(), hit.midPoint().z(), m_dataTaken );
    activeDepth *= RadDamage::chargeCollectionEfficiency( f, sensor.sensorHV() );

    if ( m_monitoring ) {
      profile1D( localPos.rho(), f, "FluenceAtRadius", "Fluence At local Radius [mm] per MCHit", 0, 55.0, 110 );
      profile2D( localPos.rho(), to_unsigned( sensorId ), f, "FluenceAtRadiusvSensor",
                 "Fluence At local radius [mm] (global z) per MCHit per sensor", 0., 55., -0.5, VP::NSensors - 0.5, 110,
                 VP::NSensors );
      profile1D( localPos.rho(), activeDepth * 1000, "activeDepthAtRadius",
                 "ActiveDepth (#mu m) At local radius [mm] per MCHit", 0, 55.0, 110 );
      profile2D( localPos.rho(), to_unsigned( sensorId ), activeDepth * 1000, "activeDepthAtRadiusvSensor",
                 "Average ActiveDepth (#mu m) At local radius [mm] per MCHit per sensor", 0., 55., -0.5,
                 VP::NSensors - 0.5, 110, VP::NSensors );
    }
  }
  // Calculate the distance between two points on the trajectory.
  Gaudi::XYZPoint  entry = sensor.globalToLocal( hit.entry() );
  Gaudi::XYZPoint  exit  = sensor.globalToLocal( hit.exit() );
  Gaudi::XYZVector step  = ( exit - entry ) / static_cast<double>( nPoints );
  // Coordinates of the first point on the trajectory.
  Gaudi::XYZPoint point = entry + 0.5 * step;
  // Calculate scaling factor to match total deposited charge.
  const double adjust = charge / sum;
  // Accumulate deposits on pixels.
  std::map<LHCb::Detector::VPChannelID, float> pixels;
  for ( unsigned int i = 0; i < nPoints; ++i ) {
    charges[i] *= adjust;
    if ( m_monitoring ) { plot( charges[i], "ChargePerPoint", "Number of electrons per point", 0., 2000., 100 ); }
    // Calculate the distance to the pixel side of the sensor.
    // In local coordinates, the pixel side is at z = -thickness/2.
    const double dz = std::abs( point.z() + 0.5 * thickness );
    if ( m_irradiated ) {
      // Skip points outside the active depth.
      if ( dz > activeDepth ) {
        point += step;
        continue;
      }
    }
    const double       sigmaD = conditions.diffusionCoefficient[sensor.module()] * sqrt( dz );
    const unsigned int nSplit = 5;
    const double       q      = charges[i] / nSplit;
    for ( unsigned int j = 0; j < nSplit; ++j ) {
      const double                dx       = sigmaD * m_gauss();
      const double                dy       = sigmaD * m_gauss();
      Gaudi::XYZPoint             endpoint = point + Gaudi::XYZVector( dx, dy, 0. );
      LHCb::Detector::VPChannelID channel;
      bool                        valid = sensor.pointToChannel( endpoint, true, channel );
      if ( valid && !sensor.OKPixel( channel ) ) {
        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << "Killed MCVPdigit in channel " << channel << " due to bad pixel" << endmsg;
        if ( m_monitoring ) {
          plot2D( endpoint.x(), endpoint.y(), "BadChannelAreas", "Bad sensor areas (all sensors) [mm]", -5.0, 45.0,
                  -5.0, 20.0, 500, 250 );
        }
        valid = false;
      }
      if ( valid ) {
        if ( auto [it, ok] = pixels.try_emplace( channel, q ); !ok ) ( *it ).second += q;
      }
      if ( m_monitoring ) {
        if ( valid ) {
          plot2D( endpoint.x(), endpoint.y(), "ActiveSensorArea", "Active sensor area [mm]", -5.0, 45.0, -5.0, 20.0,
                  500, 250 );
        } else {
          plot2D( endpoint.x(), endpoint.y(), "DeadSensorArea", "Dead sensor area [mm]", -5.0, 45.0, -5.0, 20.0, 500,
                  250 );
        }
      }
    }
    point += step;
  }
  for ( const auto [id, charge] : pixels ) {
    LHCb::MCVPDigit* digit = digits.object( id );
    // time assuming detector is timed in to delta(t) = z/c and no timewalk
    float time =
        timeOff + static_cast<float>( hit.time() - std::abs( hit.entry().z() / Gaudi::Units::mm ) * s_timeFromDist );

    // add time offset due to timewalk from Larissa's talk 25/5/18
    // using testpulses the function is
    // delta(T) = p0/(Q-p1)**p2 + p3
    // p1 approx threshold level and p3 corrected for in time alignment
    // p0 = 5*25 in ns (plot in BX)
    // p2 = 0.5 mostly (see page 22, some pixels at 0.8) ignore for now
    // scale to e- i.e scale by 1/72 (1000e- -> 13.5 in testpulse a.u.)
    // Then subtract 2ns flat due to pixel to pixel variation
    float timeWalk = 999.; // default to not appearing in next two BX if too small
    if ( charge > m_threshold ) { timeWalk = ( 5. * 25. ) / sqrt( ( charge - m_threshold ) / 72. ) - 2. * m_uniform(); }
    time += timeWalk; // apply time walk

    if ( digit ) {
      // MC digit already exists.
      if ( fabs( timeOff ) < 1. ) {
        // main bunch (even if out of time)
        digit->addToMcHits( &hit, charge, time );
      } else {
        // spillover does not set mchit
        digit->addToMcHits( static_cast<const LHCb::MCHit*>( nullptr ), charge, time );
      }
    } else {
      digit = new LHCb::MCVPDigit();
      if ( fabs( timeOff ) < 1. ) {
        // main bunch (even if out of time)
        digit->addToMcHits( &hit, charge, time );
      } else {
        // spillover does not set mchit
        digit->addToMcHits( static_cast<const LHCb::MCHit*>( nullptr ), charge, time );
      }
      digits.insert( digit, id );
    }
    if ( m_monitoring ) {
      plot( charge / 1000, "ChargeInPixel", "Charge (ke) deposited in each pixel", 0., 100., 100 );
      profile1D( localPos.rho(), charge / 1000, "ChargeInPixelvRadius",
                 "Average charge (ke) deposited in each pixel verse local radius [mm]", 0., 55., 110 );
      profile2D( localPos.rho(), to_unsigned( sensorId ), charge / 1000, "ChargeInPixelvRadiusvSensor",
                 "Average charge (ke) deposited in each pixel verse local radius [mm] per sensor", 0., 55., -0.5,
                 VP::NSensors - 0.5, 110, VP::NSensors );
      if ( fabs( timeOff ) < 1. ) {
        plot( time, "TimeAtSensorMain", "Time of MCVPDigit at sensor, flight time corrected (ns), main bunch", -50.,
              90., 140 );
      } else {
        plot( time, "TimeAtSensorOther", "Time of MCVPDigit at sensor, flight time corrected (ns), other bunchs", -50.,
              90., 140 );
      }
    }
  }
}

//=============================================================================
// Sample charge from 1 / n^2 distribution.
//=============================================================================
double VPDepositCreator::randomTail( const double qmin, const double qmax ) const {
  const double offset = 1. / qmax;
  const double range  = ( 1. / qmin ) - offset;
  const double u      = offset + m_uniform() * range;
  return 1. / u;
}

//=============================================================================
// Use the luminosity and a parameterised hit profile to simulate deadtime
// hiding pixels
//=============================================================================
void VPDepositCreator::deadTimeSim( LHCb::MCVPDigits& digits, const DeVP& det, const double deadTimeScaled,
                                    const VPDepositCreatorConditions::ConditionsCache& conditions ) const {

  unsigned int numKilled = 0;
  // loop over all digits, push time to -9999ns (i.e. out of scope) randomly to
  // simulate deadtime
  for ( auto& iDigit : digits ) {
    const LHCb::Detector::VPChannelID& channel = iDigit->key();
    // need the radius of the hit
    const DeVPSensor& sensor = det.sensor( channel.sensor() );
    const double      radius = ( sensor.channelToPoint( channel, false ) ).Rho();
    const auto        module = sensor.module();
    // Use deadtime to determine probability to kill clusters by deadtime verse module and radius
    const double killScaleFrac =
        deadTimeScaled * conditions.deadTimeParam[module][0] *
        ( ( ( conditions.deadTimeParam[module][1] * exp( conditions.deadTimeParam[module][2] * radius ) ) +
            ( ( 1 - conditions.deadTimeParam[module][1] ) *
              ( exp( conditions.deadTimeParam[module][3] * radius ) ) ) ) );

    if ( m_uniform() < killScaleFrac ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Deadtime removal of VP channel " << channel << endmsg;
      auto depTimeCopy = iDigit->depositAndTimes();
      for ( auto& dT : depTimeCopy ) {
        dT.second = -9999.; // out of time window
      }
      iDigit->setDepositAndTimes( depTimeCopy );
      ++numKilled;
      if ( m_monitoring ) {
        plot2D( sensor.module(), radius, "NumKilledPixAtRadiusvModule",
                "Count of killed pixels at radius (mm) verse module number", -0.5, 51.5, 0., 50., 52, 100 );
      }
    }
  }
  m_numVPDeadtimeKilled += numKilled;
  return;
}
