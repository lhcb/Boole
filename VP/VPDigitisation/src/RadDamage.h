/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <GaudiKernel/SystemOfUnits.h>
#include <algorithm>
#include <array>
#include <cmath>

namespace RadDamage {
  /// Fluence as function of position r,z (in mm) and integrated luminosity (in fb-1).
  double fluence( const double r, const double z, const double lint ) {
    // Set fit values for fluence.
    // See https://indico.cern.ch/event/245878/contributions/541328/attachments/421815/585699/22April2013.pdf
    // for the data these parameters are fit to
    static constexpr std::array<double, 7> m_a{3.62911,     0.000271931, -3.30213e-06, -3.42959e-09,
                                               6.35852e-12, 1.93743e-15, 1.62919e-18};
    static constexpr std::array<double, 7> m_k{2.29997,     -0.000121513, -5.11296e-06, 6.21903e-09,
                                               2.79419e-11, -6.86717e-14, 4.18305e-17};

    // calculations of fluence done in cm units for radius and mm for z.
    auto           radius  = r / Gaudi::Units::cm;
    double         a       = 0.;
    double         k       = 0.;
    constexpr auto nTermsA = m_a.size();
    constexpr auto nTermsK = m_k.size();
    constexpr auto nTerms  = std::max( nTermsA, nTermsK );
    double         u       = 1.;
    for ( std::size_t j = 0; j < nTerms; ++j ) {
      if ( j < nTermsA ) a += 1.e13 * m_a[j] * u;
      if ( j < nTermsK ) k += m_k[j] * u;
      u *= z;
    }
    return a * lint * std::pow( radius, -k );
  }
  /// Charge collection efficiency as function of 1 MeV n eq. fluence and bias.
  double chargeCollectionEfficiency( const double fluence, const double voltage ) {
    using std::pow;
    // this returns the fractional depletion depth

    // unirradiated sensor
    static constexpr double s_depth = 200.; // default depth in microns
    // assumes 105V to deplete 200microns
    const double depth = 20 * pow( voltage, 0.5 );
    // fraction of default depth
    double unirradFrac = 1.;
    // unirradiated depth fraction
    if ( depth < s_depth ) unirradFrac = depth / s_depth;

    if ( fluence < 1.e13 ) return unirradFrac; // below 1e13 no measureable effect

    // high fluence case
    // Collected charge in ke- before irradiation (assuming 200 um thickness).
    static constexpr double s_mpvNonIrradiated = 15.;
    const double            a                  = 2.2e4 * pow( fluence, -0.306 );
    const double            b                  = -0.1363 * pow( fluence, 0.2276 ) + 191.6;
    if ( ( voltage + b ) < 0. ) return 0.; // undepleted sensor (protect sqrt below)
    const double collectedCharge = a * pow( voltage + b, 0.5 );
    if ( collectedCharge > s_mpvNonIrradiated ) return unirradFrac; // over depleted sensor
    return collectedCharge / s_mpvNonIrradiated;                    // partially depleted sensor}
  }
} // namespace RadDamage
