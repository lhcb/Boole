/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/VP/VPChannelID.h"
#include "Event/MCHit.h"
#include "Event/MCVPDigit.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/VPConstants.h"
#include "LHCbAlgs/Consumer.h"

using namespace Gaudi::Units;

/** @class VPDepositMonitor VPDepositMonitor.h
 *
 *
 */

class VPDepositMonitor : public LHCb::Algorithm::Consumer<void( const LHCb::MCHits&, const LHCb::MCVPDigits& ),
                                                          Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {

public:
  /// Standard constructor
  VPDepositMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  void       operator()( const LHCb::MCHits& hits, const LHCb::MCVPDigits& mcdigits ) const override;

private:
  void monitorHits( const LHCb::MCHits& ) const;
  void monitorDigits( const LHCb::MCVPDigits& ) const;
};

DECLARE_COMPONENT( VPDepositMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPDepositMonitor::VPDepositMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {KeyValue{"MCHitsLocation", LHCb::MCHitLocation::VP},
                KeyValue{"MCVPDigitLocation", LHCb::MCVPDigitLocation::Default}}} {}

//=============================================================================
/// Initialization
//=============================================================================
StatusCode VPDepositMonitor::initialize() {
  return GaudiTupleAlg::initialize().andThen( [&] { setHistoTopDir( "VP/" ); } );
}

//=============================================================================
// Main execution
//=============================================================================
void VPDepositMonitor::operator()( const LHCb::MCHits& hits, const LHCb::MCVPDigits& mcdigits ) const {
  monitorHits( hits );
  monitorDigits( mcdigits );
}

//=============================================================================
// Fill histograms for MC hits
//=============================================================================
void VPDepositMonitor::monitorHits( const LHCb::MCHits& hits ) const {
  for ( const LHCb::MCHit* hit : hits ) {
    plot( hit->pathLength() / Gaudi::Units::micrometer, "PathInSensor", "Path in sensor [um]", 0.0, 300.0, 60 );
    plot( hit->energy() / Gaudi::Units::keV, "EnergyInSensor", "Energy deposited in sensor [keV]", 0.0, 200.0, 40 );
  }
}

//=============================================================================
// Fill histograms for MC digits
//=============================================================================
void VPDepositMonitor::monitorDigits( const LHCb::MCVPDigits& mcdigits ) const {
  plot( mcdigits.size(), "nDigits", "Number of MCVPDigits / event", 0., 10000., 50 );
  std::vector<unsigned int> numDigitPerSensor( VP::NSensors, 0 );
  for ( const LHCb::MCVPDigit* mcdigit : mcdigits ) {
    LHCb::Detector::VPChannelID channel = mcdigit->channelID();
    plot( channel.module(), "DigitsPerModule", "Number of digits / module", -0.5, 51.5, 52 );
    ++numDigitPerSensor[(unsigned int)channel.sensor()];
    // Loop over the deposits.
    double charge = 0.;
    for ( const auto& [ch, _] : mcdigit->depositAndTimes() ) {
      charge += ch;
      plot( ch, "ChargePerDeposit", "Charge/deposit [e]", 0., 20000., 100 );
    }
    plot( charge, "ChargePerPixel", "Charge/pixel [e]", 0., 25000.0, 100 );
  }
  for ( unsigned int numSens = 0; numSens < VP::NSensors; ++numSens ) {
    profile1D( (double)numSens, (double)numDigitPerSensor[numSens], "DigitsPerSensor", "Number of pixels per sensor",
               -0.5, VP::NSensors - 0.5, VP::NSensors );
  }
}
