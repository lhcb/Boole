/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCVPDigit.h"
#include "Event/VPDigit.h"
#include "Kernel/VPConstants.h"
#include "Kernel/VPDataFunctor.h"
#include "LHCbAlgs/Transformer.h"

#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/RndmGenerators.h"

#include <VPDet/DeVP.h>
#include <map>
#include <numeric>

#include <yaml-cpp/yaml.h>

using namespace LHCb;

namespace {
  const unsigned int HITEFF_BINNING_SIZE = 16;

  const size_t nEfficienciesPerSensor = VP::NChipsPerSensor * VP::NRows * VP::NColumns /
                                        ( HITEFF_BINNING_SIZE * HITEFF_BINNING_SIZE ); // (3*256*256)/(16*16)

  using Thresholds            = std::array<double, VP::NSensors>;
  using SensorHitEfficiencies = std::array<float, nEfficienciesPerSensor>;
  using HitEfficiencies       = std::array<SensorHitEfficiencies, VP::NSensors>;

  float getHitEfficiency( const HitEfficiencies& all_efficiencies, LHCb::Detector::VPChannelID channel ) {
    const auto x_bin        = channel.scol() / HITEFF_BINNING_SIZE;
    const auto y_bin        = to_unsigned( channel.row() ) / HITEFF_BINNING_SIZE;
    const auto sensorNumber = to_unsigned( channel.sensor() );

    assert( sensorNumber < all_efficiencies.size() );

    unsigned int sequential_bin_id = y_bin * VP::NChipsPerSensor * VP::NColumns / HITEFF_BINNING_SIZE + x_bin;

    assert( sequential_bin_id < nEfficienciesPerSensor );

    return all_efficiencies[sensorNumber][sequential_bin_id];
  }
} // namespace

namespace VPDigitCreatorConditions {
  const std::string PATH_HIT_EFFICIENCIES_DETDESC = "Conditions/Calibration/VP/VPHitEfficiencies/HitEfficiencies";
  const std::string PATH_THRESHOLDS_DETDESC       = "Conditions/Calibration/VP/VPDigitisationParam/Thresholds";
  const std::string PATH_SIMCOND_DD4HEP           = "/world/BeforeMagnetRegion/VP:SimCond";

  struct ConditionsCache {
    ConditionsCache( Thresholds thresholds, HitEfficiencies hitEfficiencies )
        : thresholds( std::move( thresholds ) ), hitEfficiencies( std::move( hitEfficiencies ) ) {}

    Thresholds      thresholds;
    HitEfficiencies hitEfficiencies;
  };
} // namespace VPDigitCreatorConditions

/**
 *  Using MCVPDigits as input, this algorithm simulates the response of the
 *  VeloPix ASIC and produces a VPDigit for each MCVPDigit above threshold.
 *
 *  @author Thomas Britton
 *  @date   2010-07-07
 */
class VPDigitCreator
    : public LHCb::Algorithm::Transformer<
          LHCb::VPDigits( LHCb::MCVPDigits const&, const DeVP&, VPDigitCreatorConditions::ConditionsCache const& ),
          LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeVP, VPDigitCreatorConditions::ConditionsCache>> {

public:
  /// Standard constructor
  VPDigitCreator( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode     initialize() override; ///< Algorithm initialization
  LHCb::VPDigits operator()( LHCb::MCVPDigits const&, const DeVP&,
                             VPDigitCreatorConditions::ConditionsCache const& ) const override;

private:
  /// Noise in number of electrons
  Gaudi::Property<double> m_electronicNoise{this, "ElectronicNoise", 130.0};

  /// Window in which pixel rise time is registered
  Gaudi::Property<double> m_timingWindow{this, "TimingWindow", 25.};

  /// Offset from z/c arrival that timing window starts
  Gaudi::Property<double> m_timingOffset{this, "TimingOffset", 0.};

  /// Option to simulate masked pixels
  Gaudi::Property<bool> m_maskedPixels{this, "MaskedPixels", true};

  /// Option to simulate hit efficiencies from data
  Gaudi::Property<bool> m_applyHitEfficiency{this, "ApplyHitEfficiency", true};

  /// Option to simulate noisy pixels
  Gaudi::Property<bool> m_noisyPixels{this, "NoisyPixels", false};

  /// Add crosstalk in cardial directions to pixels
  Gaudi::Property<double> m_crosstalk{this, "CrossTalk", 0.05};

  /// Fraction of masked pixels (100 per tile estimate = 0.05%)
  Gaudi::Property<double> m_fractionMasked{this, "FractionMasked",
                                           100. / ( VP::NChipsPerSensor * VP::NColumns * VP::NRows )};

  /// Fraction of noisy pixels (100e, with a 1000 threshold is <3e-16 noise pixels per event)
  Gaudi::Property<double> m_fractionNoisy{this, "FractionNoisy", 0.};

  /// Flag to activate monitoring histograms or not
  Gaudi::Property<bool> m_monitoring{this, "Monitoring", true};

  /// Number of signal VPDigits created
  mutable Gaudi::Accumulators::StatCounter<> m_numSignalDigitsCreated = {this, "1 Signal VPDigits"};

  /// Number of noise VPDigits created
  mutable Gaudi::Accumulators::StatCounter<> m_numNoiseDigitsCreated = {this, "2 Noise VPDigits"};

  /// Number of VPDigits missed due to masked pixels
  mutable Gaudi::Accumulators::StatCounter<> m_numDigitsKilledMasked = {this, "3 Lost Random bad pixels"};

  /// Number of VPDigits created due to spillover
  mutable Gaudi::Accumulators::StatCounter<> m_numSpillover = {this, "4 Spillover VPDigits"};

  /// Number of VPDigits lost due to timing window
  mutable Gaudi::Accumulators::StatCounter<> m_numLostTiming = {this, "5 Out of time VPDigits"};

  /// Number of VPDigits created by pixel to pixel crosstalk
  mutable Gaudi::Accumulators::StatCounter<> m_numCrosstalk = {this, "6 Crosstalk VPDigits"};

  /// Number of VPDigits lost due to hit efficiency
  mutable Gaudi::Accumulators::Counter<> m_numLostHitEfficiency = {this, "7 Removed per hit efficiency condition"};

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_hitEff_SensorNotFound{
      this, "Failed to find sensor in hit efficiency conditions"};

  /// Random number generators
  mutable Rndm::Numbers m_gauss;
  mutable Rndm::Numbers m_uniform;
  mutable Rndm::Numbers m_poisson;
};

DECLARE_COMPONENT( VPDigitCreator )

VPDigitCreator::VPDigitCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {{"InputLocation", LHCb::MCVPDigitLocation::Default},
                   KeyValue{"DeVPLocation", DeVPLocation::Default},
                   KeyValue{"ConditionsCache", "AlgorithmSpecific-" + name + "-ConditionsCache"}},
                  {"OutputLocation", LHCb::VPDigitLocation::Default}} {}

StatusCode VPDigitCreator::initialize() {
  return Transformer::initialize()
      .andThen( [&] { return m_gauss.initialize( randSvc(), Rndm::Gauss( 0., 1. ) ); } )
      .andThen( [&] { return m_uniform.initialize( randSvc(), Rndm::Flat( 0., 1. ) ); } )
      .andThen( [&] {
        if ( !m_noisyPixels ) return StatusCode( StatusCode::SUCCESS ); // nothing else to do
        double averageNoisy = m_fractionNoisy * VP::NSensors * VP::NPixelsPerSensor;
        if ( averageNoisy < 1 ) {
          m_noisyPixels = false;
          return StatusCode( StatusCode::SUCCESS );
        }
        return m_poisson.initialize( randSvc(), Rndm::Poisson( averageNoisy ) );
      } )
#ifdef USE_DD4HEP
      .andThen( [&] {
        addConditionDerivation(
            {inputLocation<DeVP>(), VPDigitCreatorConditions::PATH_SIMCOND_DD4HEP},
            inputLocation<VPDigitCreatorConditions::ConditionsCache>(),
            [&]( DeVP const& det, YAML::Node const& simCond ) -> VPDigitCreatorConditions::ConditionsCache {
              HitEfficiencies hitEfficiencies{};

              if ( simCond.IsDefined() && simCond["HitEfficiencies"].IsDefined() ) {
                det.runOnAllSensors( [&]( const DeVPSensor& sensor ) {
                  // One list of efficiencies per sensor in sequential_bin_id order, see above
                  // Condition name is Sensor_MM_S where MM is the module number
                  // and S sensor number in the module (derived by sensorNumber modulo 4 below)
                  // All located in the HitEfficiencies section of SimCond
                  // in the Conditions/VP/Simulation.yml file
                  auto sensorCondName =
                      fmt::format( "Sensor_{:02d}_{:1d}", sensor.module(), to_unsigned( sensor.sensorNumber() ) % 4 );
                  auto sensorEffList = simCond["HitEfficiencies"][sensorCondName];
                  if ( !sensorEffList.IsDefined() ) {
                    error() << "SimCond[HitEfficiencies] is defined in Conditions/VP/Simulation.yml but "
                            << sensorCondName << " is missing, setting eff = 1";
                    hitEfficiencies[to_unsigned( sensor.sensorNumber() )].fill( 1.0 );
                  }
                  try {
                    hitEfficiencies[to_unsigned( sensor.sensorNumber() )] = sensorEffList.as<SensorHitEfficiencies>();
                  } catch ( const std::runtime_error& e ) {
                    error() << "In Conditions/VP/Simulation.yml SimCond[HitEfficiencies][" << sensorCondName
                            << "] could not be converted to 768 floats, setting eff = 1";
                    hitEfficiencies[to_unsigned( sensor.sensorNumber() )].fill( 1.0 );
                  }
                } );
              } else {
                Warning( "No sensor hit efficiencies in Conditions/VP/Simulation.yml", StatusCode::SUCCESS, 1. )
                    .ignore();
                det.runOnAllSensors( [&]( const DeVPSensor& sensor ) {
                  // just fill with 1.0, as the efficiencies are not yet in dd4hep
                  hitEfficiencies[to_unsigned( sensor.sensorNumber() )].fill( 1.0 );
                } );
              }

              Thresholds thresholds;

              if ( simCond.IsDefined() && simCond["Thresholds"].IsDefined() ) {
                thresholds = simCond["Thresholds"].as<Thresholds>();
              } else {
                thresholds.fill( 1000. );
              }

              return {thresholds, hitEfficiencies};
            } );
        return StatusCode( StatusCode::SUCCESS );
      } )
#else
      .andThen( [&] {
        if ( existDet<Condition>( VPDigitCreatorConditions::PATH_HIT_EFFICIENCIES_DETDESC ) ) {
          addConditionDerivation( {inputLocation<DeVP>(), VPDigitCreatorConditions::PATH_THRESHOLDS_DETDESC,
                                   VPDigitCreatorConditions::PATH_HIT_EFFICIENCIES_DETDESC},
                                  inputLocation<VPDigitCreatorConditions::ConditionsCache>(),
                                  [&]( DeVP const& det, YAML::Node const& thresholdCond,
                                       YAML::Node const& efficiencyCond ) -> VPDigitCreatorConditions::ConditionsCache {
                                    HitEfficiencies hitEfficiencies{};

                                    det.runOnAllSensors( [&]( const DeVPSensor& sensor ) {
                                      const auto  sensorNumber  = to_unsigned( sensor.sensorNumber() );
                                      std::string conditionName = format( "VP-Sensor%03i-HitEfficiency", sensorNumber );

                                      if ( efficiencyCond[conditionName].IsDefined() ) {
                                        hitEfficiencies[sensorNumber] =
                                            efficiencyCond[conditionName].as<SensorHitEfficiencies>();
                                      } else {
                                        ++m_hitEff_SensorNotFound;
                                        hitEfficiencies[sensorNumber].fill( 1.0 );
                                      }
                                    } );

                                    Thresholds thresholds;

                                    if ( thresholdCond.IsDefined() )
                                      thresholds = thresholdCond["ThresholdPerSensor"].as<Thresholds>();
                                    else {
                                      thresholds.fill( 1000. );
                                    }

                                    return {thresholds, hitEfficiencies};
                                  } );
        } else {
          addConditionDerivation(
              {inputLocation<DeVP>(), VPDigitCreatorConditions::PATH_THRESHOLDS_DETDESC},
              inputLocation<VPDigitCreatorConditions::ConditionsCache>(),
              [&]( DeVP const& det, YAML::Node const& thresholdCond ) -> VPDigitCreatorConditions::ConditionsCache {
                HitEfficiencies hitEfficiencies{};

                det.runOnAllSensors( [&]( const DeVPSensor& sensor ) {
                  const auto sensorNumber = to_unsigned( sensor.sensorNumber() );
                  hitEfficiencies[sensorNumber].fill( 1.0 );
                } );

                Thresholds thresholds;

                if ( thresholdCond.IsDefined() )
                  thresholds = thresholdCond["ThresholdPerSensor"].as<Thresholds>();
                else {
                  thresholds.fill( 1000. );
                }

                return {thresholds, hitEfficiencies};
              } );
        }
        return StatusCode( StatusCode::SUCCESS );
      } )
#endif
      .andThen( [&] { setHistoTopDir( "VP/" ); } );
}

LHCb::VPDigits VPDigitCreator::operator()( LHCb::MCVPDigits const&                          mcdigits, const DeVP&,
                                           VPDigitCreatorConditions::ConditionsCache const& conditions ) const {
  // Create a container for the digits and transfer ownership to the TES.
  const auto& threshold = conditions.thresholds;

  LHCb::VPDigits digits;
  unsigned int   numSpillover           = 0;
  unsigned int   numLostTiming          = 0;
  unsigned int   numDigitsKilledMasked  = 0;
  unsigned int   numSignalDigitsCreated = 0;
  unsigned int   numNoiseDigitsCreated  = 0;
  unsigned int   numCrosstalk           = 0;
  auto           numLostHitEfficiency   = m_numLostHitEfficiency.buffer();

  // Loop over the MC digits.
  for ( const LHCb::MCVPDigit* mcdigit : mcdigits ) {
    // Sum up the collected charge from all deposits, in the time windows
    double     chargeMain     = 0.;
    const auto chargeAndTimes = mcdigit->depositAndTimes();
    for ( unsigned int iDep = 0; iDep < chargeAndTimes.size(); ++iDep ) {
      auto depTime = chargeAndTimes[iDep].second + m_timingOffset;
      if ( depTime > 0. && depTime < m_timingWindow ) {
        // is this in the main bunch
        chargeMain += chargeAndTimes[iDep].first;
      } else {
        // if has MCHit but out of time window
        if ( mcdigit->mcHits()[iDep].data() ) ++numLostTiming;
      }
    }
    const LHCb::Detector::VPChannelID channel = mcdigit->channelID();

    if ( m_applyHitEfficiency.value() ) {
      // get the hit efficiency value
      const auto hit_efficiency_channel = getHitEfficiency( conditions.hitEfficiencies, channel );

      // Use it as an 'inefficiency' (only remove if above)
      if ( m_uniform() > hit_efficiency_channel ) {
        ++numLostHitEfficiency;
        continue;
      }
    }

    if ( m_monitoring ) {
      plot1D( chargeMain, "MainChargeBefore", "Pixel Charge in main BX, e- before threshold", 0, 50000, 100 );
      plot2D( to_unsigned( channel.sensor() ), chargeMain, "Sensor verse Pixel Charge before threshold (e-)", -0.5,
              VP::NSensors - 0.5, 0, 50000, VP::NSensors, 100 );
    }
    // Check if the collected charge exceeds the threshold.
    if ( chargeMain < threshold[to_unsigned( channel.sensor() )] + m_electronicNoise * m_gauss() ) continue;

    if ( m_monitoring ) {
      plot1D( chargeMain, "MainChargeAfter", "Pixel harge in main BX, e- after threshold", 0, 50000, 100 );
      plot2D( to_unsigned( channel.sensor() ), chargeMain, "Sensor verse Pixel Charge after threshold (e-)", -0.5,
              VP::NSensors - 0.5, 0, 50000, VP::NSensors, 100 );
    }
    if ( m_maskedPixels ) {
      // Draw a random number to choose if this pixel is masked.
      if ( m_uniform() < m_fractionMasked ) {
        ++numDigitsKilledMasked;
        continue;
      }
    }
    // Create a new digit.
    digits.insert( new LHCb::VPDigit(), channel );
    // see if any of it was from main bunch (has MCHit), otherwise spillover
    bool spillover = true; // assume spillover if no MCHits
    for ( auto mcHInfo : mcdigit->mcHits() ) {
      if ( mcHInfo.data() ) {
        spillover = false;
        break;
      }
    }
    if ( spillover ) {
      ++numSpillover;
    } else {
      ++numSignalDigitsCreated; // all digits from tracks (in any bunch)
    }
  }
  if ( m_noisyPixels ) {
    const auto numToAdd = m_poisson();
    // Add additional digits without a corresponding MC digit.
    for ( unsigned int i = 0; i < numToAdd; ++i ) {
      // Sample the sensor, chip, column and row of the noise pixel.
      const auto sensor = LHCb::Detector::VPChannelID::SensorID( int( floor( m_uniform() * VP::NSensors ) ) );
      const auto chip   = LHCb::Detector::VPChannelID::ChipID( int( floor( m_uniform() * VP::NChipsPerSensor ) ) );
      const auto col    = LHCb::Detector::VPChannelID::ColumnID( int( floor( m_uniform() * VP::NColumns ) ) );
      const auto row    = LHCb::Detector::VPChannelID::RowID( int( floor( m_uniform() * VP::NRows ) ) );
      LHCb::Detector::VPChannelID channel( sensor, chip, col, row );
      // Make sure the channel has not yet been added to the container.
      if ( digits.object( channel ) ) continue;
      digits.insert( new LHCb::VPDigit(), channel );
      ++numNoiseDigitsCreated;
    }
  }
  // add cross talk to the pixels
  if ( m_crosstalk > 0. ) {
    std::vector<LHCb::Detector::VPChannelID> crossDigits;
    crossDigits.reserve( int( digits.size() * 4 * ( m_crosstalk + 0.1 ) ) );
    for ( auto d : digits ) {
      // top: col = col+1
      if ( m_uniform() < m_crosstalk ) {
        unsigned int col = to_unsigned( d->channelID().col() );
        if ( col + 1 < VP::NColumns ) {
          auto                        sensor = d->channelID().sensor();
          auto                        chip   = d->channelID().chip();
          auto                        row    = d->channelID().row();
          LHCb::Detector::VPChannelID newChan( sensor, chip, LHCb::Detector::VPChannelID::ColumnID( col + 1 ), row );
          crossDigits.push_back( newChan );
        }
      }
      // bottom: col = col-1
      if ( m_uniform() < m_crosstalk ) {
        unsigned int col = to_unsigned( d->channelID().col() );
        if ( col > 0 ) {
          auto                        sensor = d->channelID().sensor();
          auto                        chip   = d->channelID().chip();
          auto                        row    = d->channelID().row();
          LHCb::Detector::VPChannelID newChan( sensor, chip, LHCb::Detector::VPChannelID::ColumnID( col - 1 ), row );
          crossDigits.push_back( newChan );
        }
      }
      // left: row = row-1
      if ( m_uniform() < m_crosstalk ) {
        unsigned int row = to_unsigned( d->channelID().row() );
        if ( row > 0 ) {
          auto                        sensor = d->channelID().sensor();
          auto                        chip   = d->channelID().chip();
          auto                        col    = d->channelID().col();
          LHCb::Detector::VPChannelID newChan( sensor, chip, col, LHCb::Detector::VPChannelID::RowID( row - 1 ) );
          crossDigits.push_back( newChan );
        }
      }
      // right: row = row+1
      if ( m_uniform() < m_crosstalk ) {
        unsigned int row = to_unsigned( d->channelID().row() );
        if ( row + 1 < VP::NRows ) {
          auto                        sensor = d->channelID().sensor();
          auto                        chip   = d->channelID().chip();
          auto                        col    = d->channelID().col();
          LHCb::Detector::VPChannelID newChan( sensor, chip, col, LHCb::Detector::VPChannelID::RowID( row + 1 ) );
          crossDigits.push_back( newChan );
        }
      }
    }
    // merge crossDigits into main container
    for ( auto cd : crossDigits ) {
      if ( digits.object( cd ) ) continue;
      digits.insert( new LHCb::VPDigit(), cd );
      numCrosstalk++;
    }
  }

  // add the values from this event to the running totals
  m_numSpillover += numSpillover;
  m_numLostTiming += numLostTiming;
  m_numDigitsKilledMasked += numDigitsKilledMasked;
  m_numSignalDigitsCreated += numSignalDigitsCreated;
  m_numNoiseDigitsCreated += numNoiseDigitsCreated;
  m_numCrosstalk += numCrosstalk;

  std::sort( digits.begin(), digits.end(), VPDataFunctor::Less_by_Channel<const LHCb::VPDigit*>() );
  return digits;
}
