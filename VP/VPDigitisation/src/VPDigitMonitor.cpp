/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/VP/VPChannelID.h"
#include "Event/MCHit.h"
#include "Event/MCVPDigit.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "VPDet/DeVP.h"
#include <DetDesc/GenericConditionAccessorHolder.h>

struct VPDigitMonitor : Gaudi::Functional::Consumer<void( const LHCb::MCVPDigits&, const DeVP& ),
                                                    LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeVP>> {
  VPDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator );
  void                                   operator()( const LHCb::MCVPDigits&, const DeVP& ) const override;
  mutable Gaudi::Accumulators::Counter<> m_nEvt{this, "eventCount"};
};

DECLARE_COMPONENT( VPDigitMonitor )

VPDigitMonitor::VPDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {{"MCVPDigitLocation", LHCb::MCVPDigitLocation::Default}, {"DeVPLocation", DeVPLocation::Default}}} {}

void VPDigitMonitor::operator()( const LHCb::MCVPDigits& mcdigits, const DeVP& vpDet ) const {
  Tuple tuple = nTuple( "VPPixels" );

  // make mchit -> vpdigit lookup table
  std::map<const LHCb::MCHit*, std::vector<const LHCb::MCVPDigit*>> hitToDigit;
  // Loop over the MC digits.
  for ( const LHCb::MCVPDigit* mcdigit : mcdigits ) {
    for ( unsigned int iDep = 0; iDep < mcdigit->depositAndTimes().size(); ++iDep ) {
      auto mcHit = mcdigit->mcHits()[iDep].data();
      if ( !mcHit ) continue; // noise
      hitToDigit[mcHit].push_back( mcdigit );
    }
  }
  for ( auto hitDigit = hitToDigit.begin(); hitDigit != hitToDigit.end(); ++hitDigit ) {
    auto mchit = hitDigit->first;
    if ( !mchit ) {
      std::cout << "missing mc hit!\n";
      continue;
    }

    // mchit pos midpoint global
    float mcPosGlobalX = mchit->midPoint().x();
    float mcPosGlobalY = mchit->midPoint().y();
    float mcPosGlobalZ = mchit->midPoint().z();
    // mchit energy
    float mcHitEnergy = mchit->energy();
    // mchit eta
    float mcHitEta = mchit->displacement().eta();

    // MCParticle p,eta,phi,pid
    auto  mcp       = mchit->mcParticle();
    float mcPartP   = mcp->p();
    float mcPartEta = mcp->momentum().eta();
    float mcPartPhi = mcp->momentum().phi();
    int   mcPartPID = mcp->particleID().pid();

    auto              vDigits       = hitDigit->second;
    auto              firstDigit    = vDigits.front();
    const DeVPSensor& firstSensor   = vpDet.sensor( firstDigit->key() );
    auto              localMCHitPos = firstSensor.globalToLocal( mchit->midPoint() );
    // mchit pos midpoint local
    float mcPosLocalX = localMCHitPos.x();
    float mcPosLocalY = localMCHitPos.y();
    float mcPosLocalZ = localMCHitPos.z();

    unsigned int           nDigit   = vDigits.size();
    constexpr unsigned int maxDigit = 100;
    if ( nDigit > maxDigit ) {
      std::cout << "too many pixels reduce to " << maxDigit << " \n";
      nDigit = maxDigit;
    }
    // digit sensor
    std::vector<unsigned int> sensor;
    // digit chip
    std::vector<unsigned int> chip;
    // chan x,y
    std::vector<unsigned int> row;
    std::vector<unsigned int> col;
    // local pos
    std::vector<float> digitPosLocalX;
    std::vector<float> digitPosLocalY;
    std::vector<float> digitPosLocalZ;
    // distance (local frame) of mcHit to pixel
    std::vector<float> digitDeltaLocalX;
    std::vector<float> digitDeltaLocalY;
    std::vector<float> digitDeltaLocalZ;
    // electrons
    std::vector<float> digitNelec;
    // time
    std::vector<float> digitTime;

    sensor.resize( nDigit, 999 );
    chip.resize( nDigit, 999 );
    row.resize( nDigit, 999 );
    col.resize( nDigit, 999 );

    digitPosLocalX.resize( nDigit, -999. );
    digitPosLocalY.resize( nDigit, -999. );
    digitPosLocalZ.resize( nDigit, -999. );

    digitDeltaLocalX.resize( nDigit, -999. );
    digitDeltaLocalY.resize( nDigit, -999. );
    digitDeltaLocalZ.resize( nDigit, -999. );

    digitNelec.resize( nDigit, -999. );
    digitTime.resize( nDigit, -999. );

    for ( unsigned int iDigit = 0; iDigit < nDigit; ++iDigit ) {
      auto channelID           = vDigits[iDigit]->key();
      sensor[iDigit]           = to_unsigned( channelID.sensor() );
      chip[iDigit]             = to_unsigned( channelID.chip() );
      row[iDigit]              = to_unsigned( channelID.row() );
      col[iDigit]              = to_unsigned( channelID.col() );
      auto digitPosLocal       = firstSensor.channelToLocalPoint( channelID );
      digitPosLocalX[iDigit]   = digitPosLocal.x();
      digitPosLocalY[iDigit]   = digitPosLocal.y();
      digitPosLocalZ[iDigit]   = digitPosLocal.z();
      auto localDelta          = localMCHitPos - digitPosLocal;
      digitDeltaLocalX[iDigit] = localDelta.x();
      digitDeltaLocalY[iDigit] = localDelta.y();
      digitDeltaLocalZ[iDigit] = localDelta.z();
      // find deposit for this MCHit
      unsigned int iMC = 0;
      for ( iMC = 0; iMC < vDigits[iDigit]->mcHits().size(); ++iMC ) {
        if ( vDigits[iDigit]->mcHits()[iMC].data() == mchit ) break;
      }
      if ( iMC >= vDigits[iDigit]->mcHits().size() ) {
        std::cout << "Failed to match MCHit\n";
        iMC = 0;
      }
      digitNelec[iDigit] = vDigits[iDigit]->depositAndTimes()[iMC].first;
      digitTime[iDigit]  = vDigits[iDigit]->depositAndTimes()[iMC].second;
    }

    tuple->column( "eventCount", m_nEvt.nEntries() ).ignore();

    tuple->column( "mcPosGlobalX", mcPosGlobalX ).ignore();
    tuple->column( "mcPosGlobalY", mcPosGlobalY ).ignore();
    tuple->column( "mcPosGlobalZ", mcPosGlobalZ ).ignore();
    tuple->column( "mcHitEnergy", mcHitEnergy ).ignore();
    tuple->column( "mcHitEta", mcHitEta ).ignore();
    tuple->column( "mcPosLocalX", mcPosLocalX ).ignore();
    tuple->column( "mcPosLocalY", mcPosLocalY ).ignore();
    tuple->column( "mcPosLocalZ", mcPosLocalZ ).ignore();

    tuple->column( "mcPartP", mcPartP ).ignore();
    tuple->column( "mcPartEta", mcPartEta ).ignore();
    tuple->column( "mcPartPhi", mcPartPhi ).ignore();
    tuple->column( "mcPartPID", mcPartPID ).ignore();

    tuple->farray( "sensor", sensor, "nDigit", maxDigit ).ignore();
    tuple->farray( "chip", chip, "nDigit", maxDigit ).ignore();
    tuple->farray( "row", row, "nDigit", maxDigit ).ignore();
    tuple->farray( "col", col, "nDigit", maxDigit ).ignore();
    tuple->farray( "digitPosLocalX", digitPosLocalX, "nDigit", maxDigit ).ignore();
    tuple->farray( "digitPosLocalY", digitPosLocalY, "nDigit", maxDigit ).ignore();
    tuple->farray( "digitPosLocalZ", digitPosLocalZ, "nDigit", maxDigit ).ignore();
    tuple->farray( "digitDeltaLocalX", digitDeltaLocalX, "nDigit", maxDigit ).ignore();
    tuple->farray( "digitDeltaLocalY", digitDeltaLocalY, "nDigit", maxDigit ).ignore();
    tuple->farray( "digitDeltaLocalZ", digitDeltaLocalZ, "nDigit", maxDigit ).ignore();
    tuple->farray( "digitNelec", digitNelec, "nDigit", maxDigit ).ignore();
    tuple->farray( "digitTime", digitTime, "nDigit", maxDigit ).ignore();
    tuple->write().ignore();

    ++m_nEvt;
  }
}
