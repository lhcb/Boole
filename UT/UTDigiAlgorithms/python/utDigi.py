###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import *

from Configurables import MCUTDepositCreator, UTChargeSharingTool
from Configurables import SiAmplifierResponse, UTSelectClustersByChannel, UTRndmEffSelector

MCUTDepositCreator = MCUTDepositCreator('MCUTDepositCreator')

# time of flight vector
MCUTDepositCreator.TofVector = [7.8, 8.8]

MCUTDepositCreator.SpillVector = [
    "/PrevPrev/", "/Prev/", "/", "/Next/", "/Next/Next/"
]
MCUTDepositCreator.SpillTimes = [-50., -25., 0., 25., 50.]
MCUTDepositCreator.SampleTimes = [0., 0., 0., 0., 0.]

# charge sharing function  for UT
MCUTDepositCreator.ChargeSharerTypes = ["250"]
cst = MCUTDepositCreator.addTool(
    UTChargeSharingTool, name='UTChargeSharingTool250')
# NOTE: Charge sharer uses m_thickness as a matchness metric, i.e.
# the best sharer has its m_thickness cloest to the UT sensor thickness.
# Since we only have one sharer for UT, the Thickness value below
# becomes irrelevant, and will be used in sensor with different thicknesses.
cst.Thickness = 0.250 * mm
cst.UseAnalyticErf = True
cst.ErfWidth = 0.005

# 5% cross talk
MCUTDepositCreator.XTalkParamsLeftOdd = [0.05, 0]
MCUTDepositCreator.XTalkParamsLeftEven = [0.05, 0]
MCUTDepositCreator.XTalkParamsRightOdd = [0.05, 0]
MCUTDepositCreator.XTalkParamsRightEven = [0.05, 0]

# Tools
ToolSvc().addTool(UTSelectClustersByChannel, 'UTKiller').addTool(
    UTRndmEffSelector, 'Selector')
