/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/UT/ChannelID.h"
#include "Event/UTDigit.h"
#include "IUTCMSimTool.h"
#include "IUTPedestalSimTool.h"
#include "Kernel/LHCbConstants.h"
#include "LHCbMath/LHCbMath.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

#include "LHCbAlgs/Transformer.h"

#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SmartIF.h"

#include "gsl/gsl_math.h"
#include <yaml-cpp/yaml.h>

#include <algorithm>
#include <cmath>
#include <map>
#include <numeric>
#include <string>
#include <vector>

#ifdef USE_DD4HEP
// unused by needed as IUpdateManagerSvc.h uses it in Gaudi (yes!)
struct ValidDataObject {};
#endif

using namespace LHCb;

class UTCommonModeSim
    : public LHCb::Algorithm::Transformer<UTDigits( UTDigits const&, DeUTDetector const& ),
                                          LHCb::DetDesc::usesBaseAndConditions<GaudiAlgorithm, DeUTDetector>> {
public:
  UTCommonModeSim( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  UTDigits   operator()( UTDigits const&, DeUTDetector const& ) const override;

private:
  void processPort( LHCb::UTDigits::const_iterator& start, LHCb::UTDigits::const_iterator& end,
                    LHCb::UTDigits* outputCont, DeUTDetector const& det ) const;

  SmartIF<IRndmGen> m_gaussDist;

  Gaudi::Property<double> m_outlierCut{this, "outlierCut", 4.0};

  ToolHandle<IUTPedestalSimTool> m_pedestalTool{this, "pedestalToolName", "UTPedestalSimTool"};
  ToolHandle<IUTCMSimTool>       m_cmTool{this, "cmToolName", "UTCMSimTool"};

#ifndef USE_DD4HEP
  StatusCode loadCutsFromConditions();

  Gaudi::Property<bool>        m_forceOptions{this, "forceOptions", false};
  Gaudi::Property<std::string> m_conditionLocation{this, "conditionLocation",
                                                   "/dd/Conditions/ReadoutConf/UT/ClusteringThresholds"};
#endif
};

DECLARE_COMPONENT( UTCommonModeSim )

namespace {
  UTDigits::const_iterator collectByPort( UTDigits::const_iterator start, UTDigits::const_iterator end ) {
    const Detector::UT::ChannelID firstChan = ( *start )->channelID();
    return std::find_if( std::next( start ), end,
                         [port   = firstChan.strip() / LHCbConstants::nStripsInPort,
                          sector = firstChan.uniqueSector()]( const UTDigit* dig ) {
                           if ( dig->channelID().uniqueSector() != sector ) return true;
                           const unsigned int testport = dig->channelID().strip() / LHCbConstants::nStripsInPort;
                           return port != testport;
                         } );
  }
} // namespace

UTCommonModeSim::UTCommonModeSim( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {{"inputLocation", UTDigitLocation::UTDigits}, {"UTLocation", DeUTDetLocation::location()}},
                  {"outputLocation", UTDigitLocation::UTDigits + "CMCorrected"}} {}

StatusCode UTCommonModeSim::initialize() {
  return Transformer::initialize().andThen( [&]() -> StatusCode {
    // Gaussian
    m_gaussDist = randSvc()->generator( Rndm::Gauss( 0., 1. ) );
#ifndef USE_DD4HEP
    // get the cuts from condition if present
    if ( !existDet<YAML::Node>( m_conditionLocation ) || m_forceOptions.value() ) {
      info() << "Default to jobOptions for cuts" << endmsg;
      return StatusCode::SUCCESS;
    } else {
      registerCondition( m_conditionLocation, &UTCommonModeSim::loadCutsFromConditions );
      return runUpdate();
    }
#else
    return StatusCode::SUCCESS;
#endif
  } );
}

UTDigits UTCommonModeSim::operator()( UTDigits const& inputCont, DeUTDetector const& det ) const {

  UTDigits outputCont;
  outputCont.reserve( inputCont.size() );

  // collect all digits in a sector...
  auto iterDigit = inputCont.begin();
  while ( iterDigit != inputCont.end() ) {
    // collect hits
    auto endPort = collectByPort( iterDigit, inputCont.end() );
    // apply correction
    processPort( iterDigit, endPort, &outputCont, det );
    iterDigit = endPort;
  } // iterDigit

  return outputCont;
}

void UTCommonModeSim::processPort( UTDigits::const_iterator& start, UTDigits::const_iterator& end, UTDigits* outputCont,
                                   DeUTDetector const& det ) const {

#ifdef USE_DD4HEP
  auto         sector      = det.findSector( ( *start )->channelID() );
  const double noiseCounts = sector.sectorNoise().value();
#else
  auto& sector = *det.findSector( ( *start )->channelID() );
  const double noiseCounts = sector.sectorNoise();
#endif
  const double cmNoise  = m_cmTool->noise( ( *start )->channelID(), det );
  unsigned int nMissed  = LHCbConstants::nStripsInPort - ( end - start );
  double       ranNoise = cmNoise + m_gaussDist->shoot() * noiseCounts / sqrt( (double)nMissed );

  // make list of pedestal corrected adc scaled to 7 bit
  typedef std::pair<LHCb::Detector::UT::ChannelID, double> digitCharge;
  std::vector<digitCharge>                                 startCharge;
  std::vector<double>                                      portVec( LHCbConstants::nStripsInPort, ranNoise );
  std::vector<double>                                      threshold( LHCbConstants::nStripsInPort, 0 );
  for ( auto iterDigit = start; iterDigit != end; ++iterDigit ) {
    int    pedestal = LHCb::Math::round( m_pedestalTool->pedestal( ( *iterDigit )->channelID() ) );
    double tCharge  = std::min( static_cast<int>( ( *iterDigit )->channelID() ) - pedestal, 127 );
    startCharge.emplace_back( ( *iterDigit )->channelID(), tCharge );
    unsigned int pos = ( *iterDigit )->channelID().strip() - 1 % LHCbConstants::nStripsInPort;
    portVec[pos]     = tCharge;
#ifdef USE_DD4HEP
    threshold[pos] = m_outlierCut * sector.noise( ( *iterDigit )->channelID() ).value();
#else
    threshold[pos] = m_outlierCut * sector.noise( ( *iterDigit )->channelID() );
#endif
  }

  // first mean
  double mean = std::accumulate( portVec.begin(), portVec.end(), 0. ) / LHCbConstants::nStripsInPort;

  // mask outliers
  std::bitset<LHCbConstants::nStripsInPort> mask;
  for ( unsigned int i2 = 0; i2 != LHCbConstants::nStripsInPort; ++i2 ) {
    double testVal = portVec[i2] - mean;
    if ( testVal > threshold[i2] ) {
      mask.set( i2 );
      if ( i2 != 0 ) mask.set( i2 - 1 );
      if ( i2 != LHCbConstants::nStripsInPort - 1 ) mask.set( i2 + 1 );
    }
  } // i

  // second mean
  double tCharge = 0.0;
  for ( unsigned int i = 0; i != LHCbConstants::nStripsInPort; ++i ) {
    if ( !mask[i] ) tCharge = portVec[i]; // FIXME: = -> += ???
  }
  mean = tCharge / LHCbConstants::nStripsInPort;

  // feedback correction to the input values...
  for ( const auto& iter : startCharge ) {
    outputCont->insert( new UTDigit( std::min( LHCb::Math::round( iter.second - mean ), 127l ) ), iter.first );
  }
}

#ifndef USE_DD4HEP
StatusCode UTCommonModeSim::loadCutsFromConditions() {

  // load conditions
  auto& cInfo = *getDet<YAML::Node>( m_conditionLocation );
  info() << "Loading cuts tagged as " << cInfo["tag"].as<std::string>() << endmsg;

  m_outlierCut = cInfo["CMThreshold"].as<double>();

  return StatusCode::SUCCESS;
}
#endif
