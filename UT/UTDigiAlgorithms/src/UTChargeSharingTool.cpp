/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "IUTChargeSharingTool.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiMath/GaudiMath.h"

#include "gsl/gsl_sf_erf.h"
#include <boost/assign/list_of.hpp>

using namespace GaudiMath;
using namespace GaudiMath::Interpolation;
using namespace Gaudi::Units;

/** @class UTChargeSharingTool UTChargeSharingTool.h
 *
 *  Tool for charge sharing UT strip
 *
 *  @author M.Needham
 *  @date   14/3/2002
 */

class UTChargeSharingTool : public extends<GaudiTool, IUTChargeSharingTool> {

public:
  using extends::extends;
  StatusCode initialize() override;

  /// calculate charge fraction as function of the relative distance
  double sharing( const double relDist ) const override;

  /// return thickness of the corresponding sensor
  double thickness() const override;

private:
  /// Spline function to fit to the charge sharing function
  std::unique_ptr<GaudiMath::SimpleSpline> m_responseSpline;

  // Job options:
  /// Fraction of charge on strip as funtion of the relative distance
  Gaudi::Property<std::vector<double>> m_sharingFunction{
      this,
      "SharingFunction",
      {1.0, 1.0, 0.97, 0.935, 0.90, 0.83, 0.73, 0.5, 0.44, 0.34, 0.20, 0.11, 0.065, 0.038, 0.016, 0.0, 0.0}};
  Gaudi::Property<bool>   m_useAnalyticErf{this, "UseAnalyticErf", true,
                                         "Flag to switch to using the analytic error function"};
  Gaudi::Property<double> m_erfWidth{this, "ErfWidth", 0.0224, "Width of the charge sharing (in terms of pitch)"};
  Gaudi::Property<double> m_thickness{this, "Thickness", 0.500 * mm,
                                      "Sensor thickness corresponding to the parameters"};

  // errors
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_NoChargeSharingData{this, "no charge sharing data !" + name()};
};

DECLARE_COMPONENT( UTChargeSharingTool )

StatusCode UTChargeSharingTool::initialize() {
  return extends::initialize().andThen( [&]() -> StatusCode {
    if ( !m_useAnalyticErf.value() ) {
      unsigned int nBin = m_sharingFunction.size();
      if ( nBin == 0 ) ++m_NoChargeSharingData;

      // bin centers
      std::vector<double> binCenters;
      binCenters.reserve( nBin );
      double binSize = 1.0 / (double)nBin;
      for ( unsigned int iBin = 0; iBin < nBin; ++iBin ) {
        binCenters.push_back( ( 0.5 + (double)iBin ) * binSize );
      } // iBin

      // Fit the spline.
      m_responseSpline = std::make_unique<SimpleSpline>( binCenters, m_sharingFunction, Linear );
    }
    return StatusCode::SUCCESS;
  } );
}

double UTChargeSharingTool::sharing( const double relDist ) const {
  if ( relDist < 0.0 || relDist > 1.0 ) return 0.0;
  return !m_useAnalyticErf.value() ? m_responseSpline->eval( relDist )
                                   : 0.5 * gsl_sf_erfc( sqrt( 0.5 ) * ( relDist - 0.5 ) / m_erfWidth );
}

double UTChargeSharingTool::thickness() const { return m_thickness; }
