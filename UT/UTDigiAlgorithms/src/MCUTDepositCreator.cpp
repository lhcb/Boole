/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/UT/ChannelID.h"
#include "Event/MCHit.h"
#include "Event/MCUTDeposit.h"
#include "IUTChargeSharingTool.h"
#include "Kernel/UTDataFunctor.h"
#include "LHCbMath/Functions.h"
#include "LHCbMath/LHCbMath.h"
#include "MCInterfaces/ISiDepositedCharge.h"
#include "Magnet/DeMagnet.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include "UTDet/DeUTSensor.h"

#include "LHCbAlgs/Consumer.h"

#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/SystemOfUnits.h"
#include <Detector/UT/UTConstants.h>

using namespace LHCb;
using namespace Gaudi::Units;

/**
 *  Algorithm for creating MCUTDeposits from MCHits. The following processes
 *  are simulated:
 *  - For each available spill the MCHits are digitized (spill-over).
 *  - The total charge is calculated using the deposited charge tool.
 *  - The entry or exit point is tilted due to Lorentz force.
 *  - The charge is distributed in space (bins) between the strips.
 *  - Charge sharing between the strips is applied by looping over these bins.
 *  - Capacitive coupling between the strips is applied using the xTalkParams.
 *  - The time sampling of the beetle chip is simulated using the available \
      ISiAmplifierResponse tools.
 *  - The number of electrons is converted into a ADC value using the S/N tool.
 *  - A deposit is created from the ADC value, the channel and the MCHit link.
 *  - The deposits are sorted by UTChannelID.
 *
 *  FIXME This algorithm is not really functional although it looks like it
 *  It retrieves a lot of data directly from the transient store manually in
 *  operator() and also stores a lot. This has to be fixed.
 *
 *  @author M.Needham
 *  @author J. van Tilburg
 *  @date   05/01/2006
 *  @author H. Wu
 *  @date   19/08/2024
 */
class MCUTDepositCreator
    : public LHCb::Algorithm::Consumer<void( DeUTDetector const&, DeMagnet const& ),
                                       LHCb::DetDesc::usesBaseAndConditions<GaudiAlgorithm, DeUTDetector, DeMagnet>> {

public:
  MCUTDepositCreator( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer{
            name,
            pSvcLocator,
            {KeyValue{"UTLocation", DeUTDetLocation::location()}, KeyValue{"Magnet", LHCb::Det::Magnet::det_path}}} {}
  StatusCode initialize() override;
  void       operator()( DeUTDetector const&, DeMagnet const& ) const override;

private:
  /// create the deposits
  void createDeposits( const LHCb::MCHits* mcHitsCont, const double spillTime,
                       std::vector<LHCb::MCUTDeposits*>& depositCont, DeUTDetector const&, DeMagnet const& ) const;

  /// check can digitize this tracking hit
  bool hitToDigitize( const LHCb::MCHit* aHit ) const;

  /// create vector of charge sites
  std::vector<double> distributeCharge( const double entryU, const double exitU ) const;

  std::map<unsigned int, double> chargeSharing( const std::vector<double>& sites, const DeUTSensor& aSensor,
                                                double& possibleCollectedCharge ) const;

  double beetleResponse( const double time, const DeUTSector& aSector, unsigned int asic ) const;

  void lorentzShift( const DeUTSensor& sensor, const DeMagnet& magnet, const Gaudi::XYZPoint& midPoint,
                     Gaudi::XYZPoint& entry, Gaudi::XYZPoint& exit ) const;

  // Tools

  /// List of tools for the different charge sharing parameters
  std::vector<IUTChargeSharingTool*> m_chargeSharer;
  ISiDepositedCharge*                m_depositedCharge{}; ///< Tool calculates accumulated charge

  Gaudi::Property<std::string> m_inputLocation{this, "MCHitLocation", MCHitLocation::UT}; ///< Input: MCHits
  Gaudi::Property<std::string> m_outputLocation{this, "MCUTDepositLocation",
                                                MCUTDepositLocation::UTDeposits}; ///< Output: MCUTDeposits

  std::vector<std::string> m_spillPaths; ///< Full path name of spills
  std::vector<std::string> m_outPaths;   /// < Output paths

  Gaudi::Property<std::vector<double>>      m_tofVector{this, "TofVector", {}, "vector of flight times"};
  Gaudi::Property<std::vector<std::string>> m_sampleNames{
      this, "SamplesVector", {"/"}, "Names of spills (e.g. Next, Prev)"};
  Gaudi::Property<std::vector<double>>      m_sampleTimes{this, "SampleTimes", {0.}, "Corresponding time offsets"};
  Gaudi::Property<std::vector<std::string>> m_spillNames{this,
                                                         "SpillVector",
                                                         {"/", "/Prev/", "/PrevPrev/", "/Next/", "/LHCBackground/"},
                                                         "Names of spills (e.g. Next, Prev)"};
  Gaudi::Property<std::vector<double>>      m_spillTimes{
      this, "SpillTimes", {0.0, -25.0, -50.0, 25.0, -3.3}, "Corresponding time offsets"};
  Gaudi::Property<double> m_minDistance{this, "MinDist", 5.0e-3 * Gaudi::Units::mm, "Min. pathlength through sensor"};
  Gaudi::Property<std::string>              m_chargeSharerName{this, "ChargeSharerName", "UTChargeSharingTool",
                                                  "Charge charing tool name"};
  Gaudi::Property<std::vector<std::string>> m_chargeSharerTypes{
      this, "ChargeSharerTypes", {}, "Defines tool names for the different charge sharing tools"};
  Gaudi::Property<std::string> m_depChargeToolName{this, "DepChargeTool", "SiDepositedCharge",
                                                   "Name of tool to calculate charge"};
  Gaudi::Property<double>      m_siteSize{this, "SiteSize", 0.001 * Gaudi::Units::mm, "Binning for charge sharing"};
  Gaudi::Property<int>         m_maxNumSites{this, "MaxNumSites", 150, "Max number of charge sharing bins"};
  Gaudi::Property<std::vector<double>> m_xTalkParamsRightEven{
      this, "XTalkParamsRightEven", {0.0503, 0.001754 / Gaudi::Units::picofarad}, "Cross talk parameters"};
  Gaudi::Property<std::vector<double>> m_xTalkParamsLeftEven{
      this, "XTalkParamsLeftEven", {0.0185, 0.001625 / Gaudi::Units::picofarad}, "Cross talk parameters"};
  Gaudi::Property<std::vector<double>> m_xTalkParamsRightOdd{
      this, "XTalkParamsRightOdd", {0.0215, 0.001824 / Gaudi::Units::picofarad}, "Cross talk parameters"};
  Gaudi::Property<std::vector<double>> m_xTalkParamsLeftOdd{
      this, "XTalkParamsLeftOdd", {0.0494, 0.001416 / Gaudi::Units::picofarad}, "Cross talk parameters"};
  Gaudi::Property<double> m_scaling{this, "Scaling", 1.0, "Scale the deposited charge"};
  Gaudi::Property<bool>   m_applyScaling{this, "ApplyScaling", true, "Flag to apply the scaling"};
  Gaudi::Property<bool>   m_useStatusConditions{this, "useStatusConditions", true, "use dead strip info)"};
  Gaudi::Property<double> m_pMin{this, "pMin", 1e-4 * Gaudi::Units::MeV, "min momentum particle to digitize"};

  Gaudi::Property<bool>   m_applyLorentzCorrection{this, "ApplyLorentzCorrection", true};
  Gaudi::Property<double> m_lorentzFactor{this, "LorentzFactor", 0.025 / Gaudi::Units::tesla};

  // warnings
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_NoSectorFound{this, "Failed to find sector", 1};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_NoSensorFound{this, "Failed to find sensor", 1};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_HitNotDigitizedZeroP{this, "Hit with zero p - not digitized",
                                                                               1};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_HitNotDigitizedSameZ{
      this, "Entry and exit at same z - not digitized", 1};
};

DECLARE_COMPONENT_WITH_ID( MCUTDepositCreator, "MCUTDepositCreator" )

StatusCode MCUTDepositCreator::initialize() {
  if ( !m_applyScaling ) info() << "Cross talk scaling disabled" << endmsg;
  return ConditionAccessorHolder::initialize().andThen( [&] {
    // charge sharing tool
    std::transform( m_chargeSharerTypes.begin(), m_chargeSharerTypes.end(), std::back_inserter( m_chargeSharer ),
                    [&]( const std::string& n ) {
                      return tool<IUTChargeSharingTool>( m_chargeSharerName, m_chargeSharerName + n, this );
                    } );
    // deposited charge
    m_depositedCharge = tool<ISiDepositedCharge>( m_depChargeToolName, "DepCharge", this );
    // construct container names once
    std::transform( m_spillNames.begin(), m_spillNames.end(), std::back_inserter( m_spillPaths ),
                    [&]( const std::string& n ) { // path in Transient data store
                      return "/Event" + n + m_inputLocation.value();
                    } );
    std::transform( m_sampleNames.begin(), m_sampleNames.end(), std::back_inserter( m_outPaths ),
                    [&]( const std::string& n ) { // path in Transient data store
                      return "/Event" + n + m_outputLocation.value();
                    } );
    return StatusCode::SUCCESS;
  } );
}

void MCUTDepositCreator::operator()( DeUTDetector const& det, DeMagnet const& magnet ) const {
  // make output containers and put them in the store
  std::vector<MCUTDeposits*> depositsVec;
  for ( const auto& path : m_outPaths ) {
    auto* depositsCont = new MCUTDeposits();
    depositsCont->reserve( 10000 );
    put( depositsCont, path );
    depositsVec.push_back( depositsCont );
  }

  // loop over spills
  for ( unsigned int iSpill = 0; iSpill < m_spillPaths.size(); ++iSpill ) {

    const MCHits* hits = getIfExists<MCHits>( m_spillPaths[iSpill] );
    if ( !hits ) {
      // failed to find hits
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Unable to retrieve " + m_spillPaths[iSpill] << endmsg;
    } else {
      // found spill - create digitizations and add them to deposits container
      createDeposits( hits, m_spillTimes[iSpill], depositsVec, det, magnet );
    }
  } // iSpill

  // sort by channel
  for ( MCUTDeposits* depositsCont : depositsVec ) {
    std::stable_sort( depositsCont->begin(), depositsCont->end(),
                      UTDataFunctor::Less_by_Channel<const MCUTDeposit*>() );
  } // for each
}

#ifdef USE_DD4HEP
using UTStatus = LHCb::Detector::UT::Status;
#else
using UTStatus = DeUTSector::Status;
#endif

void MCUTDepositCreator::createDeposits( const MCHits* mcHitsCont, const double spillTime,
                                         std::vector<MCUTDeposits*>& depositCont, DeUTDetector const& det,
                                         DeMagnet const& magnet ) const {
  // loop over MChits

  for ( MCHit* aHit : *mcHitsCont ) {

#ifdef USE_DD4HEP
    auto aSector = det.findSector( aHit->midPoint() );
    if ( !aSector.isValid() ) {
#else
    auto aSectorP = det.findSector( aHit->midPoint() );
    if ( !aSectorP ) {
#endif
      if ( msgLevel( MSG::DEBUG ) ) debug() << "point " << aHit->midPoint() << endmsg;
      ++m_NoSectorFound;
      continue;
    }
#ifndef USE_DD4HEP
    auto& aSector = *aSectorP;
#endif

    if ( hitToDigitize( aHit ) ) {

      if ( m_useStatusConditions && aSector.sectorStatus() == UTStatus::Dead ) continue;

      // find the sensor
      const Gaudi::XYZPoint globalMidPoint = aHit->midPoint();
#ifdef USE_DD4HEP
      auto aSensor = aSector.findSensor( globalMidPoint );
      if ( !aSensor.isValid() ) {
#else
      auto aSensorP = aSector.findSensor( globalMidPoint );
      if ( !aSensorP ) {
#endif
        ++m_NoSensorFound;
        continue;
      }
#ifndef USE_DD4HEP
      auto& aSensor = *aSensorP;
#endif
      // from now on work in local sensor frame
      Gaudi::XYZPoint       entryPoint = aSensor.toLocal( aHit->entry() );
      Gaudi::XYZPoint       exitPoint  = aSensor.toLocal( aHit->exit() );
      const Gaudi::XYZPoint midPoint   = aSensor.toLocal( globalMidPoint );

      if ( m_applyLorentzCorrection ) lorentzShift( aSensor, magnet, globalMidPoint, entryPoint, exitPoint );

      if ( aSensor.localInActive( midPoint ) ) {

        // Calculate the deposited charge on strip
        // NOTE: only simulate positive charge
        const double ionization = m_depositedCharge->charge( aHit );

        // distribute charge to n sites
        std::vector<double> chargeSites = distributeCharge( entryPoint.x(), exitPoint.x() );

        // doing charge sharing + go to strips
        double                         totWeightedCharge = 0.0;
        std::map<unsigned int, double> stripMap          = chargeSharing( chargeSites, aSensor, totWeightedCharge );

        if ( totWeightedCharge > 1e-3 ) {

          Detector::UT::ChannelID elemChan = aSector.elementID();

          // loop over strips and simulate capacitive coupling
          unsigned int firstStrip = ( stripMap.begin() )->first;
          unsigned int lastStrip  = std::prev( stripMap.end() )->first;

          for ( unsigned int iStrip = firstStrip; iStrip <= lastStrip; ++iStrip ) {

            // old utchannelID starts from 1, but new id starts from 0:
            // Determine whether Tell1 neighbour(!) channel is even or odd
            bool tell1ChanEven = ( ( iStrip % 2 == 1 ) );

            // Determine cross talk level for this readout sector
            double xTalkRight = 0.0;
            double xTalkLeft  = 0.0;
            if ( tell1ChanEven ) {
              xTalkRight = m_xTalkParamsRightEven[0] + m_xTalkParamsRightEven[1] * aSector.capacitance();
              xTalkLeft  = m_xTalkParamsLeftEven[0] + m_xTalkParamsLeftEven[1] * aSector.capacitance();
            } else {
              xTalkRight = m_xTalkParamsRightOdd[0] + m_xTalkParamsRightOdd[1] * aSector.capacitance();
              xTalkLeft  = m_xTalkParamsLeftOdd[0] + m_xTalkParamsLeftOdd[1] * aSector.capacitance();
            }

            // Get the charge of the previous and next strips
            double prevCharge = 0.0;
            if ( iStrip != firstStrip ) prevCharge = stripMap[iStrip - 1];
            double nextCharge = 0.0;
            if ( iStrip != lastStrip ) nextCharge = stripMap[iStrip + 1];
            double leftCharge  = prevCharge;
            double rightCharge = nextCharge;

            // Capacitive coupling
            double weightedCharge =
                ( 1. - xTalkLeft - xTalkRight ) * stripMap[iStrip] + xTalkRight * leftCharge + xTalkLeft * rightCharge;

            // Do not store deposits which have too small charge
            if ( weightedCharge < 1e-3 ) continue;

            // Scaling for the number of electrons (JvT: do we need this?)
            const double scaling = m_applyScaling ? m_scaling * ( 1.0 + xTalkLeft + xTalkRight ) : 1.0;

            // amplifier response - fraction of charge it sees
            for ( unsigned int iTime = 0; iTime < m_outPaths.size(); ++iTime ) {
              double       beetleFrac;
              double       samplingTime = m_sampleTimes[iTime];
              unsigned int asicNum      = aSector.beetle( iStrip );
              if ( iStrip != firstStrip && iStrip != lastStrip ) {
                beetleFrac = beetleResponse(
                    m_tofVector[elemChan.station() - 1] - aHit->time() - spillTime + samplingTime, aSector, asicNum );
              } else {
                beetleFrac = beetleResponse(
                    m_tofVector[elemChan.station() - 1] - aHit->time() - spillTime + samplingTime, aSector, asicNum );
              }
              if ( msgLevel( MSG::DEBUG ) ) {
                debug() << "iTime: " << iTime << "\thit time: " << aHit->time() << "\tspillTime: " << spillTime
                        << "\tsamplingTime: " << samplingTime << "\tbeetleFrac: " << beetleFrac << endmsg;
              }
              Detector::UT::ChannelID aChan = aSector.stripToChan( iStrip );

              if ( m_useStatusConditions == false || aSector.isOKStrip( aChan ) == true ) {
                const double electrons = ionization * beetleFrac * scaling * weightedCharge / totWeightedCharge;

                const double adcCounts  = aSector.toADC( electrons );
                auto*        newDeposit = new MCUTDeposit( adcCounts, aChan, aHit );
                depositCont[iTime]->insert( newDeposit );
              } else {
                if ( msgLevel( MSG::DEBUG ) ) {
                  debug() << "--------------------------------\n";
                  debug() << "Sector: " << aSector.name() << '\n';
                  debug() << aSector.elementID() << '\n';
                  debug() << "m_useStatusConditions: " << m_useStatusConditions.value() << '\n';
                  for ( auto const& value : aSector.stripStatus() ) { debug() << (int)value << " "; }
                  debug() << "\n";
                  debug() << aSector.elementID().getFullSector() << ",strip=" << iStrip << ",disabled" << endmsg;
                }
              } // ok strip
            }   // loop sampling times
          }     // loop strip
        }       // if has some charge
      }         // in active area
    }           // hitToDigitize
  }             // iterHit
}

bool MCUTDepositCreator::hitToDigitize( const MCHit* aHit ) const {
  // check whether it is reasonable to digitize this hit

  // check if it goes through some Si ....
  if ( aHit->pathLength() < m_minDistance ) return false;

  // some hits have a zero p...
  if ( aHit->p() < m_pMin ) {
    ++m_HitNotDigitizedZeroP;
    return false;
  }

  // check if entry and exit point are at the same z-position
  if ( fabs( aHit->entry().z() - aHit->exit().z() ) < LHCb::Math::lowTolerance ) {
    ++m_HitNotDigitizedSameZ;
    return false;
  }

  return true;
}

std::vector<double> MCUTDepositCreator::distributeCharge( const double entryU, const double exitU ) const {
  // distribute charge homogeneously at n Sites
  const double deltaU = fabs( exitU - entryU );
  int          nSites;
  if ( deltaU <= m_siteSize ) {
    nSites = 1;
  } else {
    nSites = std::min( (int)floor( deltaU / m_siteSize ) + 1, m_maxNumSites.value() );
  }

  double startU = entryU;
  double stopU  = exitU;
  if ( startU > stopU ) { std::swap( startU, stopU ); }

  const double siteSize = deltaU / nSites;

  std::vector<double> sites;
  sites.reserve( nSites );
  for ( int iSite = 0; iSite < nSites; ++iSite ) {
    sites.push_back( startU + ( (double)iSite + 0.5 ) * siteSize );
  } // iSite
  return sites;
}

std::map<unsigned int, double> MCUTDepositCreator::chargeSharing( const std::vector<double>& sites,
                                                                  const DeUTSensor&          aSensor,
                                                                  double& possibleCollectedCharge ) const {
  // init
  const double chargeOnSite = 1.0 / ( (double)sites.size() );
  double       frac         = 0.0;

  // Find the appropriate charge sharing tool
  IUTChargeSharingTool* chargeSharer = nullptr;
  double                deltaT       = 1E10 * m;

  // NOTE: Charge sharer uses m_thickness as a quality metric
  for ( const auto& cs : m_chargeSharer ) {
    if ( std::abs( ( cs )->thickness() - aSensor.thickness() ) < deltaT ) {
      deltaT       = fabs( cs->thickness() - aSensor.thickness() );
      chargeSharer = cs;
    }
  }
  std::map<unsigned int, double> stripMap;
  // loop on sites
  for ( const auto& site : sites ) {

    // closest strips to site
    unsigned int firstStrip  = aSensor.localUToStrip( site );
    unsigned int secondStrip = firstStrip + 1;

    // do the sharing: first Strip!
    if ( aSensor.isStrip( firstStrip ) ) {
      frac = chargeSharer->sharing( fabs( site - aSensor.localU( firstStrip ) ) / aSensor.pitch() );
      stripMap[firstStrip] += frac * chargeOnSite;
      possibleCollectedCharge += frac * chargeOnSite;
    }

    // second strip - if there is one
    if ( aSensor.isStrip( secondStrip ) ) {
      frac = chargeSharer->sharing( fabs( site - aSensor.localU( secondStrip ) ) / aSensor.pitch() );
      stripMap[secondStrip] += frac * chargeOnSite;
      possibleCollectedCharge += frac * chargeOnSite;
    }
  }

  // add entry for strip before first and after last
  auto startIter = stripMap.begin();
  auto lastIter  = std::prev( stripMap.end() );

  auto firstStrip = startIter->first;
  if ( ( firstStrip > 0 ) && ( aSensor.isStrip( firstStrip - 1 ) ) ) { stripMap[firstStrip - 1] += 0.0; }

  const unsigned int lastStrip = lastIter->first;
  if ( aSensor.isStrip( lastStrip + 1 ) ) { stripMap[lastStrip + 1] += 0.0; }

  return stripMap;
}

double MCUTDepositCreator::beetleResponse( const double time, const DeUTSector& aSector, unsigned int asic ) const {

  auto                         responseParameters = aSector.response( asic );
  double                       peak               = responseParameters.mean;
  double                       sigmaL             = responseParameters.sigmaL;
  double                       sigmaR             = responseParameters.sigmaR;
  double                       amplitudeRatio     = responseParameters.amplitudeRatio;
  Gaudi::Math::BifurcatedGauss saltResponse( peak, sigmaL, sigmaR );

  // Normalization to make amplitude 1
  double pdfAtPeak = saltResponse.pdf( peak );
  double response  = saltResponse.pdf( time ) / pdfAtPeak;
  return response * amplitudeRatio;
}

void MCUTDepositCreator::lorentzShift( const DeUTSensor& sensor, const DeMagnet& magnet,
                                       const Gaudi::XYZPoint& midPoint, Gaudi::XYZPoint& entry,
                                       Gaudi::XYZPoint& exit ) const {
  // Get the local By
  Gaudi::XYZVector field = magnet.fieldVector( midPoint );
  Gaudi::XYZVector normalY( sensor.globalPoint( 0, 1, 0 ) - sensor.globalPoint( 0, 0, 0 ) );
  double           localBy = field.Dot( normalY.Unit() );

  // Calculate the Lorentz shift in local x.
  const double dz = exit.z() - entry.z();
  double       dx = fabs( dz ) * localBy * m_lorentzFactor;

  // Apply a small tilt to particle trajectory by shifting either the entry or
  // exit point
  if ( dz > 0 ) {
    exit.SetX( exit.x() + dx );
  } else {
    entry.SetX( entry.x() + dx );
  }
}
