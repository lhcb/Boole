/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCTruth.h"
#include "Event/MCUTDigit.h"
#include "Event/UTDigit.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDataFunctor.h"
#include "LHCbMath/LHCbMath.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

#include "LHCbAlgs/Transformer.h"

#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SmartIF.h"
#include <Detector/UT/ChannelID.h>
#include <algorithm>
#include <boost/proto/transform/env.hpp>
#include <cstdlib>
#include <stdexcept>

#ifdef USE_DD4HEP
#  include <DD4hep/GrammarUnparsed.h>
#endif

#include "gsl/gsl_sf_erf.h"

using namespace LHCb;

namespace {
  // cache the number of noise strips
  struct NoiseStrips {
    unsigned int number;
  };
} // namespace

/** @class DigitsOnAsic
 *
 *  Helper class to group UTDigits by ASIC
 *  and apply common mode subtraction
 *
 *  @author Hangyi Wu
 *  @date   2024-07-23
 */
class DigitsOnAsic final {
  std::vector<UTDigit>                             m_digits;
  unsigned int                                     m_asicChanID;
  unsigned int                                     m_adcRange;
  int                                              m_mcm;
  bool                                             m_mcmComputed;
  bool                                             m_cmsApplied;
  bool                                             m_thresApplied;
  bool                                             m_hitLimitApplied;
  std::array<float, Detector::UT::nStripsInBeetle> m_rawAdcValues;

public:
  DigitsOnAsic() { clear(); }
  DigitsOnAsic( unsigned int asicChan, unsigned int adcRange ) : m_asicChanID( asicChan ), m_adcRange( adcRange ) {
    clear();
  }
  void clear() {
    m_digits.clear();
    m_mcm             = 0;
    m_mcmComputed     = false;
    m_cmsApplied      = false;
    m_thresApplied    = false;
    m_hitLimitApplied = false;
    // NOTE: According to SALT manual, the masked channels contribute
    // as if they are ADC=0 digits. Thus we initialize raw ADC values
    // to be all zeros.
    for ( size_t i = 0; i < Detector::UT::nStripsInBeetle; i++ ) m_rawAdcValues[i] = 0.0f;
  }
  Detector::UT::ChannelID asicChanID() { return Detector::UT::ChannelID{m_asicChanID}; }
  unsigned int            asicNumber() { return asicChanID().asic(); }
  void                    moveDigit( UTDigit& aDigit ) {
    unsigned int channelInAsic = aDigit.channelID().strip() % Detector::UT::nStripsInBeetle;
    if ( m_rawAdcValues[channelInAsic] != 0 )
      throw std::runtime_error( "Duplicated ChannelID from a previously added digit." );
    m_rawAdcValues[channelInAsic] = aDigit.depositedCharge();
    m_digits.push_back( std::move( aDigit ) );
  }

  int getMeanCommonMode() {
    if ( m_mcmComputed ) return m_mcm;
    float adcSum = 0;
    for ( auto const& value : m_rawAdcValues ) {
      if ( value <= m_adcRange ) adcSum += value;
    }
    m_mcm         = adcSum / Detector::UT::nStripsInBeetle;
    m_mcmComputed = true;
    return m_mcm;
  };

  void applyCommonModeSubtraction( double saturation ) {
    // NOTE: adc_mcm = adc_ped - mcm
    // adc_mcm can have following values
    // *             0, if strip is disabled
    // *           -32, if adc_ped - mcm < -32 (never happens when only +charge simulated)
    // * adc_ped - mcm, if -32 < adc_ped - mcm <= 31
    // *            31, if adc_ped - mcm > 31
    if ( m_cmsApplied ) return;
    for ( auto& digit : m_digits ) {
      float before_cms = digit.depositedCharge();
      float after_cms  = before_cms - getMeanCommonMode();
      auto  adcCount   = std::min( (float)after_cms, (float)saturation );
      // std::cout << "ASIC: " << m_asicChanID << "\tBefore CMS: " << before_cms << "\tAfter CMS: " << after_cms <<
      // '\n';
      digit.setDepositedCharge( adcCount );
    }
    m_cmsApplied = true;
  }
  void applyASICThreshold( unsigned int threshold ) {
    if ( !m_cmsApplied ) throw std::runtime_error( "ASIC threshold cannot be applied before common mode subtraction." );
    if ( m_thresApplied ) return;
    // std::cout << "ASIC: " << m_asicChanID << "\tThreshold: " << threshold << "\tSize before: " << m_digits.size();
    auto new_end = std::remove_if( m_digits.begin(), m_digits.end(),
                                   [&]( UTDigit& digit ) { return digit.depositedCharge() <= threshold; } );
    m_digits.erase( new_end, m_digits.end() );
    // std::cout << "\tSize after: " << m_digits.size() << '\n';
    m_thresApplied = true;
  }
  void applyHitLimit( unsigned int limit ) {
    if ( !( m_cmsApplied && m_thresApplied ) )
      throw std::runtime_error( "ASIC hit limit cannot be applied before common mode subtraction and zs_th." );
    if ( m_hitLimitApplied ) return;
    // std::cout << "ASIC: " << m_asicChanID << "\tHitLimit: " << limit << "\tSize before: " << m_digits.size();
    if ( m_digits.size() > limit ) m_digits.clear();
    // std::cout << "\tSize after: " << m_digits.size() << '\n';
  }
  void                        empty() {}
  const std::vector<UTDigit>& digits() const { return m_digits; }
};

/**
 *  Algorithm for creating UTDigits from MCUTDigits. The following processes
 *  are simulated:
 *  - Apply readout inefficiency/dead channels by masking MCUTDigits
 *  - Generate flat noise over all readout sectors. The ADC value follows a \
 *    gaussian tail distribution.
 *  - Merge the signal and noise digits in one container.
 *  - Add neighbours to each digit with an ADC value of a gaussian core noise.
 *  - Common mode noise
 *  - Common mode subtraction for each ASIC
 *
 *  @author M.Needham
 *  @author J. van Tilburg
 *  @date   05/01/2006
 *  @author H. Wu  @date 23/07/2024
 */
class UTDigitCreator : public LHCb::Algorithm::Transformer<
                           UTDigits( const MCUTDigits&, DeUTDetector const&, NoiseStrips const& ),
                           LHCb::DetDesc::usesBaseAndConditions<GaudiAlgorithm, DeUTDetector, NoiseStrips>> {

public:
  UTDigitCreator( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  UTDigits   operator()( const MCUTDigits& mcDigitCont, DeUTDetector const& det, NoiseStrips const& ) const override;

private:
  using digitPair = std::pair<double, LHCb::Detector::UT::ChannelID>;

  void   createDigits( const LHCb::MCUTDigits* mcDigitCont, LHCb::UTDigits* digitCont, DeUTDetector const& det,
                       NoiseStrips const& noiseStrips ) const;
  void   addNeighbours( LHCb::UTDigits* digitsCont, DeUTDetector const& det ) const;
  double genInverseTail() const;
  bool   aboveASICThreshold( const UTDigit* aDigit, const DeUTSector& aSector ) const;
  double adcValue( double value ) const;
  void   groupByAsic( UTDigits& digitCont, std::map<unsigned int, DigitsOnAsic>& digitVectors ) const;

  // smart interface to generators
  SmartIF<IRndmGen> m_uniformDist;
  SmartIF<IRndmGen> m_gaussDist;
  SmartIF<IRndmGen> m_gaussTailDist;

  Gaudi::Property<unsigned int> m_cmsADCRange{this, "CMSADCRange", 31,
                                              "ADC range in which the common mode subtraction (CMS) is applied."};
  Gaudi::Property<double>       m_tailStart{this, "TailStart", 2.5};
  Gaudi::Property<double>       m_saturation{this, "Saturation", 31};
  Gaudi::Property<bool>         m_simulatePedestal{this, "simulatePedestal", false};
  Gaudi::Property<bool>         m_addCommonMode{this, "addCommonMode", false};
  Gaudi::Property<bool>         m_applyCommonModeSubtraction{this, "ApplyCMS", true};
  Gaudi::Property<bool>         m_applyThreshold{this, "ApplyThreshold", true};
  Gaudi::Property<bool>         m_applyHitLimit{this, "ApplyHitLimit", true};
  Gaudi::Property<bool>         m_allStrips{this, "allStrips", false};
  Gaudi::Property<bool>         m_useStatusConditions{this, "useStatusConditions", true, "use dead strip info"};
  mutable std::map<unsigned int, double> m_cmValues; // coherent noise for each ASIC
  //
  ToolHandle<IUTReadoutTool> m_readoutTool{this, "ReadoutTool", "UTReadoutTool"};
};

DECLARE_COMPONENT_WITH_ID( UTDigitCreator, "UTDigitCreator" )

namespace {
  struct Less_by_Channel {
    template <typename Pair>
    bool operator()( const Pair& obj1, const Pair& obj2 ) const {
      return obj1.second < obj2.second;
    }
  };

  template <typename Iterator>
  std::pair<bool, Iterator> findDigit( const Detector::UT::ChannelID& aChan, Iterator first, Iterator last ) {
    first = std::find_if_not( first, last, [&aChan]( const auto& i ) { return i->channelID() < aChan; } );
    return {first != last && ( *first )->channelID() == aChan, first};
  }
} // namespace

double UTDigitCreator::adcValue( double value ) const {
  return LHCb::Math::round( std::min( value, m_saturation.value() ) );
}

void UTDigitCreator::groupByAsic( UTDigits& digitCont, std::map<unsigned int, DigitsOnAsic>& digitsOnASICs ) const {
  for ( auto const& digit : digitCont ) {
    auto         aChan      = digit->channelID();
    unsigned int uniqueAsic = aChan.uniqueAsic();
    try {
      digitsOnASICs.at( uniqueAsic );
    } catch ( const std::out_of_range& e ) {
      digitsOnASICs[uniqueAsic] = DigitsOnAsic( aChan.getFullAsic(), m_cmsADCRange.value() );
    }
    digitsOnASICs[uniqueAsic].moveDigit( *digit );
  }
  digitCont.clear();
}

UTDigitCreator::UTDigitCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {{"InputLocation", MCUTDigitLocation::UTDigits},
                   {"UTLocation", DeUTDetLocation::location()},
                   {"NoiseStripsLocation", "UTDigitCreatorNoiseStrips"}},
                  {"OutputLocation", UTDigitLocation::UTDigits}} {}

StatusCode UTDigitCreator::initialize() {
  return Transformer::initialize().andThen( [&] {
    if ( m_applyCommonModeSubtraction ) {
      info() << "Enabled common mode subtraction on each UT ASIC chips" << endmsg;
    } else {
      info() << "Disable common mode subtraction on each UT ASIC chips" << endmsg;
    }
    // random numbers generators (flat, gaussian and gaussian tail)
    m_uniformDist   = randSvc()->generator( Rndm::Flat( 0., 1. ) );
    m_gaussDist     = randSvc()->generator( Rndm::Gauss( 0., 1. ) );
    m_gaussTailDist = randSvc()->generator( Rndm::GaussianTail( m_tailStart, 1. ) );

    // cache the number of noise strips
    addConditionDerivation(
        {DeUTDetLocation::location()}, this->template inputLocation<NoiseStrips>(), [this]( DeUTDetector const& det ) {
          if ( m_addCommonMode ) {

            det.applyToAllSectors( [&]( const DeUTSector& sector ) {
#ifdef USE_DD4HEP
              unsigned int secID = sector.channelID().channelID();
#else
            unsigned int secID = sector.elementID().channelID();
#endif
              for ( unsigned int i = 0; i < sector.nBeetle(); i++ ) {
                LHCb::Detector::UT::ChannelID asicID =
                    LHCb::Detector::UT::ChannelID{secID + i * Detector::UT::nStripsInBeetle};
#ifdef USE_DD4HEP
                m_cmValues[asicID.uniqueAsic()] = sector.cmNoise( asicID ).value() * m_gaussDist->shoot();
#else
                m_cmValues[asicID.uniqueAsic()] = sector.cmNoise( asicID ) * m_gaussDist->shoot();
#endif
              }
            } );
          }
          if ( !m_allStrips ) {
            double fracOfNoiseStrips = 0.5 * gsl_sf_erfc( m_tailStart / sqrt( 2.0 ) );
            return NoiseStrips{(unsigned int)( fracOfNoiseStrips * det.nStrip() )};
          } else {
            return NoiseStrips{det.nStrip()};
          }
        } );
    return StatusCode::SUCCESS;
  } );
}

UTDigits UTDigitCreator::operator()( const MCUTDigits& mcDigitCont, DeUTDetector const& det,
                                     NoiseStrips const& noiseStrips ) const {

  // create UTDigits
  UTDigits digitsCont;
  createDigits( &mcDigitCont, &digitsCont, det, noiseStrips );

  // Calculate and subtract MCM
  // group UTDigits by ASIC
  std::map<unsigned int, DigitsOnAsic> digitsOnASICs;
  groupByAsic( digitsCont, digitsOnASICs );

  // Loop for each ASIC
  for ( auto iterASIC = digitsOnASICs.begin(); iterASIC != digitsOnASICs.end(); ++iterASIC ) {
    DigitsOnAsic& digitsOnASIC = iterASIC->second;
    unsigned int  asicNumber   = digitsOnASIC.asicNumber();
    auto          aSector      = det.findSector( digitsOnASIC.asicChanID() );
#ifdef USE_DD4HEP
    auto threshold = aSector.asicThreshold( asicNumber );
    auto hitLimit  = aSector.asicHitLimit( asicNumber );
#else
    auto   threshold   = aSector->asicThreshold( asicNumber );
    auto   hitLimit    = aSector->asicHitLimit( asicNumber );
#endif
    if ( m_applyCommonModeSubtraction ) digitsOnASIC.applyCommonModeSubtraction( m_saturation );
    if ( m_applyThreshold ) digitsOnASIC.applyASICThreshold( threshold.value() );
    if ( m_applyHitLimit ) digitsOnASIC.applyHitLimit( hitLimit.value() );
    for ( auto& digit : digitsOnASIC.digits() ) {
      // check saturation again
      auto    adcCount = adcValue( digit.depositedCharge() );
      UTDAQID utdaqID  = m_readoutTool->channelIDToDAQID( (const Detector::UT::ChannelID)digit.channelID() );
      digitsCont.insert( new UTDigit( digit.channelID(), static_cast<float>( adcCount ), utdaqID.id() ),
                         digit.channelID() );
    }
  }

  // resort by channel
  std::stable_sort( digitsCont.begin(), digitsCont.end(), UTDataFunctor::Less_by_Channel<const UTDigit*>() );

  // Make the link between digits and mcdigits
  setMCTruth( &digitsCont, &mcDigitCont )
      .orThrow( "Failed to set MCTruth info", "UTDigitCreator" )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  return digitsCont;
}

void UTDigitCreator::createDigits( const MCUTDigits* mcDigitCont, UTDigits* digitsCont, DeUTDetector const& det,
                                   NoiseStrips const& noiseStrips ) const {
  // add gaussian noise to real hit channels + allow xxx% dead channels
  digitsCont->reserve( mcDigitCont->size() + noiseStrips.number );
  auto iterMC = mcDigitCont->begin();
  for ( ; iterMC != mcDigitCont->end(); ++iterMC ) {

    // adding CMS noise, the DeUTSector::noise method gets the value from conditions
#ifdef USE_DD4HEP
    auto   sector      = det.findSector( ( *iterMC )->channelID() );
    double totalCharge = m_gaussDist->shoot() * sector.noise( ( *iterMC )->channelID() ).value();
#else
    auto&  sector      = *det.findSector( ( *iterMC )->channelID() );
    double totalCharge = m_gaussDist->shoot() * sector.noise( ( *iterMC )->channelID() );
#endif
    const SmartRefVector<MCUTDeposit> depositCont = ( *iterMC )->mcDeposit();
    totalCharge = std::accumulate( depositCont.begin(), depositCont.end(), totalCharge,
                                   UTDataFunctor::Accumulate_Charge<const MCUTDeposit*>() );

    // add pedestal
    if ( m_simulatePedestal ) { totalCharge += sector.pedestal( ( *iterMC )->channelID() ); }

    // sim cm noise
    if ( m_addCommonMode ) { totalCharge += m_cmValues[( *iterMC )->channelID().uniqueAsic()]; }

    auto adcCount = floor( totalCharge );

#ifdef USE_DD4HEP
    char sensorType = sector.sensor().sensorType();
#else
    char   sensorType  = sector.moduletype();
#endif
    if ( adcCount >= 32 && sensorType == 'A' ) {
      // for n-type, if adc>=32, discard hit
      adcCount = -100;
    } else {
      // for all types, if adc>saturation, adc will be set to saturation
      adcCount = std::min( adcCount, m_saturation.value() );

      // subtract previously added pedestal
      if ( m_simulatePedestal ) { adcCount -= sector.pedestal( ( *iterMC )->channelID() ); }
      adcCount = std::min( adcCount, m_saturation.value() ); // pedestal can be negative thus apply saturation again
    }

    // make digit and add to container.
    digitsCont->insert( new UTDigit( adcCount ), ( *iterMC )->channelID() );
  } // iterDigit
}

void UTDigitCreator::addNeighbours( UTDigits* digitsCont, DeUTDetector const& det ) const {

  std::vector<digitPair> tmpCont;
  for ( auto curDigit = digitsCont->begin(); curDigit != digitsCont->end(); ++curDigit ) {

    // Get left neighbour
#ifdef USE_DD4HEP
    auto aSector = det.findSector( ( *curDigit )->channelID() );
#else
    auto&  aSector     = *det.findSector( ( *curDigit )->channelID() );
#endif
    Detector::UT::ChannelID leftChan = aSector.nextLeft( ( *curDigit )->channelID() );

    // Don't add left neighbour if this neighbour is already hit
    Detector::UT::ChannelID prevDigitChan( 0u );
    if ( curDigit != digitsCont->begin() ) {
      auto prevDigit = std::prev( curDigit );
      prevDigitChan  = ( *prevDigit )->channelID();
    }
    if ( leftChan != prevDigitChan && leftChan != 0u ) { tmpCont.emplace_back( genInverseTail(), leftChan ); }

    // Get right neighbour
    Detector::UT::ChannelID rightChan = aSector.nextRight( ( *curDigit )->channelID() );

    // Don't add right neighbour if this neighbour is already hit
    Detector::UT::ChannelID nextDigitChan( 0u );
    auto                    nextDigit = std::next( curDigit );
    if ( nextDigit != digitsCont->end() ) { nextDigitChan = ( *nextDigit )->channelID(); }
    if ( rightChan != nextDigitChan && rightChan != 0u ) { tmpCont.emplace_back( genInverseTail(), rightChan ); }
  } // iterDigit

  for ( auto [charge, id] : tmpCont ) {
    if ( !digitsCont->object( id ) ) {
      // do better sometimes we can make twice ie we start with 101
#ifdef USE_DD4HEP
      auto aSector = det.findSector( id );
#else
      auto& aSector = *det.findSector( id );
#endif
      if ( !m_useStatusConditions || aSector.isOKStrip( id ) ) {
        digitsCont->insert( new UTDigit( adcValue( charge ) ), id );
      } // ok strip
    }
  } // iterP
}

double UTDigitCreator::genInverseTail() const {
  double testVal;
  do { testVal = m_gaussDist->shoot(); } while ( testVal > m_tailStart );
  return testVal;
}

bool UTDigitCreator::aboveASICThreshold( const UTDigit* aDigit, const DeUTSector& aSector ) const {

  auto               digitADCCount = aDigit->depositedCharge();
  const unsigned int threshold     = aSector.asicThreshold( aDigit->channelID().asic() ).value();
#ifdef USE_DD4HEP
  if ( aSector.sensor().sensorType() == 'A' && digitADCCount == 31 ) return false;
#else
  if ( aSector.moduletype() == 'A' && digitADCCount == 31 ) return false;
#endif
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Sector:" << aSector.name() << "\tADC: " << digitADCCount << "\tthreshold: " << threshold << endmsg;
  }
  return digitADCCount > threshold;
}
