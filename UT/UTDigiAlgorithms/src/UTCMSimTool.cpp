/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "IUTCMSimTool.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/SmartIF.h"

/**
 *  Tool for for simulating pedestal
 *
 *  @author M.Needham
 *  @date   14/3/2010
 */

class UTCMSimTool : public extends<GaudiTool, IUTCMSimTool, IIncidentListener> {

public:
  using extends::extends;

  StatusCode initialize() override;

  /// return the simulated cm noise in this sector for this event
  double noise( const LHCb::Detector::UT::ChannelID&, DeUTDetector const& ) const override;

  /** Implement the handle method for the Incident service.
   *  This is used to inform the tool of software incidents.
   *
   *  @param incident The incident identifier
   */
  void handle( const Incident& incident ) override;

private:
  void initEvent( DeUTDetector const& ) const;

  SmartIF<IRndmGen>       m_gaussDist;
  Gaudi::Property<bool>   m_forceOptions{this, "forceOptions", true};
  Gaudi::Property<double> m_cmNoise{this, "cmNoise", 2.0}; // value to use if forceOptions true

  mutable bool                                            m_isInitialized = false;
  mutable std::map<LHCb::Detector::UT::ChannelID, double> m_cmValues;
};

DECLARE_COMPONENT( UTCMSimTool )

StatusCode UTCMSimTool::initialize() {
  return extends::initialize().andThen( [&] {
    // Add incident at begin of each event
    incSvc()->addListener( this, IncidentType::BeginEvent );
  } );
}

void UTCMSimTool::handle( const Incident& incident ) {
  if ( IncidentType::BeginEvent == incident.type() ) m_isInitialized = false;
}

void UTCMSimTool::initEvent( DeUTDetector const& det ) const {
  // make the map
  m_cmValues.clear();
  det.applyToAllSectors( [&]( DeUTSector const& sector ) {
    if ( m_forceOptions.value() ) {
      m_cmValues[sector.elementID()] = m_cmNoise * m_gaussDist->shoot();
    } else {
#ifdef USE_DD4HEP
      m_cmValues[sector.elementID()] = sector.cmSectorNoise().value() * m_gaussDist->shoot();
#else
      m_cmValues[sector.elementID()] = sector.cmSectorNoise() * m_gaussDist->shoot();
#endif
    }
  } );
}

double UTCMSimTool::noise( const LHCb::Detector::UT::ChannelID& chan, DeUTDetector const& det ) const {
  if ( !m_isInitialized ) initEvent( det );
  return m_cmValues[chan];
}
