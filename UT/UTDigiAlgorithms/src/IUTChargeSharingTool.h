/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IUTCHARGESHARINGTOOL_H
#define IUTCHARGESHARINGTOOL_H 1

#include "GaudiKernel/IAlgTool.h"

/** @class IUTChargeSharingTool IUTChargeSharingTool.h
 *
 *  Interface Class for charge sharing UT strip
 *
 *  @author M.Needham
 *  @date   14/3/2002
 */

struct IUTChargeSharingTool : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IUTChargeSharingTool, 1, 0 );

  /// calc sharing
  virtual double sharing( const double relDist ) const = 0;

  /// return thickness of the corresponding sensor
  virtual double thickness() const = 0;
};

#endif // IUTCHARGESHARINGTOOL_H
