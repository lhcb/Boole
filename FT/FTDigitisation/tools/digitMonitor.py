###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
if 'PYTHONSTARTUP' in os.environ:
    execfile(os.environ['PYTHONSTARTUP'])
from ROOT import *
from array import array
import os, sys
if len(sys.argv) < 2:
    print "Not enough arguments. Usage: "
    print "   python -i digitMonitor.py <root-file>"
    print
    sys.exit()
rootFile = sys.argv[1]

outputFile = "plots/DigitMonitor.pdf"
os.system("mkdir -p plots")

gROOT.ProcessLine(
    ".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v5r0/RootTools/LHCbStyle/src/lhcbStyle.C"
)
gStyle.SetPalette(1)
gStyle.SetMarkerSize(1.0)
gStyle.SetPadTopMargin(0.07)
gStyle.SetPadLeftMargin(0.20)
gStyle.SetPadRightMargin(0.11)
gStyle.SetTitleYOffset(1.4)
gStyle.SetPalette(kRainBow)
gStyle.SetNumberContours(999)

f = TFile(rootFile)


def histo(name):
    h = f.Get(name)
    if h.Class_Name() == "TObject":
        h = TH1D()
    return h


c1 = TCanvas("c1", "Electronics response", 300, 250)
hResponse = histo("MCFTDigitMonitor/ElectronicsResponse")
hResponse.SetFillStyle(1001)
hResponse.SetFillColor(kYellow + 1)
hResponse.Draw("hist")

c1.Modified()
c1.Update()
c1.SaveAs(outputFile + "(", "pdf")

c2 = TCanvas("c2", "Occupancy", 800, 600)
c2.Divide(2, 2)
c2.cd(1)
hnDigits = histo("MCFTDigitMonitor/nDigits")
hnSignalDigits = histo("MCFTDigitMonitor/nSignalDigits")
hnNoiseDigits = histo("MCFTDigitMonitor/nNoiseDigits")
hnSpillDigits = histo("MCFTDigitMonitor/nSpilloverDigits")
hnSignalDigits.SetLineColor(kGray + 1)
hnNoiseDigits.SetLineColor(kRed)
hnSpillDigits.SetLineColor(kBlue)

leg = TLegend(0.65, 0.60, 0.92, 0.92)
leg.SetMargin(.2)
leg.SetBorderSize(0)
leg.SetTextSize(.07)
leg.SetTextFont(132)
leg.SetLineStyle(0)
leg.SetFillColor(0)
leg.SetFillStyle(0)
leg.AddEntry(hnDigits, "All", "L")
leg.AddEntry(hnSignalDigits, "Signal", "L")
leg.AddEntry(hnSpillDigits, "Spillover", "L")
leg.AddEntry(hnNoiseDigits, "Noise", "L")

hnDigits.SetMaximum(
    1.05 * max(hnDigits.GetMaximum(), hnNoiseDigits.GetMaximum(),
               hnSignalDigits.GetMaximum(), hnSpillDigits.GetMaximum()))
hnDigits.Draw("hist")
hnNoiseDigits.Draw("hist same")
hnSignalDigits.Draw("hist same")
hnSpillDigits.Draw("hist same")
leg.Draw()

c2.cd(2)

hOccPseudoSignal = histo("MCFTDigitMonitor/Signal/DigitsPerPseudoChannel")
hOccPseudoSpill = histo("MCFTDigitMonitor/Spillover/DigitsPerPseudoChannel")
hOccPseudoNoise = histo("MCFTDigitMonitor/Noise/DigitsPerPseudoChannel")
hOccPseudo = histo("MCFTDigitMonitor/DigitsPerPseudoChannel")

nEvt = hnDigits.GetEntries()
hOccPseudo.Scale(1. / (nEvt * 48))
hOccPseudoSignal.Scale(1. / (nEvt * 48))
hOccPseudoNoise.Scale(1. / (nEvt * 48))
hOccPseudoSpill.Scale(1. / (nEvt * 48))

hOccPseudoSignal.SetLineColor(kGray + 1)
hOccPseudoSpill.SetLineColor(kBlue)
hOccPseudoNoise.SetLineColor(kRed)

hOccPseudo.SetMinimum(0)
hOccPseudo.Draw("hist")
hOccPseudoSignal.Draw("hist same")
hOccPseudoSpill.Draw("hist same")
hOccPseudoNoise.Draw("hist same")

c2.cd(3)

hXYSignal = histo("MCFTDigitMonitor/Signal/XYDistribution")
hXYSignal.Draw("colz")
latex = TLatex()
latex.DrawLatexNDC(0.2, 0.95, "Signal")

c2.cd(4)

hOccChanSignal = histo("MCFTDigitMonitor/Signal/DigitsPerChannel")
hOccChanSpill = histo("MCFTDigitMonitor/Spillover/DigitsPerChannel")
hOccChanNoise = histo("MCFTDigitMonitor/Noise/DigitsPerChannel")
hOccChan = histo("MCFTDigitMonitor/DigitsPerChannel")

hOccChanSignal.SetLineColor(kGray + 1)
hOccChanSpill.SetLineColor(kBlue)
hOccChanNoise.SetLineColor(kRed)

hOccChan.SetMinimum(0)
hOccChan.Draw("hist")
hOccChanNoise.Draw("hist same")
hOccChanSpill.Draw("hist same")
hOccChanSignal.Draw("hist same")

c2.Modified()
c2.Update()
c2.SaveAs(outputFile, "pdf")

c3 = TCanvas("c3", "Charge of digits", 800, 600)
c3.Divide(2, 2)

c3.cd(1)
hHitsPerDigitSignal = histo("MCFTDigitMonitor/Signal/nMCHitsPerDigit")
hHitsPerDigitSpill = histo("MCFTDigitMonitor/Spillover/nMCHitsPerDigit")
hHitsPerDigitSignal.SetLineColor(kGray + 1)
hHitsPerDigitSpill.SetLineColor(kBlue)
hHitsPerDigitSignal.Draw("hist")
hHitsPerDigitSpill.Draw("hist same")
leg.Draw()

c3.cd(2)
hPhotonsPerDigitSignal = histo("MCFTDigitMonitor/Signal/nPhotonsPerDigit")
hPhotonsPerDigitSpill = histo("MCFTDigitMonitor/Spillover/nPhotonsPerDigit")
hPhotonsPerDigitNoise = histo("MCFTDigitMonitor/Noise/nPhotonsPerDigit")
hPhotonsPerDigit = histo("MCFTDigitMonitor/nPhotonsPerDigit")
hPhotonsPerDigitSignal.SetLineColor(kGray + 1)
hPhotonsPerDigitSpill.SetLineColor(kBlue)
hPhotonsPerDigitNoise.SetLineColor(kRed)
hPhotonsPerDigit.Draw("hist")
hPhotonsPerDigitNoise.Draw("hist same")
hPhotonsPerDigitSignal.Draw("hist same")
hPhotonsPerDigitSpill.Draw("hist same")

c3.cd(3)
hPESignal = histo("MCFTDigitMonitor/Signal/nPhotoElectrons")
hPESpill = histo("MCFTDigitMonitor/Spillover/nPhotoElectrons")
hPENoise = histo("MCFTDigitMonitor/Noise/nPhotoElectrons")
hPE = histo("MCFTDigitMonitor/nPhotoElectrons")
hPESignal.SetLineColor(kGray + 1)
hPESpill.SetLineColor(kBlue)
hPENoise.SetLineColor(kRed)
hPE.Draw("hist")
hPENoise.Draw("hist same")
hPESignal.Draw("hist same")
hPESpill.Draw("hist same")

c3.cd(4)
hADCSignal = histo("MCFTDigitMonitor/Signal/ADCCounts")
hADCSpill = histo("MCFTDigitMonitor/Spillover/ADCCounts")
hADCNoise = histo("MCFTDigitMonitor/Noise/ADCCounts")
hADC = histo("MCFTDigitMonitor/ADCCounts")
hADCSignal.SetLineColor(kGray + 1)
hADCSpill.SetLineColor(kBlue)
hADCNoise.SetLineColor(kRed)
hADC.Draw("hist")
hADCSignal.Draw("hist same")
hADCSpill.Draw("hist same")
hADCNoise.Draw("hist same")

c3.Modified()
c3.Update()
c3.SaveAs(outputFile + ")", "pdf")
