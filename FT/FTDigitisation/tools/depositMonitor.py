###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
if 'PYTHONSTARTUP' in os.environ:
    execfile(os.environ['PYTHONSTARTUP'])
from ROOT import *
from array import array
import os, sys
if len(sys.argv) < 2:
    print "Not enough arguments. Usage: "
    print "   python -i depositMonitor.py <root-file>"
    print
    sys.exit()
rootFile = sys.argv[1]

outputFile = "plots/DepositMonitor.pdf"
os.system("mkdir -p plots")

gROOT.ProcessLine(
    ".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v5r0/RootTools/LHCbStyle/src/lhcbStyle.C"
)
gStyle.SetPalette(1)
gStyle.SetMarkerSize(1.0)
gStyle.SetPadTopMargin(0.07)
gStyle.SetPadLeftMargin(0.20)
gStyle.SetPadRightMargin(0.09)
gStyle.SetTitleYOffset(1.4)

f = TFile(rootFile)


def histo(name):
    h = f.Get(name)
    if h.Class_Name() == "TObject":
        h = TH1D()
    return h


c1 = TCanvas("c1", "Deposit Monitor", 800, 800)
c1.Divide(2, 3)
c1.cd(1)
hnDeposits = histo("MCFTDepositMonitor/nDeposits")
hnSignalDeposits = histo("MCFTDepositMonitor/nSignalDeposits")
hnNoiseDeposits = histo("MCFTDepositMonitor/nNoiseDeposits")
hnSpillDeposits = histo("MCFTDepositMonitor/nSpilloverDeposits")
hnSignalDeposits.SetLineColor(kGray + 2)
hnNoiseDeposits.SetLineColor(kRed)
hnSpillDeposits.SetLineColor(kBlue)

leg = TLegend(0.65, 0.55, 0.90, 0.90)
leg.SetMargin(.2)
leg.SetBorderSize(0)
leg.SetTextSize(.07)
leg.SetTextFont(132)
leg.SetLineStyle(0)
leg.SetFillColor(0)
leg.SetFillStyle(0)
leg.AddEntry(hnDeposits, "All", "L")
leg.AddEntry(hnSignalDeposits, "Signal", "L")
leg.AddEntry(hnSpillDeposits, "Spillover", "L")
leg.AddEntry(hnNoiseDeposits, "Noise", "L")

hnDeposits.Draw("hist")
hnNoiseDeposits.Draw("hist same")
hnSignalDeposits.Draw("hist same")
hnSpillDeposits.Draw("hist same")
leg.Draw()

c1.cd(2)

hOccPseudoSignal = histo(
    "MCFTDepositMonitor/SignalDirect/PhotonsPerPseudoChannel")
hOccPseudoSpill = histo("MCFTDepositMonitor/Spillover/PhotonsPerPseudoChannel")
hOccPseudoNoise = histo("MCFTDepositMonitor/Noise/PhotonsPerPseudoChannel")
hOccPseudo = histo("MCFTDepositMonitor/PhotonsPerPseudoChannel")

hOccPseudoSignal.SetLineColor(kGray + 2)
hOccPseudoSpill.SetLineColor(kBlue)
hOccPseudoNoise.SetLineColor(kRed)

hOccPseudo.SetMinimum(0)
hOccPseudo.Draw("hist")
hOccPseudoSpill.Draw("hist same")
hOccPseudoNoise.Draw("hist same")
hOccPseudoSignal.Draw("hist same")

c1.cd(3)

hOccChanSignal = histo("MCFTDepositMonitor/SignalDirect/PhotonsPerChannel")
hOccChanSpill = histo("MCFTDepositMonitor/Spillover/PhotonsPerChannel")
hOccChanNoise = histo("MCFTDepositMonitor/Noise/PhotonsPerChannel")
hOccChan = histo("MCFTDepositMonitor/PhotonsPerChannel")

hOccChanSignal.SetLineColor(kGray + 2)
hOccChanSpill.SetLineColor(kBlue)
hOccChanNoise.SetLineColor(kRed)

hOccChan.SetMinimum(0)
hOccChan.Draw("hist")
hOccChanNoise.Draw("hist same")
hOccChanSpill.Draw("hist same")
hOccChanSignal.Draw("hist same")

c1.cd(4)
hTimePerPhotonSignal = histo(
    "MCFTDepositMonitor/SignalDirect/ArrivalTimePerPhoton")
hTimePerPhotonRefl = histo(
    "MCFTDepositMonitor/SignalRefl/ArrivalTimePerPhoton")
hTimePerPhotonSpill = histo(
    "MCFTDepositMonitor/Spillover/ArrivalTimePerPhoton")
hTimePerPhotonNoise = histo("MCFTDepositMonitor/Noise/ArrivalTimePerPhoton")
hTimePerPhoton = histo("MCFTDepositMonitor/ArrivalTimePerPhoton")
hTimePerPhotonSignal.SetLineColor(kGray + 2)
hTimePerPhotonRefl.SetLineColor(kGray)
hTimePerPhotonSpill.SetLineColor(kBlue)
hTimePerPhotonNoise.SetLineColor(kRed)
hTimePerPhoton.SetMinimum(0.0)
hTimePerPhoton.Draw("hist")
hTimePerPhotonNoise.Draw("hist same")
hTimePerPhotonSignal.Draw("hist same")
hTimePerPhotonRefl.Draw("hist same")
hTimePerPhotonSpill.Draw("hist same")

leg2 = leg.Clone("leg2")
leg2.AddEntry(hTimePerPhotonRefl, "Reflection", "L")
leg2.Draw()

c1.cd(5)

hPhotonsPerContrSignal = histo(
    "MCFTDepositMonitor/SignalDirect/nPhotonsPerDeposit")
hPhotonsPerContrRefl = histo(
    "MCFTDepositMonitor/SignalRefl/nPhotonsPerDeposit")
hPhotonsPerContrSpill = histo(
    "MCFTDepositMonitor/Spillover/nPhotonsPerDeposit")
hPhotonsPerContrNoise = histo("MCFTDepositMonitor/Noise/nPhotonsPerDeposit")
hPhotonsPerContr = histo("MCFTDepositMonitor/nPhotonsPerDeposit")
hPhotonsPerContrSignal.SetLineColor(kGray + 2)
hPhotonsPerContrSpill.SetLineColor(kBlue)
hPhotonsPerContrNoise.SetLineColor(kRed)
hPhotonsPerContr.Draw("hist")
hPhotonsPerContrNoise.Draw("hist same")
hPhotonsPerContrSignal.Draw("hist same")
hPhotonsPerContrSpill.Draw("hist same")

c1.cd(6)
gPad.SetLogy()

hPhotonsPerContr.Draw("hist")
hPhotonsPerContrNoise.Draw("hist same")
hPhotonsPerContrSignal.Draw("hist same")
hPhotonsPerContrSpill.Draw("hist same")

c1.Modified()
c1.Update()
c1.SaveAs(outputFile, "pdf")
