#!/bin/env/python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

################################################
# Script to create the SiPM Response function
#  from a raw testbeam data file
# by Jacco de Vries [03-11-2014]
###############################################

from ROOT import TH1F, TCanvas, TGraph, TLine, TSpline3, gStyle
from array import array

gStyle.SetOptStat(0)


def getCSV(filename, phe, ptype):
    f = open(filename, "r")
    arr = []
    for line in f:
        if line != "\n":
            arr += [line.split(",")]

    # identify columns of importance
    pheName = "eventN"
    if (ptype == "pacific3"):
        timeName = "arrival"
        spillName = "BX0"
        nextName = "BX1"
        prevName = "BXm1"
    if (ptype == "pacific4"):
        timeName = "tarrival"
        spillName = "TH_meas_BX0"
        nextName = "BX1"
        prevName = "BX-1"

    icolPhe = arr[0].index(pheName)
    icolT = arr[0].index(timeName)
    icolBX0 = arr[0].index(spillName)
    icolBX1 = arr[0].index(nextName)
    icolBXm1 = arr[0].index(prevName)

    # extract lists
    colPhe = [arr[i][icolPhe] for i in range(len(arr))]
    colT = [arr[i][icolT] for i in range(len(arr))]
    colBX0 = [arr[i][icolBX0] for i in range(len(arr))]
    colBX1 = [arr[i][icolBX1] for i in range(len(arr))]
    colBXm1 = [arr[i][icolBXm1] for i in range(len(arr))]

    # get correct range for one #phe shape
    phe = str(phe)  # reference peak for number of photo-electrons
    first = colPhe.index(phe)
    colPhe.reverse()
    last = len(colPhe) - colPhe.index(phe)
    colT = colT[first:last]
    colBX0 = colBX0[first:last]
    colBX1 = colBX1[first:last]
    colBXm1 = colBXm1[first:last]

    # convert to numbers
    for col in [colT, colBX0, colBX1, colBXm1]:
        for i in range(len(col)):
            col[i] = float(col[i])

    # convert to ns
    colT = [round(time * 1e9, 2) for time in colT]

    returnlist = [colT, colBX0, colBX1, colBXm1]
    print "==> raw (%s, phe=%s)" % (ptype, phe)
    print returnlist
    return [colT, colBX0, colBX1, colBXm1]


# read points from file
ptype = "pacific4"
phe = "3"
if (ptype == "pacific3"): filename = "PACIFICr3_TH_tarrival.csv"
if (ptype == "pacific4"): filename = "PACIFICr4_TH_tarrival.csv"
[times, colBX0, colBX1, colBXm1] = getCSV(filename, phe, ptype)

# shift spill times
xlistBX0 = times
xlistBX1 = [time - 25. for time in times]
xlistBXm1 = [time + 25. for time in times]

# draw
grBX0 = TGraph(len(xlistBX0), array("d", xlistBX0), array("d", colBX0))
grBX1 = TGraph(len(xlistBX1), array("d", xlistBX1), array("d", colBX1))
grBXm1 = TGraph(len(xlistBXm1), array("d", xlistBXm1), array("d", colBXm1))

grBX0.SetLineColor(46)
grBX0.SetMarkerColor(46)
grBX1.SetLineColor(9)
grBX1.SetMarkerColor(9)
grBXm1.SetLineColor(8)
grBXm1.SetMarkerColor(8)

xmin = -30.
#xmax = 60.
xmax = 100.

# for the frame
h1 = TH1F("h1",
          "Electronics response (%s, phe=%s); time [ns]; value" % (ptype, phe),
          100, xmin, xmax)
h1.GetYaxis().SetRangeUser(1.1 * min(colBX0 + colBX1 + colBXm1),
                           1.1 * max(colBX0 + colBX1 + colBXm1))

line = TLine(xmin, 0, xmax, 0)
line.SetLineColor(4)

c1 = TCanvas("c1", "Raw pulse shape")
h1.Draw()
line.Draw("same")
grBX0.Draw("*Lsame")
grBX1.Draw("*Lsame")
grBXm1.Draw("*Lsame")
c1.Update()

# average points in multiple spills
xlist = []
pulseShape = []
for time in (xlistBX1 + xlistBX0 + xlistBXm1):
    if not time in xlist:
        xlist += [time]
        measurements = []
        if (time in xlistBX1): measurements += [colBX1[xlistBX1.index(time)]]
        if (time in xlistBX0): measurements += [colBX0[xlistBX0.index(time)]]
        if (time in xlistBXm1):
            measurements += [colBXm1[xlistBXm1.index(time)]]
        avg = sum(measurements) / len(measurements)
        pulseShape += [avg]
        print str(measurements) + " --> " + str(avg)

# normalize (comment out for combination comparison)
maxY = max(pulseShape)
for i in range(len(pulseShape)):
    pulseShape[i] /= maxY
h1.GetYaxis().SetRangeUser(-0.2, 1.1)

# shift
t0 = 0.
xlist = [time + t0 for time in xlist]

# reverse x-axis
pulseShape.reverse()

# smooth the function
smoothF = 1
pulseShapeSmooth = [
    sum(pulseShape[i * smoothF:i * smoothF + smoothF]) / smoothF
    for i in range(len(pulseShape) / smoothF)
]
xlistSmooth = [
    sum(xlist[i * smoothF:i * smoothF + smoothF]) / smoothF
    for i in range(len(xlist) / smoothF)
]
grSmooth = TGraph(
    len(xlistSmooth), array("d", xlistSmooth), array("d", pulseShapeSmooth))
#grSmooth.Draw("Lsame")
spline = TSpline3("spline", grSmooth)
spline.SetLineColor(1)
spline.Draw("same")

c1.Update()
c1.SaveAs("%s_response_%s.pdf" % (ptype, phe))

# print vector for SiPMResponse.cpp
print ""
print "This is the shape to use normally!"
print "=================================="
print "SiPM Integrator pulse height for SiPMResponse.cpp:",
for i, entry in enumerate(pulseShapeSmooth):
    if (i % 8 == 0): print ""
    print "%6.3f," % (entry),
print
print
print "SiPM Integrator pulse times for SiPMResponse.cpp:",
for i, entry in enumerate(xlistSmooth):
    if (i % 8 == 0): print ""
    print "%6.2f," % (entry),
print ""

## LOAD the old shape

Tprev = [
    3.22188688e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05,
    3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05,
    3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05,
    3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05,
    3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05,
    3.22145863e-05, 3.22145863e-05, 3.22145863e-05, 3.22145863e-05,
    3.22145863e-05, 3.22145863e-05
]
Tsig = [
    2.88048286e-02, 2.76166647e-02, 2.84853231e-02, 2.86509678e-02,
    2.87964850e-02, 2.90245415e-02, 2.92078012e-02, 2.93951976e-02,
    2.95711670e-02, 2.96709382e-02, 2.97279647e-02, 2.96909134e-02,
    2.94095934e-02, 2.89146629e-02, 2.80610778e-02, 2.67112946e-02,
    2.47312816e-02, 2.20768793e-02, 1.86479251e-02, 1.44897527e-02,
    9.82891757e-03, 5.23039367e-03, 1.56929340e-03, -2.17283810e-04,
    -1.82921036e-05, 6.66654783e-05
]
Tnext = \
  [-0.00136548, -0.00089388, -0.0012617,  -0.00137867, -0.00145512, -0.00160822,
   -0.00173438, -0.00187304, -0.00201678, -0.00213902, -0.0022567,  -0.00234672,
   -0.0023399, -0.00226133, -0.00202669, -0.00154593, -0.00067318,  0.00060853,
   0.00250173,  0.00515046,  0.00876429,  0.01327495,  0.01883291,  0.02372474,
   0.02788753,  0.0281931 ]

Tnext2 = \
  [-0.00034101, -0.00025578, -0.00030324, -0.00031672, -0.00032477, -0.00034131,
   -0.00035811, -0.00037248, -0.00038796, -0.00040745, -0.00042273, -0.00044454,
   -0.00046555, -0.00048982, -0.00051403, -0.00054482, -0.00056937, -0.00060258,
   -0.00064924, -0.00068913, -0.00070694, -0.00073142, -0.00099868, -0.00083826,
   -0.00119224, -0.00104176]

Tprev.reverse()
Tsig.reverse()
Tnext.reverse()
Tnext2.reverse()

xSig = [i for i in range(26)]
xPrev = [i for i in range(-25, 1)]
xNext = [i for i in range(25, 51)]
xNext2 = [i for i in range(50, 76)]

grSig = TGraph(len(xSig), array("d", xSig), array("d", Tsig))
grPrev = TGraph(len(xPrev), array("d", xPrev), array("d", Tprev))
grNext = TGraph(len(xNext), array("d", xNext), array("d", Tnext))
grNext2 = TGraph(len(xNext2), array("d", xNext2), array("d", Tnext2))

pulseShapeOld = [Tprev[i] - Tprev[0] for i in range(26)]
pulseShapeOld.extend(
    [Tsig[i] - Tsig[0] + pulseShapeOld[25] for i in range(1, 26)])
pulseShapeOld.extend(
    [Tnext[i] - Tnext[0] + pulseShapeOld[50] for i in range(1, 26)])
pulseShapeOld.extend(
    [Tnext2[i] - Tnext2[0] + pulseShapeOld[75] for i in range(1, 26)])

c3 = TCanvas("c3", "c3", 700, 500)
h1.DrawCopy()
x = [i for i in range(-25, 76)]

maxY = max(pulseShapeOld)
for i in range(len(pulseShapeOld)):
    pulseShapeOld[i] /= maxY

gr = TGraph(len(x), array("d", x), array("d", pulseShapeOld))
gr.Draw("C")

# Draw the new shape on top of the old one

maxOld = pulseShapeOld.index(max(pulseShapeOld))
maxNew = pulseShapeSmooth.index(max(pulseShapeSmooth))
tshift = xlistSmooth[maxNew] - x[maxOld]
#tshift -= 2  # manual shift, to make the integrals overlap by eye
tshift = 9
print "==> tshift = %s" % (tshift)
for i in range(len(xlistSmooth)):
    xlistSmooth[i] += tshift

grSmooth2 = TGraph(
    len(xlistSmooth), array("d", xlistSmooth), array("d", pulseShapeSmooth))
spline2 = TSpline3("spline2", grSmooth2)
spline2.SetLineColor(8)  #8=pacific4(green), 9=pacific3(blue)
spline2.SetLineWidth(2)
spline2.Draw("same")

c3.Modified()
c3.Update()
c3.SaveAs("%s_responseComp_%s.pdf" % (ptype, phe))

# Print f(t) = 1, for SciFi Testbeam comparison
#print ""
#print "Here is the shape for f(t) = 1:"
#print "=================================="
#print "SiPM Integrator pulse height for SiPMResponse.cpp:"
#for i,entry in enumerate(pulseShapeSmooth):
#  if(i%5==0) : print ""
#  print "(" + "1." + ")",
