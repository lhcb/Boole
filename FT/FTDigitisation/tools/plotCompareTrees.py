###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import os, re
import ROOT


def compare(input_ref_file, input_improved_file, output_directory, histo_name,
            variable):
    """Creates a histogram to compare the histogram of name histo_name between the reference file and
	the file generated with new version of the improved simulation"""

    ref_file = ROOT.TFile(input_ref_file)
    improved_file = ROOT.TFile(input_improved_file)

    ref_histo = ref_file.Get(histo_name)
    improved_histo = improved_file.Get(histo_name)

    ref_histo.SetLineColor(4)
    ref_histo.SetLineWidth(2)

    improved_histo.SetLineColor(2)
    improved_histo.SetLineWidth(2)

    c1 = ROOT.TCanvas("c1", "Plots", 0, 0, 1200, 650)

    hs = ROOT.THStack('hs', '')
    hs.Add(ref_histo)
    hs.Add(improved_histo)
    hs.Draw('nostack')

    hs.GetXaxis().SetTitle(variable)
    hs.GetXaxis().SetTitleSize(0.05)
    hs.GetXaxis().SetTitleOffset(0.8)
    bin = 700 / 100
    binstr = "%.2f" % bin

    hs.GetYaxis().SetTitle('A.U. / (' + binstr + ')')
    hs.GetYaxis().SetTitleOffset(1.1)
    hs.GetYaxis().SetTitleSize(0.045)

    #Creates the legend
    leg = ROOT.TLegend(0.75, 0.75, 0.98, 0.98)
    leg.SetFillColor(10)

    leg.AddEntry(ref_histo, "Ref", "l")
    leg.AddEntry(improved_histo, "Improved", "l")
    leg.Draw('same')

    #	c1.SaveAs('compare_RefImproved_'+variable+'.root')
    c1.SaveAs(output_directory + '/compare_RefImproved_' + variable + '.root')
    c1.SaveAs(output_directory + '/compare_RefImproved_' + variable + '.eps')


if __name__ == '__main__':

    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument(
        'input_improved_file',
        type=str,
        help='Name of your input improved ROOT file')
    parser.add_argument(
        'output_directory',
        type=str,
        help='Base output name',
        default=os.getcwd())
    args = parser.parse_args()

    ref_file = '/home/vbellee/ImprovedBoole2017_01_30/BooleDev_v31r0/MiniBias_v20_nu25_EC-10ev-histos.root'

    configs = [
        ['MCFTDepositMonitor/ContributionsPerSiPM', 'ContributionsPerSiPM'],
        ['MCFTDepositMonitor/nDeposits', 'nDeposits'],
        ['MCFTDepositMonitor/nSignalDeposits', 'nSignalDeposits'],
        ['MCFTDepositMonitor/nSpilloverDeposits', 'nSpilloverDeposits'],
        #	       ['MCFTDepositMonitor/nNoiseDeposits','nNoiseDeposits'],
        [
            'MCFTDepositMonitor/SignalDirect/nPhotonsPerContribution',
            'SignalDirectnPhotonsPerContribution'
        ],
        [
            'MCFTDepositMonitor/SignalDirect/ArrivalTimePerContribution',
            'SignalDirectnArrivalTimePerContribution'
        ],
        [
            'MCFTDepositMonitor/SignalDirect/ContributionsPerStation',
            'SignalDirectContributionsPerStation'
        ],
        [
            'MCFTDepositMonitor/SignalDirect/ContributionsPerModule',
            'SignalDirectContributionsPerModule'
        ],
        [
            'MCFTDepositMonitor/SignalDirect/ContributionsPerChannel',
            'SignalDirectContributionsPerChannel'
        ],
        [
            'MCFTDepositMonitor/SignalDirect/ContributionsPerPseudoChannel',
            'SignalDirectContributionsPerPseudoChannel'
        ],
        [
            'MCFTDepositMonitor/SignalRefl/nPhotonsPerContribution',
            'SignalReflnPhotonsPerContribution'
        ],
        [
            'MCFTDepositMonitor/SignalRefl/ArrivalTimePerContribution',
            'SignalReflnArrivalTimePerContribution'
        ],
        [
            'MCFTDepositMonitor/SignalRefl/ContributionsPerStation',
            'SignalReflContributionsPerStation'
        ],
        [
            'MCFTDepositMonitor/SignalRefl/ContributionsPerModule',
            'SignalReflContributionsPerModule'
        ],
        [
            'MCFTDepositMonitor/SignalRefl/ContributionsPerChannel',
            'SignalReflContributionsPerChannel'
        ],
        [
            'MCFTDepositMonitor/SignalRefl/ContributionsPerPseudoChannel',
            'SignalReflContributionsPerPseudoChannel'
        ],
        #	       ['MCFTDepositMonitor/Noise/nPhotonsPerContribution','NoisenPhotonsPerContribution'],
        #	       ['MCFTDepositMonitor/Noise/ArrivalTimePerContribution','NoisenArrivalTimePerContribution'],
        #	       ['MCFTDepositMonitor/Noise/ContributionsPerStation','NoiseContributionsPerStation'],
        #	       ['MCFTDepositMonitor/Noise/ContributionsPerModule','NoiseContributionsPerModule'],
        #	       ['MCFTDepositMonitor/Noise/ContributionsPerChannel','NoiseContributionsPerChannel'],
        #	       ['MCFTDepositMonitor/Noise/ContributionsPerPseudoChannel','NoiseContributionsPerPseudoChannel'],
        ['FTClusterMonitor/FullClusterCharge', 'FullClusterCharge'],
        ['FTClusterMonitor/FullClusterSize', 'FullClusterSize'],
        [
            'FTClusterMonitor/Signal/FullClusterCharge',
            'SignalFullClusterCharge'
        ],
        ['FTClusterMonitor/Signal/FullClusterSize', 'SignalFullClusterSize'],
        #	       ['FTClusterMonitor/Noise/FullClusterCharge','NoiseFullClusterCharge'],
        #	       ['FTClusterMonitor/Noise/FullClusterSize','NoiseFullClusterSize']
    ]

    for config in configs:
        compare(ref_file, args.input_improved_file, args.output_directory,
                config[0], config[1])
