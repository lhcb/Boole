###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Number of thermal noise clusters w/o cross talk
nEvts = 50.  # 50=number of evts used for the data (needed for the error)
pXtalk = 0.0
pDelayedXtalk = 0.0
noiseData = []
noiseData.append([0.44, 10.84, 1.60, 0.00])  #  8 MHz
noiseData.append([3.24, 71.90, 14.24, 0.44])  # 12 MHz
noiseData.append([12.48, 253.16, 64.56, 3.18])  # 16 MHz
noiseData.append([37.40, 653.42, 194.12, 15.76])  # 20 MHz
noiseData.append([86.88, 1357.94, 466.58, 54.10])  # 24 MHz
noiseData.append([172.92, 2411.02, 947.38, 148.64])  # 28 MHz
