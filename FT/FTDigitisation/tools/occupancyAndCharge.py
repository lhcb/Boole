###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
if 'PYTHONSTARTUP' in os.environ:
    execfile(os.environ['PYTHONSTARTUP'])
from ROOT import *
from array import array
import os, sys
if len(sys.argv) < 2:
    print "Not enough arguments. Usage: "
    print "   python -i occupancyAndCharge.py <root-file>"
    print
    sys.exit()
rootFile = sys.argv[1]
os.system("mkdir -p plots")
gROOT.ProcessLine(
    ".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v5r0/RootTools/LHCbStyle/src/lhcbStyle.C"
)
f = TFile(rootFile)


def histo(name):
    h = f.Get(name)
    if h.Class_Name() == "TObject":
        h = TH1D()
    return h


# Create occupancy profile function
occProf = TF1(
    "occProf", "[0]*([1]/12288 + [2]/512*(x<512) + "
    "(1.0-[1]-[2])/(exp(-512/[3])-exp(-12288/[3]))/[3]*"
    "exp(-x/[3])*(x>=512))", 0.0, 1.0e4)
occProf.SetParName(0, "#it{N}")
occProf.SetParName(1, "#it{f}_{0}")
occProf.SetParName(2, "#it{f}_{1}")
occProf.SetParName(3, "#alpha")
occProf.SetParameters(1e7, 0.413, 0.059, 2400)
occProf.SetLineColor(kRed)

# Plot the cluster occupancy
c1 = TCanvas("c1", "Cluster occupancy", 800, 600)

hOccPseudo = histo("FTClusterMonitor/Signal/LiteClustersPerPseudoChannel")
hOccPseudo.SetLineColor(kBlue)

hOccPseudo.SetMinimum(0)
hOccPseudo.Draw("hist")
hOccPseudo.Fit("occProf", "", "", 0, 10000)
occProf.Draw("same")

latex = TLatex()
latex.SetTextSize(0.05)
latex.DrawLatexNDC(0.55, 0.88, "#it{f}_{0} = %5.3f #pm %4.3f" % \
                   (occProf.GetParameter(1), occProf.GetParError(1)) )
latex.DrawLatexNDC(0.55, 0.82, "#it{f}_{1} = %5.3f #pm %4.3f" % \
                   (occProf.GetParameter(2), occProf.GetParError(2)) )
latex.DrawLatexNDC(0.55, 0.76, "#alpha = %4.0f #pm %2.0f" % \
                   (occProf.GetParameter(3), occProf.GetParError(3)) )

c1.Modified()
c1.Update()
c1.SaveAs("plots/occupancy.pdf")
c1.SaveAs("plots/occupancy.png")

print "Number of clusters ", hOccPseudo.GetEntries()

# Plot the cluster charge
c2 = TCanvas("c2", "Cluster charge", 800, 600)
c2.SetLogy()

hClusCharge = histo("FTClusterMonitor/Signal/FullClusterCharge")
hClusCharge.SetLineColor(kBlue)

# Create the Landau function
lan = TF1("lan", "[0]*TMath::Landau(x, [1], [2])", 1, 100)
lan.SetParameters(5e4, 13.0, 2.5)

hClusCharge.Draw("hist")
hClusCharge.Fit("lan", "", "", 15, 100)
lan.SetLineColor(kRed)
lan.Draw("same")

ratio = lan.Integral(0, 100) / hClusCharge.Integral(0, 100)

latex.DrawLatexNDC(0.55, 0.87, "MPV = %5.2f #pm %3.2f pe" % \
                   (lan.GetParameter(1), lan.GetParError(1)) )
latex.DrawLatexNDC(0.55, 0.82, "#sigma       = %6.2f #pm %3.2f pe" % \
                   (lan.GetParameter(2), lan.GetParError(2)) )
latex.DrawLatexNDC(0.55, 0.77, "ratio  = %4.2f " % ratio)

c2.Modified()
c2.Update()
c2.SaveAs("plots/charge.pdf")
c2.SaveAs("plots/charge.png")
