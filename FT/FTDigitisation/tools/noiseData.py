###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Number of thermal noise clusters w/ 3.3% cross talk
nEvts = 50.  # 50=number of evts used for the data (needed for the error)
pXtalk = 0.033
pDelayedXtalk = 0.027
noiseData = []
noiseData.append([7.34, 40.50, 4.38, 0.08])  #  8 MHz
noiseData.append([25.74, 179.84, 29.46, 1.18])  # 12 MHz
noiseData.append([66.80, 517.88, 115.90, 7.80])  # 16 MHz
noiseData.append([143.50, 1157.14, 315.40, 30.66])  # 20 MHz
noiseData.append([265.08, 2157.92, 694.12, 94.52])  # 24 MHz
noiseData.append([452.38, 3580.24, 1304.90, 231.02])  # 28 MHz
