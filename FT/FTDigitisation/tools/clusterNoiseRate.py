###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Standalone script to estimate number of thermal noise clusters using the
# effective noise simulation. This script can also fit the data points that
# are obtained from the full (i.e. slow) noise simulation.
import os
os.system("mkdir -p plots")
if 'PYTHONSTARTUP' in os.environ:
    execfile(os.environ['PYTHONSTARTUP'])
from ROOT import *
from math import *
from array import array
gROOT.ProcessLine(
    ".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v5r0/RootTools/LHCbStyle/src/lhcbStyle.C"
)

# Get the data
from noiseData import *
#from noiseDataNoXtalk import *
#from noiseDataNoDelayedXtalk import *

# Settings and data
nChan = 524288  # number of channels in FT
N = 104  # number of pixels per channel
logPlot = True  # Show log plot


# Binomial distribution for the number of dark photons (n) in N pixels
# (can also use a Poission, since N is relatively large)
def probHits(n, p):
    #return factorial(N)/(factorial(N-n)*factorial(n))*(p/N)**n*(1.-p/N)**(N-n)
    return exp(-p) * p**n / factorial(n)


# Binomial distribution for the number of x-talk photons (n) in N direct photons
def probXtalk(N, n, p):
    return factorial(N)/(factorial(N-n)*factorial(n)) * \
           (p)**n * (1.-p)**(N-n)


# Probability that Gaussian smearing (width s) gives value above x
def intGauss(x, s):
    #if x > 0.0 : return 1.0
    #else : return 0.0
    return 0.5 * erf(-x / (sqrt(2) * s)) + 0.5


# sigmaGain is width of gain variation, sigmaNoise is width of channel noise
def clusterSizeDistro(p, sigmaGain, sigmaNoise, pXT):
    # Calculate prob for single channel > 4.5 pe
    probSingle = 0.0
    for i in range(1, 10):  # i is the number of dark photons
        sigma = sqrt((i * sigmaGain)**2 + sigmaNoise**2)
        prob = 0.0
        for j in range(i + 1):  # j is number of Xtalk hits
            prob += probHits(i, p) * intGauss(4.5 - float(i + j),
                                              sigma) * probXtalk(i, j, pXT)
        probSingle += prob
        #print "Prob ", i, " photons = ", prob
    #print "Total probability channel > 4.5 pe:", probSingle

    # Calculate prob for single channel > 2.5 pe
    probSeed = 0.0
    for i in range(1, 10):
        sigma = sqrt((i * sigmaGain)**2 + sigmaNoise**2)
        prob = 0.0
        for j in range(i + 1):  # j is number of Xtalk hits
            prob += probHits(i, p) * intGauss(2.5 - float(i + j),
                                              sigma) * probXtalk(i, j, pXT)
        probSeed += prob
        #print "Prob ", i, " photons = ", prob
    #print "Total probability channel > 2.5 pe:", probSeed

    # Calculate prob for single channel > 1.5 pe
    probAdd = 0.0
    for i in range(1, 10):
        sigma = sqrt((i * sigmaGain)**2 + sigmaNoise**2)
        prob = 0.0
        for j in range(i + 1):  # j is number of Xtalk hits
            prob += probHits(i, p) * intGauss(1.5 - float(i + j),
                                              sigma) * probXtalk(i, j, pXT)
        probAdd += prob
        #print "Prob ", i, " photons = ", prob
    #print "Total probability channel > 1.5 pe:", probAdd

    nClus4 = nChan * probAdd**4
    nClus3 = nChan * probAdd**3 * (1. - probAdd)**2
    nClus2 = 2. * nChan * probSeed * probAdd * (1. - probAdd)**2
    nClus1 = nChan * probSingle * (1. - probAdd)**2
    nClus = nClus1 + nClus2 + nClus3 + nClus4
    return nClus, nClus1, nClus2, nClus3, nClus4


# Generate an example case for p=0.35 (equals pDCR=14 MHz)
nClus, nClus1, nClus2, nClus3, nClus4 = clusterSizeDistro(
    0.35 * 0.8737, 0.05, 0.1, pXtalk)
print
print "Example: number of clusters for pDCR=14 MHz"
print "--------------------------------------------------"
print "Total number of noise clusters:          ", nClus
print "Total number of 1-channel noise clusters:", nClus1
print "Total number of 2-channel noise clusters:", nClus2
print "Total number of 3-channel noise clusters:", nClus3
print "Total number of 4-channel noise clusters:", nClus4
print
print
print "Now fit the data points"
print

# Fill the histogram
h2 = TH2D("h2", "h2;pDCR [MHz];Cluster size;Noise clusters", 6, 6, 30, 4, 0.5,
          4.5)
for i in range(0, 6):
    for j in range(0, 4):
        h2.SetBinContent(i + 1, j + 1, noiseData[i][j])
        h2.SetBinError(i + 1, j + 1, sqrt(noiseData[i][j] / nEvts))


def sizeFun2D(x, p):
    if (p[0] * x[0] < 0. or p[1] < 0. or x[1] < 0.5 or x[1] >= 4.5): return 0.
    sizes = clusterSizeDistro(p[0] * x[0] / 40., p[1], p[2],
                              pXtalk + p[3] * pDelayedXtalk)
    return sizes[int(round(x[1]))]


def sizeFun1D(x, p):
    if (p[0] * x[0] < 0. or p[1] < 0.): return 0.
    sizes = clusterSizeDistro(p[0] * x[0] / 40., p[1], p[2],
                              pXtalk + p[3] * pDelayedXtalk)
    return sizes[int(round(p[4]))]


clusFun2D = TF2("clusFun2D", sizeFun2D, 6, 32, 0.5, 4.5, 4)
clusFun2D.SetParName(0, "OverlapProb")
clusFun2D.SetParName(1, "SigmaGain")
clusFun2D.SetParName(2, "SigmaNoise")
clusFun2D.SetParName(3, "DelayedXTalkScale")
clusFun2D.FixParameter(0, 0.8737)  # effective probability for 2 hits in time
clusFun2D.FixParameter(1, 0.05)  # gain
clusFun2D.FixParameter(2, 0.10)  # noise
clusFun2D.FixParameter(3, 0.577)  # scale factor for delayed cross talk

c1 = TCanvas("c1", "c1", 600, 500)
if logPlot: c1.SetLogy()
h2.Fit("clusFun2D", "", "Q0")

clusFuns = []
for i in range(0, 5):
    clusFuns.append(TF1("clusFuns" + str(i), sizeFun1D, 6, 32, 5))
    clusFuns[i].SetParameters(
        clusFun2D.GetParameter(0), clusFun2D.GetParameter(1),
        clusFun2D.GetParameter(2), clusFun2D.GetParameter(3), i)

leg = TLegend(0.2, 0.65, 0.47, 0.92)
leg.SetMargin(.3)
leg.SetBorderSize(0)
leg.SetTextSize(.05)
leg.SetTextFont(132)
leg.SetLineStyle(0)
leg.SetFillColor(0)
leg.SetFillStyle(0)

proj = h2.ProjectionX("proj")
proj.SetMinimum(1.0)
proj.GetYaxis().SetTitle("Noise clusters")
proj.SetTitleOffset(1.15, "y")
if logPlot: proj.SetMaximum(proj.GetMaximum() * 5.)
proj.DrawCopy("e1 x0")
clusFuns[0].Draw("same")
colors = [kRed, kBlue, kGreen + 1, kOrange, kPink]
for i in range(1, 5):
    proj = h2.ProjectionX("proj" + str(i), i, i)
    proj.SetMarkerColor(colors[i])
    proj.SetLineColor(colors[i])
    proj.DrawCopy("same e1 x0")
    clusFuns[i].SetLineColor(colors[i])
    clusFuns[i].Draw("same")
    leg.AddEntry(proj, "size-" + str(i), "PL")
leg.Draw()

latex = TLatex()
latex.SetTextSize(0.05)
latex.DrawLatexNDC(0.42, 0.89, "Direct X-talk = %3.1f%%" % (100 * pXtalk))
latex.DrawLatexNDC(0.42, 0.84,
                   "Delayed X-talk = %3.1f%%" % (100 * pDelayedXtalk))

logText = ""
if logPlot: logText = "log"

c1.Modified()
c1.Update()
c1.SaveAs("plots/noiseRate_Xtalk%3.1fand%3.1f" % \
          (pXtalk*100, pDelayedXtalk*100) + logText + ".pdf")
c1.SaveAs("plots/noiseRate_Xtalk%3.1fand%3.1f" % \
          (pXtalk*100, pDelayedXtalk*100) + logText + ".png")
