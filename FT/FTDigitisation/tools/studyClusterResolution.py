#!/bin/env/python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

################################################
# Fits different distributions to the output of a given FTClusterTuple algorithm (in FT/FTDigitisation).
# Usage: lb-run ROOT python studyClusterResolution.py
# by Louis Henry [10-12-2019]
###############################################

from ROOT import TFile, TTree, TChain, TCanvas, gStyle, gROOT, TLegend, TH1F, TF1, TH2F, TF1NormSum, TGraph, TH3F
from ROOT import RooHist, RooPlot, RooRealVar, RooAddPdf, RooGaussian
from ROOT import TPaveText

import math
gROOT.SetBatch(True)
gStyle.SetOptStat(0)

limits = 1.5


def setFromRes(func, res):
    for i in range(len(res.Parameters())):
        func.SetParameter(i, res.Parameter(i))


def makeDoubleGaussian():
    """
    For sizes 4 and 5, a core and a tail with a factor between the widths.
    """
    func = TF1(
        "func",
        "[3]/sqrt(2*TMath::Pi())/[1]*([0]*exp(-x*x/2./[1]/[1])+(1.-[0])*exp(-x*x/2./[1]/[1]/[2]/[2])/[2])",
        -limits, limits)
    func.SetParLimits(0, 0.0, 1.)
    func.SetParLimits(1, 0.05, 0.12)
    func.SetParLimits(2, 1.01, 20.)
    func.SetParameter(2, 3.)
    func.SetParameter(3, 0.02)
    func.SetLineColor(1)
    return func


def makeSeveralGaussians(size):
    """
    Makes a double Gaussian as well as a pair of off-center Gaussians that describe shifted peaks.
    """
    func = TF1(
        "func",
        "[3]/sqrt(2*TMath::Pi())/[1]*([4]*([0]*exp(-x*x/2./[1]/[1])+(1.-[0])*exp(-x*x/2./[1]/[1]/[2]/[2])/[2]) + (1.-[4])*(exp(-(x-[5])*(x-[5])/2./[1]/[1]/[6]/[6])+exp(-(x+[5])*(x+[5])/2./[1]/[1]/[6]/[6]))/2./[6])",
        -limits, limits)
    func.SetParLimits(0, 0.01, 0.99)
    func.SetParLimits(1, 0.03, 0.50)
    func.SetParLimits(2, 1.01, 20.)
    func.SetParameter(2, 3.)
    func.SetParameter(3, 0.02)
    func.SetParameter(4, 0.60)
    func.SetParLimits(4, 0.01, 0.99)
    if size == "5":
        func.SetParameter(4, 0.90)
        func.SetParameter(5, 0.26)
    if size == "6":
        func.SetParameter(4, 0.90)
        func.SetParameter(5, 0.35)
    if size == "7":
        func.SetParameter(5, 0.4)
    if size == "8":
        func.SetParameter(5, 0.4)
    func.SetParLimits(5, 0., 1.)
    func.SetParLimits(6, 1.01, 5.)
    return func


def makeSeveralGaussiansCoreOffCenter():
    """
    Makes an off-center double Gaussian as well as a pair of off-center Gaussians that describe shifted peaks.
    This is useful for size 0 as, for now, the fractional position has been wrongly computed and so the usual center peak is shifted.
    This will be solved in a next update.
    """
    func = TF1(
        "func",
        "[3]/sqrt(2*TMath::Pi())/[1]*([4]*([0]*exp(-(x-[7])*(x-[7])/2./[1]/[1])+(1.-[0])*exp(-(x-[7])*(x-[7])/2./[1]/[1]/[2]/[2])/[2]) + (1.-[4])*(exp(-(x-[5])*(x-[5])/2./[1]/[1]/[6]/[6])+exp(-(x+[5])*(x+[5])/2./[1]/[1]/[6]/[6]))/2./[6])",
        -limits, limits)
    func.SetParLimits(0, 0.01, 0.99)
    func.SetParLimits(1, 0.03, 0.50)
    func.SetParLimits(2, 1.01, 20.)
    func.SetParameter(2, 3.)
    func.SetParameter(3, 0.02)
    func.SetParameter(4, 0.60)
    func.SetParLimits(4, 0.01, 0.99)
    func.SetParameter(5, 0.4)
    func.SetParLimits(5, 0., 1.)
    func.SetParLimits(6, 1.01, 5.)
    func.SetParLimits(7, -1., 1.)
    return func


def makeTwoFromDouble(res):
    """
    Returns the sub-functions of a double Gaussian.
    """
    core = TF1("func",
               "[3]/sqrt(2*TMath::Pi())/[1]*([0]*exp(-x*x/2./[1]/[1]))",
               -limits, limits)
    tail = TF1(
        "func",
        "[3]/sqrt(2*TMath::Pi())/[1]*((1.-[0])*exp(-x*x/2./[1]/[1]/[2]/[2])/[2])",
        -limits, limits)
    setFromRes(core, res)
    setFromRes(tail, res)
    core.SetLineStyle(2)
    core.SetLineColor(2)
    tail.SetLineStyle(2)
    tail.SetLineColor(4)
    return [core, tail]


def makeThreeFromSeveral(res):
    """
    Returns the sub-functions of a double Gaussian + shifted peaks
    """
    core = TF1("func",
               "[3]/sqrt(2*TMath::Pi())/[1]*[4]*[0]*exp(-x*x/2./[1]/[1])",
               -limits, limits)
    tail = TF1(
        "func",
        "[3]/sqrt(2*TMath::Pi())/[1]*[4]*(1.-[0])*exp(-x*x/2./[1]/[1]/[2]/[2])/[2]",
        -limits, limits)
    other = TF1(
        "func",
        "[3]/sqrt(2*TMath::Pi())/[1]*(1.-[4])*(exp(-(x-[5])*(x-[5])/2./[1]/[1]/[6]/[6])+exp(-(x+[5])*(x+[5])/2./[1]/[1]/[6]/[6]))/2./[6]",
        -limits, limits)
    setFromRes(core, res)
    setFromRes(tail, res)
    setFromRes(other, res)
    core.SetLineStyle(2)
    core.SetLineColor(2)
    tail.SetLineStyle(2)
    tail.SetLineColor(4)
    other.SetLineStyle(2)
    other.SetLineColor(3)
    return [core, tail, other]


def makeThreeFromSeveralOffCenter(res):
    """
    Returns the sub-functions of an off-center double Gaussian + shifted peaks
    """
    core = TF1(
        "func",
        "[3]/sqrt(2*TMath::Pi())/[1]*[4]*[0]*exp(-(x-[7])*(x-[7])/2./[1]/[1])",
        -limits, limits)
    tail = TF1(
        "func",
        "[3]/sqrt(2*TMath::Pi())/[1]*[4]*(1.-[0])*exp(-(x-[7])*(x-[7])/2./[1]/[1]/[2]/[2])/[2]",
        -limits, limits)
    other = TF1(
        "func",
        "[3]/sqrt(2*TMath::Pi())/[1]*(1.-[4])*(exp(-(x-[5])*(x-[5])/2./[1]/[1]/[6]/[6])+exp(-(x+[5])*(x+[5])/2./[1]/[1]/[6]/[6]))/2./[6]",
        -limits, limits)
    setFromRes(core, res)
    setFromRes(tail, res)
    setFromRes(other, res)
    core.SetLineStyle(2)
    core.SetLineColor(2)
    tail.SetLineStyle(2)
    tail.SetLineColor(4)
    other.SetLineStyle(2)
    other.SetLineColor(3)
    return [core, tail, other]


def plotClusterRes(name,
                   fileName,
                   outDir="./",
                   angleDef=("sin(abs(atan(txHit)))", "sin#theta", 0., 1.)):
    """
    Main method.
    Sizes 45 and 456 must be understood as (size 4 and 5) and (size 4, 5 and 6), respectively.
    """
    sizes = ["0", "4", "5", "6", "7", "8", "45", "456"]
    hists = {}
    C = TCanvas("C", "", 1200, 800)
    C.SetTopMargin(0.01)
    C.SetRightMargin(0.01)
    C.SetGridx()
    C.SetGridy()
    try:
        f = TFile.Open(fileName)
        t = f.Get("FTClusterTuple/FTLiteCluster")
    except:
        print "ERROR: Could not find tree FTClusterTuple/FTLiteCluster in " + fileName
        return
    globCut = "origZ < 2000"
    for size in sizes:
        hists[size + "none"] = TH1F(size + "none",
                                    ";Cluster resolution [mm]; Arbitrary", 100,
                                    -limits, limits)
        hists[size + "none-Angle"] = TH2F(
            size + "none-Angle", ";sin#theta;Cluster resolution [mm];", 100,
            0., 1., 100, -limits, limits)
        hists[size + "none-P"] = TH2F(size + "none-P",
                                      ";P [MeV/c];Cluster resolution [mm];",
                                      100, 0., 10000, 100, -limits, limits)
        if len(size) == 1:
            cut = globCut + " && pseudoSize == " + str(size)
        else:
            cut = globCut + "&& ( 0 "
            for s in size:
                cut += " || pseudoSize == " + str(s)
            cut += ")"
        t.Project(size + "none", "resolution", cut)
        #        t.Project(size+"none-Angle", "resolution:sin(abs(atan(txHit))*180./TMath::Pi())", cut)
        t.Project(size + "none-Angle", "resolution:sin(abs(atan(txHit)))", cut)
        t.Project(size + "none-P", "resolution:pMCHit", cut)
        hists[size + "none"].Sumw2()
        hists[size + "none"].Scale(1. / hists[size + "none"].GetSum())
        if size == "4" or size == "45" or size == "456":
            func = makeDoubleGaussian()
        elif size == "0":
            func = makeSeveralGaussiansCoreOffCenter()
        else:
            func = makeSeveralGaussians(size)
        res = hists[size + "none"].Fit(func, "Q" "S", "")
        res.Print()
        hists[size + "none-Angle"].Draw("colz")
        C.SaveAs(outDir + "ClusterRes_FromTuple-" + name + "-" + size +
                 "-vsAngle.png")
        hists[size + "none-P"].Draw("colz")
        C.SaveAs(outDir + "ClusterRes_FromTuple-" + name + "-" + size +
                 "-vsP.png")
        hists[size + "none"].Draw("HIST")
        if size == '4' or size == "45" or size == "456":
            subs = makeTwoFromDouble(res)
        elif size != "0":
            subs = makeThreeFromSeveral(res)
        else:
            subs = makeThreeFromSeveralOffCenter(res)
        pt = TPaveText(0.79, 0.59, 0.99, 0.99, "NDC")
        pt.SetLineWidth(1)
        pt.SetLineColor(1)
        pt.SetFillColor(0)
        pt.AddText("Size " + size)
        pt.AddText(("RMS(all) = {:4f}").format(hists[size + "none"].GetRMS()))
        pt.AddText(("#sigma(core) = {:4f}").format(res.Parameter(1)))
        pt.AddText(("#sigma(tail) = {:4f}").format(
            res.Parameter(2) * res.Parameter(1)))
        pt.AddText(("f(core/tail) = {:4f}").format(res.Parameter(0)))
        if size == '4' or size == "45" or size == "456":
            pt.AddText(("#sigma(eff) = {:4f}").format(
                math.sqrt(
                    res.Parameter(0) * res.Parameter(1) * res.Parameter(1) +
                    (1. - res.Parameter(0)) * res.Parameter(2) *
                    res.Parameter(2) * res.Parameter(1) * res.Parameter(1))))
        else:
            pt.AddText(("f(main/shift) = {:4f}").format(res.Parameter(4)))
            pt.AddText(("peak shift = {:.4f}").format(res.Parameter(5)))
            pt.AddText(("factor shift = {:.4f}").format(res.Parameter(6)))
        hists[size + "none"].Draw("HIST")
        pt.Draw("same")
        func.Draw("same")
        for sub in subs:
            sub.Draw("same")
        C.SaveAs(outDir + "ClusterRes_FromTuple-" + name + "-" + size + ".png")


def plotAngleDependence(name, fileName, outDir="./"):
    C = TCanvas("C", "", 1200, 800)
    C.SetTopMargin(0.01)
    C.SetRightMargin(0.01)
    C.SetGridx()
    C.SetGridy()
    try:
        f = TFile.Open(fileName)
        t = f.Get("FTClusterTuple/FTLiteCluster")
    except:
        print "ERROR: Could not find tree FTClusterTuple/FTLiteCluster in " + fileName
        return
    sizes = ["4", "5", "6", "7", "8"]
    sizes = ["5", "6", "7", "8"]
    limitVal = 1.5
    limits = 1.5
    for size in sizes:
        cut = "pseudoSize == " + str(size)
        #        angularBounds = [0.,10.,15.,20.,25.,30.,35.,40.,45.,50.,55.,60.]
        #        angularBounds = [0.,60.]
        #        angularBounds = [0.,10.,15.,20.,25.,30.,35.,40.]
        #        angularBounds = [0.,5.,10.,12.5,15.,17.5,20.,22.5,25.,27.5,30.,32.5,35.,37.5,40.,42.5,45.0,47.5,50.0]
        angularBounds = [0., 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.]
        shiftAt0 = [0., 0., 0., 0., 0., 0.30, 0.35, 0.50,
                    0.65]  #position of secondary peaks at 0deg
        angleCenter = [90., 90., 90., 90., 90., 32.0, 40.0, 45.0,
                       55.0]  #angle where the centered behaviour is
        graphs = []
        for i in range(0, 7):
            graphs.append(TGraph(len(angularBounds) - 1))
            graphs[-1].SetName("Graph" + str(i))
        for i in range(0, len(angularBounds) - 1):
            angCut = "sin(atan(txHit)) > " + str(
                angularBounds[i]) + " && sin(atan(txHit)) < " + str(
                    angularBounds[i + 1])
            h = TH1F(size + "none-Ang" + str(i),
                     ";Cluster resolution [mm]; Arbitrary", 100, -limits,
                     limits)
            t.Project(size + "none-Ang" + str(i), "resolution",
                      cut + "&&(" + angCut + ")")
            h.Sumw2()
            h.Scale(1. / h.GetSum())
            coreFormula = "[0]/sqrt(2*TMath::Pi())/[1]*(exp(-x*x/2./[1]/[1]))"
            tailFormula = "[0]/sqrt(2*TMath::Pi())/[1]/[2]*(exp(-x*x/2./[1]/[1]/[2]/[2]))"
            #            secFormula  = "[0]/2./sqrt(2*TMath::Pi())/[1]/[3]*(exp(-(x-[5])*(x-[5])/2./[1]/[1]/[3]/[3])+exp(-(x+[5])*(x+[5])/2./[1]/[1]/[3]/[3]))"
            secFormula = "[0]/2./sqrt(2*TMath::Pi())/[3]*(exp(-(x-[5])*(x-[5])/2./[3]/[3])+exp(-(x+[5])*(x+[5])/2./[3]/[3]))"

            func = TF1(
                "func", coreFormula + "+" + tailFormula.replace("[0]", "[4]") +
                "+" + secFormula.replace("[0]", "[6]"), -limitVal, limitVal)
            func.SetParLimits(1, 0.01, 0.2)
            #            func.SetParameter(1,0.05)
            func.FixParameter(1, [0.075, 0.11, 0.11, 0.11,
                                  0.11][int(size) - 4])
            func.SetParLimits(2, 2., 10.)
            #            func.SetParLimits(3,1.,5.)
            func.FixParameter(3, [0.15, 0.15, 0.15, 0.15, 0.15][int(size) - 4])
            #            func.FixParameter(3,1.)
            func.SetParLimits(5, 0.02, 1.)
            func.SetParLimits(0, 0.000001, 0.5)
            func.SetParameter(0, h.GetBinContent(h.GetXaxis().FindBin(0.)))
            func.SetParLimits(4, 0., 0.5)
            func.SetParLimits(6, 0., 0.5)
            func.SetParameter(
                5, shiftAt0[int(size)] *
                (1 - (angularBounds[i] / angleCenter[int(size)])))
            res = h.Fit(func, "Q" "S", "")
            #            res.Print()
            # Fractions
            graphs[0].SetPoint(
                i, angularBounds[i],
                func.GetParameter(0) /
                (func.GetParameter(0) + func.GetParameter(4) +
                 func.GetParameter(6)))
            graphs[4].SetPoint(
                i, angularBounds[i],
                func.GetParameter(4) /
                (func.GetParameter(0) + func.GetParameter(4) +
                 func.GetParameter(6)))
            graphs[6].SetPoint(
                i, angularBounds[i],
                func.GetParameter(6) /
                (func.GetParameter(0) + func.GetParameter(4) +
                 func.GetParameter(6)))
            if (func.GetParameter(0) /
                (func.GetParameter(0) + func.GetParameter(4) +
                 func.GetParameter(6))):
                graphs[1].SetPoint(i, angularBounds[i], func.GetParameter(1))
            if (func.GetParameter(4) /
                (func.GetParameter(0) + func.GetParameter(4) +
                 func.GetParameter(6))):
                graphs[2].SetPoint(i, angularBounds[i],
                                   func.GetParameter(1) * func.GetParameter(2))
            if (func.GetParameter(6) /
                (func.GetParameter(0) + func.GetParameter(4) +
                 func.GetParameter(6)) > 0.1):
                #                graphs[3].SetPoint(i,angularBounds[i],func.GetParameter(1)*func.GetParameter(3))
                graphs[3].SetPoint(i, angularBounds[i], func.GetParameter(3))
                graphs[5].SetPoint(i, angularBounds[i], func.GetParameter(5))
            h.Draw("HIST")
            func.Draw("same")
            #Sub-functions
            core = TF1("core", coreFormula, -limitVal, limitVal)
            tail = TF1("tail", tailFormula.replace("[0]", "[4]"), -limitVal,
                       limitVal)
            sec1 = TF1("sec1", secFormula.replace("[0]", "[6]"), -limitVal,
                       limitVal)
            setFromRes(core, res)
            setFromRes(tail, res)
            setFromRes(sec1, res)
            core.SetLineStyle(2)
            core.SetLineColor(1)
            tail.SetLineStyle(2)
            tail.SetLineColor(4)
            sec1.SetLineStyle(2)
            sec1.SetLineColor(3)
            core.Draw("same")
            tail.Draw("same")
            sec1.Draw("same")
            C.SaveAs(outDir + "ClusterRes_FromTuple-" + name + "-" + size +
                     "-Ang" + str(i) + ".png")
        for i in range(0, len(graphs)):
            graphs[i].Draw("AP*")
            graphName = [
                "FracCore", "SigCore", "SigTail", "SigSec1", "FracTail",
                "Shifts", "FracShifts"
            ][i]
            C.SaveAs(outDir + "ClusterRes_FromTuple-" + name + "-" + size +
                     "-" + graphName + ".png")

    for size in ["4", "5", "6", "7", "8"]:
        cut = "pseudoSize == " + str(size)
        coreBounds = [(0., 10.), (26., 34.), (38., 44.), (44., 54.), (48.,
                                                                      58.)]
        angCut = "abs(atan(txHit))*180./TMath::Pi() > " + str(coreBounds[int(
            size) - 4][0]) + " && abs(atan(txHit))*180./TMath::Pi() < " + str(
                coreBounds[int(size) - 4][1])
        h = TH1F(size + "none", ";Cluster resolution [mm]; Arbitrary", 100,
                 -limits, limits)
        t.Project(size + "none", "resolution", cut + "&&(" + angCut + ")")
        h.Sumw2()
        h.Scale(1. / h.GetSum())
        coreFormula = "[0]/sqrt(2*TMath::Pi())/[1]*(exp(-x*x/2./[1]/[1]))"
        tailFormula = "[0]/sqrt(2*TMath::Pi())/[1]/[2]*(exp(-x*x/2./[1]/[1]/[2]/[2]))"
        func = TF1("func",
                   coreFormula + "+" + tailFormula.replace("[0]", "[4]"),
                   -limitVal, limitVal)
        func.SetParLimits(1, 0.01, 0.2)
        #        func.FixParameter(1,0.075)
        func.SetParLimits(2, 2., 10.)
        func.SetParLimits(4, 0., 0.5)
        func.SetParLimits(6, 0., 0.5)
        res = h.Fit(func, "Q" "S", "")
        res.Print()
        h.Draw("HIST")
        func.Draw("same")
        #Sub-functions
        core = TF1("core", coreFormula, -limitVal, limitVal)
        tail = TF1("tail", tailFormula.replace("[0]", "[4]"), -limitVal,
                   limitVal)
        setFromRes(core, res)
        setFromRes(tail, res)
        core.SetLineStyle(2)
        core.SetLineColor(1)
        tail.SetLineStyle(2)
        tail.SetLineColor(4)
        core.Draw("same")
        tail.Draw("same")
        C.SaveAs(outDir + "ClusterRes_FromTuple-" + name + "-" + size +
                 "-Core.png")


def plotRMSvsAngle(name,
                   fileName,
                   outDir="./",
                   angleDef=("abs(txHit)", "t_{x}", 0., 2.),
                   globalCut="1"):
    C = TCanvas("C", "", 1200, 800)
    C.SetTopMargin(0.01)
    C.SetRightMargin(0.01)
    C.SetGridx()
    C.SetGridy()
    try:
        f = TFile.Open(fileName)
        t = f.Get("FTClusterTuple/FTLiteCluster")
    except:
        print "ERROR: Could not find tree FTClusterTuple/FTLiteCluster in " + fileName
        return
    sizes = ["0", "4", "5", "6", "7", "8"]
    limits = 1.5
    nResBins = 100
    nAngBins = 50
    hTot = TH3F("hTot", ";" + angleDef[1] + ";RMS", 9, -0.5, 8.5, nAngBins,
                angleDef[2], angleDef[3], nResBins, -1.5, 1.5)
    t.Project("hTot", "resolution:" + angleDef[0] + ":pseudoSize", globalCut)
    if (hTot.GetSum() == 0):
        exit()
    for size in sizes:
        hRMS = TH1F("hRMS", ";" + angleDef[1] + ";RMS", nAngBins, angleDef[2],
                    angleDef[3])
        h2D = TH2F("h2D", "", nAngBins, angleDef[2], angleDef[3], nResBins,
                   -1.5, 1.5)
        for i in range(1, nAngBins + 1):
            for j in range(1, nResBins + 1):
                h2D.SetBinContent(
                    i, j,
                    hTot.GetBinContent(hTot.GetXaxis().FindBin(float(size)), i,
                                       j))
        for i in range(1, nAngBins + 1):
            h1D = TH1F("h1D", "", nResBins, -1.5, 1.5)
            for j in range(1, nResBins + 1):
                h1D.SetBinContent(j, h2D.GetBinContent(i, j))
            rms = h1D.GetRMS()
            err = h1D.GetRMSError()
            hRMS.SetBinContent(i, rms)
            hRMS.SetBinError(i, err)
            del h1D
        del h2D
        hRMS.GetYaxis().SetRangeUser(0., 1.2)
        hRMS.Draw()
        C.SaveAs(outDir + "ClusterRes_FromTuple-" + name + "-" + size +
                 "-RMS-nofit.png")
        res1 = hRMS.Fit("pol2", "ILRS", "")
        #        res2 = hRMS.Fit("pol0","ILRS","",3.,5.)
        hRMS.Draw()
        res1.Print()
        #        res2.Print()
        C.SaveAs(outDir + "ClusterRes_FromTuple-" + name + "-" + size +
                 "-RMS.png")


def plotResolutionvsQuarter(name, fileName, outDir="./", globalCut="1"):
    C = TCanvas("C", "", 1200, 800)
    C.SetTopMargin(0.01)
    C.SetRightMargin(0.01)
    C.SetGridx()
    C.SetGridy()
    try:
        f = TFile.Open(fileName)
        t = f.Get("FTClusterTuple/FTLiteCluster")
    except:
        print "ERROR: Could not find tree FTClusterTuple/FTLiteCluster in " + fileName
        return
    sizes = ["0", "4", "5", "6", "7", "8"]
    #    sizes = ["5","6","7","8"]
    limitVal = 1.5
    limits = 1.5
    for size in sizes:
        cut = "pseudoSize == " + str(size)
        nBins = 20
        hists = []
        colors = [1, 2, 3, 4]
        for i in [0, 1, 2, 3]:
            angCut = "quarter == " + str(i)
            h = TH1F(size + "none-Ang" + str(i),
                     ";Cluster resolution [mm]; Arbitrary", 100, -limits,
                     limits)
            t.Project(size + "none-Ang" + str(i), "resolution",
                      cut + "&&(" + angCut + ")&&(" + globalCut + ")")
            h.Sumw2()
            h.Scale(1. / h.GetSum())
            h.SetLineColor(colors[i])
            hists.append(h)

        opt = "HIST"
        hists[0].Draw(opt)
        hists[1].Draw(opt + "same")
        hists[2].Draw(opt + "same")
        hists[3].Draw(opt + "same")
        C.SaveAs(outDir + "ClusterRes_FromTuple-" + name + "-" + size +
                 "-Quarter.png")


if __name__ == "__main__":
    #    plotClusterRes("Test","./Test-tuple.root")
    #    plotRMSvsAngle("Test","../Test-tuple.root","./",("abs(txHit)","t_{x}",0.,5.))
    plotRMSvsAngle("Test-hasT", "./Test-tuple.root", "./",
                   ("abs(txHit)", "t_{x}", 0., 2.), "hasT")
    plotRMSvsAngle("Test-hasVeloAndT", "./Test-tuple.root", "./",
                   ("abs(txHit)", "t_{x}", 0., 2.), "hasVeloAndT")
    plotRMSvsAngle("Test-hasT", "./Test-tuple.root", "./",
                   ("abs(txHit)", "t_{x}", 0., 2.), "hasT")
    plotRMSvsAngle("Test-hasVeloAndT", "./Test-tuple.root", "./",
                   ("abs(txHit)", "t_{x}", 0., 2.), "hasVeloAndT")
    #    plotResolutionvsQuarter("Test","./Test-tuple.root")
    #    plotAngleDependence("Test","./Test-tuple.root")
