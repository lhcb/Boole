###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Author: Julian Wishahi
# Date: 2016-09-23
# Description: Boole options to profile the digitisation.
#   Run by calling
#     ./run gaudirun.py --profilerName=valgrindcallgrind --profilerExtraOptions="__instr-atstart=no -v __smc-check=all-non-file __dump-instr=yes __trace-jump=yes __callgrind-out-file=callgrind.improved_gauss.out" runDigitisationForProfiling.py inputfiles.py
#   from a BooleDev directory (assuming lb-dev etc.)

from Gaudi.Configuration import *
from Configurables import Boole, LHCbApp, DDDBConf, CondDB
from PRConfig import TestFileDB

LHCbApp().Simulation = True
CondDB().Upgrade = True

Boole().DDDBtag = 'dddb-20160304'
Boole().CondDBtag = 'sim-20150716-vc-md100'

from Configurables import CondDB
CondDB().addLayer(dbFile="DDDB_FT61.db", dbName="DDDB")

Boole().DataType = "Upgrade"

Boole().DetectorDigi = ['VP', 'UT', 'FT', 'Magnet']
Boole().DigiSequence = ['VP', 'UT', 'FT']
Boole().DetectorLink = ['VP', 'UT', 'Tr', 'FT', 'Magnet']
Boole().DetectorMoni = []
Boole().EvtMax = 20

EventSelector().Input = [
    f"DATAFILE='{TestFileDB.test_file_db['BooleDigitizationForProfiling']}?svcClass=default' TYP='POOL_ROOTTREE' OPT='READ'"
]

#===============================================================================
# Configure FT Digitization
#===============================================================================

#-------------------------------------------------------------------------------
# MCFTDepositCreator
#-------------------------------------------------------------------------------
from Configurables import MCFTDepositCreator, MCFTDistributionChannelTool

myAlgDeposit = MCFTDepositCreator()
myAlgDeposit.SimulationType = "detailed"  # detailed, improved, effective
myAlgDeposit.SimulateNoise = True
myAlgDeposit.SimulateIntraChannelXTalk = True

distrChanTool = myAlgDeposit.addTool(MCFTDistributionChannelTool())
distrChanTool.LightSharing = "old"  # no, old, gauss

#-------------------------------------------------------------------------------
# MCFTDigitCreator
#-------------------------------------------------------------------------------
from Configurables import MCFTDigitCreator
myAlgDigit = MCFTDigitCreator()

#-------------------------------------------------------------------------------
# FTClusterCreator
#-------------------------------------------------------------------------------
from Configurables import FTClusterCreator

myAlgCluster = FTClusterCreator()
myAlgCluster.WriteFullClusters = True

#-------------------------------------------------------------------------------
# FTRawBankEncoder
#-------------------------------------------------------------------------------
from Configurables import FTRawBankEncoder
myAlgRawBankEncoder = FTRawBankEncoder()

#-------------------------------------------------------------------------------
# MCFTDepositMonitor
#-------------------------------------------------------------------------------
from Configurables import MCFTDepositMonitor


def removeMonitors():
    GaudiSequencer("DigiFTSeq").Members.remove("MCFTDepositMonitor")


from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction(removeMonitors)

#===============================================================================
# Outputname
#===============================================================================
Boole(
).DatasetName = "DigitisationForProfiling_" + myAlgDeposit.SimulationType + '_' + distrChanTool.LightSharing


#===============================================================================
# Profiling
#===============================================================================
def addProfile():
    from Configurables import CallgrindProfile
    p = CallgrindProfile('CallgrindProfile')
    p.StartFromEventN = 5
    p.StopAtEventN = 15
    p.DumpAtEventN = 16
    p.DumpName = 'callgrind_' + myAlgDeposit.SimulationType + '_' + distrChanTool.LightSharing + '.out'
    GaudiSequencer('DigiFTSeq').Members.insert(0, p)


#appendPostConfigAction(addProfile)
