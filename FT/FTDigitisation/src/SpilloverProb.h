/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/GenericConditionAccessorHolder.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiMath/GaudiMath.h"

#include <Detector/FT/FTConstants.h>

namespace FTConstants = LHCb::Detector::FT;

static const InterfaceID IID_SpilloverProb( "SpilloverProb", 1, 0 );

/**
 *  This class describes the SpillOver probability
 *
 *  @author Zehua Xu
 *  @date   2024-05-26
 */
class SpilloverProb : public LHCb::DetDesc::ConditionAccessorHolder<GaudiTool> {

public:
  static const InterfaceID& interfaceID() { return IID_SpilloverProb; }
  SpilloverProb( const std::string& type, const std::string& name, const IInterface* parent );
  StatusCode initialize() override;

  virtual double response( const int index_sipm ) const;

private:
  struct Conditions {
    std::vector<double> prob_values;
    double              scale_factor{0};
  };
  ConditionAccessor<Conditions> m_conditions{this, name() + "_Conditions"};

  Gaudi::Property<double> m_scale_factor{this, "ScaleFactor", 0.0, "calibration of the gain"};
  // Gaudi::Property<std::vector<double>> m_values{ this, "ProbSpillOver", std::vector<double>(
  // FTConstants::nSiPMsTotal, 0.0 ), "Prob per channel"};
  Gaudi::Property<std::vector<double>> m_values{this, "ProbSpillOver", {}, "Prob per channel"};

#ifdef USE_DD4HEP
  Gaudi::Property<std::string> m_conditionLocation{this, "conditionLocation",
                                                   "/world/AfterMagnetRegion/T/FT:SpilloverProb"};
  Gaudi::Property<std::string> m_conditionLocation_backup{this, "conditionLocationBackup",
                                                          "/world/AfterMagnetRegion/T/FT:SiPMResponse"};

#else
  Gaudi::Property<std::string> m_conditionLocation{this, "conditionLocation", "/dd/Conditions/Sim/FT/SpilloverProb"};
  Gaudi::Property<std::string> m_conditionLocation_backup{this, "conditionLocationBackup",
                                                          "/dd/Conditions/Sim/FT/SiPMResponse"};
#endif
};
