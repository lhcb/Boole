/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @file MCFTDistributionChannelTool.cpp
 *
 *  Implementation of class : MCFTDistributionChannelTool
 *
 *  @author WISHAHI Julian, BELLEE Violaine
 *  @date   2017-02-20
 */

// local
#include "MCFTDistributionChannelTool.h"

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCFTDistributionChannelTool )

//=============================================================================
// Tool initialization
//=============================================================================
StatusCode MCFTDistributionChannelTool::initialize() {
  auto sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  // Choose the light sharing
  if ( m_lightSharing == "gauss" ) {
    m_numOfAdditionalChannels = 2;
    m_getProbFraction = std::bind( &MCFTDistributionChannelTool::probFracGaussSharing, this, std::placeholders::_1 );
  } else if ( m_lightSharing == "old" ) {
    m_numOfAdditionalChannels = 2;
    m_getProbFraction = std::bind( &MCFTDistributionChannelTool::probFracOldSharing, this, std::placeholders::_1 );
  } else if ( m_lightSharing == "no" ) {
    m_numOfAdditionalChannels = 0;
    m_getProbFraction = std::bind( &MCFTDistributionChannelTool::probFracNoSharing, this, std::placeholders::_1 );
  } else {
    return Error( "Invalid sharing type " + m_lightSharing, StatusCode::FAILURE );
  }

  return sc;
}

LHCb::Detector::FTChannelID MCFTDistributionChannelTool::targetChannel( double posX, const DeFTMat& mat ) const {
  return targetChannel( posX, 0., 0., 0., mat );
}

LHCb::Detector::FTChannelID MCFTDistributionChannelTool::targetChannel( double posX, double posZ, double dXdY,
                                                                        double dZdY, const DeFTMat& mat ) const {
  // check if photon hits sipm in z dir
  if ( std::abs( findFinalPosSlope( posZ, dZdY ) ) <= m_channelSizeZ / 2. ) {
    const auto [channel, frac] = mat.calculateChannelAndFrac( findFinalPosSlope( posX, dXdY ) );
    return ( std::abs( frac ) < 0.5f ) ? channel : LHCb::Detector::FTChannelID( 0 );
  } else {
    return {};
  }
}

//=============================================================================
// Transform deposited energies in fibres into energy in channels taking into
// account line segments
//=============================================================================
FTChannelIDsAndFracs MCFTDistributionChannelTool::targetChannelsFractions( double posXentry, double posXexit,
                                                                           const DeFTMat& mat ) const {

  FTChannelIDsAndFracs channelsAndFractions;
  // Find the channels
  double xBegin               = std::min( posXentry, posXexit );
  double xEnd                 = std::max( posXentry, posXexit );
  auto   channelsAndLeftEdges = mat.calculateChannels( xBegin, xEnd, m_numOfAdditionalChannels );
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Number of relevant channels " << channelsAndLeftEdges.size() << endmsg;

  // break up the line in segments
  std::vector<std::pair<double, double>> segments;
  double                                 thisX        = xBegin;
  double                                 channelPitch = mat.channelPitch();
  double                                 eps          = m_minEnergyFraction * channelPitch;
  for ( auto const& channelAndEdge : channelsAndLeftEdges ) {
    double leftEdge = channelAndEdge.second;
    if ( xEnd < leftEdge ) break;
    if ( thisX < leftEdge && xEnd > leftEdge ) {
      if ( leftEdge - thisX > eps ) segments.emplace_back( thisX, leftEdge );
      thisX = leftEdge;
    }
    double rightEdge = channelAndEdge.second + channelPitch;
    if ( thisX < rightEdge && xEnd > rightEdge ) {
      segments.emplace_back( thisX, rightEdge );
      thisX = rightEdge;
    }
  }
  segments.emplace_back( thisX, xEnd );
  if ( msgLevel( MSG::VERBOSE ) )
    verbose() << "Number of straight line segments from MCHit " << segments.size() << endmsg;
  // Loop over the channels and apply light sharing
  double norm = m_packingFactor / ( xEnd - xBegin );
  for ( auto const& channelAndEdge : channelsAndLeftEdges ) {
    double channelMeanPos = channelAndEdge.second + 0.5 * channelPitch;
    double totFraction    = 0.0;
    for ( auto segment : segments ) {
      // Calculate the average x-position of the hit in this channel
      double hitMeanPos   = 0.5 * ( segment.first + segment.second );
      double fracMeanDist = std::abs( hitMeanPos - channelMeanPos ) / channelPitch;
      // if outside of light-sharing bounds: skip
      if ( fracMeanDist > m_numOfAdditionalChannels + 0.5 ) continue;
      // Calculate the fractional length for this segment
      double fracLength = ( segment.second - segment.first ) * norm;
      // Apply light-sharing factor
      double lsFactor = m_getProbFraction( fracMeanDist );
      totFraction += fracLength * lsFactor;
    }
    channelsAndFractions.emplace_back( channelAndEdge.first, totFraction );
  }
  return channelsAndFractions;
}
