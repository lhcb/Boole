/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Event/MCFTDeposit.h>
#include <FTDet/DeFTDetector.h>
#include <GaudiKernel/IAlgTool.h>

/** @class IFTSiPMTool IFTSiPMTool.h
 *
 *  Interface for the tool that adds noise from the SiPM and
 *  the efficiency function for detecting photons.
 *
 *  @author Violaine Bellee, Julian Wishahi
 *  @date   2017-02-14
 */

struct IFTSiPMTool : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( IFTSiPMTool, 2, 0 );
  virtual void   addNoise( const DeFT& det, LHCb::MCFTDeposits* depositConts ) const = 0;
  virtual float  returnEffNoise( float p0, float p1 ) const                          = 0;
  virtual int    generateDirectXTalk( int nPhotons ) const                           = 0;
  virtual int    generateDelayedXTalk( int nPhotons ) const                          = 0;
  virtual float  generateDelayedXTalkTime() const                                    = 0;
  virtual int    generateChannelXTalk() const                                        = 0;
  virtual float  photonDetectionEfficiency( double wavelength ) const                = 0;
  virtual bool   sipmDetectsPhoton( double wavelength ) const                        = 0;
  virtual double threshold1() const                                                  = 0;
  virtual double threshold2() const                                                  = 0;
  virtual double threshold3() const                                                  = 0;
};
