/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <GaudiKernel/IAlgTool.h>
#include <GaudiKernel/Point3DTypes.h>

/** @class IMCFTDistributionFibreTool IMCFTDistributionFibreTool.h
 *
 *  Interface for the tool that calculates the coordinates and fractional path
 *  length in the core of each fibre
 *
 *  @author Julian Wishahi, Tobias Tekampe
 *  @date   2015-10-27
 */

struct IMCFTDistributionFibreTool : extend_interfaces<IAlgTool> {
  // Return the interface ID
  DeclareInterfaceID( IMCFTDistributionFibreTool, 2, 0 );

  struct GeomCache;

  virtual const GeomCache& getGeomCache() const = 0;

  // Return a vector of fibre positions and for each the path length fraction in
  // the fibre core
  virtual std::vector<std::pair<Gaudi::XYZPoint, double>> effectivePathFracInCores( const Gaudi::XYZPoint& enPoint,
                                                                                    const Gaudi::XYZPoint& exPoint,
                                                                                    const GeomCache& cache ) const = 0;
};
