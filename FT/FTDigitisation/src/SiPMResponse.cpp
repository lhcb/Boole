/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "SiPMResponse.h"

#include "Core/FloatComparison.h"
#include "DetDesc/Condition.h"

#include <yaml-cpp/yaml.h>

//-----------------------------------------------------------------------------
// Implementation file for class : SiPMResponse
//
//   This class describes the SiPM response to a single pulse
//
// 2013-11-12 : Maurizio Martinelli
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( SiPMResponse )

namespace {
  GaudiMath::Interpolation::Type typeFromString( std::string const& splineType ) {
    GaudiMath::Interpolation::Type aType;
    using namespace GaudiMath::Interpolation;
    if ( splineType == "Cspline" )
      aType = Cspline;
    else if ( splineType == "Linear" )
      aType = Linear;
    else if ( splineType == "Polynomial" )
      aType = Polynomial;
    else if ( splineType == "Akima" )
      aType = Akima;
    else if ( splineType == "Akima_Periodic" )
      aType = Akima_Periodic;
    else if ( splineType == "Cspline_Periodic" )
      aType = Cspline_Periodic;
    else
      aType = Cspline; // default to cubic
    return aType;
  }
} // namespace

SiPMResponse::SiPMResponse( const std::string& type, const std::string& name, const IInterface* parent )
    : ConditionAccessorHolder( type, name, parent ) {
  declareInterface<SiPMResponse>( this );
}

StatusCode SiPMResponse::initialize() {
  return ConditionAccessorHolder::initialize().andThen( [&] {
    addConditionDerivation( {m_conditionLocation}, m_conditions.key(), [&]( YAML::Node const& rInfo ) -> Conditions {
      // Get data from condition DB
      auto                times = rInfo["times"].as<std::vector<double>>();
      std::vector<double> values;
      double              tshift{m_tshift}; // Time shift (t0) of the response shape [ns]
      double              sipmGainShift{m_sipmGainShift};
      std::string         suffix = "";
      if ( m_electronicsResponse == "pacific5q_pz5" )
        suffix = "P5_pz5";
      else if ( m_electronicsResponse == "pacific5q_pz6" )
        suffix = "P5_pz6";
      else {
        throw GaudiException( "SiPMResponse::initialize",
                              ( "Electronics response not supported: " + m_electronicsResponse ).c_str(),
                              StatusCode::FAILURE );
      }
      values = rInfo["values_" + suffix].as<std::vector<double>>();
      if ( LHCb::essentiallyZero( m_sipmGainShift.value() ) ) {
        sipmGainShift = rInfo["gainshift_" + suffix].as<double>();
      } else {
        warning() << "SiPM gain shift set by hand and over-riding the SIMCOND. Expert use only." << endmsg;
      }
      if ( LHCb::essentiallyEqual( m_tshift.value(), -100. ) ) {
        tshift = rInfo["timeshift_" + suffix].as<double>();
      } else {
        warning() << "SiPM time shift set by hand and over-riding the SIMCOND. Expert use only." << endmsg;
      }
      // shift times by tshift
      std::for_each( times.begin(), times.end(), [tshift]( double& n ) { n += tshift; } );
      // Check if the times and values are of equal length
      if ( times.size() != values.size() ) {
        throw GaudiException( "SiPMResponse::initialize", "inconsistent data !", StatusCode::FAILURE );
      }
      // Normalize function. Set maximum to 1.
      double maxVal = *std::max_element( std::begin( values ), std::end( values ) );
      std::transform( values.begin(), values.end(), values.begin(), [maxVal]( double item ) { return item / maxVal; } );
      // Fit the spline to the data
      return {{times, values, typeFromString( m_splineType )}, times.front(), times.back(), sipmGainShift};
    } );
    return StatusCode::SUCCESS;
  } );
}

double SiPMResponse::response( const double time ) const {
  auto& conds = m_conditions.get();
  return ( ( time > conds.tMin ) && ( time < conds.tMax ) ? conds.responseSpline.eval( time ) * conds.sipmGainShift
                                                          : 0.0 );
}
