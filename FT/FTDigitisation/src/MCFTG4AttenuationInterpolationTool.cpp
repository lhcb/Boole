/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "IMCFTAttenuationTool.h" // Base Class

#include "Kernel/STLExtensions.h"
#include "Kernel/TaggedBool.h"
#include <DetDesc/GenericConditionAccessorHolder.h>

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/SystemOfUnits.h"

#include <math.h>

#include <yaml-cpp/yaml.h>

namespace {
  enum class ParameterName { mask, a_refl, a_dir, b_refl, b_dir, c_refl, c_dir, d_refl, d_dir, y_pos };
  ParameterName convert( std::string const& key ) {
    if ( key == "mask" ) return ParameterName::mask;
    if ( key == "a_refl" ) return ParameterName::a_refl;
    if ( key == "a_dir" ) return ParameterName::a_dir;
    if ( key == "b_refl" ) return ParameterName::b_refl;
    if ( key == "b_dir" ) return ParameterName::b_dir;
    if ( key == "c_refl" ) return ParameterName::c_refl;
    if ( key == "c_dir" ) return ParameterName::c_dir;
    if ( key == "d_refl" ) return ParameterName::d_refl;
    if ( key == "d_dir" ) return ParameterName::d_dir;
    if ( key == "y_pos" )
      return ParameterName::y_pos;
    else
      throw std::runtime_error( "unknown ParameterName " + key );
  }

  struct Conditions {
    unsigned int                                 nBinsX, nBinsY;
    std::vector<double>                          xEdges, yEdges, effDir, effRef;
    std::map<ParameterName, std::vector<double>> parameters;
  };
} // namespace

/**
 *  Tool that reads LYAM parameters from the CondDB and creates a interpolated attenuation map
 *  for a given irradiation dose and (time) age of the fibres.
 *
 *  @author Martin Bieker based on implementation of M. Demmer and J. Wishahi
 *  @date   2018-28-08
 */
class MCFTG4AttenuationInterpolationTool
    : public extends<LHCb::DetDesc::ConditionAccessorHolder<GaudiTool>, IMCFTAttenuationTool> {

public:
  using extends::extends;
  StatusCode initialize() override;

  /// Calculate the direct attenuation and the attenuation with reflection
  Attenuation attenuation( double x, double y ) const override;

protected:
  using ReflectedPhotons = LHCb::tagged_bool<struct IsReflection_tag>;
  double calculateAttenuation( int binID, MCFTG4AttenuationInterpolationTool::ReflectedPhotons isRefl,
                               Conditions const& conds );

private:
  Conditions processParameterMap( YAML::Node const& cond, double mirrorReflectivity );
  int        findBin( LHCb::span<const double> axis, double position ) const;
#ifdef USE_DD4HEP
  Gaudi::Property<std::string> m_parameterLocation{this, "ParameterLocation",
                                                   "/world/AfterMagnetRegion/T/FT:AttenuationInterpolation",
                                                   "Location of ParameterFile in CondDB"};
#else
  Gaudi::Property<std::string> m_parameterLocation{this, "ParameterLocation",
                                                   "/dd/Conditions/Calibration/FT/AttenuationInterpolation",
                                                   "Location of ParameterFile in CondDB"};
#endif
  Gaudi::Property<float>        m_fibreAge{this, "FibreAge", 0, "Age of fibres in months"};
  Gaudi::Property<float>        m_irrad{this, "IrradiationLevel", 0, "Irradiation level in 1/fb"};
  Gaudi::Property<float>        m_mirrorReflectivity{this, "MirrorReflectivity", 0.75, "Reflectivity of the Mirror"};
  ConditionAccessor<Conditions> m_conditions{this, name() + "_Conditions"};
};

// names of the parameters describing the binning of the LYAMs
namespace {
  static const std::set<std::string> s_binParams{"x_n_bins", "y_n_bins", "x_edges", "y_edges"};
}

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCFTG4AttenuationInterpolationTool )

StatusCode MCFTG4AttenuationInterpolationTool::initialize() {
  return extends::initialize().andThen( [&] {
    info() << "Using the FT attenuation interpolation tool with L= " << std::to_string( m_irrad ) << "/fb " << endmsg;
    addConditionDerivation( {m_parameterLocation}, m_conditions.key(), [&]( YAML::Node const& cond ) -> Conditions {
      return this->processParameterMap( cond, m_mirrorReflectivity );
    } );
    return StatusCode::SUCCESS;
  } );
}

// Return calculated attenution to MCFTDepositCreator
IMCFTAttenuationTool::Attenuation MCFTG4AttenuationInterpolationTool::attenuation( double hitXPosition,
                                                                                   double hitYPosition ) const {
  hitXPosition = std::abs( hitXPosition );
  hitYPosition = std::abs( hitYPosition );

  auto& conds = m_conditions.get();
  int   binX  = findBin( conds.xEdges, hitXPosition );
  int   binY  = findBin( conds.yEdges, hitYPosition );

  // Hit outside quadrant (should never happen!)
  if ( binX == -1 || binY == -1 ) {
    return {0.0, 0.0};
  } else {
    return {conds.effDir[binY * conds.nBinsX + binX], conds.effRef[binY * conds.nBinsX + binX]};
  }
}

// Read in and validate parameter map from CondDB
Conditions MCFTG4AttenuationInterpolationTool::processParameterMap( YAML::Node const& cond,
                                                                    double            mirrorReflectivity ) {
  Conditions outConds;
  // Process binning information
  outConds.nBinsX = cond["x_n_bins"].as<int>();
  outConds.nBinsY = cond["y_n_bins"].as<int>();
  outConds.xEdges = cond["x_edges"].as<std::vector<double>>();
  outConds.yEdges = cond["y_edges"].as<std::vector<double>>();
  std::sort( begin( outConds.xEdges ), end( outConds.xEdges ) );
  std::sort( begin( outConds.yEdges ), end( outConds.yEdges ) );

  // Loop over maps of parameters
  for ( auto it = cond.begin(); it != cond.end(); it++ ) {
    auto name = it->first.as<std::string>();
    if ( s_binParams.count( name ) != 0 ) { continue; }
    outConds.parameters[convert( name )] = it->second.as<std::vector<double>>();
  }

  // check for consistency
  if ( outConds.nBinsX != outConds.xEdges.size() - 1 || outConds.nBinsY != outConds.yEdges.size() - 1 ||
       std::any_of(
           outConds.parameters.begin(), outConds.parameters.end(),
           [nb = outConds.nBinsX * outConds.nBinsY]( const auto& i ) { return nb != std::get<1>( i ).size(); } ) ) {
    throw GaudiException( "MCFTG4AttenuationInterpolationTool::processParameterMap",
                          "Consitency check of parameter map failed: Please check SIMCOND tag", StatusCode::FAILURE );
  }

  // fill maps
  outConds.effDir.reserve( outConds.nBinsX * outConds.nBinsY );
  outConds.effRef.reserve( outConds.nBinsX * outConds.nBinsY );
  for ( unsigned int bin = 0; bin < outConds.nBinsX * outConds.nBinsY; ++bin ) {
    outConds.effDir.push_back( calculateAttenuation( bin, ReflectedPhotons{false}, outConds ) );
    outConds.effRef.push_back( calculateAttenuation( bin, ReflectedPhotons{true}, outConds ) * mirrorReflectivity );
  }

  return outConds;
}

int MCFTG4AttenuationInterpolationTool::findBin( LHCb::span<const double> axis, double position ) const {
  // axis container must not be empty to prevent undefined behaviour
  assert( !axis.empty() );

  auto iterator = std::upper_bound( axis.begin(), axis.end(), position );
  // If position is outside of map: use first or last bin.
  if ( iterator == axis.end() )
    --iterator;
  else if ( iterator == axis.begin() )
    ++iterator;
  return iterator - axis.begin() - 1;
}

double MCFTG4AttenuationInterpolationTool::calculateAttenuation(
    int binID, MCFTG4AttenuationInterpolationTool::ReflectedPhotons isRefl, Conditions const& conds ) {
  if ( isRefl ) {
    return conds.parameters.at( ParameterName::mask )[binID] *
           ( ( conds.parameters.at( ParameterName::a_refl )[binID] * m_irrad +
               +conds.parameters.at( ParameterName::b_refl )[binID] *
                   exp( -conds.parameters.at( ParameterName::c_refl )[binID] * m_irrad ) +
               conds.parameters.at( ParameterName::d_refl )[binID] ) *
             exp( -4.2e-4 * ( conds.parameters.at( ParameterName::y_pos )[binID] + 2.5 ) * m_fibreAge ) );

  } else {
    return conds.parameters.at( ParameterName::mask )[binID] *
           ( ( conds.parameters.at( ParameterName::a_dir )[binID] * m_irrad +
               +conds.parameters.at( ParameterName::b_dir )[binID] *
                   exp( -conds.parameters.at( ParameterName::c_dir )[binID] * m_irrad ) +
               conds.parameters.at( ParameterName::d_dir )[binID] ) *
             exp( -4.2e-4 * ( 2.5 - conds.parameters.at( ParameterName::y_pos )[binID] ) * m_fibreAge ) );
  }
}
