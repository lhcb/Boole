/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <Detector/FT/FTConstants.h>

#include "SiPMResponse.h"
#include <Event/MCFTDigit.h>
#include <FTDet/DeFTDetector.h>
#include <GaudiAlg/GaudiHistoAlg.h>
#include <LHCbAlgs/Consumer.h>
#include <boost/range/irange.hpp>

/** @file MCFTDigitMonitor.cpp
 *
 *
 *  @author Eric Cogneras, Luca Pescatore
 *  @date   2012-07-05
 */

namespace FTConstants = LHCb::Detector::FT;

class MCFTDigitMonitor : public LHCb::Algorithm::Consumer<void( const LHCb::MCFTDigits& ),
                                                          LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeFT>> {
public:
  MCFTDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  void       operator()( const LHCb::MCFTDigits& digits ) const override;

private:
  void fillHistograms( const LHCb::MCFTDigit* mcDigit, const std::set<const LHCb::MCHit*>& mcHits,
                       const std::string& hitType ) const;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MCFTDigitMonitor )

MCFTDigitMonitor::MCFTDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, {KeyValue{"DigitLocation", LHCb::MCFTDigitLocation::Default}} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode MCFTDigitMonitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;

  // Plot the electronics response function
  SiPMResponse* sipmResponse = tool<SiPMResponse>( "SiPMResponse", "SiPMResponse" );
  for ( auto time : boost::irange( -60, 120 ) ) {
    plot( time, "ElectronicsResponse", "Electronics response function; #it{t} [ns]; Relative gain", -60., 120., 180,
          sipmResponse->response( time ) );
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
void MCFTDigitMonitor::operator()( const LHCb::MCFTDigits& mcDigitsCont ) const {

  // Count signal, noise and spillover digits
  int nSignal = 0, nNoise = 0, nSpillover = 0;

  // Loop over MCFTDigits
  for ( auto mcDigit : mcDigitsCont ) {

    // Check if digit is from pure spillover or from pure noise
    bool                         isSpillover = true;
    std::set<const LHCb::MCHit*> mcHits;
    for ( const auto& deposit : mcDigit->deposits() ) {
      const auto& mcHit = deposit->mcHit();
      if ( mcHit != nullptr ) {
        mcHits.insert( mcHit );
        if ( mcHit->parent()->registry()->identifier() == "/Event/MC/FT/Hits" ) isSpillover = false;
      }
    }
    if ( mcHits.empty() ) isSpillover = false;

    // Fill the histograms for all types
    std::string hitType = "";
    fillHistograms( mcDigit, mcHits, hitType );

    // Check the spill type for this digit
    hitType = "Signal/";
    if ( mcHits.empty() ) {
      nNoise++;
      hitType = "Noise/";
    } else if ( isSpillover ) {
      nSpillover++;
      hitType = "Spillover/";
    } else {
      nSignal++;
    }
    fillHistograms( mcDigit, mcHits, hitType );
  }

  // number of clusters
  plot( mcDigitsCont.size(), "nDigits", "Number of digits; Digits/event; Events", 0., 60e3, 50 );
  plot( nSignal, "nSignalDigits", "Number of signal digits; Digits/event; Events", 0., 60e3, 50 );
  plot( nSpillover, "nSpilloverDigits", "Number of spillover digits; Digits/event; Events", 0., 60e3, 50 );
  plot( nNoise, "nNoiseDigits", "Number of noise digits; Digits/event; Events", 0., 60e3, 50 );

  return;
}

void MCFTDigitMonitor::fillHistograms( const LHCb::MCFTDigit* mcDigit, const std::set<const LHCb::MCHit*>& mcHits,
                                       const std::string& hitType ) const {
  // plot number of hits
  plot( mcHits.size(), hitType + "nMCHitsPerDigit", "Number of MCHits per digit; MCHits/digit; Number of digits", -0.5,
        10.5, 11 );

  // plot digit PEs
  plot( mcDigit->photoElectrons(), hitType + "nPhotoElectrons", "Photoelectrons per digit; PEs/digit; Number of digits",
        0., 15., 200 );

  // plot digit adc counts
  plot( mcDigit->adcCount(), hitType + "ADCCounts", "Charge in 2bit ADC; Charge/digit [ADC]; Number of digits", -0.5,
        FTConstants::RawBank::maxAdcCharge - 0.5, FTConstants::RawBank::maxAdcCharge );

  // deposits distribution in (x,y)
  if ( !mcHits.empty() ) {
    plot2D( ( *mcHits.begin() )->midPoint().x(), ( *mcHits.begin() )->midPoint().y(), hitType + "XYDistribution",
            "Digits XY Distribution; #it{x} [mm]; #it{y} [mm]", -3600., 3600., -2700., 2700., 100, 100 );
    // average photoElectron counts per hits
    plot( mcDigit->photoElectrons() / float( mcHits.size() ), hitType + "PEsPerMCHit",
          "PE counts / MCHit; PE count / nHits;Number of digits", 0., 30., 100 );
  }

  // plot occupancy
  const LHCb::Detector::FTChannelID chanID = mcDigit->channelID();
  plot( (float)chanID.station(), hitType + "DigitsPerStation", "Digits per station; Station; Digits", 0.5,
        FTConstants::nStations - 0.5, FTConstants::nStations );
  plot( (float)chanID.module(), hitType + "DigitsPerModule", "Digits per module; Module; Digits", -0.5,
        FTConstants::nModulesMax - 0.5, FTConstants::nModulesMax );
  plot( (float)chanID.sipmInModule(), hitType + "DigitsPerSiPM", "Digits per SiPM; SiPMID; Digits", -0.5,
        FTConstants::nSiPMsPerModule - 0.5, FTConstants::nSiPMsPerModule );
  plot( (float)chanID.channel(), hitType + "DigitsPerChannel", "Digits per channel; Channel; Digits", -0.5,
        FTConstants::nChannels - 0.5, FTConstants::nChannels );

  int pseudoChannel = chanID.localChannelIdx_quarter();
  plot( pseudoChannel, hitType + "DigitsPerPseudoChannel",
        "Digits per pseudochannel;Pseudochannel;Digits/(64 channels)", -0.5, FTConstants::nMaxChannelsPerQuarter - 0.5,
        FTConstants::nMaxSiPMsPerQuarter * 2 );

  // Count the number of photons for this digit
  int nPhotonsPerDigit = 0;
  for ( const auto& deposit : mcDigit->deposits() ) nPhotonsPerDigit += deposit->nPhotons();
  plot( (float)nPhotonsPerDigit, hitType + "nPhotonsPerDigit", "Photons per digit;Photons/digit;Number of digits", 0.5,
        40.5, 40 );
}

//=============================================================================
