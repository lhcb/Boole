/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
/// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"
#include "LHCbAlgs/Transformer.h"

/// from Event
#include "Event/MCFTDeposit.h"
#include "Event/MCFTPhoton.h"
#include "Event/MCHit.h"

// FTDet
#include "FTDet/DeFTDetector.h"

// from Boole/FT/FTDigitisation
#include "IFTSiPMTool.h"
#include "IMCFTAttenuationTool.h"
#include "IMCFTDistributionChannelTool.h"
#include "IMCFTDistributionFibreTool.h"
#include "IMCFTPhotonTool.h"

#include <range/v3/all.hpp>

typedef std::pair<double, const LHCb::MCHits*> SpillPair;

/**
 *  From the list of MCHits, this algorithm fill a container of FTChannelID
 *  with the photons in each of them, and store it in the
 *  transient data store.
 *
 *
 *  @author DE VRIES Jacco, WISHAHI Julian, BELLEE Violaine, COGNERAS Eric, PESCATORE Luca
 *  @date   2016-12-19
 */
class MCFTDepositCreator final
    : public LHCb::Algorithm::MultiTransformer<std::tuple<LHCb::MCFTDeposits, LHCb::MCFTPhotons>(
                                                   std::array<SpillPair, 4> const&, DeFT const& ),
                                               LHCb::DetDesc::usesConditions<DeFT>> {

public:
  MCFTDepositCreator( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode                                        initialize() override;
  std::tuple<LHCb::MCFTDeposits, LHCb::MCFTPhotons> operator()( std::array<SpillPair, 4> const&,
                                                                DeFT const& ) const override;

private:
  void convertHitToPhotons( const LHCb::MCHit* mchit, const DeFTMat& mat, double timeOffset, LHCb::MCFTPhotons& photons,
                            const IMCFTDistributionFibreTool::GeomCache& toolCache ) const;

  void convertPhotonsToDepositsDetailed( const LHCb::MCFTPhotons& photons, LHCb::MCFTDeposits& deposits,
                                         DeFT const& deFT ) const;

  void convertHitToDepositsEffective( const LHCb::MCHit* mchit, const DeFTMat& mat, double timeOffset,
                                      LHCb::MCFTDeposits& deposits ) const;

  // Simulation types
  enum class SimulationType { effective, detailed };
  const std::map<std::string, SimulationType> m_simulationTypes{{"effective", SimulationType::effective},
                                                                {"detailed", SimulationType::detailed}};
  SimulationType                              m_simulationType = SimulationType::detailed;
  Gaudi::Property<std::string>                m_simulationTypeChoice{this, "SimulationType", "detailed",
                                                      "Simulation type (effective, detailed)"};

  ToolHandle<IMCFTAttenuationTool>       m_attenuationTool{this, "AttenuationToolName", "MCFTG4AttenuationTool",
                                                     "Tool describing the attenuation in the fibres"};
  ToolHandle<IMCFTDistributionFibreTool> m_distrFibreTool{
      this, "DistributionFibreToolName", "MCFTDistributionFibreTool",
      "Tool describing the energy distribution in the mat's fibres"};
  ToolHandle<IMCFTPhotonTool>              m_photonTool{this, "PhotonToolName", "MCFTPhotonTool",
                                           "Tool describing the distribution of photon observables"};
  ToolHandle<IMCFTDistributionChannelTool> m_distrChannelTool{this, "DistributionChannelToolName",
                                                              "MCFTDistributionChannelTool",
                                                              "Tool distributing energy/photons to the channels"};
  ToolHandle<IFTSiPMTool>                  m_sipmTool{this, "SiPMToolName", "FTSiPMTool",
                                     "Tool adding the noise deposits and the wavelenght-dependent efficiency"};

  Gaudi::Property<bool> m_simulateNoise{this, "SimulateNoise", true, "Simulate noise"};
  Gaudi::Property<bool> m_simulateIntraChannelXTalk{this, "SimulateIntraChannelXTalk", true,
                                                    "Simulate intra-channel cross-talk"};
  Gaudi::Property<bool> m_simulatePDE{this, "SimulatePDE", false, "Simulate Effect of SiPM PDE"};
  Gaudi::Property<bool> m_simulateChannelXT{this, "SimulateChannelXT", true,
                                            "Simulate Effect of channel to channel cross talk"};

  mutable Gaudi::Accumulators::AveragingCounter<> m_nbOfMCHits{this, "NbOfMCHits"};
  mutable Gaudi::Accumulators::Counter<>          m_nbOfMissedMCHits{this, "NbOfMissedMCHits"};
  mutable Gaudi::Accumulators::Counter<>          m_nbOfNoMatMCHits{this, "NbOfNoMatMCHits"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MCFTDepositCreator )

MCFTDepositCreator::MCFTDepositCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer( name, pSvcLocator,
                        // Input
                        {KeyValue{"InputLocation", "/Event/MC/FT/MergedHits"},
                         KeyValue{"FTDetectorLocation", DeFTDetectorLocation::Default}},
                        // Output
                        {KeyValue{"OutputLocation", LHCb::MCFTDepositLocation::Default},
                         KeyValue{"MCFTPhotonsLocation", LHCb::MCFTPhotonLocation::Default}} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode MCFTDepositCreator::initialize() {
  return MultiTransformer::initialize().andThen( [&] {
    // check type of chosen simulation
    auto simulationTypePair = m_simulationTypes.find( m_simulationTypeChoice );
    if ( simulationTypePair != m_simulationTypes.end() ) {
      m_simulationType = simulationTypePair->second;
    } else {
      std::string error_message( "Simulation type " + m_simulationTypeChoice + " is unknown. Please choose from: " );
      for ( auto simulationType : m_simulationTypes ) { error_message += simulationType.first + " "; }
      throw GaudiException( error_message, "MCFTDepositCreator::initialize", StatusCode::FAILURE );
    }
    return StatusCode::SUCCESS;
  } );
}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<LHCb::MCFTDeposits, LHCb::MCFTPhotons> MCFTDepositCreator::
                                                  operator()( std::array<SpillPair, 4> const& spills, DeFT const& deFT ) const {

  std::tuple<LHCb::MCFTDeposits, LHCb::MCFTPhotons> returnContainers;
  auto&                                             deposits = std::get<0>( returnContainers );
  auto&                                             photons  = std::get<1>( returnContainers );

  const IMCFTDistributionFibreTool::GeomCache* toolCache = nullptr;
  // main loop over spills and MCHits
  switch ( m_simulationType ) {
  case SimulationType::detailed:
    photons.reserve( 10e5 );  // reserve 1.0M elements
    deposits.reserve( 14e5 ); // reserve 1.4M elements
    toolCache = &m_distrFibreTool->getGeomCache();
    break;
  case SimulationType::effective:
    photons.reserve( 0 );     // reserve zero elements
    deposits.reserve( 10e5 ); // reserve 1.0M elements
    break;
  }

  // main loop over spills and MCHits
  for ( const auto& spill : spills ) {
    const LHCb::MCHits* mchits = spill.second;

    // Check if spill is missing
    if ( mchits == nullptr ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Spillover missing in the loop at " << spill.first << " ns" << endmsg;
      continue;
    }
    m_nbOfMCHits += mchits->size();

    for ( const LHCb::MCHit* mchit : *mchits ) {

      // get mat element of MCHit
      const auto& mat_ptr = deFT.findMat( mchit->midPoint() );

      if ( !mat_ptr ) {
        ++m_nbOfMissedMCHits;
        debug() << "FT not found mat element " << endmsg;
        continue;
      }

      int sensDetID = mat_ptr->elementID();

      // Check if sensDetID is filled and if a mat can be associated
      if ( sensDetID == -1 ) {
        ++m_nbOfNoMatMCHits;
        debug() << "Empty sensDetID of MCHit (happens <1 times/event)" << endmsg;
        continue;
      }

      const auto& mat = *mat_ptr;
      // choose which function to call depending on simulation type
      switch ( m_simulationType ) {
      case SimulationType::detailed:
        assert( toolCache );
        convertHitToPhotons( mchit, mat, spill.first, photons, *toolCache );
        break;
      case SimulationType::effective:
        convertHitToDepositsEffective( mchit, mat, spill.first, deposits );
        break;
      }
    } // loop on hits
  }   // loop on spills

  // Finally, sort the photon container according to mat ID
  std::stable_sort( photons.begin(), photons.end(), LHCb::MCFTPhoton::lowerByMatID );

  // For detailed simulation loop over photons is required
  switch ( m_simulationType ) {
  case SimulationType::detailed:
    convertPhotonsToDepositsDetailed( photons, deposits, deFT );
    break;
  case SimulationType::effective:
    break;
  }
  if ( m_simulateNoise ) m_sipmTool->addNoise( deFT, &deposits );

  // Finally, sort the deposit container according to channel ID
  std::stable_sort( deposits.begin(), deposits.end(), LHCb::MCFTDeposit::lowerByChannelID );

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Number of MCFTDeposits created: " << deposits.size() << endmsg;

  return returnContainers;
}

//=============================================================================
// Detailed simulation
//=============================================================================
void MCFTDepositCreator::convertHitToPhotons( const LHCb::MCHit* mchit, const DeFTMat& mat, double timeOffset,
                                              LHCb::MCFTPhotons&                           photons,
                                              const IMCFTDistributionFibreTool::GeomCache& toolCache ) const {
  // Get MCFTPhotons for mat or create it
  const unsigned int sensDetID = mat.elementID();

  // Get deposited energy
  const double hitEnergy = mchit->energy();

  // Get the local coordinates
  const Gaudi::XYZPoint  localEntry = mat.toLocal( mchit->entry() );
  const Gaudi::XYZVector localDispl = mat.toLocal( mchit->displacement() );

  const Gaudi::XYZPoint localExit = localEntry + localDispl;
  const Gaudi::XYZPoint localMid  = localEntry + 0.5 * localDispl;

  // -- Calculate the attenuation using the AttenuationTool
  auto [attDir, attRef] = m_attenuationTool->attenuation( mchit->midPoint().X(), mchit->midPoint().Y() );

  // Calculate fibre centers and path fraction in the fibres hit by the MCHit
  auto posAndFracs = m_distrFibreTool->effectivePathFracInCores( localEntry, localExit, toolCache );

  // -- Calculate the minimal arrival time
  const double timeEvent = mchit->time() + timeOffset;
  const double distDir   = mat.distanceToSiPM( localMid );
  const double distRef   = 2. * mat.fibreLength() - distDir;

  const double propTimeDirMean = timeEvent + m_photonTool->averagePropagationTime( distDir );
  const double propTimeRefMean = timeEvent + m_photonTool->averagePropagationTime( distRef );

  for ( auto posAndFrac : posAndFracs ) {
    // get fibre position and deposited energy
    double posXFibre = posAndFrac.first.X();
    double posZFibre = posAndFrac.first.Z();
    double energyDir = posAndFrac.second * hitEnergy * attDir;
    double energyRef = posAndFrac.second * hitEnergy * attRef;

    // get number of expected photons
    double nPhotonsExpDir = m_photonTool->numExpectedPhotons( energyDir );
    double nPhotonsExpRef = m_photonTool->numExpectedPhotons( energyRef );

    switch ( m_simulationType ) {
    case SimulationType::detailed: {
      int nPhotonsDir = m_photonTool->numObservedPhotons( nPhotonsExpDir );
      int nPhotonsRef = m_photonTool->numObservedPhotons( nPhotonsExpRef );

      // create direct photons
      for ( int i = 0; i < nPhotonsDir; ++i ) {
        double time, wavelength, posX, posZ, dXdY, dZdY;
        m_photonTool->generatePhoton( time, wavelength, posX, posZ, dXdY, dZdY );
        LHCb::MCFTPhoton* photon = new LHCb::MCFTPhoton( sensDetID, 1., mchit, propTimeDirMean + time, wavelength,
                                                         posXFibre + posX, posZFibre + posZ, dXdY, dZdY, false );
        if ( !m_simulatePDE || m_sipmTool->sipmDetectsPhoton( wavelength ) ) photons.add( photon );
      }

      // create reflected photons
      for ( int i = 0; i < nPhotonsRef; ++i ) {
        double time, wavelength, posX, posZ, dXdY, dZdY;
        m_photonTool->generatePhoton( time, wavelength, posX, posZ, dXdY, dZdY );
        LHCb::MCFTPhoton* photon = new LHCb::MCFTPhoton( sensDetID, 1., mchit, propTimeRefMean + time, wavelength,
                                                         posXFibre + posX, posZFibre + posZ, dXdY, dZdY, true );

        if ( !m_simulatePDE || m_sipmTool->sipmDetectsPhoton( wavelength ) ) photons.add( photon );
      }
    } break;
    case SimulationType::effective:
      error() << "Calling function for detailed simulation, "
              << "but simulation type is 'effective'!" << endmsg;
      break;
    }
  }
}

void MCFTDepositCreator::convertPhotonsToDepositsDetailed( const LHCb::MCFTPhotons& photons,
                                                           LHCb::MCFTDeposits& deposits, DeFT const& deFT ) const {

  int prevSensDetID = -1000;
#ifdef USE_DD4HEP
  std::optional<DeFTMat> mat_ptr = {};
#else
  const DeFTMat* mat_ptr = NULL;
#endif
  for ( const auto& photon : photons ) {
    if ( photon->sensDetID() != prevSensDetID ) {
      prevSensDetID = photon->sensDetID();
      mat_ptr       = deFT.findMat( LHCb::Detector::FTChannelID( prevSensDetID ) );
      if ( !mat_ptr ) {
        error() << "FT module not found for sensDetID = " << photon->sensDetID() << endmsg;
        continue;
      }
    }

    // Find the associated channel, add it to the deposit
    LHCb::Detector::FTChannelID targetChannel =
        m_distrChannelTool->targetChannel( photon->posX(), photon->posZ(), photon->dXdY(), photon->dZdY(), *mat_ptr );
    if ( targetChannel.channelID() == 0u ) continue;

    if ( m_simulateChannelXT ) {

      if ( !( mat_ptr->hasGapLeft( targetChannel ) ) ) {
        int nPhotons = m_sipmTool->generateChannelXTalk();
        if ( nPhotons > 0 ) {
          LHCb::MCFTDeposit* deposit =
              new LHCb::MCFTDeposit( LHCb::Detector::FTChannelID{targetChannel - 1u}, photon->mcHit(), 1,
                                     photon->time(), photon->isReflected() );
          deposits.push_back( deposit );
        }
      }
      if ( !( mat_ptr->hasGapRight( targetChannel ) ) ) {
        int nPhotons = m_sipmTool->generateChannelXTalk();
        if ( nPhotons > 0 ) {
          LHCb::MCFTDeposit* deposit =
              new LHCb::MCFTDeposit( LHCb::Detector::FTChannelID{targetChannel + 1u}, photon->mcHit(), 1,
                                     photon->time(), photon->isReflected() );
          deposits.push_back( deposit );
        }
      }
    }

    // Add direct x-talk
    int nPhotonsXTalk = ( m_simulateIntraChannelXTalk ) ? m_sipmTool->generateDirectXTalk( photon->nPhotons() ) : 0;

    LHCb::MCFTDeposit* deposit = new LHCb::MCFTDeposit(
        targetChannel, photon->mcHit(), photon->nPhotons() + nPhotonsXTalk, photon->time(), photon->isReflected() );
    deposits.add( deposit ); // TODO: why not an emplace_back?

    // Generate delayed XTalk
    if ( m_simulateIntraChannelXTalk ) {
      int nDelayedXtalk = m_sipmTool->generateDelayedXTalk( photon->nPhotons() );
      for ( int i = 0; i < nDelayedXtalk; ++i ) {
        float              addTime = m_sipmTool->generateDelayedXTalkTime();
        LHCb::MCFTDeposit* deposit =
            new LHCb::MCFTDeposit( targetChannel, photon->mcHit(), 1, photon->time() + addTime, photon->isReflected() );
        deposits.add( deposit );
      }
    }
  } // loop over photons
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Number of deposits after looping over photons = " << deposits.size() << endmsg;
}

//=============================================================================
// Effective simulation
//=============================================================================
void MCFTDepositCreator::convertHitToDepositsEffective( const LHCb::MCHit* mchit, const DeFTMat& mat, double timeOffset,
                                                        LHCb::MCFTDeposits& deposits ) const {
  // Get deposited energy
  const double hitEnergy = mchit->energy();

  // Get the local coordinates
  const Gaudi::XYZPoint  localEntry = mat.toLocal( mchit->entry() );
  const Gaudi::XYZVector localDispl = mat.toLocal( mchit->displacement() );

  const Gaudi::XYZPoint localExit = localEntry + localDispl;
  const Gaudi::XYZPoint localMid  = localEntry + 0.5 * localDispl;

  // -- Calculate the attenuation using the AttenuationTool
  auto [attDir, attRef] = m_attenuationTool->attenuation( mchit->midPoint().X(), mchit->midPoint().Y() );

  // -- Calculate the minimal arrival time
  const double timeEvent = mchit->time() + timeOffset;
  const double distDir   = mat.distanceToSiPM( localMid );
  const double distRef   = 2. * mat.fibreLength() - distDir;

  // -- Get average propagation times
  const double propTimeDirMean = timeEvent + m_photonTool->averagePropagationTime( distDir );
  const double propTimeRefMean = timeEvent + m_photonTool->averagePropagationTime( distRef );

  // Get channels and fractions
  auto channelsAndFracs = m_distrChannelTool->targetChannelsFractions( localEntry.X(), localExit.X(), mat );

  // Time observables
  double timeScint = m_photonTool->generateScintillationTime();
  double timeDir   = timeScint + propTimeDirMean;
  double timeRef   = timeScint + propTimeRefMean;

  for ( auto channelAndFrac : channelsAndFracs ) {
    bool isReflected = false;
    for ( const auto& pair : {std::make_pair( attDir, timeDir ), std::make_pair( attRef, timeRef )} ) {
      // calculate fractional energy
      double energy = hitEnergy * channelAndFrac.second * pair.first;

      // get number of expected photons
      double nPhotonsExp = m_photonTool->numExpectedPhotons( energy );

      // get number of observed photons
      int nPhotons = m_photonTool->numObservedPhotons( nPhotonsExp );

      // simulate direct cross-talk
      unsigned int nPhotonsXTalk = 0;
      if ( m_simulateIntraChannelXTalk ) nPhotonsXTalk = m_sipmTool->generateDirectXTalk( nPhotons );

      auto deposit = new LHCb::MCFTDeposit( channelAndFrac.first, mchit, nPhotons + nPhotonsXTalk, pair.second,
                                            isReflected ); // TODO: why not emplace_back?
      deposits.add( deposit );

      // Generate delayed XTalk
      if ( m_simulateIntraChannelXTalk ) {
        int nDelayedXtalk = m_sipmTool->generateDelayedXTalk( nPhotons );
        for ( int i = 0; i < nDelayedXtalk; ++i ) {
          float addTime = m_sipmTool->generateDelayedXTalkTime();
          auto  deposit = new LHCb::MCFTDeposit( channelAndFrac.first, mchit, 1, pair.second + addTime,
                                                isReflected ); // TODO: why not emplace_back?
          deposits.add( deposit );
        }
      }

      isReflected = true; // set to true for 2nd iteration
    }
  }
}
