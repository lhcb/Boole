/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @file MCFTDigitCreator.cpp
 *
 *  From the list of MCFTDeposit (deposited energy in each FTChannel),
 *  this algorithm converts the deposited energy in ADC charge according
 *  to the mean number of photoelectron per MIP
 *  + the mean ADC count per photoelectron.
 *  Created digits are put in transient data store.
 *
 *  @author COGNERAS Eric, Luca Pescatore
 *  @date   2012-04-04
 *  @author Louis Henry
 *  @date   2024-03-20
 */

#ifndef MCFTDIGITCREATOR_H
#  define MCFTDIGITCREATOR_H 1

// from Gaudi
#  include "GaudiAlg/Transformer.h"
#  include "GaudiKernel/RndmGenerators.h"
#  include <Gaudi/Accumulators.h>

// LHCbKernel
#  include "Detector/FT/FTChannelID.h"

// from FTEvent
#  include "Event/MCFTDeposit.h"
#  include "Event/MCFTDigit.h"

// local
#  include "IFTSiPMTool.h"
#  include "SiPMResponse.h"

class MCFTDigitCreator : public Gaudi::Functional::Transformer<LHCb::MCFTDigits( const LHCb::MCFTDeposits& )> {

public:
  /// Standard constructor

  MCFTDigitCreator( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode       initialize() override;
  LHCb::MCFTDigits operator()( const LHCb::MCFTDeposits& deposits ) const override;

private:
  mutable Rndm::Numbers m_gauss;

  Gaudi::Property<float> m_sipmGainVariation{this, "SiPMGainVariation", 0.05, "relative fluctuation of the gain"};
  Gaudi::Property<float> m_adcNoise{this, "ADCNoise", 0.34, "Sigma of the noise in the electronics ADC conversion"};
  Gaudi::Property<std::vector<double>> m_integrationOffset{
      this,
      "IntegrationOffset",
      {26 * Gaudi::Units::ns, 28 * Gaudi::Units::ns, 30 * Gaudi::Units::ns},
      "Vector of the integration offsets for T1,T2,T3"};
  Gaudi::Property<bool> m_ignoreZeroADCDigits{this, "IgnoreZeroADCDigits", true, "Do not write digits when ADC <= 0"};
  Gaudi::Property<bool> m_ignoreElectronicsSpillover{this, "IgnoreElectronicsSpillover", false,
                                                     "Ignore electronics response at t-25 ns & t+25 ns"};

  Gaudi::Property<float> m_ShiftTimeWindow{this, "ShiftTimeWindow", 0.00, "shift of time window"};

  ToolHandle<SiPMResponse> m_SiPMResponse{this, "SiPMResponse", "SiPMResponse", "SiPM integrated response function"};

  // add the SiPMTool adds the noise deposits, adc thresholds and the wavelenght-dependent efficiency
  ToolHandle<IFTSiPMTool> m_sipmTool{this, "SiPMToolName", "FTSiPMTool", "Name of the SiPMTool"};
};
#endif // MCFTDIGITCREATOR_H

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MCFTDigitCreator )

MCFTDigitCreator::MCFTDigitCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, KeyValue{"InputLocation", LHCb::MCFTDepositLocation::Default},
                   KeyValue{"OutputLocation", LHCb::MCFTDigitLocation::Default} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode MCFTDigitCreator::initialize() {
  StatusCode sc = Transformer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  sc = m_gauss.initialize( randSvc(), Rndm::Gauss( 0., 1. ) );
  if ( sc.isFailure() ) {
    error() << "Failed to get Rndm::Gauss generator" << endmsg;
    return sc;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::MCFTDigits MCFTDigitCreator::operator()( const LHCb::MCFTDeposits& deposits ) const {
  // Define digits container and register it in the transient data store
  LHCb::MCFTDigits digits;
  digits.reserve( 75e3 );

  // For each FTchannel, converts the deposited photons in ADC count
  double response       = 0.;
  int    total_photon   = 0;
  double total_t_photon = 0;

  std::vector<const LHCb::MCFTDeposit*> contributingDeposits;
  LHCb::MCFTDeposits::const_iterator    it     = deposits.begin();
  auto                                  nextIt = it + 1;
  for ( ; it != deposits.end(); ++it, ++nextIt ) {
    const LHCb::MCFTDeposit* deposit = *it;
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Make digit from " << deposit->channelID() << endmsg;

    // Calculate the electronic response for this PE
    float time_SiPM =
        deposit->time() - m_integrationOffset[deposit->channelID().globalStationIdx()]; // TODO: time alignment

    if ( time_SiPM >= m_ShiftTimeWindow && time_SiPM < ( m_ShiftTimeWindow + 25.f ) ) { // TODO: hardcoded length of
                                                                                        // time window
      response += deposit->nPhotons() * m_SiPMResponse->response( time_SiPM );
    }

    total_photon += deposit->nPhotons();
    total_t_photon += time_SiPM * deposit->nPhotons();

    contributingDeposits.push_back( deposit );

    // Save the digit when the next deposit is a different channel
    if ( nextIt == deposits.end() || deposit->channelID() != ( *nextIt )->channelID() ) {

      //== Add noise from the digitisation of the signal
      //   (pedestal subtraction + gain correction of peaks)
      float gain  = 1. + m_gauss() * m_sipmGainVariation;
      float noise = m_adcNoise * m_gauss();
      float PE    = response * gain + noise;
      int   adc   = ( PE >= m_sipmTool->threshold3()
                      ? 3
                      : PE >= m_sipmTool->threshold2() ? 2 : PE >= m_sipmTool->threshold1() ? 1 : 0 );

      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << format( "deposit2PhotoElectrons() : Response=%8.3f Gain=%8.3f "
                             "Noise=%4.2f PE = %4f ",
                             response, gain, noise, PE )
                  << endmsg;

      if ( adc > 0 || !m_ignoreZeroADCDigits ) {
        LHCb::MCFTDigit* digit = new LHCb::MCFTDigit( deposit->channelID(), PE, adc, contributingDeposits );
        digit->setTime( total_t_photon / total_photon );
        digits.insert( digit );
      }

      // reset
      response       = 0.;
      total_photon   = 0;
      total_t_photon = 0.;
      contributingDeposits.clear();
    }
  } // loop on deposits

  // Digits are sorted according to their ChannelID to prepare the clusterisation
  std::stable_sort( digits.begin(), digits.end(), LHCb::MCFTDigit::lowerByChannelID );

  return digits;
}
