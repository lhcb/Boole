/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IMCFTPHOTONTOOL_H
#define IMCFTPHOTONTOOL_H 1

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

/** @class IMCFTPhotonTool IMCFTPhotonTool.h
 *
 *  Interface for the tool that transforms deposited energies to photons exiting
 *  the fibre ends
 *
 *  @author Violaine Bellee, Julian Wishahi
 *  @date   2017-02-14
 */

struct IMCFTPhotonTool : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( IMCFTPhotonTool, 1, 0 );

  virtual double numExpectedPhotons( double effective_energy ) const     = 0;
  virtual int    numObservedPhotons( double num_expected_photons ) const = 0;
  virtual double averagePropagationTime( double distToSiPM ) const       = 0;
  virtual double generateScintillationTime() const                       = 0;
  virtual void   generatePhoton( double& time, double& wavelength, double& posX, double& posZ, double& dXdY,
                                 double& dZdY ) const                    = 0;
};

#endif // IMCFTPHOTONTOOL_H
