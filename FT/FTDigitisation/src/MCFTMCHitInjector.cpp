/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @file MCFTMCHitInjector.cpp
 *
 *  Implementation of class : MCFTMCHitInjector
 *
 *  @author Belle, Violaine and Wishahi, Julian
 *  @date   2016-11-26
 */

#include "Detector/FT/FTChannelID.h"
#include "Event/MCHit.h"
#include "FTDet/DeFTDetector.h"
#include "LHCbAlgs/Transformer.h"

#include <range/v3/all.hpp>
#include <vector>

class MCFTMCHitInjector : public LHCb::Algorithm::Transformer<std::vector<LHCb::MCHit>( DeFT const& ),
                                                              LHCb::DetDesc::usesConditions<DeFT>> {

public:
  MCFTMCHitInjector( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode               initialize() override;
  std::vector<LHCb::MCHit> operator()( DeFT const& ) const override;

private:
  Gaudi::Property<std::vector<unsigned int>> m_targetFTStations{
      this, "TargetStations", {1u}, "Target Station IDs (unsigned int)"};
  Gaudi::Property<std::vector<unsigned int>> m_targetFTLayers{
      this, "TargetLayers", {1u}, "Target Layer IDs (unsigned int)"};
  Gaudi::Property<std::vector<unsigned int>> m_targetFTQuarters{
      this, "TargetQuarters", {1u}, "Target Quarter IDs (unsigned int)"};
  Gaudi::Property<std::vector<unsigned int>> m_targetFTModules{
      this, "TargetModules", {1u}, "Target Module IDs (unsigned int)"};
  Gaudi::Property<std::vector<unsigned int>> m_targetFTMats{this, "TargetMats", {1u}, "Target Mat IDs (unsigned int)"};

  std::vector<LHCb::Detector::FTChannelID> m_targetFTChannelIDs;

  Gaudi::Property<std::vector<double>> m_propMCHitXs{this, "MCHitLocalXs", {0.}, "MCHit X entries (local)"};
  Gaudi::Property<std::vector<double>> m_propMCHitYs{this, "MCHitLocalYs", {0.}, "MCHit Y entries (local)"};
  Gaudi::Property<std::vector<double>> m_propMCHitDeltaXs{this, "MCHitDeltaXs", {0.}, "MCHit DeltaX (local)"};
  Gaudi::Property<std::vector<double>> m_propMCHitDeltaYs{this, "MCHitDeltaYs", {0.}, "MCHit DeltaY (local)"};
  Gaudi::Property<std::vector<double>> m_propMCHitEnergies{this, "MCHitEnergies", {0.3}, "MCHit deposited energies"};
  Gaudi::Property<std::vector<double>> m_propMCHitMomenta{this, "MCHitMomenta", {150000.}, "MCHit momenta"};
  Gaudi::Property<std::vector<double>> m_propMCHitTimes{this, "MCHitTimes", {24.}, "MCHit times"};
};

#if RANGE_V3_VERSION < 900
namespace ranges::views {
  using namespace ranges::view;
}
#endif

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MCFTMCHitInjector )

MCFTMCHitInjector::MCFTMCHitInjector( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, {KeyValue{"FTDetectorLocation", DeFTDetectorLocation::Default}},
                   KeyValue{"OutputLocation", "/Event/MC/FT/InjectorHits"} ) {}

StatusCode MCFTMCHitInjector::initialize() {
  return Transformer::initialize().andThen( [&] {
    for ( const auto&& [station, layer, quarter, module, mat] : ranges::views::zip(
              m_targetFTStations, m_targetFTLayers, m_targetFTQuarters, m_targetFTModules, m_targetFTMats ) ) {
      m_targetFTChannelIDs.emplace_back(
          LHCb::Detector::FTChannelID::StationID{station}, LHCb::Detector::FTChannelID::LayerID{layer},
          LHCb::Detector::FTChannelID::QuarterID{quarter}, LHCb::Detector::FTChannelID::ModuleID{module},
          LHCb::Detector::FTChannelID::MatID{mat}, 0u, 0u );
    }
    return StatusCode::SUCCESS;
  } );
}

std::vector<LHCb::MCHit> MCFTMCHitInjector::operator()( DeFT const& deFT ) const {
  // define photons container
  std::vector<LHCb::MCHit> mchitsCont;
  auto mchitProps = ranges::views::zip( m_propMCHitXs, m_propMCHitYs, m_propMCHitDeltaXs, m_propMCHitDeltaYs,
                                        m_propMCHitEnergies, m_propMCHitMomenta, m_propMCHitTimes );
  mchitsCont.reserve( mchitProps.size() );
  for ( auto targetFTChannelID : m_targetFTChannelIDs ) {
    // info() << "MCHits in: " << targetFTChannelID << endmsg;
    const auto& mat           = deFT.findMat( targetFTChannelID );
    auto        mat_thickness = mat->fibreMatThickness();
    for ( const auto&& [x, y, dx, dy, e, p, t] : mchitProps ) {
      Gaudi::XYZPoint  enPointLoc( x, y, -mat_thickness / 2. );
      Gaudi::XYZVector displVecLoc( dx, dy, mat_thickness );
      auto             enPointGlob  = mat->toGlobal( enPointLoc );
      auto             displVecGlob = mat->toGlobal( displVecLoc );
      LHCb::MCHit      mc_hit{};
      mc_hit.setEntry( enPointGlob );
      mc_hit.setDisplacement( displVecGlob );
      mc_hit.setEnergy( e );
      mc_hit.setP( p );
      mc_hit.setTime( t );
      mc_hit.setSensDetID( deFT.sensitiveVolumeID( mc_hit.midPoint() ) );
      mchitsCont.push_back( mc_hit );
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << " MCHit : "
                << "entry  " << mc_hit.entry() << ", exit   " << mc_hit.exit() << ", E " << mc_hit.energy() << ", p "
                << mc_hit.p() << ", t " << mc_hit.time() << ", sensDetID " << mc_hit.sensDetID() << endmsg;
      }
    }
  }
  return mchitsCont;
}
