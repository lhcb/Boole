/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// from Gaudi
#include "LHCbAlgs/Transformer.h"

// LHCbKernel
#include "Detector/FT/FTChannelID.h"

// from Linker
#include "Associators/Associators.h"

// from Detector
#include "Detector/FT/FTConstants.h"

// from FTEvent
#include "Event/FTCluster.h"
#include "Event/FTLiteCluster.h"
#include "Event/MCFTDeposit.h"
#include "Event/MCFTDigit.h"

/** @class FTFPGAClusterCreator FTFPGAClusterCreator.h
 *
 *
 *  @author Eric Cogneras
 *  @date   2012-04-06
 *  @author Louis Henry
 *  @date   2024-03-20
 */

class FTFPGAClusterCreator
    : public LHCb::Algorithm::MultiTransformer<
          std::tuple<LHCb::FTClusters, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey>(
              const LHCb::MCFTDigits& )> {

public:
  FTFPGAClusterCreator( const std::string& name, ISvcLocator* pSvcLocator );

  std::tuple<LHCb::FTClusters, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey>
  operator()( const LHCb::MCFTDigits& digits ) const override;

private:
  // Job options
  Gaudi::Property<bool> m_storePECharge{this, "StorePECharge", true, "Flag to store PE instead of ADC in FTCluster"};

  Gaudi::Property<unsigned int> m_clusterMinWidth{this, "ClusterMinWidth", 1, "Minimal cluster width"};
  Gaudi::Property<unsigned int> m_clusterMaxWidth{this, "ClusterMaxWidth", 4, "Maximal cluster width"};
  Gaudi::Property<float>        m_lowestFraction{this, "LowestFraction", -0.250,
                                          "The fraction is defined in the range (-0.250,0.750)"};
  Gaudi::Property<unsigned int> m_largeClusterSize{this, "LargeClusterSize", 4,
                                                   "Define when to flag unfragmented cluster as large"};

  Gaudi::Property<bool> m_usePEnotADC{this, "UsePENotADC", false, "Flag to use (float)PE instead of (int)ADC"};

  Gaudi::Property<bool> m_fragBigCluster{this, "FragBigCluster", true,
                                         "Flag to fragment clusters in ClusterMaxWidth chunks"};
  Gaudi::Property<bool> m_keepBigCluster{this, "KeepBigCluster", false,
                                         "Flag to keep (fragmented or not) big clusters"};
  Gaudi::Property<bool> m_keepIsolatedPeaks{this, "KeepIsolatedPeaks", true, "Flag to keep isolated clusters"};
  Gaudi::Property<bool> m_keepEdgeCluster{this, "KeepEdgeCluster", false,
                                          "Flag to keep sipm edge and central dead area clusters"};

  Gaudi::Property<float> m_adcThreshold1{this, "ADCThreshold1", 1, "add-to-cluster threshold"};
  Gaudi::Property<float> m_adcThreshold2{this, "ADCThreshold2", 2, "seed threshold"};
  Gaudi::Property<float> m_adcThreshold3{this, "ADCThreshold3", 3, "single-channel threshold"};
  Gaudi::Property<float> m_adcThreshold1Weight{this, "ADCThreshold1Weight", 1., "add-to-cluster weight"};
  Gaudi::Property<float> m_adcThreshold2Weight{this, "ADCThreshold2Weight", 2., "seed weight"};
  Gaudi::Property<float> m_adcThreshold3Weight{this, "ADCThreshold3Weight", 6., "single-channel weight"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTFPGAClusterCreator )

// useful constants, funcs
const int m_maxFPGAClusterWidth = 6; // FIXME: Does not correspond (anymore?) to the cluster width (4)

FTFPGAClusterCreator::FTFPGAClusterCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer(
          name, pSvcLocator, {KeyValue{"InputLocation", LHCb::MCFTDigitLocation::Default}},
          {KeyValue{"OutputLocation", LHCb::FTClusterLocation::Default},
           KeyValue{"MCToClusterLocation", Links::location( LHCb::FTLiteClusterLocation::Default )},
           KeyValue{"MCToClusterExtLocation",
                    Links::location( LHCb::FTLiteClusterLocation::Default + "WithSpillover" )},
           KeyValue{"HitToClusterLocation", Links::location( LHCb::FTLiteClusterLocation::Default + "2MCHits" )},
           KeyValue{"HitToClusterExtLocation",
                    Links::location( LHCb::FTLiteClusterLocation::Default + "2MCHitsWithSpillover" )}} ) {}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<LHCb::FTClusters, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey, LHCb::LinksByKey>
FTFPGAClusterCreator::operator()( const LHCb::MCFTDigits& digits ) const {

  // Create the objects to be returned and reserve memory.
  LHCb::LinksByKey mcToClusterLink{std::in_place_type<ContainedObject>, std::in_place_type<LHCb::MCParticle>,
                                   LHCb::LinksByKey::Order::decreasingWeight};
  LHCb::LinksByKey mcToClusterLinkExtended{std::in_place_type<ContainedObject>, std::in_place_type<LHCb::MCParticle>,
                                           LHCb::LinksByKey::Order::decreasingWeight};
  LHCb::LinksByKey hitToClusterLink{std::in_place_type<ContainedObject>, std::in_place_type<LHCb::MCHit>,
                                    LHCb::LinksByKey::Order::decreasingWeight};
  LHCb::LinksByKey hitToClusterLinkExtended{std::in_place_type<ContainedObject>, std::in_place_type<LHCb::MCHit>,
                                            LHCb::LinksByKey::Order::decreasingWeight};

  LHCb::FTClusters clusterCont{};
  clusterCont.reserve( digits.size() );

  std::vector<LHCb::Detector::FTChannelID> ids;
  ids.reserve( 4 );
  //***********************
  //* MAIN LOOP
  //***********************

  // useful constants, funcs
  // size lookup tables : different wrto FPGA code (which is to be changed)
  //(because of channel numbering and bit representation in C++ vs FPGA code!!
  // an easy adaptation shall be made in the FPGA)
  /*
    static std::array<unsigned int, 16> SIZE_TABLE4 = {  //original FPGA
    0, 0, 0, 0,
    0, 0, 0, 0,
    1, 1, 1, 1,
    2, 2, 3, 4
    };
  */
  static std::array<unsigned int, 16> SIZE_TABLE4 = {0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4};

  /*
    static std::array<unsigned int, 32> SIZE_TABLE5 = { //original FPGA
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    1, 1, 1, 1,
    1, 1, 1, 1,
    2, 2, 2, 2,
    3, 3, 4, 5
    };
  */
  static std::array<unsigned int, 32> SIZE_TABLE5 = {0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4,
                                                     0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 5};
  unsigned int uintBmask( ( 1 << m_clusterMaxWidth ) - 1 ); // used to correct std::bitset to_ulong() bug
  unsigned int int_clusterMaxWidth( m_clusterMaxWidth );    // avoid numerous warnings... (find a better way later)

  // Digits loop : Digit Container is sorted wrt channelID
  auto                                       digitIter                                 = digits.begin();
  std::bitset<LHCb::Detector::FT::nChannels> SipmBinValues                             = {};
  LHCb::MCFTDigit*                           SipmDigits[LHCb::Detector::FT::nChannels] = {};
  unsigned int                               lastUniqueSiPM( ( *digitIter )->channelID().globalSiPMID() );
  while ( digitIter != digits.end() ) {
    ids.clear();
    // loop over digits
    LHCb::MCFTDigit* digit = *digitIter;

    // first, fill non zero suppressed sipm digits various data, for each sipm in turn
    // increment iterator
    if ( digit->channelID().globalSiPMID() == lastUniqueSiPM ) {
      unsigned int nchan = digit->channelID().channel();
      SipmDigits[nchan]  = digit;
      if ( ( !m_usePEnotADC && digit->adcCount() >= m_adcThreshold1 ) ||
           ( m_usePEnotADC && digit->photoElectrons() >= m_adcThreshold1 ) ) {
        SipmBinValues.set( nchan );
      }
      digitIter++;
    }

    // new sipm (or last chan in sipm)? : operate on the full (preceding) sipm
    if ( ( digit->channelID().globalSiPMID() != lastUniqueSiPM ) || ( digitIter == digits.end() ) ) {

      //"half-sipm" variables : half sipm (account for midle sipm dead gap)
      union Ubhalf {
        std::bitset<LHCb::Detector::FT::nChannels / 2> SipmBinHalf;
        std::bitset<m_maxFPGAClusterWidth>             locbits;
      };
      union { // probably ok to use union for type punning here
        std::bitset<LHCb::Detector::FT::nChannels> SipmBinValues;
        Ubhalf                                     SipmBinHalfs[2];
      } ub                         = {SipmBinValues};
      const unsigned int chunkSize = LHCb::Detector::FT::nChannels / 2;
      const unsigned int nchunk    = 2;

      // half-sipm actions loop
      unsigned int                               SipmAdcSizes[LHCb::Detector::FT::nChannels]  = {};
      float                                      SipmAdcSums[LHCb::Detector::FT::nChannels]   = {};
      unsigned int                               SipmAdcSizes2[LHCb::Detector::FT::nChannels] = {};
      std::bitset<LHCb::Detector::FT::nChannels> SipmBigFlags                                 = {};
      for ( unsigned int ichunk = 0; ichunk < nchunk; ichunk++ ) {
        // a) build raw size array (in 2 half sipm chunks, to take the middle sipm dead gap)
        for ( unsigned int ichan = ichunk * chunkSize; ichan < ( ichunk + 1 ) * chunkSize; ichan++ ) {
          std::bitset<m_maxFPGAClusterWidth> locbits = ub.SipmBinHalfs[ichunk].locbits;

          // kill excess bits : bitset needs a fixed dim
          for ( int ibit = int_clusterMaxWidth; ibit < m_maxFPGAClusterWidth; ibit++ ) locbits.reset( ibit );
          unsigned int locChunk = uintBmask & (unsigned int)( locbits.to_ulong() );
          if ( int_clusterMaxWidth == 4 )
            SipmAdcSizes[ichan] = SIZE_TABLE4[locChunk];
          else if ( int_clusterMaxWidth == 5 )
            SipmAdcSizes[ichan] = SIZE_TABLE5[locChunk];
          else {
            unsigned int curSize( 0 );
            while ( curSize < int_clusterMaxWidth && locbits.test( curSize ) ) ++curSize;
            SipmAdcSizes[ichan] = curSize;
          };
          ub.SipmBinHalfs[ichunk].SipmBinHalf >>= 1; // shift towards smaller channel number wrto Sipm arrays
        }

        // b) build amplitude sum array (in 2 chunks again)
        for ( unsigned int ichan = ichunk * chunkSize; ichan < ( ichunk + 1 ) * chunkSize; ichan++ ) {
          unsigned int ilocchan( 0 );
          float        locsum( 0 );
          while ( ( ichan + ilocchan ) < ( ichunk + 1 ) * chunkSize && ilocchan < int_clusterMaxWidth &&
                  SipmBinValues.test( ichan + ilocchan ) ) {
            float channelWeight =
                ( m_usePEnotADC ? SipmDigits[ichan + ilocchan]->photoElectrons()
                                : SipmDigits[ichan + ilocchan]->adcCount() >= m_adcThreshold3
                                      ? m_adcThreshold3Weight.value()
                                      : SipmDigits[ichan + ilocchan]->adcCount() >= m_adcThreshold2
                                            ? m_adcThreshold2Weight.value()
                                            : SipmDigits[ichan + ilocchan]->adcCount() >= m_adcThreshold1
                                                  ? m_adcThreshold1Weight.value()
                                                  : 0. );

            locsum += channelWeight;
            ilocchan++;
          };
          SipmAdcSums[ichan] = locsum;
        };

        // c) filter clusters : keep only new clusters or new fragments, test amplitude sum.
        // Careful with the order of the non exclusive ifthenelse(s)
        //(order is crucial, and careful with the run options too)
        for ( unsigned int ichan = ichunk * chunkSize; ichan < ( ichunk + 1 ) * chunkSize; ichan++ ) {
          if ( SipmAdcSizes[ichan] == 0 ) {
            SipmAdcSizes2[ichan] = 0;
          } else if ( ( ichan >= ichunk * chunkSize + int_clusterMaxWidth ) &&
                      ( SipmAdcSizes2[ichan - int_clusterMaxWidth] == int_clusterMaxWidth ) ) {
            // set the bigflag, except for 1st big cluster
            SipmAdcSizes2[ichan] = SipmAdcSizes[ichan];
            SipmBigFlags.set( ichan );
          } else if ( m_keepEdgeCluster && ( ichan == ichunk * chunkSize ) ) {
            SipmAdcSizes2[ichan] = SipmAdcSizes[ichan]; // keep right edge clusters (no conditions)
          } else if ( m_keepEdgeCluster && ( ( ichan + SipmAdcSizes[ichan] - ( ichunk + 1 ) * chunkSize == 0 ) &&
                                             !SipmBinValues.test( ichan - 1 ) ) ) {
            SipmAdcSizes2[ichan] = SipmAdcSizes[ichan]; // keep left edge clusters (no conditions)
          } else if ( ( ( ichan + int_clusterMaxWidth < ( ichunk + 1 ) * chunkSize ) &&
                        SipmBigFlags.test( ichan + int_clusterMaxWidth ) ) // don't filter (1st) subcluster of large
                                                                           // cluster
                      ||
                      ( ( SipmAdcSizes[ichan] > m_clusterMinWidth ) // inequalities : m_clusterMinWidth=2 in FPGA logic
                                                                    // ! (not 1)
                        && ( SipmAdcSums[ichan] < m_adcThreshold1Weight.value() + m_adcThreshold2Weight.value() ) ) ) {
            SipmAdcSizes2[ichan] = 0;
          } else if ( ( SipmAdcSizes[ichan] <= m_clusterMinWidth ) // inequalities changed (see above)
                      && ( !m_keepIsolatedPeaks || ( SipmAdcSums[ichan] < m_adcThreshold3Weight.value() ) ) ) {
            SipmAdcSizes2[ichan] = 0;
          } else if ( ( ichan > ichunk * chunkSize && !SipmBinValues.test( ichan - 1 ) ) ||
                      ( ichan == ichunk * chunkSize ) ) {
            // keep only useful sizes  SipmAdcSizes2[ichan] = SipmAdcSizes[ichan];
            SipmAdcSizes2[ichan] = SipmAdcSizes[ichan];
          } else {
            SipmAdcSizes2[ichan] = 0;
          };
        };
      }; // end chunk actions loop

      // d) remove the BigFlag channels between 1st and last if enabled
      if ( !m_keepBigCluster ) {
        for ( int ichan = 0; ichan < static_cast<int>( LHCb::Detector::FT::nChannels - int_clusterMaxWidth );
              ichan++ ) {
          if ( SipmBigFlags.test( ichan ) && SipmBigFlags.test( ichan + int_clusterMaxWidth ) )
            SipmAdcSizes2[ichan] = 0;
        };
      };

      // now "SipmAdcSizes2" is an nchannels array containing zeros except when a cluster exists
      // this array is parallel to "SipmDigits" and "SipmBigFlags", we can go on with the (old)
      // remaining cluster infos computations

      // e) calculate the cluster charge / mean position
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " ---> Done with cluster finding, now calculating charge / frac.pos" << endmsg;

      // MC infos : list of contributing MCParticles (MCHits)
      std::set<const LHCb::MCParticle*> contributingMCParticles;
      std::set<const LHCb::MCHit*>      contributingMCHits;
      for ( unsigned int ichan = 0; ichan < LHCb::Detector::FT::nChannels; ichan++ ) {
        if ( SipmAdcSizes2[ichan] != 0 ) {
          float  totalCharge   = 0.0;
          double totalChargePE = 0.0;
          double wsumPosition  = 0.0;
          // calculate channel 'weights' as seen by the hardware thresholds
          // (2bits implementation: 3 adc thresholds represent some #PE)
          // (threshold1 < threshold2 < threshold3)
          for ( unsigned int ilocchan = 0; ilocchan < SipmAdcSizes2[ichan]; ilocchan++ ) {
            float channelWeight =
                ( m_usePEnotADC ? SipmDigits[ichan + ilocchan]->photoElectrons()
                                : SipmDigits[ichan + ilocchan]->adcCount() >= m_adcThreshold3
                                      ? m_adcThreshold3Weight.value()
                                      : SipmDigits[ichan + ilocchan]->adcCount() >= m_adcThreshold2
                                            ? m_adcThreshold2Weight.value()
                                            : SipmDigits[ichan + ilocchan]->adcCount() >= m_adcThreshold1
                                                  ? m_adcThreshold1Weight.value()
                                                  : 0. );

            totalCharge += channelWeight;
            totalChargePE += SipmDigits[ichan + ilocchan]->photoElectrons();

            // mean position will be [ (rel. pos. from left) * charge ] / totalCharge (see below)
            ids.push_back( SipmDigits[ichan + ilocchan]->channelID() );
            wsumPosition += ilocchan * channelWeight;
          } // end of loop over digits in cluster

          // compute position : when cluster is an edge of a double or big cluster keep
          // the middle of the cluster only (no weighting), otherwise use weights
          // also add channelID (uint) offset
          double clusPosition;
          if ( SipmBigFlags.test( ichan ) ) { // big cluster trail
            clusPosition = SipmDigits[ichan]->channelID() + float( SipmAdcSizes2[ichan] - 1 ) / 2;
          } else if ( ( ichan + int_clusterMaxWidth < LHCb::Detector::FT::nChannels ) &&
                      SipmBigFlags.test( ichan + int_clusterMaxWidth ) ) {
            // big cluster start
            clusPosition = SipmDigits[ichan]->channelID() + float( SipmAdcSizes2[ichan] - 1 ) / 2;
          } else {
            clusPosition = SipmDigits[ichan]->channelID() + wsumPosition / totalCharge;
          }
          // The fractional position is defined in (-0.250, 0.750)
          unsigned int clusChanPosition     = std::floor( clusPosition - m_lowestFraction );
          double       fractionChanPosition = ( clusPosition - clusChanPosition );
          int          frac                 = int( 2 * ( fractionChanPosition - m_lowestFraction ) );

          // Define new lite cluster and add to container
          LHCb::FTCluster* newCluster =
              new LHCb::FTCluster( LHCb::Detector::FTChannelID{clusChanPosition}, fractionChanPosition, frac, ids,
                                   SipmBigFlags.test( ichan ), m_storePECharge ? totalChargePE : totalCharge );
          clusterCont.insert( newCluster );

          //--Setup MCParticle to cluster link
          // (also for spillover / not accepted clusters, as long as it's not noise)
          // check contributions from MCHits, for linking
          for ( unsigned int ilocchan = 0; ilocchan < SipmAdcSizes2[ichan]; ilocchan++ ) {
            for ( const auto& deposit : SipmDigits[ichan + ilocchan]->deposits() ) {
              const auto& mcHit = deposit->mcHit();
              if ( mcHit != nullptr ) {
                contributingMCHits.insert( mcHit );
                if ( mcHit->mcParticle() != nullptr ) contributingMCParticles.insert( mcHit->mcParticle() );
              }
            }
          } // end of loop over digits in cluster

          for ( const auto& i : contributingMCParticles ) {
            mcToClusterLinkExtended.link( clusChanPosition, i ); // Needed for xdigi / xdst
            if ( i->parent()->registry()->identifier() == "/Event/" + LHCb::MCParticleLocation::Default )
              mcToClusterLink.link( clusChanPosition, i );
          }
          // Setup MCHit to cluster link
          for ( const auto& i : contributingMCHits ) {
            hitToClusterLinkExtended.link( clusChanPosition, i ); // Needed for xdigi / xdst
            if ( i->parent()->registry()->identifier() == "/Event/" + LHCb::MCHitLocation::FT )
              hitToClusterLink.link( clusChanPosition, i );
          }

        } // end of existing cluster

      } // end of current sipm treatment

      //----------------------> refresh for new sipm
      lastUniqueSiPM = digit->channelID().globalSiPMID();
      for ( unsigned int i = 0; i < LHCb::Detector::FT::nChannels; i++ ) { SipmDigits[i] = 0; };
      SipmBinValues.reset();

    } // end of new sipm only code

  } // end of loop over Digits

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Number of FTClusters created: " << clusterCont.size() << endmsg;

  // Extra checks
  static_assert( std::is_move_constructible<LHCb::FTClusters>::value,
                 "FTClusters must be move constructible for this to work." );

  return std::make_tuple( std::move( clusterCont ), std::move( mcToClusterLink ), std::move( mcToClusterLinkExtended ),
                          std::move( hitToClusterLink ), std::move( hitToClusterLinkExtended ) );
}
