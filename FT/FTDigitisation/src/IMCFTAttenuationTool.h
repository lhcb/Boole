/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/IAlgTool.h"

/**
 *  Interface for the tool that calculates the light attenuation for the MCFTDepositCreator
 *
 *  @author Michel De Cian
 *  @date   2015-01-15
 */
struct IMCFTAttenuationTool : extend_interfaces<IAlgTool> {
  // Return the interface ID
  DeclareInterfaceID( IMCFTAttenuationTool, 1, 0 );

  struct Attenuation {
    double att, attRef;
  };
  virtual Attenuation attenuation( double hitXPosition, double hitYPosition ) const = 0;
};
