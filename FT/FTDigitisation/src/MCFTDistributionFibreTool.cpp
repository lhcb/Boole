/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MCFTDistributionFibreTool.h"

#include <Core/FloatComparison.h>
#include <Detector/FT/FTChannelID.h>

//-----------------------------------------------------------------------------
// Implementation file for class : MCFTDistributionFibreTool
//
// 2017-02-20 Violaine Bellee, Julian Wishahi, Luca Pescatore
//-----------------------------------------------------------------------------
//
// Declaration of the Tool Factory
DECLARE_COMPONENT( MCFTDistributionFibreTool )

IMCFTDistributionFibreTool::GeomCache::GeomCache( const MCFTDistributionFibreTool& self, const DeFT& det ) {
  if ( det.version() < 61 )
    throw GaudiException( "This version requires FTDet v6.1 or higher", "IMCFTDistributionFibreTool::GeomCache",
                          StatusCode::FAILURE );
  const auto& mat = det.findMat( {LHCb::Detector::FTChannelID::StationID{1u}, LHCb::Detector::FTChannelID::LayerID{0u},
                                  LHCb::Detector::FTChannelID::QuarterID{0u}, LHCb::Detector::FTChannelID::ModuleID{0u},
                                  LHCb::Detector::FTChannelID::MatID{0u}, 0, 0} ); // get arbitrary mat
  float       matSizeX = mat->fibreMatWidth();

  m_minZ = -0.5f * ( ( self.m_nlayers - 1 ) * self.m_deltaZ + 2.f * self.m_fibreCoreRadius );

  // Get the first and last fibres for even (0) and odd (1) layers
  int nFibres = static_cast<int>( matSizeX / self.m_fibrePitch );
  if ( nFibres % 2 == 0 ) {
    m_firstFibre1 = -nFibres / 2;
    m_firstFibre0 = m_firstFibre1 + 1;
    m_lastFibre1  = -m_firstFibre0;
  } else {
    m_firstFibre0 = -( nFibres - 1 ) / 2;
    m_firstFibre1 = m_firstFibre0;
    m_lastFibre1  = -m_firstFibre0 - 1;
  }
}

StatusCode MCFTDistributionFibreTool::initialize() {
  return ConditionAccessorHolder::initialize().andThen( [this] {
    addConditionDerivation( {DeFTDetectorLocation::Default}, m_geomCache.key(), [this]( const DeFT& det ) {
      return GeomCache{*this, det};
    } );
  } );
}

const MCFTDistributionFibreTool::GeomCache& MCFTDistributionFibreTool::getGeomCache() const {
  return m_geomCache.get();
}

// Perform the calculation in 2D, i.e. in x and z of the local coordinate
// system of the mat. The coordinate system is such that (0,0) is at the
// center of the mat.
std::vector<std::pair<Gaudi::XYZPoint, double>>
MCFTDistributionFibreTool::effectivePathFracInCores( const Gaudi::XYZPoint& enPoint, const Gaudi::XYZPoint& exPoint,
                                                     const GeomCache& cache ) const {
  // Initialize list of fibre positions and path fractions
  std::vector<std::pair<Gaudi::XYZPoint, double>> fibreposAndPathfracs;

  // Omit hits with very small z path
  if ( std::abs( enPoint.Z() - exPoint.Z() ) < 0.005 ) return fibreposAndPathfracs;

  // Set entry and exit point in 2d plane. Define entry point as the point at
  // the lower edge of the fibre mat, even if the hit flies into negative z
  // direction.
  Vec2 vec_en( enPoint.X(), enPoint.Z() );
  Vec2 vec_ex( exPoint.X(), exPoint.Z() );

  // Calculate slopes and total path
  double path_x     = exPoint.X() - enPoint.X();
  double path_z     = exPoint.Z() - enPoint.Z();
  double path_total = sqrt( std::pow( path_x, 2 ) + std::pow( path_z, 2 ) );
  double dx_over_dz = path_x / path_z;
  double dy_over_dz = ( exPoint.Y() - enPoint.Y() ) / path_z;

  // Make sure that entrance point is always at lower z
  if ( path_z < 0.0 ) std::swap<Vec2>( vec_en, vec_ex );

  // Initialize list of fibre numbers and path fractions
  std::vector<std::pair<std::pair<int, int>, double>> fibresAndPathfracs;

  // Calculate the positions of the fibres to consider in the
  // calculation. For this, we calculate the x position of the hit at the z of
  // the layer of the fibre cores. Then, we get the indices of fibres in the
  // layer that need to be taken into account for each layer.
  double z_min = cache.m_minZ;
  for ( int iLayer = 0; iLayer < m_nlayers; ++iLayer, z_min += m_deltaZ ) {
    Vec2 vec_fibre_center( 0., getFibreZ( iLayer ) );

    // check if the layer is within the path of the MC hit, if not, continue
    if ( vec_en( 1 ) > vec_fibre_center( 1 ) + m_fibreCoreRadius ||
         vec_ex( 1 ) < vec_fibre_center( 1 ) - m_fibreCoreRadius )
      continue;

    // get the minimal and maximal fibre_indices to consider
    double x_min      = MCFT::getXOnPath( vec_en, vec_ex, z_min );
    auto   fibreRange = getAdjacentFibreIds( x_min, dx_over_dz, iLayer, cache );

    for ( int iFibre = fibreRange.first; iFibre <= fibreRange.second; ++iFibre ) {
      // calculate distance to fibre
      vec_fibre_center( 0 ) = getFibreX( iLayer, iFibre );

      // add distance in fibres only if distance is smaller than the core radius
      if ( MCFT::minDistanceFromLineSegmentToPoint( vec_en, vec_ex, vec_fibre_center ) < m_fibreCoreRadius ) {
        double minDist           = MCFT::minDistanceFromLineToPoint( vec_en, vec_ex, vec_fibre_center );
        double secant_halflength = sqrt( std::pow( m_fibreCoreRadius, 2 ) - std::pow( minDist, 2 ) );

        // Split needed to handle the case where MCHit does not fully traverse layer.
        double dist_in_core = 0.;
        if ( MCFT::distanceTo( vec_en, vec_fibre_center ) > m_fibreCoreRadius ) {
          dist_in_core += secant_halflength;
        } else {
          dist_in_core += ROOT::Math::Dot( ( vec_ex - vec_en ), ( vec_fibre_center - vec_en ) ) / path_total;
        }
        if ( MCFT::distanceTo( vec_ex, vec_fibre_center ) > m_fibreCoreRadius ) {
          dist_in_core += secant_halflength;
        } else {
          dist_in_core += ROOT::Math::Dot( ( vec_ex - vec_en ), ( vec_ex - vec_fibre_center ) ) / path_total;
        }

        fibresAndPathfracs.push_back( {{iFibre, iLayer}, dist_in_core / path_total} );
      }
    }
  }

  // Apply cross talk
  if ( m_crossTalkProb > 0.0 ) {
    // The contribution from cross talk
    std::vector<std::pair<std::pair<int, int>, double>> fibresAndFracsCT;

    for ( auto& fibreAndPathfrac : fibresAndPathfracs ) {
      double fracExchanged = fibreAndPathfrac.second * m_crossTalkProb;
      //  Remove the contribution from the considered fibre
      fibreAndPathfrac.second -= fracExchanged;
      int iFibre = fibreAndPathfrac.first.first;
      int iLayer = fibreAndPathfrac.first.second;
      // Add contribution from the 2 side fibres
      if ( iFibre > cache.firstFibre( iLayer ) )
        fibresAndFracsCT.push_back( {{iFibre - 1, iLayer}, fracExchanged * m_CTmodelSide} );
      if ( iFibre < cache.lastFibre( iLayer ) )
        fibresAndFracsCT.push_back( {{iFibre + 1, iLayer}, fracExchanged * m_CTmodelSide} );
      // Add contribution from the 4 touching fibres
      double xtalkTouching = fracExchanged * m_CTmodelTouching;
      if ( iLayer > 0 ) {
        if ( iLayer % 2 == 0 ) {
          fibresAndFracsCT.push_back( {{iFibre, iLayer - 1}, xtalkTouching} );
          fibresAndFracsCT.push_back( {{iFibre - 1, iLayer - 1}, xtalkTouching} );
        } else {
          if ( iFibre >= cache.firstFibre( iLayer - 1 ) )
            fibresAndFracsCT.push_back( {{iFibre, iLayer - 1}, xtalkTouching} );
          if ( iFibre + 1 <= cache.lastFibre( iLayer - 1 ) )
            fibresAndFracsCT.push_back( {{iFibre + 1, iLayer - 1}, xtalkTouching} );
        }
      }
      if ( iLayer < m_nlayers - 1 ) {
        if ( iLayer % 2 == 0 ) {
          fibresAndFracsCT.push_back( {{iFibre, iLayer + 1}, xtalkTouching} );
          fibresAndFracsCT.push_back( {{iFibre - 1, iLayer + 1}, xtalkTouching} );
        } else {
          if ( iFibre >= cache.firstFibre( iLayer + 1 ) )
            fibresAndFracsCT.push_back( {{iFibre, iLayer + 1}, xtalkTouching} );
          if ( iFibre + 1 <= cache.lastFibre( iLayer + 1 ) )
            fibresAndFracsCT.push_back( {{iFibre + 1, iLayer + 1}, xtalkTouching} );
        }
      }
      // Add contribution from the 2 fibres that are 2 layers away
      if ( iLayer > 1 ) fibresAndFracsCT.push_back( {{iFibre, iLayer - 2}, fracExchanged * m_CTmodel2layers} );
      if ( iLayer < m_nlayers - 2 )
        fibresAndFracsCT.push_back( {{iFibre, iLayer + 2}, fracExchanged * m_CTmodel2layers} );
    }

    // Combine the original and cross talk contributions
    for ( const auto& fibreAndFracCT : fibresAndFracsCT ) {
      // find the fibre of fibreAndFracCT that also occurs in fibresAndPathfracs
      auto fibreAndPathfrac =
          std::find_if( fibresAndPathfracs.begin(), fibresAndPathfracs.end(),
                        [&fibreAndFracCT]( const auto& i ) { return i.first == fibreAndFracCT.first; } );
      if ( fibreAndPathfrac != fibresAndPathfracs.end() ) {
        fibreAndPathfrac->second += fibreAndFracCT.second;
      } else {
        fibresAndPathfracs.push_back( fibreAndFracCT );
      }
    }
  }

  // loop over new vector to create final result
  for ( const auto& fibreAndPathfrac : fibresAndPathfracs ) {
    int    iFibre      = fibreAndPathfrac.first.first;
    int    iLayer      = fibreAndPathfrac.first.second;
    double zLayer      = getFibreZ( iLayer );
    double yPosAtLayer = enPoint.Y() + ( zLayer - enPoint.Z() ) * dy_over_dz;
    fibreposAndPathfracs.push_back(
        {Gaudi::XYZPoint( getFibreX( iLayer, iFibre ), yPosAtLayer, zLayer ), fibreAndPathfrac.second} );
  }
  return fibreposAndPathfracs;
}

double MCFT::distanceToSquared( const Vec2& v, const Vec2& p ) {
  return std::pow( p( 0 ) - v( 0 ), 2 ) + std::pow( p( 1 ) - v( 1 ), 2 );
}

double MCFT::distanceTo( const Vec2& v, const Vec2& p ) { return sqrt( MCFT::distanceToSquared( v, p ) ); }

double MCFT::minDistanceFromLineSegmentToPoint( const Vec2& v, const Vec2& w, const Vec2& p ) {
  const double distSq = MCFT::distanceToSquared( v, w );                  // i.e. |w-v|^2 ... avoid a sqrt
  if ( LHCb::essentiallyZero( distSq ) ) return MCFT::distanceTo( v, p ); // v == w case

  // consider the line extending the segment, parameterized as v + t (w - v)
  // we find projection of point p onto the line
  // it falls where t = [(p-v) . (w-v)] / |w-v|^2

  const double t = ROOT::Math::Dot( p - v, w - v ) / distSq;
  if ( t < 0.0 )
    return MCFT::distanceTo( v, p ); // beyond the v end of the segment
  else if ( t > 1.0 )
    return MCFT::distanceTo( w, p ); // beyond the w end of the segment

  // projection falls on the segment
  const Vec2 projection = v + ( ( w - v ) * t );

  return MCFT::distanceTo( p, projection );
}

double MCFT::minDistanceFromLineToPoint( const Vec2& v, const Vec2& w, const Vec2& p ) {
  // absolute cross-product of the vector (w-v)/|w-v| and (p-v)
  return ( ( v( 1 ) - w( 1 ) ) * p( 0 ) + ( w( 0 ) - v( 0 ) ) * p( 1 ) + ( v( 0 ) * w( 1 ) - w( 0 ) * v( 1 ) ) ) /
         MCFT::distanceTo( v, w );
}

std::pair<int, int> MCFTDistributionFibreTool::getAdjacentFibreIds( double x, double dx_over_dz, unsigned int iLayer,
                                                                    const GeomCache& cache ) const {
  // transform x into local coordinate system x' of layer, in which the fibre
  // with index 0 is at x' = 0
  std::pair<int, int> fibreRange;
  double              xprime = x - ( iLayer % 2 ) * 0.5 * m_fibrePitch;
  fibreRange.first           = std::lround( xprime / m_fibrePitch );

  // get x at the upper edge of the sensitive material
  xprime += dx_over_dz * 2 * m_fibreCoreRadius;
  fibreRange.second = std::lround( xprime / m_fibrePitch );

  // swap indices to allow for ++i iteration
  if ( fibreRange.first > fibreRange.second ) std::swap<int>( fibreRange.first, fibreRange.second );

  // Restrict the fibres to the boundaries of the mat
  if ( iLayer % 2 == 0 ) {
    fibreRange.first  = std::max( cache.m_firstFibre0, fibreRange.first );
    fibreRange.second = std::min( -cache.m_firstFibre0, fibreRange.second );
  } else {
    fibreRange.first  = std::max( cache.m_firstFibre1, fibreRange.first );
    fibreRange.second = std::min( cache.m_lastFibre1, fibreRange.second );
  }

  return fibreRange;
}
