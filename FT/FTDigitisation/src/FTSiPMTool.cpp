/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "IFTSiPMTool.h"
#include "MCFTCommonTools.h"
#include "SiPMResponse.h"

#include "Core/FloatComparison.h"
#include <CLHEP/Random/RandPoissonQ.h>
#include <FTDet/DeFTDetector.h>
#include <GaudiAlg/GaudiTool.h>
#include <GaudiKernel/RndmGenerators.h>

#include "SiPMResponse.h" // gainshift & pacific version

#include <GaudiKernel/IRndmGenSvc.h>
#include <GaudiKernel/SystemOfUnits.h>
#include <Math/PdfFunc.h>
#include <Math/ProbFunc.h>
#include <numeric>

/** @class FTSiPMTool FTSiPMTool.h
 *
 *  Tool that adds cross-talk and noise from the SiPM and
 *  the efficiency function for detecting photons.
 *
 *  @author Violaine Belle, Julian Wishahi
 *  @date   2017-02-21
 *  @author Jacco de Vries, Violaine Bellee, and Julian Wishahi
 *  @date   2017-02-14
 */

class FTSiPMTool : public extends<GaudiTool, IFTSiPMTool> {

public:
  using base_class::base_class;

  StatusCode initialize() override;

  ///==========================================================================
  /// Main method for simulating noise
  ///==========================================================================
  void addNoise( const DeFT& det, LHCb::MCFTDeposits* depositCont ) const override;

  //=============================================================================
  // Return effective noise function output
  //=============================================================================
  float returnEffNoise( float p0, float p1 ) const override;

  //===========================================================================
  // Return number of direct crosstalk PEs based on signal photons
  //===========================================================================
  int generateDirectXTalk( int nPhotons ) const override {

    float nXTPhotons = 0;
    //---LoH: 1 - Sum(p^n for n: 1,inf)
    // So this is the probability of 0 photons
    float a0 = ( 1. - 2. * m_probDirectXTalk ) / ( 1. - m_probDirectXTalk );
    // For each photon, determine how many crosstalk photons are created
    for ( int iPhoton = 0; iPhoton < nPhotons; iPhoton++ ) {
      float           x           = a0;
      float           p           = m_probDirectXTalk;
      float           r           = m_rndmFlat();
      constexpr float MAX_PHOTONS = 5; // Maximum 5 crosstalk photons
      int             iXTPhotons  = 0;
      for ( int tempiXTPhotons = 0; tempiXTPhotons < MAX_PHOTONS; tempiXTPhotons++ ) {
        if ( r < x ) break;
        x += p;
        p *= m_probDirectXTalk;
        iXTPhotons = tempiXTPhotons;
      }
      // Update the number of crosstalk photons
      nXTPhotons += iXTPhotons;
    }
    return nXTPhotons;
  }

  //===========================================================================
  // Generate channel to channel crosstalk deposits
  //===========================================================================

  int generateChannelXTalk() const override { return m_rndmCLHEPPoisson->fire( m_channelXTalkProb ); }

  //===========================================================================
  // Return number of delayed crosstalk PEs based on signal photons
  //===========================================================================
  int generateDelayedXTalk( int nPhotons ) const override {

    float nXTPhotons = 0;
    //---LoH: 1 - Sum(p^n for n: 1,inf)
    // So this is the probability of 0 photons
    float a0 = ( 1. - 2. * m_probDelayedXTalk ) / ( 1. - m_probDelayedXTalk );
    // For each photon, determine how many crosstalk photons are created
    for ( int iPhoton = 0; iPhoton < nPhotons; iPhoton++ ) {
      float           x           = a0;
      float           p           = m_probDelayedXTalk;
      float           r           = m_rndmFlat();
      constexpr float MAX_PHOTONS = 5; // Maximum 5 crosstalk photons
      int             iXTPhotons  = 0;
      for ( int tempiXTPhotons = 0; tempiXTPhotons < MAX_PHOTONS; tempiXTPhotons++ ) {
        if ( r < x ) break;
        x += p;
        p *= m_probDelayedXTalk;
        iXTPhotons = tempiXTPhotons;
      }
      // Update the number of crosstalk photons
      nXTPhotons += iXTPhotons;
    }
    return nXTPhotons;
  }

  //===========================================================================
  // Return (additional) time of delayed crosstalk
  //===========================================================================
  float generateDelayedXTalkTime() const override { return -log( m_rndmFlat() ) * m_delayedXtalkDecayTime; }

  //===========================================================================
  // Return PDE for specific wavelength
  //===========================================================================
  float photonDetectionEfficiency( double wavelength ) const override;

  //===========================================================================
  // Return if SiPM is efficient for this wavelength
  //===========================================================================
  bool sipmDetectsPhoton( double wavelength ) const override;

  //=============================================================================
  // Return thresholds
  //=============================================================================
  double threshold1() const override { return m_PEThreshold1; }

  double threshold2() const override { return m_PEThreshold2; }

  double threshold3() const override { return m_PEThreshold3; }

  int avgNumThermNoiseChans( const DeFT& det ) const { return det.nChannels() * m_noiseProb * m_numNoiseWindows; }

private:
  void addThermalNoise( const DeFT& det, LHCb::MCFTDeposits* depositCont ) const;
  void addThermalNoiseEffective( const DeFT& det, LHCb::MCFTDeposits* depositCont ) const;
  void addProfileNoise( const DeFT& det, LHCb::MCFTDeposits* deposits, const float scaleFactor ) const;
  LHCb::Detector::FTChannelID     generateFromOccupancyProfile( const DeFT& det ) const;
  void                            generateNoiseDeposits( const LHCb::Detector::FTChannelID noiseChannel,
                                                         std::vector<LHCb::MCFTDeposit*>&  deposits ) const;
  std::vector<LHCb::MCFTDeposit*> makeNoiseDeposits( const LHCb::Detector::FTChannelID noiseChannel, int nPhotons,
                                                     double time ) const;

  /// Value for H2017 October batch
  /// from https://indico.cern.ch/event/674476/contributions/2759450/
  Gaudi::Property<double> m_probDirectXTalk{this, "ProbDirectXTalk", 0.025, "Direct cross-talk probability per pe"};

  /// Value for H2017 October batch
  /// from https://indico.cern.ch/event/674476/contributions/2759450/
  Gaudi::Property<double> m_probDelayedXTalk{this, "ProbDelayedXTalk", 0.055, "Delayed cross-talk probability per pe"};

  /// Estimated from Fig.6 of LHCb-INT-2017-006
  Gaudi::Property<double> m_delayedXtalkDecayTime{this, "DelayedXtalkDecayTime", 17.7 * Gaudi::Units::ns,
                                                  "Parameter for the exponential decay of the delayed X-talk"};

  /// Value for H2017 October batch
  /// from https://indico.cern.ch/event/674476/contributions/2759450/
  Gaudi::Property<double> m_probAfterpulse{this, "AfterpulseProb", 0.00, "Afterpulse probability per pe"};

  /// Number of clusters/event, needed for correlated noise estimates
  /// Estimate from minimum bias data (nu=7.6). Only signal.
  Gaudi::Property<float> m_nClustersInEvent{this, "NClustersInEvent", 3100.,
                                            "Average number of clusters per min.bias event"};

  /// Charge distribution (Landau) of the signal (for correlated noise)
  /// Landau only needs to describe the tail well.
  /// This leads to scale factor for the number of clusters
  Gaudi::Property<float> m_meanLandau{this, "MeanLandau", 17.1, "Mean of Landau for cluster charge of signal in PE"};
  Gaudi::Property<float> m_widthLandau{this, "WidthLandau", 2.34, "Mean of Landau for cluster charge of signal in PE"};
  Gaudi::Property<float> m_scaleLandau{this, "ScaleLandau", 0.64,
                                       "Ratio between integral Landau and total number of clusters"};

  /// Analytic description of the occupancy profile
  Gaudi::Property<int>   m_nCut{this, "NCut", 512, "Boundary pseudoChannel for cutout in occupancy profile"};
  Gaudi::Property<int>   m_nChan{this, "NChan", 512 * 4 * 6, "Total number of pseudoChannels in quadrant"};
  Gaudi::Property<float> m_alpha{this, "Alpha", 2274., "Exponent for falling part of occupancy profile"};
  Gaudi::Property<float> m_f0{this, "Fraction0", 0.438, "Fraction of constant part in occupancy profile"};
  Gaudi::Property<float> m_f1{this, "Fraction1", 0.053, "Fraction of cutout (central) part in occupancy profile"};

  /// Parameters for the thermal noise simulation
  Gaudi::Property<double> m_ThermalNoise{this, "ThermalNoiseRate", 14.,
                                         "Thermal noise rate (pDCR) in MHz at reference temperature"};
  Gaudi::Property<double> m_readoutFrequency{this, "ReadoutFrequency", 40., "Readout frequency in MHz (BX rate)"};
  Gaudi::Property<float>  m_referenceIrradiation{this, "ReferenceIrradiation", 6.,
                                                "Reference irradiation in x 10^11 neq/cm2"};
  Gaudi::Property<float>  m_sipmIrradiation{this, "IrradiationLevel", 6., "Irradiation in x 10^11 neq/cm2"};
  Gaudi::Property<float>  m_referenceTemperature{this, "ReferenceTemperature", -40.,
                                                "Temperature in C corresponding to the given thermal noise rate"};
  Gaudi::Property<float>  m_sipmTemperature{this, "Temperature", -40., "Temperature of the SiPMs in Celsius"};
  Gaudi::Property<float>  m_temperatureCoefficient{this, "TemperatureCoefficient", 10.,
                                                  "Temperature coefficient in K for factor 2 reduction"};

  /// Parameters for simulating the arrival times of the noise hits
  Gaudi::Property<int> m_numNoiseWindows{this, "ThermalNoiseWindows", 2, "Thermal noise readout windows to simulate"};
  Gaudi::Property<std::vector<double>> m_integrationOffsets{
      this,
      "IntegrationOffsets",
      {26 * Gaudi::Units::ns, 28 * Gaudi::Units::ns, 30 * Gaudi::Units::ns},
      "Starting time of integration window T1, T2, T3 for noise simulation"};

  /// (Additional) parameters for the _effective_ thermal noise simulation
  Gaudi::Property<bool>  m_simulateEffectiveNoise{this, "SimulateEffectiveNoise", true,
                                                 "Flag to switch on effective thermal noise simulation"};
  Gaudi::Property<float> m_timeOfNoiseHits{this, "TimeOfNoiseHits", 18. * Gaudi::Units::ns,
                                           "Time of noise hits for effective noise simulation"};
  Gaudi::Property<float> m_effNoiseOverlapProb{this, "EffNoiseOverlapProb", 0.8737,
                                               "Probability for overlap of two noise hits in time "
                                               "(depends on electronics response function)"};
  Gaudi::Property<float> m_delayedXTalkOverlapProb{this, "DelayedXTalkOverlapProb", 0.577,
                                                   "Probability for overlap of delayed Xtalk with signal"
                                                   "(depends on electronics response function)"};
  Gaudi::Property<float> m_channelXTalkProb{
      this, "ChannelXTalkProb", 0.05, "Probability of channel to channel cross talk to each neighbouring channel"};

  Gaudi::Property<float> m_PEThreshold1{this, "PEThreshold1", 1.5, "PE low threshold"};
  Gaudi::Property<float> m_PEThreshold2{this, "PEThreshold2", 2.5, "PE mid threshold"};
  Gaudi::Property<float> m_PEThreshold3{this, "PEThreshold3", 4.5, "PE high threshold"};

  /// PDE-vs-wavelength model. 3rd order polynomial
  Gaudi::Property<float> m_PDEmodelp3{this, "PDEmodelp3", 35.9 * std::pow( Gaudi::Units::um, -3 ),
                                      "PDE-vs-wavelength model, 3rd order"};
  Gaudi::Property<float> m_PDEmodelp2{this, "PDEmodelp2", -11.7 * std::pow( Gaudi::Units::um, -2 ),
                                      "PDE-vs-wavelength model, 2nd order"};
  Gaudi::Property<float> m_PDEmodelp0{this, "PDEmodelp0", 0.431, "PDE-vs-wavelength model, max PDE"};
  Gaudi::Property<float> m_PDEmodelShift{this, "PDEmodelShift", 471 * Gaudi::Units::nm,
                                         "PDE-vs-wavelength model, max wavelength"};

  // Number of noise deposits over total detector, in 'm_rdwindows' readoud windows.
  float                m_noiseProb = 0.0;
  std::array<float, 4> m_nThermNoiseChansEff; ///< Effective noise simulation
  std::array<int, 4>   m_nPhotonsPerSize;     ///< Effective noise simulation
  float                m_rateThermalNoise = 0.0;

  SiPMResponse* m_SiPMResponse = nullptr; ///< pointer to SiPM integrated response function

  // Random number generators
  Rndm::Numbers                        m_rndmFlat;
  Rndm::Numbers                        m_rndmGauss;
  Rndm::Numbers                        m_rndmLandau;
  MCFTCommonTools::HepRndmEngnIncptn   m_hepRndmEngnIncptn;
  std::unique_ptr<CLHEP::RandPoissonQ> m_rndmCLHEPPoisson;
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( FTSiPMTool )

static const InterfaceID IID_FTSiPMTool( "FTSiPMTool", 1, 0 );

//=============================================================================
// Tool initialization
//=============================================================================
StatusCode FTSiPMTool::initialize() {
  auto sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  // tools
  if ( msgLevel( MSG::DEBUG ) ) debug() << ": initializing SiPMResponse" << endmsg;
  m_SiPMResponse = tool<SiPMResponse>( "SiPMResponse", this );

  auto randSvc = svc<IRndmGenSvc>( "RndmGenSvc", true );
  if ( !randSvc ) { return Error( "Failed to access Random Generator Service", StatusCode::FAILURE ); }

  sc = m_rndmFlat.initialize( randSvc, Rndm::Flat( 0.0, 1.0 ) );
  if ( sc.isFailure() ) return Error( "Failed to get Rndm::Flat generator", sc );
  sc = m_rndmLandau.initialize( randSvc, Rndm::Landau( m_meanLandau, m_widthLandau ) );
  if ( sc.isFailure() ) return Error( "Failed to get Rndm::Landau generator", sc );

  m_hepRndmEngnIncptn.setGaudiRndmEngine( randSvc->engine() );
  m_rndmCLHEPPoisson = std::make_unique<CLHEP::RandPoissonQ>( m_hepRndmEngnIncptn, 1.0 );

  // scale thermal noise by total crosstalk
  m_rateThermalNoise = m_ThermalNoise / ( 1.0 + m_probDirectXTalk + m_probDelayedXTalk );

  // Set thermal noise probability in 25 ns window
  m_noiseProb = ( m_rateThermalNoise / m_readoutFrequency ) * ( m_sipmIrradiation / m_referenceIrradiation ) *
                std::pow( 2, ( m_sipmTemperature - m_referenceTemperature ) / m_temperatureCoefficient );

  //=============================================================================
  // Reading in effective noise parameters
  //=============================================================================

  std::vector<double> p0;
  std::vector<double> p1;

  if ( m_SiPMResponse->responsefunction() == "pacific4" ) {
    if ( LHCb::essentiallyEqual( m_PEThreshold1.value(), 1.5f ) &&
         LHCb::essentiallyEqual( m_PEThreshold2.value(), 2.5f ) &&
         LHCb::essentiallyEqual( m_PEThreshold3.value(), 4.5f ) ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.00720794, 0.0254307, 0.000474503, 4.77496e-07, 2.5108e-10, 0.0215386} );
      p1.insert( p1.begin(), {3.22954, 3.51195, 4.38762, 5.86713, 7.59096, 3.68478} );
    } else if ( LHCb::essentiallyEqual( m_PEThreshold1.value(), 1.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold2.value(), 2.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold3.value(), 3.5f ) ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.379491, 0.0267754, 0.000436412, 4.04182e-07, 2.51158e-10, 0.149768} );
      p1.insert( p1.begin(), {2.61262, 3.49416, 4.41247, 5.91844, 7.57975, 3.20534} );
    } else if ( LHCb::essentiallyEqual( m_PEThreshold1.value(), 2.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold2.value(), 3.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold3.value(), 4.5f ) ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.00552157, 6.74359e-06, 1.43427e-09, 4.52659e-13, 6.32876e-12, 0.00259679} );
      p1.insert( p1.begin(), {3.38788, 5.10414, 6.94038, 8.31251, 6.63043, 3.71505} );
    } else if ( LHCb::essentiallyEqual( m_PEThreshold1.value(), 2.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold2.value(), 3.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold3.value(), 5.5f ) ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {3.32346e-05, 7.67269e-06, 1.13245e-09, 8.59171e-15, 1.5103e-06, 2.08099e-05} );
      p1.insert( p1.begin(), {4.3258, 5.06488, 6.99903, 9.47962, 2.84794, 4.87941} );
    } else {
      warning() << "Effective noise parameters not available!! No noise simulated. " << endmsg;
      p0.insert( p0.begin(), {0, 0, 0, 0, 0, 0} );
      p1.insert( p1.begin(), {0, 0, 0, 0, 0, 0} );
    }
  }

  if ( m_SiPMResponse->responsefunction() == "pacific5q_pz5" ) {
    if ( LHCb::essentiallyEqual( m_PEThreshold1.value(), 1.5f ) &&
         LHCb::essentiallyEqual( m_PEThreshold2.value(), 2.5f ) &&
         LHCb::essentiallyEqual( m_PEThreshold3.value(), 4.5f ) ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.284981, 0.409399, 0.00395029, 6.89594e-06, 9.47956e-09, 0.49435} );
      p1.insert( p1.begin(), {2.46383, 2.75665, 3.73996, 5.04828, 6.47118, 2.83164} );
    } else if ( LHCb::essentiallyEqual( m_PEThreshold1.value(), 1.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold2.value(), 2.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold3.value(), 3.5f ) ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {6.25031, 0.408655, 0.00391049, 7.17203e-06, 9.22992e-09, 3.68113} );
      p1.insert( p1.begin(), {1.97668, 2.75197, 3.73903, 5.02978, 6.47477, 2.35792} );
    } else if ( LHCb::essentiallyEqual( m_PEThreshold1.value(), 2.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold2.value(), 3.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold3.value(), 4.5f ) ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.235113, 0.00146589, 2.0333e-06, 9.28805e-11, 1.50335e-14, 0.153428} );
      p1.insert( p1.begin(), {2.58342, 3.77002, 5.03691, 7.09069, 8.81151, 2.80113} );
    } else if ( LHCb::essentiallyEqual( m_PEThreshold1.value(), 2.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold2.value(), 3.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold3.value(), 5.5f ) ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.00946568, 0.00102273, 1.13127e-06, 2.3943e-09, 2.88884e-16, 0.00514476} );
      p1.insert( p1.begin(), {3.08778, 3.8851, 5.2196, 6.06228, 10.1229, 3.56729} );
    } else {
      warning() << "Effective noise parameters not available!! No noise simulated. " << endmsg;
      p0.insert( p0.begin(), {0, 0, 0, 0, 0, 0} );
      p1.insert( p1.begin(), {0, 0, 0, 0, 0, 0} );
    }
  }

  if ( m_SiPMResponse->responsefunction() == "pacific5q_pz6" ) {
    if ( LHCb::essentiallyEqual( m_PEThreshold1.value(), 1.5f ) &&
         LHCb::essentiallyEqual( m_PEThreshold2.value(), 2.5f ) &&
         LHCb::essentiallyEqual( m_PEThreshold3.value(), 4.5f ) ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.0930256, 0.0759345, 0.000627617, 4.6727e-07, 2.04502e-09, 0.0957085} );
      p1.insert( p1.begin(), {2.72018, 3.2095, 4.26998, 5.8252, 6.89003, 3.27303} );
    } else if ( LHCb::essentiallyEqual( m_PEThreshold1.value(), 1.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold2.value(), 2.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold3.value(), 3.5f ) ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {2.8924, 0.0801284, 0.000663995, 7.29289e-07, 5.23242e-10, 1.15848} );
      p1.insert( p1.begin(), {2.13368, 3.19246, 4.24953, 5.68602, 7.29637, 2.64726} );
    } else if ( LHCb::essentiallyEqual( m_PEThreshold1.value(), 2.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold2.value(), 3.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold3.value(), 4.5f ) ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.0538636, 0.000118291, 5.08479e-08, 3.06742e-12, 1.87791e-06, 0.0313937} );
      p1.insert( p1.begin(), {2.9391, 4.40892, 5.98974, 7.89901, 2.84991, 3.18273} );
    } else if ( LHCb::essentiallyEqual( m_PEThreshold1.value(), 2.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold2.value(), 3.5f ) &&
                LHCb::essentiallyEqual( m_PEThreshold3.value(), 5.5f ) ) {
      debug() << "Reading effective noise parameters for " << m_SiPMResponse->responsefunction()
              << " with thresholds {T1,T1,T3} = {" << m_PEThreshold1.value() << "," << m_PEThreshold2.value() << ","
              << m_PEThreshold3.value() << "}" << endmsg;
      p0.insert( p0.begin(), {0.00592561, 0.000160619, 4.24277e-08, 6.79141e-11, 4.78152e-09, 0.00164226} );
      p1.insert( p1.begin(), {3.08554, 4.31038, 6.03317, 6.96397, 4.48837, 3.77228} );
    } else {
      warning() << "Effective noise parameters not available!! No noise simulated. " << endmsg;
      p0.insert( p0.begin(), {0, 0, 0, 0, 0, 0} );
      p1.insert( p1.begin(), {0, 0, 0, 0, 0, 0} );
    }
  }

  m_nThermNoiseChansEff[0] = returnEffNoise( p0[0], p1[0] );
  m_nThermNoiseChansEff[1] = returnEffNoise( p0[1], p1[1] );
  m_nThermNoiseChansEff[2] = returnEffNoise( p0[2], p1[2] );
  m_nThermNoiseChansEff[3] = returnEffNoise( p0[3], p1[3] );

  // Number of photons for 1-,2-,3-,4-size clusters (should be > m_thresholds)
  m_nPhotonsPerSize[0] = int( m_PEThreshold3 ) + 1;
  m_nPhotonsPerSize[1] = int( m_PEThreshold2 ) + 1;
  m_nPhotonsPerSize[2] = int( m_PEThreshold2 ) + 1;
  m_nPhotonsPerSize[3] = int( m_PEThreshold2 ) + 1;

  return sc;
}

//=============================================================================
// Return effective noise function output
//=============================================================================
float FTSiPMTool::returnEffNoise( float p0, float p1 ) const {
  float rate = m_rateThermalNoise;
  float func = p0 * std::pow( rate, p1 );
  return func;
}

//=============================================================================
// Add noise to deposit container
//=============================================================================
void FTSiPMTool::addNoise( const DeFT& det, LHCb::MCFTDeposits* deposits ) const {

  // Thermal noise
  if ( m_simulateEffectiveNoise )
    addThermalNoiseEffective( det, deposits );
  else
    addThermalNoise( det, deposits );

  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Number of deposits after adding thermal noise = " << deposits->size() << endmsg;

  // After pulse noise
  if ( m_probAfterpulse > 0.005 ) addProfileNoise( det, deposits, m_probAfterpulse );
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Number of deposits after adding afterpulses = " << deposits->size() << endmsg;
}

//=============================================================================
// Simulate detailed thermal noise
//=============================================================================
void FTSiPMTool::addThermalNoise( const DeFT& det, LHCb::MCFTDeposits* deposits ) const {
  // Draw poissonian for the number of noise clusters
  int nNoise = m_rndmCLHEPPoisson->fire( avgNumThermNoiseChans( det ) );
  // Loop over all thermal noise PEs
  for ( int i = 0; i < nNoise; ++i ) {
    // Get random channel and time to add 1 PE to
    LHCb::Detector::FTChannelID noiseChannel = det.getRandomChannelFromSeed( m_rndmFlat() );

    // Simulate more than 25ns for the time response
    // and subtract integration time offset per station
    double time = m_rndmFlat() * m_numNoiseWindows * 25. * Gaudi::Units::ns +
                  m_integrationOffsets[noiseChannel.globalStationIdx()];

    std::vector<LHCb::MCFTDeposit*> noiseDeps = makeNoiseDeposits( noiseChannel, 1, time );
    for ( const auto deposit : noiseDeps ) deposits->add( deposit );
  } // end of loop over thermal noise PEs
}

//=============================================================================
// Simulate effective thermal noise
//=============================================================================
void FTSiPMTool::addThermalNoiseEffective( const DeFT& det, LHCb::MCFTDeposits* deposits ) const {

  // First add noise to the channels which have signal deposits
  // Sort the deposits, such that deposits for the same channel are neighbours
  std::stable_sort( deposits->begin(), deposits->end(), LHCb::MCFTDeposit::lowerByChannelID );

  // Loop over signal deposits
  std::vector<LHCb::MCFTDeposit*> tmpDeposits;
  LHCb::Detector::FTChannelID     thisChannel( 0 );
  for ( const auto deposit : *deposits ) {
    if ( deposit->channelID() != thisChannel ) {
      if ( thisChannel != 0 && deposit->channelID() != thisChannel + 1 ) {
        // Generate noise deposits for next channel of previous signal deposit
        if ( thisChannel.channel() != ( FTConstants::nChannels - 1 ) )
          generateNoiseDeposits( LHCb::Detector::FTChannelID{thisChannel + 1}, tmpDeposits );
        if ( deposit->channelID() != thisChannel + 2 && deposit->channelID().channel() != 0u ) {
          // Generate noise deposits for previous channel of this signal deposit
          generateNoiseDeposits( LHCb::Detector::FTChannelID{deposit->channelID() - 1}, tmpDeposits );
        }
      }
      thisChannel = deposit->channelID();
      // Generate noise deposits for the current channel
      generateNoiseDeposits( thisChannel, tmpDeposits );
    }
  }
  for ( const auto deposit : tmpDeposits ) deposits->add( deposit );

  // Add the thermal noise
  // Loop over the possible cluster size
  for ( unsigned int clusSize = 1; clusSize <= 4; ++clusSize ) { // TODO: Hardcoded cluster size
    // Draw poissonian for the number of noise clusters
    int nNoise = m_rndmCLHEPPoisson->fire( m_nThermNoiseChansEff[clusSize - 1] );

    // Loop over the number of noise clusters for this cluster size
    for ( int j = 0; j < nNoise; ++j ) {
      LHCb::Detector::FTChannelID noiseChannel = det.getRandomChannelFromSeed( m_rndmFlat() );
      double                      time = m_timeOfNoiseHits + m_integrationOffsets[noiseChannel.globalStationIdx()];
      // Create the deposits for this cluster size
      auto firstChannel = LHCb::Detector::FTChannelID{noiseChannel - int( 0.5 * clusSize )};
      for ( auto iChannel = firstChannel; iChannel - firstChannel < clusSize; iChannel.advance() ) {
        // Check if the deposit is still in the same module
        if ( iChannel.globalModuleIdx() == noiseChannel.globalModuleIdx() ) {
          std::vector<LHCb::MCFTDeposit*> noiseDeps =
              makeNoiseDeposits( iChannel, m_nPhotonsPerSize[clusSize - 1], time );
          for ( const auto deposit : noiseDeps ) deposits->add( deposit );
        }
      }
    }
  }
}

//=============================================================================
// Simulate noise from occupancy profile
//=============================================================================
void FTSiPMTool::addProfileNoise( const DeFT& det, LHCb::MCFTDeposits* deposits, const float scaleFactor ) const {

  // loop over all 'clusters in the event'
  // multiply amount of generated noise with amount of readout windows to simulate.
  int nClusters = int( m_nClustersInEvent * m_numNoiseWindows * m_scaleLandau );
  for ( int i = 0; i < nClusters; ++i ) {
    // get number of noise PEs for this cluster
    int nNoisePEs = m_rndmLandau() * scaleFactor;

    // Only generate a noise hit if above threshold
    if ( nNoisePEs >= int( m_PEThreshold3 ) ) { // single cluster threshold
      // For simplicity put all charge in single channel
      auto noiseChannel = generateFromOccupancyProfile( det );

      // Loop over all photoelectrons
      for ( int k = 0; k < nNoisePEs; ++k ) {
        // Generate a flat time distribution
        double time = m_rndmFlat() * m_numNoiseWindows * 25 * Gaudi::Units::ns + // Todo: Hardcoded window size
                      m_integrationOffsets.value().at( noiseChannel.globalStationIdx() );
        std::vector<LHCb::MCFTDeposit*> noiseDeps = makeNoiseDeposits( noiseChannel, 1, time );
        for ( const auto deposit : noiseDeps ) deposits->add( deposit );
      } // end of all photoelectrons
    }
  } // end loop clusters
}

//=============================================================================
// Randomly generate noise channel from occupancy map
//=============================================================================
LHCb::Detector::FTChannelID FTSiPMTool::generateFromOccupancyProfile( const DeFT& det ) const {

  // Generate pseudoChannel according to occupancy profile
  // PDF for occupancy profile (x=pseudochannel)
  //   for x<nCut  :  P(x) = f0/nChan + f1/nCut
  //   for x>=nCut :  P(x) = f0/nChan + (1-f0-f1)*exp(-x/alpha)/normExp
  //          with normExp = alpha * (exp(-nCut/alpha) - exp(-nTot/alpha))
  static const float rMax = exp( -float( m_nCut ) / m_alpha );
  static const float rMin = exp( -float( m_nChan ) / m_alpha );

  LHCb::Detector::FTChannelID noiseChannel( 0u );
  while ( noiseChannel.channelID() == 0u ) {
    float rndFlat       = m_rndmFlat(); // Draw flat random number [0,1]
    int   pseudoChannel = 0;

    // flat overall component
    if ( rndFlat < m_f0 ) {
      rndFlat /= m_f0;                          // renormalize rndFlat in range [0,1]
      pseudoChannel = int( m_nChan * rndFlat ); // in range [0,nChan]
    }

    // central flat component
    else if ( rndFlat < m_f0.value() + m_f1.value() ) {
      rndFlat       = ( rndFlat - m_f0 ) / m_f1; // renormalize rndFlat in range [0,1]
      pseudoChannel = int( m_nCut * rndFlat );   // in range [0,nCut]
    }

    // exponential component
    else {
      // renormalize rndFlat in [rMin, rMax]
      rndFlat       = ( rndFlat - m_f0 - m_f1 ) / ( 1. - m_f0 - m_f1 ) * ( rMax - rMin ) + rMin;
      pseudoChannel = int( -log( rndFlat ) * m_alpha ); // in range [nCut,nChan]
    }

    noiseChannel = det.getRandomChannelFromPseudo( pseudoChannel, m_rndmFlat() );
  }

  return noiseChannel;
}

//=============================================================================
// Generate noise deposits for a given channel
//=============================================================================
void FTSiPMTool::generateNoiseDeposits( const LHCb::Detector::FTChannelID noiseChannel,
                                        std::vector<LHCb::MCFTDeposit*>&  deposits ) const {
  int nPhotons = m_rndmCLHEPPoisson->fire( m_noiseProb * m_numNoiseWindows );
  for ( int i = 0; i < nPhotons; ++i ) {
    double time = m_rndmFlat() * m_numNoiseWindows * 25. * Gaudi::Units::ns + // TODO: Hardcoded
                  m_integrationOffsets[noiseChannel.globalStationIdx()];

    std::vector<LHCb::MCFTDeposit*> noiseDeps = makeNoiseDeposits( noiseChannel, 1, time );
    for ( const auto deposit : noiseDeps ) deposits.push_back( deposit );
  }
}

//=============================================================================
// Add a noise deposit to the deposit container
//=============================================================================
std::vector<LHCb::MCFTDeposit*> FTSiPMTool::makeNoiseDeposits( const LHCb::Detector::FTChannelID noiseChannel,
                                                               int nPhotons, double time ) const {

  std::vector<LHCb::MCFTDeposit*> deposits;

  // Create noise deposit with direct cross talk
  LHCb::MCFTDeposit* deposit =
      new LHCb::MCFTDeposit( noiseChannel, nullptr, nPhotons + generateDirectXTalk( nPhotons ), time, false );
  deposits.push_back( deposit );

  // generate delayed XTalk
  int nDelayedXtalk = generateDelayedXTalk( nPhotons );
  for ( int i = 0; i < nDelayedXtalk; ++i ) {
    LHCb::MCFTDeposit* deposit =
        new LHCb::MCFTDeposit( noiseChannel, nullptr, 1, time + generateDelayedXTalkTime(), false );
    deposits.push_back( deposit );
  }

  return deposits;
}

//=============================================================================
// Return SiPM PDE
//=============================================================================
float FTSiPMTool::photonDetectionEfficiency( double wavelength ) const {
  float x = wavelength - m_PDEmodelShift;
  return ( m_PDEmodelp3 * x + m_PDEmodelp2 ) * x * x + m_PDEmodelp0;
}

//=============================================================================
// Check if photon is detected due to SiPM PDE
//=============================================================================
bool FTSiPMTool::sipmDetectsPhoton( double wavelength ) const {
  // PDE of 2014 SiPM
  return photonDetectionEfficiency( wavelength ) > m_rndmFlat();
}
