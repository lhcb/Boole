/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "IMCFTAttenuationTool.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

/**
 *  Tool to calculate the attenuation length for direct and reflectec light in a scintillating fibre.
 *
 *  - ShortAttenuationLength: Distance along the fibre to divide the light amplitude by a factor e : short component
 *  - LongAttenuationLength: Distance along the fibre to divide the light amplitude by a factor e : long component
 *  - FractionShort: Fraction of short attenuation at SiPM
 *  - XMaxIrradiatedZone: Size in x of the zone where fibres are irradiated
 *  - YMaxIrradiatedZone: Size in y of the zone where fibres are irradiated
 *  - IrradiatedAttenuationLength:  Attenuation length by steps in the zone
 *  - ReflectionCoefficient: Reflection coefficient of the fibre mirrored side, from 0 to 1
 *  - BeginReflectionLossY:  Beginning of zone where reflection is too late and lost
 *  - EndReflectionLossY: End of zone where reflection is too late and lost
 *  - XStepOfMap:  Step  along x-axis of the FullAttenuationMap (in mm)
 *  - YStepOfMap:  Step  along y-axis of the FullAttenuationMap (in mm)
 *
 *  @author Michel De Cian, based on Eric Cogneras' implementation
 *  @date   2015-01-15
 */

class MCFTAttenuationTool : public extends<GaudiTool, IMCFTAttenuationTool> {

public:
  using base_class::base_class;
  StatusCode initialize() override;

  /// Calculate the direct attenuation and the attenuation with reflection
  IMCFTAttenuationTool::Attenuation attenuation( double x, double y ) const override;

private:
  inline double calcAtt( const double fracX, const double fracY, const int kx, const int ky ) const {
    return fracX * ( fracY * m_transmissionMap[m_nYSteps * ( kx + 1 ) + ky + 1] +
                     ( 1 - fracY ) * m_transmissionMap[m_nYSteps * ( kx + 1 ) + ky] ) +
           ( 1 - fracX ) * ( fracY * m_transmissionMap[m_nYSteps * kx + ky + 1] +
                             ( 1 - fracY ) * m_transmissionMap[m_nYSteps * kx + ky] );
  }

  inline double calcAttRef( const double fracX, const double fracY, const int kx, const int ky ) const {
    return fracX * ( fracY * m_transmissionRefMap[m_nYSteps * ( kx + 1 ) + ky + 1] +
                     ( 1 - fracY ) * m_transmissionRefMap[m_nYSteps * ( kx + 1 ) + ky] ) +
           ( 1 - fracX ) * ( fracY * m_transmissionRefMap[m_nYSteps * kx + ky + 1] +
                             ( 1 - fracY ) * m_transmissionRefMap[m_nYSteps * kx + ky] );
  }

  Gaudi::Property<double> m_shortAttenuationLength{this, "ShortAttenuationLength", 200 * Gaudi::Units::mm,
                                                   "Attenuation length of the light along the fibre: short component"};
  Gaudi::Property<double> m_longAttenuationLength{this, "LongAttenuationLength", 4700 * Gaudi::Units::mm,
                                                  "Attenuation length of the light along the fibre: long component"};
  Gaudi::Property<double> m_fractionShort{this, "FractionShort", 0.18, "Fraction of short attenuation at SiPM"};
  Gaudi::Property<double> m_xMaxIrradiatedZone{this, "XMaxIrradiatedZone", 2000. * Gaudi::Units::mm,
                                               "Size in x of the zone where fibres are irradiated"};
  Gaudi::Property<double> m_yMaxIrradiatedZone{this, "YMaxIrradiatedZone", 500. * Gaudi::Units::mm,
                                               "Size in y of the zone where fibres are irradiated"};
  Gaudi::Property<std::vector<double>> m_irradiatedAttenuationLength{
      this,
      "IrradiatedAttenuationLength",
      {3000. * Gaudi::Units::mm, 3000. * Gaudi::Units::mm, 3000. * Gaudi::Units::mm, 1500. * Gaudi::Units::mm,
       300. * Gaudi::Units::mm},
      "Attenuation length by steps in the zone"};
  Gaudi::Property<double> m_reflectionCoefficient{this, "ReflectionCoefficient", 0.7,
                                                  "Reflection coefficient of the fibre mirrored side, from 0 to 1"};
  Gaudi::Property<double> m_beginReflectionLossY{this, "BeginReflectionLossY", 1000. * Gaudi::Units::mm,
                                                 "begin of zone where reflection is too late and lost"};
  Gaudi::Property<double> m_endReflectionLossY{this, "EndReflectionLossY", 1500. * Gaudi::Units::mm,
                                               "end of zone where reflection is too late and lost"};

  Gaudi::Property<double> m_xStepOfMap{this, "XStepOfMap", 200. * Gaudi::Units::mm,
                                       "Step  along X-axis of the FullAttenuationMap(in mm)"};
  Gaudi::Property<double> m_yStepOfMap{this, "YStepOfMap", 100. * Gaudi::Units::mm,
                                       "Step  along Y-axis of the FullAttenuationMap(in mm)"};

  Gaudi::Property<double> m_xMax{this, "XMax", 3164.4 * Gaudi::Units::mm,
                                 "Length of the FullAttenuationMap(in mm) in X-axis"};

  Gaudi::Property<double> m_yMax{this, "YMax", 2426.0 * Gaudi::Units::mm,
                                 "Length of the FullAttenuationMap(in mm) in Y-axis"};

  int m_nXSteps{0};
  int m_nYSteps{0};

  std::vector<double> m_transmissionMap;    ///< Maps hits to transmitted energy from the direct pulse
  std::vector<double> m_transmissionRefMap; ///< Maps hits to transmitted energy from the reflected pulse
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCFTAttenuationTool )

IMCFTAttenuationTool::Attenuation MCFTAttenuationTool::attenuation( double hitXPosition, double hitYPosition ) const {
  //== Compute attenuation by interpolation in the table
  //== No difference made here between x-layers and (u,v)-layers : all layers assumed to be x-layers
  const int    kx    = std::min( m_nXSteps - 2, (int)( fabs( hitXPosition ) / m_xStepOfMap ) );
  const int    ky    = std::min( m_nYSteps - 2, (int)( fabs( hitYPosition ) / m_yStepOfMap ) );
  const double fracX = fabs( hitXPosition ) / m_xStepOfMap - kx;
  const double fracY = fabs( hitYPosition ) / m_yStepOfMap - ky;
  return {calcAtt( fracX, fracY, kx, ky ), calcAttRef( fracX, fracY, kx, ky )};
}

StatusCode MCFTAttenuationTool::initialize() {
  return extends::initialize().andThen( [&] {
    //== Generate the transmission map.
    //== Two attenuation length, then in the radiation area a different attenuation length
    //== Compute also the transmission of the reflected signal, attenuated by the two length
    //   and the irradiated area

    // Setup maps size
    m_nXSteps = m_xMax / m_xStepOfMap + 2; // add x=0 and x > max position
    m_nYSteps = m_yMax / m_yStepOfMap + 2; // same for y

    // const double xMax = (m_nXSteps - 1) * m_xStepOfMap;
    const double yMax = ( m_nYSteps - 1 ) * m_yStepOfMap;

    // m_transmissionMap set at 1E-10 initialisatino value to avoid 'division by zero' bug
    m_transmissionMap.resize( m_nXSteps * m_nYSteps, 1E-10 );
    m_transmissionRefMap.resize( m_nXSteps * m_nYSteps, 1E-10 );

    // Loop on the x axis
    for ( int kx = 0; m_nXSteps > kx; ++kx ) {
      const double x = kx * m_xStepOfMap;

      // -- is this a model, a fact, an urban legend?
      double radZoneSize = 2 * m_yMaxIrradiatedZone * ( 1 - x / m_xMaxIrradiatedZone );
      if ( 0. > radZoneSize ) radZoneSize = 0.;
      const double yBoundaryRadZone = .5 * radZoneSize;

      // Loop on the y axis and calculate transmission map
      for ( int ky = 0; m_nYSteps > ky; ++ky ) {
        const double y = yMax - ky * m_yStepOfMap;

        // Compute attenuation according to the crossed fibre length
        double att = ( m_fractionShort * exp( -( yMax - y ) / m_shortAttenuationLength ) +
                       ( 1 - m_fractionShort ) * exp( -( yMax - y ) / m_longAttenuationLength ) );

        // plot2D(x,y,"FibreAttenuationMap","Attenuation coefficient as a function of the position (Quarter 3); x [mm];y
        // [mm]",
        //       0., m_nXSteps*m_xStepOfMap, 0.,m_nYSteps*m_yStepOfMap, m_nXSteps, m_nYSteps, att);

        if ( y < yBoundaryRadZone ) {
          att = ( m_fractionShort * exp( -( yMax - yBoundaryRadZone ) / m_shortAttenuationLength ) +
                  ( 1 - m_fractionShort ) * exp( -( yMax - yBoundaryRadZone ) / m_longAttenuationLength ) );

          double lInRadiation = yBoundaryRadZone - y;

          for ( unsigned int kz = 0; m_irradiatedAttenuationLength.size() > kz; ++kz ) {
            if ( lInRadiation > m_yStepOfMap ) {
              att *= exp( -m_yStepOfMap / m_irradiatedAttenuationLength[kz] );
            } else if ( lInRadiation > 0. ) {
              att *= exp( -lInRadiation / m_irradiatedAttenuationLength[kz] );
            }
            lInRadiation -= m_yStepOfMap;
          }
        }
        m_transmissionMap[m_nYSteps * ( kx + 1 ) - ky - 1] = att;
      }

      // Loop on the y axis and calculate transmission of the reflected pulse
      // The energy from the reflected pulse is calculated as the energy coming from y = 0 and attenuated
      // by the reflection coefficient, further attenuated depending on the y.
      if ( m_reflectionCoefficient > 0. ) {

        // Reflected energy from y = 0
        double reflected = m_transmissionMap[m_nYSteps * kx] * m_reflectionCoefficient;

        for ( int kk = 0; m_nYSteps > kk; ++kk ) {
          // const double y = kk * m_yStepOfMap;

          // Evaluate the attenuation factor.
          // Rather than calculating it use information from the transmission map in the same point
          double att = 0;

          if ( m_nYSteps > ( kk + 1 ) )
            att = m_transmissionMap[m_nYSteps * kx + kk] / m_transmissionMap[m_nYSteps * kx + kk + 1];

          // Fill the map
          m_transmissionRefMap[m_nYSteps * kx + kk] = reflected;

          // Prepare the reflection ratio for next iteration
          reflected *= att;
        }
      }
    }
    return StatusCode::SUCCESS;
  } );
}
