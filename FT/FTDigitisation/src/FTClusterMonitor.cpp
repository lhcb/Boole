/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <AIDA/IHistogram1D.h>
#include <Associators/Associators.h>
#include <Detector/FT/FTChannelID.h>
#include <Event/FTCluster.h>
#include <Event/FTLiteCluster.h>
#include <Event/MCHit.h>
#include <FTDet/DeFTDetector.h>
#include <GaudiAlg/GaudiHistoAlg.h>
#include <GaudiKernel/SystemOfUnits.h>
#include <LHCbAlgs/Consumer.h>

namespace Digi {
  /** @class FTClusterMonitor FTClusterMonitor.h
   *
   *
   *  @author Eric Cogneras
   *  @date   2012-07-05
   */

  class FTClusterMonitor : public LHCb::Algorithm::Consumer<void( const LHCb::FTClusters&,
                                                                  // const LHCb::MCHits&,
                                                                  const LHCb::LinksByKey&, const DeFT& ),
                                                            LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeFT>> {
  public:
    FTClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode finalize() override;
    void       operator()( const LHCb::FTClusters&,
                     // const LHCb::MCHits& mcHits,
                     const LHCb::LinksByKey& links, const DeFT& det ) const override;

  private:
    Gaudi::Property<float> m_minPforResolution{this, "MinPforResolution", 0.0 * Gaudi::Units::GeV,
                                               "Minimum momentum for resolution plots"};
  };

  DECLARE_COMPONENT( FTClusterMonitor )
} // namespace Digi
using namespace Digi;

FTClusterMonitor::FTClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"ClusterLocation", LHCb::FTClusterLocation::Default},
                 // KeyValue{"HitsLocation", LHCb::MCHitLocation::FT},
                 KeyValue{"LinkerLocation", Links::location( std::string( LHCb::FTLiteClusterLocation::Default ) +
                                                             "2MCHitsWithSpillover" )},
                 KeyValue{"FT", DeFTDetectorLocation::Default}} ) {}

//=============================================================================
// Main execution
//=============================================================================
void FTClusterMonitor::operator()( const LHCb::FTClusters& clusters,
                                   // const LHCb::MCHits& mcHits,
                                   const LHCb::LinksByKey& links, const DeFT& det ) const {

  // retrieve FTLiteClustertoMCHitLink
  auto myClusterToHitLink = InputLinks<ContainedObject, LHCb::MCHit>( links );

  plot( clusters.size(), "nClusters", "Number of clusters; Clusters/event; Events", 0., 15.e3, 100 );

  uint prevSiPMIdx = 0u, prevModuleIdx = 0u;
  int  clustersInSiPM = 0;

  // Loop over FTCluster
  for ( const auto cluster : clusters ) {

    if ( cluster->isLarge() == 2 ) continue; // intermediate fragments of large clusters

    // Get the FTChannelID
    LHCb::Detector::FTChannelID chanID = cluster->channelID();

    // draw cluster channel properties
    plot( chanID.localModuleIdx(), "ClustersPerModule", "Clusters per module; Module; Clusters", -0.5,
          FTConstants::nModulesMax - 0.5, FTConstants::nModulesMax );
    plot( chanID.localSiPMIdx_module(), "ClustersPerSiPM", "Clusters per SiPM; SiPMID; Clusters", -0.5,
          FTConstants::nSiPMsPerModule - 0.5, FTConstants::nSiPMsPerModule );
    plot( chanID.localChannelIdx(), "ClustersPerChannel", "Clusters per channel; Channel; Clusters", -0.5,
          FTConstants::nChannels - 0.5, FTConstants::nChannels );

    plot( chanID.localChannelIdx_module(), "ClustersPerPseudoChannel",
          "Clusters per pseudo channel;Pseudo channel;Clusters/(64 channels)", -0.5,
          FTConstants::nMaxChannelsPerQuarter - 0.5, FTConstants::nMaxSiPMsPerQuarter * 2 );
    if ( cluster->isLarge() == 0 ) {

      plot( cluster->fraction(), "ClusterFraction", "Cluster fraction; Fraction", -0.25, 0.75, 10 );
      plot( cluster->size(), "ClusterSize", "Cluster size; Size", -0.5, 4.5, 5 );
      plot( cluster->charge(), "ClusterCharge", "Cluster charge; Charge", 10.5, 100.5, 90 );
      plot2D( cluster->size(), cluster->charge(), "ClusterSizeVsFraction", "Cluster size vs Charge; Size; Charge", -0.5,
              4.5, 0., 100., 5, 5 );
      plot2D( cluster->size(), cluster->fraction(), "ClusterSizeVsFraction",
              "Cluster size vs fraction ; Size; Fraction", -0.5, 4.5, -0.25, 0.75, 5, 100 );
    }

    // Plots for cluster position resolution
    // Get the correct mat
    const auto& mat        = det.findMat( chanID );
    const auto  mcHitLinks = myClusterToHitLink.from( chanID );

    if ( mat ) {

      // Loop over all links to MCHits
      for ( const auto& imcHit : mcHitLinks ) {
        const auto mcHit = imcHit.to();

        // Plot the resolution only for p > pMin
        if ( mcHit->p() < m_minPforResolution ) continue;

        // Plot the resolution only for the signal spill
        if ( mcHit->parent()->registry()->identifier() != "/Event/" + LHCb::MCHitLocation::FT ) continue;

        double dXCluster = mat->distancePointToChannel( mcHit->midPoint(), chanID, cluster->fraction() );

        plot( dXCluster, "Resolution/ClusterResolution",
              "Cluster resolution; Cluster - MCHit x position [mm]; Number of clusters", -1, 1, 100 );

        plot( dXCluster,
              "Resolution/ClusterResolution" + std::string( cluster->isLarge() == 0 ? "Small" : "Fragmented" ),
              "Cluster resolution " + std::string( cluster->isLarge() == 0 ? "small" : "fragmented" ) + " clusters" +
                  "; Cluster - MCHit x position [mm]; Number of clusters",
              -1, 1, 100 );

        if ( cluster->isLarge() == 0 ) {

          plot( dXCluster, "Resolution/ClusterResolutionSize" + std::to_string( cluster->size() ),
                "Cluster resolution size" + std::to_string( cluster->size() ) +
                    "; Cluster - MCHit x position [mm]; Number of clusters",
                -1, 1, 100 );
        }
      }
    }

    // Count the number of clusters per SiPM
    uint thisSiPMIdx = chanID.globalSipmIdx();
    if ( thisSiPMIdx != prevSiPMIdx ) {
      if ( clustersInSiPM != 0 ) {
        plot( clustersInSiPM, "ClustersInSiPM", "Clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5,
              21 );
        plot( clustersInSiPM,
              "ClustersInSiPM_Module" + std::to_string( prevModuleIdx ), // FIXME: better make a 2D plot vs.
                                                                         // prevModuleID... (and slice downstream)
              "Clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5, 21 );
        clustersInSiPM = 0;
      }
      prevSiPMIdx   = thisSiPMIdx;
      prevModuleIdx = chanID.globalModuleIdx();
    }
    ++clustersInSiPM;
  }

  // Fill this for the last time
  plot( clustersInSiPM, "ClustersInSiPM", "Clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5, 21 );
  plot( clustersInSiPM, "ClustersInSiPM_Module" + std::to_string( prevModuleIdx ),
        "Clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5,
        21 ); // FIXME: better make a 2D plot vs. prevModuleIdx... (and slice downstream)

  return;
}

//=============================================================================
// Finalization
//=============================================================================
StatusCode FTClusterMonitor::finalize() {

  AIDA::IHistogram1D* hNClusters = histo1D( HistoID( "nClusters" ) );

  info() << " --------FT clusters------------" << endmsg;
  if ( hNClusters != nullptr ) {
    info() << "Number of clusters per event  = " << format( "%4.1f", hNClusters->mean() ) + " +/- "
           << format( "%4.1f", hNClusters->rms() ) << endmsg;
  } else
    info() << "No clusters were found!" << endmsg;

  info() << " -------------------------------" << endmsg;

  return Consumer::finalize(); // must be executed first
}
