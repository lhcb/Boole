/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "SpilloverProb.h"

#include "Core/FloatComparison.h"
#include "DetDesc/Condition.h"
#include <Detector/FT/FTConstants.h>

#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/ICondSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ServiceHandle.h"

#include <yaml-cpp/yaml.h>

//-----------------------------------------------------------------------------
// Implementation file for class : SpillOver
//
//   This class describes the probability of spillover clusters
//
// 2024-05-26 : Zehua Xu
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( SpilloverProb )

namespace FTConstants = LHCb::Detector::FT;

SpilloverProb::SpilloverProb( const std::string& type, const std::string& name, const IInterface* parent )
    : ConditionAccessorHolder( type, name, parent ) {
  declareInterface<SpilloverProb>( this );
}

StatusCode SpilloverProb::initialize() {

#ifdef USE_DD4HEP
  return ConditionAccessorHolder::initialize().andThen( [&] {
    addConditionDerivation( {m_conditionLocation_backup}, m_conditions.key(),
                            [&]( YAML::Node const& rInfo ) -> Conditions {
                              (void)rInfo;
                              m_values       = std::vector<double>( FTConstants::nSiPMsTotal, 0.0 );
                              m_scale_factor = 0;

                              return {m_values, m_scale_factor};
                            } );

    return StatusCode( StatusCode::SUCCESS );
  } );

#else
  return ConditionAccessorHolder::initialize().andThen( [&] {
    if ( existDet<Condition>( m_conditionLocation ) ) {

      addConditionDerivation( {m_conditionLocation}, m_conditions.key(), [&]( YAML::Node const& rInfo ) -> Conditions {
        // Get data from condition DB
        m_values       = rInfo["SpilloverProbSiPM"].as<std::vector<double>>();
        m_scale_factor = rInfo["SpilloverScale"].as<double>();

        return {m_values, m_scale_factor};
      } );

    } else {

      addConditionDerivation( {m_conditionLocation_backup}, m_conditions.key(),
                              [&]( YAML::Node const& rInfo ) -> Conditions {
                                (void)rInfo;
                                m_values       = std::vector<double>( FTConstants::nSiPMsTotal, 0.0 );
                                m_scale_factor = 0;

                                return {m_values, m_scale_factor};
                              } );
    }
    return StatusCode( StatusCode::SUCCESS );
  } );
#endif
}

double SpilloverProb::response( const int index_sipm ) const {
  auto& conds = m_conditions.get();
  return conds.prob_values.at( index_sipm );
}
