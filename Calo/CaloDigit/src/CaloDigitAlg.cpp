/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Core/FloatComparison.h"
#include <CaloDet/DeCalorimeter.h>
#include <DetDesc/GenericConditionAccessorHolder.h>
#include <Event/CaloAdc.h>
#include <Event/MCCaloDigit.h>
#include <Event/MCTruth.h>
#include <GaudiAlg/GaudiHistoAlg.h>
#include <GaudiKernel/IRndmGenSvc.h>
#include <GaudiKernel/RndmGenerators.h>
#include <GaudiKernel/SystemOfUnits.h>
#include <GaudiUtils/Aida2ROOT.h>
#include <Kernel/FPEGuard.h>
#include <TF1.h>
#include <TH1D.h>
#include <array>
#include <boost/assign.hpp>

using namespace boost::assign;

// ============================================================================
/** @file CaloDigitAlg.cpp
 *
 *    Calorimeter Digitisation, including noise, and Zero suppression.
 *
 *    @author: Olivier Callot
 *    @date:   5 June 2000
 *
 *    P. Koppenburg removed L0 on 2 September 2020
 */

// ============================================================================
/** @class CaloDigitAlg CaloDigitAlg.h
 *
 *  a (sub)Algorithm responsible
 *  for digitisation of MC-information
 *
 *  @author: Olivier Callot
 *   @date:   5 June 2000
 */

class CaloDigitAlg : public LHCb::DetDesc::ConditionAccessorHolder<GaudiHistoAlg> {

public:
  CaloDigitAlg( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;

private:
  std::string m_detectorName;  ///< Detector element location
  std::string m_inputData;     ///< Input container
  std::string m_outputData;    ///< Output container
  std::string m_inputPrevData; ///< Input data (prev.BX)

  double              m_gainError{0.01};     ///< error in PMT gain
  double              m_deadCellFraction{0}; ///< Fraction of dead cells
  std::vector<double> m_phe;                 // number of photoelectron per MIP (for each area)
  bool                m_zSup{false};

  enum { GAUS_INTEGRAL = 0, GAUS_MEAN = 1, GAUS_SIGMA = 2 };
  bool   m_monitorNoise{false};          ///< create noise monitoring histogram for each section
  bool   m_useAdvancedNoise{false};      ///< use advanced noise spectra generation
  double m_noiseIntegral[3] = {0, 0, 0}; ///< integrals of noise spectra
  std::map<int, std::vector<std::vector<double>>> m_advancedNoise;       /// advanced noise spectra for different areas
  AIDA::IHistogram1D*                             m_noiseHist[3] = {{}}; ///< monitoring histograms from advanced noise

private:
  /// protected poisson distribution against FPE
  inline double safe_poisson( double num ) {
    // turn off the inexact FPE
    FPE::Guard    underFPE( FPE::Guard::mask( "AllExcept" ), true );
    Rndm::Numbers nPe( randSvc(), Rndm::Poisson( num ) );
    return nPe();
  }

  struct Cache {
    Cache() = default;
    Cache( const CaloDigitAlg& self, const DeCalorimeter& calo ) : det{calo} {
      //** detector parameters
      m_numberOfCells = det->numberOfCells();
      m_activeToTotal = det->activeToTotal();
      m_saturatedAdc  = det->adcMax();

      //** Digitization parameters
      m_sat         = det->dynamicsSaturation();
      m_maxAdcValue = (int)( m_sat * m_saturatedAdc );
      if ( m_maxAdcValue == 0 ) { m_maxAdcValue = 1; } // For SPD, one bit output
      m_pedestalShift   = det->pedestalShift();        // pedestal shift in ADC unit
      m_coherentNoise   = det->coherentNoise();
      m_incoherentNoise = det->incoherentNoise();

      m_mip           = det->mipDeposit();
      m_fracPrev      = det->fractionFromPrevious();
      m_zSupThreshold = det->zSupThreshold();

      // this is not really needed, but it makes the modernization simpler
      auto info = [&self]() -> MsgStream& { return self.info(); };

      // Printout
      // --------
      info() << "----- Detector parameters (from condDB) ------------------" << endmsg;
      int icalo = LHCb::Detector::Calo::CellCode::caloNum( self.name() );

      for ( unsigned int iarea = 0; iarea < det->numberOfAreas(); ++iarea ) {
        std::string area = LHCb::Detector::Calo::CellCode::caloArea( icalo, iarea );
        info() << format( "Nominal gain parameters for %8s region :  %5.1f + %5.1f x sin(theta)", area.c_str(),
                          det->maxEtInCenter( iarea ), det->maxEtSlope( iarea ) )
               << endmsg;
      }
      if ( !self.m_useAdvancedNoise )
        info() << format( "Noise          : %5.1f + %5.1f (coherent) counts.", m_incoherentNoise, m_coherentNoise )
               << endmsg;
      else
        info() << "Advanced noise simulation is applied " << endmsg;
      info() << format( "Pedestal shift : %5.2f counts", m_pedestalShift ) << endmsg;
      info() << format( "Actual dynamics saturation : %6.2f x ADCMax ", m_sat ) << endmsg;
      if ( "" != self.m_inputPrevData ) info() << format( "Subtract %6.4f of previous BX ", m_fracPrev ) << endmsg;
      if ( !LHCb::essentiallyZero( m_mip ) ) info() << format( " MIP deposit    : %6.2f MeV", m_mip ) << endmsg;
      for ( unsigned int iarea = 0; iarea < det->numberOfAreas(); ++iarea ) {
        std::string area = LHCb::Detector::Calo::CellCode::caloArea( icalo, iarea );
        if ( !LHCb::essentiallyZero( det->numberOfPhotoElectrons( iarea ) ) )
          info() << format( "Number of photo-electrons per MIP for %8s region :  %6.2f ", area.c_str(),
                            det->numberOfPhotoElectrons( iarea ) )
                 << endmsg;
      }
      if ( self.m_zSup ) info() << format( "Zero-suppression threshold : %6.2f ADC ", m_zSupThreshold ) << endmsg;
      for ( unsigned int iarea = 0; iarea < det->numberOfAreas(); ++iarea ) {
        std::string area = LHCb::Detector::Calo::CellCode::caloArea( icalo, iarea );
      }
    }

    DetElementRef<DeCalorimeter> det;

    int    m_numberOfCells{0};   ///< Number of cells of this detector.
    double m_activeToTotal{0};   ///< Ratio activeE to total energy in ADC.
    int    m_saturatedAdc{0};    ///< Value to set in case ADC is saturated.
    double m_sat{0};             // fraction of full dynamics to consider (taking ped substraction into account)
    int    m_maxAdcValue{0};     ///< Maximum ADC content, after ped. substr
    double m_pedestalShift{0};   ///< pedestal shift due to noise
    double m_coherentNoise{0};   ///< magnitude of coherent noise
    double m_incoherentNoise{0}; ///< magnitude of incoherent noise
    double m_mip{0};             // MIP deposit (Prs/Spd)
    double m_fracPrev{0};        ///< fraction of previous BX subtracted
    double m_zSupThreshold{0};   ///< Zero suppression for data, PreShower only
  };

  Cache                    makeCache( const DeCalorimeter& calo ) const { return Cache{*this, calo}; }
  ConditionAccessor<Cache> m_cache{this, name() + "-GeomCache"};
};

DECLARE_COMPONENT( CaloDigitAlg )

//=============================================================================
// Standard creator, initializes variables
//=============================================================================
CaloDigitAlg::CaloDigitAlg( const std::string& name, ISvcLocator* pSvcLocator ) //
    : ConditionAccessorHolder( name, pSvcLocator ) {
  //** Declare the algorithm's properties which can be set at run time and
  //** their default values
  declareProperty( "DetectorName", m_detectorName );
  declareProperty( "InputData", m_inputData );
  declareProperty( "OutputData", m_outputData );
  declareProperty( "InputPrevData", m_inputPrevData );

  declareProperty( "GainError", m_gainError );
  declareProperty( "DeadCellFraction", m_deadCellFraction );
  declareProperty( "MonitorNoise", m_monitorNoise, "Switch on/off noise monitoring" );
  declareProperty( "ApplyZeroSuppression", m_zSup, "Apply 0-suppression" );
  declareProperty( "UseAdvancedNoise", m_useAdvancedNoise, "Switch on/off advanced noise" );
  declareProperty( "AdvancedNoiseOuter", m_advancedNoise[0], "Advanced noise for outer area." );
  declareProperty( "AdvancedNoiseMiddle", m_advancedNoise[1], "Advanced noise for middle area." );
  declareProperty( "AdvancedNoiseInner", m_advancedNoise[2], "Advanced noise for inner area." );

  // default setting
  m_advancedNoise[0].clear();
  m_advancedNoise[1].clear();
  m_advancedNoise[2].clear();
  m_noiseHist[0] = nullptr;
  m_noiseHist[1] = nullptr;
  m_noiseHist[2] = nullptr;
}

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode CaloDigitAlg::initialize() {

  StatusCode sc = ConditionAccessorHolder::initialize();
  if ( sc.isFailure() ) return sc;

  std::string begName = name().substr( 0, 8 );
  std::string detName, inputData, outputData;
  if ( "EcalDigi" == begName ) {
    detName    = DeCalorimeterLocation::Ecal;
    inputData  = LHCb::MCCaloDigitLocation::Ecal;
    outputData = LHCb::CaloAdcLocation::FullEcal;
    //  advanced noise (pick-up CW noise)
    if ( m_advancedNoise[0].empty() )
      m_advancedNoise[0] += list_of( 0.618503 )( 0.2671 )( 1.30606 ), list_of( 0.288860 )( 0.8269 )( 2.55909 ),
          list_of( 0.081979 )( 3.5597 )( 5.67153 ), list_of( 0.005044 )( 20.3323 )( 5.70424 ),
          list_of( 0.004536 )( -11.3676 )( 5.51563 ), list_of( 0.001078 )( -28.2179 )( 5.42412 );
    if ( m_advancedNoise[1].empty() )
      m_advancedNoise[1] += list_of( 0.579320 )( 0.2466 )( 1.31034 ), list_of( 0.304418 )( 0.8750 )( 2.54487 ),
          list_of( 0.103213 )( 2.9349 )( 5.80636 ), list_of( 0.006098 )( 21.9765 )( 7.15202 ),
          list_of( 0.005976 )( -9.1017 )( 6.18245 ), list_of( 0.000975 )( -26.2710 )( 5.48446 );
    if ( m_advancedNoise[2].empty() )
      m_advancedNoise[2] += list_of( 0.557991 )( 0.2394 )( 1.36762 ), list_of( 0.312109 )( 0.9972 )( 2.85723 ),
          list_of( 0.120438 )( 2.5708 )( 7.18906 ), list_of( 0.007780 )( 27.4646 )( 11.3398 ),
          list_of( 0.001681 )( -37.0023 )( 13.6182 );
  } else if ( "HcalDigi" == begName ) {
    detName    = DeCalorimeterLocation::Hcal;
    inputData  = LHCb::MCCaloDigitLocation::Hcal;
    outputData = LHCb::CaloAdcLocation::FullHcal;
  }
  if ( m_detectorName.empty() ) m_detectorName = detName;
  if ( m_inputData.empty() ) m_inputData = inputData;
  if ( m_outputData.empty() ) m_outputData = outputData;

  addConditionDerivation( {m_detectorName}, m_cache.key(),
                          [&]( const DeCalorimeter& calo ) { return makeCache( calo ); } );

  // define the names of the noise monitoring histograms
  // ---------------------------------------------------
  std::array<std::string, 3> areas{"outer", "middle", "inner"};

  // configure advanced noise parameters
  // -----------------------------------
  if ( m_useAdvancedNoise ) {
    // check number of parameters
    for ( int area = 0; area < 3; area++ ) {
      if ( m_advancedNoise[area].empty() )
        return Error( "Advanced noise is configured incorectly (see options file).", StatusCode::FAILURE );
      for ( int i = 0, size = m_advancedNoise[area].size(); i < size; i++ ) {
        if ( m_advancedNoise[area][i].size() != 3 )
          return Error( "Advanced noise is configured incorectly (see options file).", StatusCode::FAILURE );
      }
    }
    const double nsigma = 6.0;
    // define monitoring histograms limits and calculate integrals
    for ( int area = 0; area < 3; area++ ) {
      const std::vector<std::vector<double>>& advancedNoise = m_advancedNoise[area];
      int                                     low           = 0xFFFF;  // low limit of the monitoring histogram
      int                                     high          = -0xFFFF; // high limit of the monitoring histogram
      if ( msgLevel( MSG::DEBUG ) ) debug() << "advanced noise parameters for " << areas[area] << " area:" << endmsg;
      double& noiseIntegral = m_noiseIntegral[area];
      noiseIntegral         = 0;
      for ( int i = 0, size = advancedNoise.size(); i < size; i++ ) {
        if ( msgLevel( MSG::DEBUG ) )
          debug() << "gaus " << i + 1 << ": "
                  << "[ integral = " << advancedNoise[i][GAUS_INTEGRAL] << ", mean = " << advancedNoise[i][GAUS_MEAN]
                  << ", sigma = " << advancedNoise[i][GAUS_SIGMA] << " ]" << endmsg;
        // check value of the sigma
        if ( advancedNoise[i][GAUS_SIGMA] <= 0 ) return Error( "Sigma is negative.", StatusCode::FAILURE );
        // calculate integral
        noiseIntegral += advancedNoise[i][GAUS_INTEGRAL];
        // calculate histograms limits
        int low_  = (int)( advancedNoise[i][GAUS_MEAN] - nsigma * advancedNoise[i][GAUS_SIGMA] );
        low       = low_ < low ? low_ : low;
        int high_ = (int)( advancedNoise[i][GAUS_MEAN] + nsigma * advancedNoise[i][GAUS_SIGMA] );
        high      = high_ > high ? high_ : high;
      }

      // noise integral have to be greater then zero
      if ( LHCb::essentiallyZero( noiseIntegral ) )
        return Error( "Noise integral is equal to zero (see options file).", StatusCode::FAILURE );
      // create monitoring histogram
      if ( m_monitorNoise ) m_noiseHist[area] = book( areas[area].c_str(), low, high, high - low );
    }
  } else if ( m_monitorNoise ) {
    // create monitoring histograms
    for ( int area = 0; area < 3; area++ ) { m_noiseHist[area] = book1D( areas[area].c_str(), -10, 10, 200 ); }
  }

  info() << "----- Digitization parameters (from CaloDigitAlg configuration) ------------------" << endmsg;
  info() << format( "Additional %4.1f%% of the cells as dead (gain=0).", 100. * m_deadCellFraction ) << endmsg;
  info() << format( "Additional gain error %4.1f%%", m_gainError * 100. ) << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloDigitAlg::execute() {
  bool isDebug = ( msgLevel() <= MSG::DEBUG );

  const Cache& cache = m_cache.get();

  // random numbers for advanced noise generation
  Rndm::Numbers noiseFlatRndm( randSvc(), Rndm::Flat( 0.0, 1.0 ) );
  Rndm::Numbers noiseGausRndm( randSvc(), Rndm::Gauss( 0.0, 1.0 ) );

  //*** get the input data
  LHCb::MCCaloDigits* mcDigits = get<LHCb::MCCaloDigits>( m_inputData );

  //=== Get the previous BX's data if needed
  LHCb::MCCaloDigits* prevDigits = 0;
  if ( "" != m_inputPrevData ) {
    if ( exist<LHCb::MCCaloDigits>( m_inputPrevData, IgnoreRootInTES ) )
      prevDigits = get<LHCb::MCCaloDigits>( m_inputPrevData, IgnoreRootInTES );
  }

  //***  prepare and register the output container into the Transient Store!
  LHCb::CaloAdcs* adcs = new LHCb::CaloAdcs();
  put( adcs, m_outputData );

  if ( isDebug ) debug() << "Processing " << mcDigits->size() << " mcDigits" << endmsg;

  // == prepare buffers for energies and tags. Size is only real cells.
  // == fill with MCCaloDigit, using the calibration activeToTotal
  std::vector<double> caloEnergies( cache.m_numberOfCells, 0.0 );
  for ( LHCb::MCCaloDigits::const_iterator mcDig = mcDigits->begin(); mcDigits->end() != mcDig; ++mcDig ) {
    if ( 0 != *mcDig ) {
      int index           = cache.det->cellIndex( ( *mcDig )->cellID() );
      caloEnergies[index] = ( *mcDig )->activeE() * cache.m_activeToTotal;
    }
  }

  //== generate coherent noise for this event, add the pedestal shift
  Rndm::Numbers CoherentNoise( randSvc(), Rndm::Gauss( 0.0, cache.m_coherentNoise ) );
  double        offset = CoherentNoise() + cache.m_pedestalShift;

  //== generate 2 sequences of random numbers, for gain error and noise
  std::vector<double> gainErrors( cache.m_numberOfCells, 1.0 );
  if ( 0 < m_gainError ) {
    std::generate( gainErrors.begin(), gainErrors.end(), Rndm::Numbers( randSvc(), Rndm::Gauss( 1.0, m_gainError ) ) );
  }
  std::vector<double> incoherentNoise( cache.m_numberOfCells, 0.0 );
  if ( false == m_useAdvancedNoise && 0 < cache.m_incoherentNoise ) {
    std::generate( incoherentNoise.begin(), incoherentNoise.end(),
                   Rndm::Numbers( randSvc(), Rndm::Gauss( 0.0, cache.m_incoherentNoise ) ) );
  }

  //== Add random dead cells if requested (set the gain to zero in this cell)
  if ( !LHCb::essentiallyZero( m_deadCellFraction ) ) {
    std::vector<double> probaDead( cache.m_numberOfCells, 1. );
    std::generate( probaDead.begin(), probaDead.end(), Rndm::Numbers( randSvc(), Rndm::Flat( 0.0, 1.0 ) ) );
    for ( unsigned int kk = 0; probaDead.size() > kk; kk++ ) {
      if ( m_deadCellFraction > probaDead[kk] ) { gainErrors[kk] = 0.; }
    }
  }

  //== loop over all cells in the detector
  for ( std::vector<double>::iterator cell = caloEnergies.begin(); cell != caloEnergies.end(); ++cell ) {
    const unsigned int                 index = cell - caloEnergies.begin();
    const LHCb::Detector::Calo::CellID id    = cache.det->cellIdByIndex( index );

    // dead cell from condDB store
    if ( cache.det->isDead( id ) ) gainErrors[index] = 0.;
    //    if( cache.det->isNoisy( id )).. ;//@TODO : noisy cell simulation (e.g. increase noise by a given factor)

    //== extract the PMT gain for the given channel , translate the energy to
    //   ADC scale, take into account coherent and incoherent noise and
    //   saturation to get adc count and transform again into energy.

    double energy = *cell; // Get the energy from the array

    //== generate the photo-electron fluctuation...
    double pePerMeV =
        ( !LHCb::essentiallyZero( cache.m_mip ) ) ? cache.det->numberOfPhotoElectrons( id.area() ) / cache.m_mip : 0;
    if ( 0. < energy && 0. < pePerMeV ) { energy = safe_poisson( energy * pePerMeV ) / pePerMeV; }

    double gain = cache.det->cellGain( id );
    double adcValue;

    // if use advanced noise then generate noise value according to the description of the noise spectra
    if ( m_useAdvancedNoise ) {
      // ************* generate advanced noise *************
      // ***************************************************
      double noise = 0;
      // select noise spectrum corresponding to the cell area
      const std::vector<std::vector<double>>& advancedNoise = m_advancedNoise[id.area()];
      // generate random flat value in the range ( 0 : noise spectrum integral )
      // this value is used to select Gaussian which will be generated (the Gaussian is selected according to its
      // component fraction)
      double selector = m_noiseIntegral[id.area()] * noiseFlatRndm();
      // find Gaussian to generate
      for ( int i = 0, size = advancedNoise.size(); i < size; i++ ) {
        // substract from the selector value current Gaussian integral
        selector -= advancedNoise[i][GAUS_INTEGRAL];
        // if it became less then zero or it is the last Gaussian (this must not happen) then we generate it
        if ( selector < 0 || i == size - 1 ) {
          noise = advancedNoise[i][GAUS_MEAN] + advancedNoise[i][GAUS_SIGMA] * noiseGausRndm();
          break;
        }
      }
      adcValue = ( !LHCb::essentiallyZero( gain ) ) ? gainErrors[index] * energy / gain + noise : 0;
      // fill monitoring histograms
      if ( m_monitorNoise && m_noiseHist[id.area()] ) fill( m_noiseHist[id.area()], noise, 1.0 );
    } else {
      // ********* generate standard noise **************
      // ************************************************
      adcValue =
          ( !LHCb::essentiallyZero( gain ) ) ? gainErrors[index] * energy / gain + incoherentNoise[index] + offset : 0;
      // fill monitoring histograms
      if ( m_monitorNoise && m_noiseHist[id.area()] )
        fill( m_noiseHist[id.area()], incoherentNoise[index] + offset, 1.0 );
    }

    // Simulate spill-over
    // -------------------
    if ( 0 != prevDigits ) {
      // Correct for spill-over in Prs/Spd by a fixed fraction.
      // Do it on signal, as Spd has a single bit adc...
      LHCb::MCCaloDigit* prevMc = prevDigits->object( id );
      if ( 0 != prevMc ) {
        double prevEnergy = prevMc->activeE() * cache.m_activeToTotal;
        //== generate the photo-electron fluctuation on the previous BX signal
        if ( 0. < prevEnergy && 0. < pePerMeV ) { prevEnergy = safe_poisson( prevEnergy * pePerMeV ) / pePerMeV; }
        double cor = cache.m_fracPrev * prevEnergy / gain;
        adcValue -= cor;
        if ( isDebug && .5 < cor )
          debug() << id << format( " adc%7.1f correct%7.1f => %7.1f", adcValue, cor, adcValue - cor ) << endmsg;
      }
    }

    // Digitisation
    // ------------
    // ADC
    int intAdc = (int)floor( adcValue + 0.5 );
    if ( intAdc > cache.m_maxAdcValue ) { intAdc = cache.m_saturatedAdc; }
    bool storeADC = true;
    if ( m_zSup && cache.m_zSupThreshold > (double)intAdc ) storeADC = false;
    if ( storeADC ) {
      LHCb::CaloAdc* adc = new LHCb::CaloAdc( id, intAdc );
      adcs->insert( adc );
      //}else {
      // debug() << "ADC not stored " << id << " " << intAdc << endmsg;
    }
  }

  // Final printout
  if ( isDebug )
    debug() << format( "Have digitized and stored %5d adcs from %5d MCDigits.", adcs->size(), mcDigits->size() )
            << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloDigitAlg::finalize() {
  // if monitor advanced noise then fit histogram and check chi2
  if ( m_useAdvancedNoise && m_monitorNoise ) {
    std::array<std::string, 3> areas{"outer", "middle", "inner"};
    for ( int area = 0; area < 3; area++ ) {
      AIDA::IHistogram1D* aidaHist = m_noiseHist[area];
      if ( 0 == aidaHist ) continue;
      const double                            noiseIntegral = m_noiseIntegral[area];
      const std::vector<std::vector<double>>& advancedNoise = m_advancedNoise[area];
      // get root histogram
      TH1D*   rootHist = Gaudi::Utils::Aida2ROOT::aida2root( aidaHist );
      double* pars     = new double[3 * advancedNoise.size()];
      if ( pars == 0 ) continue;
      // form function
      std::ostringstream stream;
      for ( int i = 0, size = advancedNoise.size(); i < size; i++ ) {
        pars[3 * i + 0] = ( rootHist->GetEntries() / noiseIntegral ) * advancedNoise[i][GAUS_INTEGRAL] /
                          ( advancedNoise[i][GAUS_SIGMA] * sqrt( 2 * 3.1415927 ) );
        pars[3 * i + 1] = advancedNoise[i][GAUS_MEAN];
        pars[3 * i + 2] = advancedNoise[i][GAUS_SIGMA];
        if ( i != 0 ) stream << " + ";
        stream << "gaus(" << i * 3 << ")";
      }

      // create root function
      TF1 func( "noiseFunc", stream.str().c_str(), rootHist->GetXaxis()->GetXmin(), rootHist->GetXaxis()->GetXmax() );
      func.SetParameters( pars );

      // fix all parameter of the function except component fractions (integrals)
      for ( int i = 0, size = 3 * advancedNoise.size(); i < size; i++ ) {
        if ( i % 3 != 0 ) func.FixParameter( i, pars[i] );
      }

      // fit noise spectra
      rootHist->Fit( "noiseFunc", "INQ" );

      // print obtained component fractions and chisquare
      info() << "noise fit (" << areas[area] << " area):" << endmsg;
      info() << "  chisquare = " << func.GetChisquare() << "/" << func.GetNDF() << endmsg;
      for ( int i = 0, size = 3 * advancedNoise.size(); i < size; i++ ) {
        if ( i % 3 == 0 )
          info() << "  amplitude for gaus #" << 1 + i / 3 << " = " << func.GetParameter( i ) << " +- "
                 << func.GetParError( i ) << " ( " << pars[i] << " )" << endmsg;
      }
      delete[] pars;
    }
  }
  return ConditionAccessorHolder::finalize();
}
