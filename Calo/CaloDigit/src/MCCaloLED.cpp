/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <CaloDet/DeCalorimeter.h>
#include <DetDesc/GenericConditionAccessorHolder.h>
#include <Event/MCCaloDigit.h>
#include <Gaudi/Accumulators.h>
#include <GaudiAlg/GaudiAlgorithm.h>
#include <GaudiKernel/IRndmGenSvc.h>
#include <GaudiKernel/RndmGenerators.h>

/** @class MCCaloLED
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-03-02
 */
class MCCaloLED : public LHCb::DetDesc::ConditionAccessorHolder<GaudiAlgorithm> {
public:
  /// initialize the algorithm
  StatusCode initialize() override;
  /// execute    the algorithm
  StatusCode execute() override;

  MCCaloLED( const std::string& name, ISvcLocator* pSvc ) : ConditionAccessorHolder( name, pSvc ) {
    setProperty( "PropertiesPrint", true ).ignore();
  }

private:
  typedef std::vector<LHCb::Detector::Calo::CellID> Cells;

  Gaudi::Property<std::string> m_output{this, "Output", LHCb::MCCaloHitLocation::Ecal, "location of output hits"};
  Gaudi::Property<std::string> m_caloName{this, "Calorimeter", DeCalorimeterLocation::Ecal, "calorimeter name"};
  // add 5 Gev of transverse energy
  Gaudi::Property<double> m_mean{this, "MeanET", 5.00 * Gaudi::Units::GeV, "mean value fo the fired trasnverse energy"};
  Gaudi::Property<double> m_sigma{this, "Sigma", 0.25 * Gaudi::Units::perCent, "relative sigma on the fired energy"};
  Gaudi::Property<double> m_fraction{this, "Fraction", 1.0 / 16.0, "fraction of fired hits"};

  mutable Gaudi::Accumulators::Counter<> m_counter{this, "#hits"};

  Cells fillCells( const DeCalorimeter& calo ) const {
    Cells cells;
    cells.reserve( calo.numberOfCells() );
    int iCalo = LHCb::Detector::Calo::CellCode::CaloNumFromName( m_caloName );
    if ( 0 > iCalo ) { throw GaudiException( "Invaild Calo Code!", name(), StatusCode::FAILURE ); }

    for ( int iArea = 0; iArea < 3; ++iArea ) {
      for ( int iRow = 0; iRow < 64; ++iRow ) {
        for ( int iCol = 0; iCol < 64; ++iCol ) {
          // create CellID
          const LHCb::Detector::Calo::CellID id( static_cast<LHCb::Detector::Calo::CellCode::Index>( iCalo ), iArea,
                                                 iRow, iCol );
          // check it !
          if ( !calo.valid( id ) ) { continue; }
          // add it into list of good cells
          cells.push_back( id );
        }
      }
    }
    {
      // remove duplicates (if any)
      std::sort( cells.begin(), cells.end() );
      Cells::iterator iend = std::unique( cells.begin(), cells.end() );
      cells.erase( iend, cells.end() );
    }

    if ( msgLevel( MSG::DEBUG ) ) { debug() << "Number of valid cells: " << cells.size() << endmsg; }
    return cells;
  }

  struct Cache {
    Cells                        cells;
    DetElementRef<DeCalorimeter> det;
  };

  Cache makeCache( const DeCalorimeter& calo ) const { return {fillCells( calo ), calo}; }

  ConditionAccessor<Cache> m_cache{this, name() + "-GeomCache"};
};
// ============================================================================
/// Declaration of the Algorithm Factory
// ============================================================================
DECLARE_COMPONENT( MCCaloLED )
// ============================================================================
// Execution of the algorithm
// ============================================================================
StatusCode MCCaloLED::execute() {
  // create MCHits
  LHCb::MCCaloHit::Container* hits = new LHCb::MCCaloHit::Container();
  // register them in TES
  put( hits, m_output );

  // prepare 2 helper objest to geenrate random numbers
  Rndm::Numbers gauss( randSvc(), Rndm::Gauss( 0.0, 1. ) );
  Rndm::Numbers flat( randSvc(), Rndm::Flat( 0.0, 1. ) );

  const Cache& cache = m_cache.get();

  // loop over the cells which needs to be fired:
  for ( const auto id : cache.cells ) {
    // should the cell be fired?
    if ( flat() > m_fraction ) { continue; } // CONTINUE

    /// generate the energy
    const double mean   = m_mean / cache.det->cellSine( id ) / cache.det->activeToTotal();
    const double energy = mean * ( 1.0 + m_sigma * gauss() );

    // create new MC hit
    LHCb::MCCaloHit* mchit = new LHCb::MCCaloHit();
    // fill it
    mchit->setCellID( id );
    mchit->setTime( 0 );
    mchit->setActiveE( energy );
    // add hit to the container
    hits->add( mchit );
  }

  // some statistics
  m_counter += hits->size();

  return StatusCode::SUCCESS;
}
// ============================================================================
/// initialize the algorithm
// ============================================================================
StatusCode MCCaloLED::initialize() {
  return ConditionAccessorHolder::initialize().andThen( [this]() {
    addConditionDerivation( {m_caloName}, m_cache.key(),
                            [&]( const DeCalorimeter& calo ) { return makeCache( calo ); } );
    return StatusCode::SUCCESS;
  } );
}
