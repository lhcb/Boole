/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <CaloDet/DeCalorimeter.h>
#include <DetDesc/GenericConditionAccessorHolder.h>
#include <Event/CaloDigit.h>
#include <Event/MCCaloDigit.h>
#include <Event/MCCaloHit.h>
#include <Event/MCHeader.h>
#include <GaudiAlg/GaudiTupleAlg.h>
#include <GaudiAlg/Tuple.h>
#include <GaudiAlg/TupleObj.h>
#include <cmath>
#include <string>
#include <vector>

// local
/** @class CaloDigitChannel CaloDigitChannel.h
 *
 *  Evaluate the Energy in the Calo Channel by Channel
 *
 *  @author Yasmine AMHIS
 *  @date   2007-03-28
 */

class CaloDigitChannel : public LHCb::DetDesc::ConditionAccessorHolder<GaudiTupleAlg> {
public:
  using ConditionAccessorHolder::ConditionAccessorHolder;
  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  Gaudi::Property<std::string>     m_nameOfDetector{this, "Detector", ""};
  ConditionAccessor<DeCalorimeter> m_detector{this, "DetectorLocation", ""};

  std::string m_inputData; ///< Input container
};

DECLARE_COMPONENT( CaloDigitChannel )

using namespace LHCb;

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode CaloDigitChannel::initialize() {
  {
    // the path to the detector depends on the name of the algorithm instance
    // and the ConditionAccessor key must be set before ConditionAccessorHolder::initialize
    // because of a limitation of the DetDesc implementation
    std::string detLoc;
    if ( "Ecal" == m_nameOfDetector ) {
      detLoc = DeCalorimeterLocation::Ecal;
    } else if ( "Hcal" == m_nameOfDetector ) {
      detLoc = DeCalorimeterLocation::Hcal;
    }
    // note that we do not have direct access to ConditionAccessor::m_key, so we have to pass through the property
    if ( m_detector.key().empty() )
      setProperty( "DetectorLocation", detLoc ).orThrow( "failed to initialize Detector property", name() );
  }

  return ConditionAccessorHolder::initialize().andThen( [this] {
    if ( "Ecal" == m_nameOfDetector ) {
      m_inputData = LHCb::CaloDigitLocation::Ecal;
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Detector name == ECAL  " << m_nameOfDetector << endmsg;
        debug() << "Input data :  " << m_inputData << endmsg;
      }
    } else if ( "Hcal" == m_nameOfDetector ) {
      m_inputData = LHCb::CaloDigitLocation::Hcal;
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << " Detector Name == HCAL  " << m_nameOfDetector << endmsg;
        debug() << "Input data  ==> Normal  " << m_inputData << endmsg;
      }
    }
  } );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloDigitChannel::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << " >>> Execute" << endmsg;
  StatusCode          sc          = StatusCode::SUCCESS;
  Tuple               tuple       = nTuple( "Channel" );
  double              nbDigit     = 0.;
  double              energyDigit = 0;
  std::vector<double> energy;
  std::vector<double> index;
  std::vector<double> x;
  std::vector<double> y;
  std::vector<double> z;
  std::vector<double> cellGain;
  std::vector<double> cellTime;
  std::vector<double> area;
  std::vector<double> col;
  std::vector<double> row;

  if ( !exist<LHCb::CaloDigits>( m_inputData ) ) return StatusCode::SUCCESS;
  LHCb::CaloDigits* digits = get<LHCb::CaloDigits>( m_inputData );
  if ( 0 != digits ) {
    DetElementRef<DeCalorimeter> calo = m_detector.get();

    LHCb::CaloDigits::const_iterator dig; // LHCb Calo Digits
    for ( dig = digits->begin(); digits->end() != dig; ++dig ) {
      nbDigit += 1.;

      LHCb::Detector::Calo::CellID id = ( *dig )->cellID();
      energyDigit                     = ( *dig )->e();
      energy.push_back( energyDigit );
      x.push_back( calo->cellX( id ) );
      y.push_back( calo->cellY( id ) );
      z.push_back( calo->cellZ( id ) );
      index.push_back( id.index() );
      area.push_back( id.area() );
      col.push_back( id.col() );
      row.push_back( id.row() );
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << " x " << x << endmsg;
        debug() << " y  " << y << endmsg;
        debug() << " id  " << id << endmsg;
        debug() << " col " << col << endmsg;
        debug() << " row " << row << endmsg;
        debug() << "The Number of Digits is : " << nbDigit << endmsg;
      }
    }
  }
  tuple->farray( "index", index, "Ndigits", 7000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  tuple->farray( "x", x, "Ndigits", 7000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  tuple->farray( "y", y, "Ndigits", 7000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  tuple->farray( "z", z, "Ndigits", 7000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  tuple->farray( "energy", energy, "Ndigits", 7000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  tuple->farray( "area", area, "Ndigits", 7000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  tuple->farray( "col", col, "Ndigits", 7000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  tuple->farray( "row", row, "Ndigits", 7000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  sc = tuple->write();
  info() << "written tuple" << endmsg;

  return sc;
}
