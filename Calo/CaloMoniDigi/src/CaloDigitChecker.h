/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALODIGITCHECKER_H
#define CALODIGITCHECKER_H 1
// Include files

// from STL
#include <string>
#include <vector>
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

/** @class CaloDigitChecker CaloDigitChecker.h
 *  Monitor the results of the Calo Digitisation. Histograms
 *
 *  @author Olivier Callot
 *  @date   25/05/2001
 */

class CaloDigitChecker : public GaudiHistoAlg {
public:
  /// Standard constructor & descructor
  CaloDigitChecker( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~CaloDigitChecker();

  StatusCode initialize() override; ///< Initialisation
  StatusCode execute() override;    ///< Execution
protected:
private:
  std::string m_nameOfDetector; // Detector short name
  std::string m_nameOfHits;     // Hits container
  std::string m_nameOfMCDigits; // MCDigit container
  std::string m_nameOfDigits;   // Digit container

  double m_maxMultiplicity; // histo limits
  double m_maxEnergy;
  double m_scaleHit;
};
#endif // CALODIGITCHECKER_H
