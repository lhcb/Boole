/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Core/FloatComparison.h"
#include "Event/MCFTDeposit.h"
#include "Event/UTDigit.h" //no need for cluster
#include <DetDesc/GenericConditionAccessorHolder.h>
#include <Event/FTLiteCluster.h>
#include <Event/MCHit.h>
#include <Event/MCParticle.h>
#include <Event/MCProperty.h>
#include <Event/MCTrackInfo.h>
#include <Event/UTCluster.h> //no need for cluster
#include <Event/VPDigit.h>
#include <FTDet/DeFTDetector.h>
#include <GaudiAlg/GaudiAlgorithm.h>
#include <Linker/LinkedTo.h>
#include <UTDet/DeUTDetector.h>
#include <VPDet/DeVP.h>

/** @class BuildMCTrackInfo BuildMCTrackInfo.h
 *  Build the Reconstructable MCProperty table.
 *
 *  @author Olivier Callot
 *  @date   2012-04-02 : Updated version for upgrade: IT, OT and FT optional, Pixel/normal Velo
 */
class BuildMCTrackInfo final : public LHCb::DetDesc::ConditionAccessorHolder<GaudiAlgorithm> {
  using ConditionAccessorHolder::ConditionAccessorHolder;
  static constexpr int shiftT = 2;

public:
  StatusCode execute() override; ///< Algorithm execution

protected:
  void updateBit( int& result, int sta, bool isX ) {
    if ( 0 == sta ) {
      result |= MCTrackInfo::maskUT1;
    } else if ( 1 == sta ) {
      result |= MCTrackInfo::maskUT2;
    } else if ( shiftT + 0 == sta ) {
      if ( isX )
        result |= MCTrackInfo::maskT1X;
      else
        result |= MCTrackInfo::maskT1S;
    } else if ( shiftT + 1 == sta ) {
      if ( isX )
        result |= MCTrackInfo::maskT2X;
      else
        result |= MCTrackInfo::maskT2S;
    } else if ( shiftT + 2 == sta ) {
      if ( isX )
        result |= MCTrackInfo::maskT3X;
      else
        result |= MCTrackInfo::maskT3S;
    }
  }

  void updateAccBit( int& result, int sta, bool isX ) {
    if ( 0 == sta ) {
      result |= MCTrackInfo::maskAccUT1;
    } else if ( 1 == sta ) {
      result |= MCTrackInfo::maskAccUT2;
    } else if ( shiftT + 0 == sta ) {
      if ( isX )
        result |= MCTrackInfo::maskAccT1X;
      else
        result |= MCTrackInfo::maskAccT1S;
    } else if ( shiftT + 1 == sta ) {
      if ( isX )
        result |= MCTrackInfo::maskAccT2X;
      else
        result |= MCTrackInfo::maskAccT2S;
    } else if ( shiftT + 2 == sta ) {
      if ( isX )
        result |= MCTrackInfo::maskAccT3X;
      else
        result |= MCTrackInfo::maskAccT3S;
    }
  }

  void computeAcceptance( std::vector<int>& station );

private:
  Gaudi::Property<bool> m_withVP{this, "WithVP", true};
  Gaudi::Property<bool> m_withUT{this, "WithUT", true};
  Gaudi::Property<bool> m_withFT{this, "WithFT", true};

  Gaudi::Accumulators::BinomialCounter<>  m_hasVP{this, "Has VP digits"};
  Gaudi::Accumulators::BinomialCounter<>  m_hasUT{this, "Has UT clusters"};
  Gaudi::Accumulators::BinomialCounter<>  m_hasT1{this, "Has T1 clusters"};
  Gaudi::Accumulators::BinomialCounter<>  m_hasT2{this, "Has T2 clusters"};
  Gaudi::Accumulators::BinomialCounter<>  m_hasT3{this, "Has T3 clusters"};
  Gaudi::Accumulators::BinomialCounter<>  m_accVP{this, "Has VP MC hits"};
  Gaudi::Accumulators::BinomialCounter<>  m_accUT{this, "Has UT MC hits"};
  Gaudi::Accumulators::BinomialCounter<>  m_accT1{this, "Has T1 MC hits"};
  Gaudi::Accumulators::BinomialCounter<>  m_accT2{this, "Has T2 MC hits"};
  Gaudi::Accumulators::BinomialCounter<>  m_accT3{this, "Has T3 MC hits"};
  Gaudi::Accumulators::AveragingCounter<> m_multVPOrig{this, "true VP multiplicity"};
  Gaudi::Accumulators::AveragingCounter<> m_multVP{this, "stored VP multiplicity"};

  ConditionAccessor<DeUTDetector> m_utDet{this, DeUTDetLocation::location()};
  ConditionAccessor<DeFT>         m_ftDet{this, DeFTDetectorLocation::Default};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( BuildMCTrackInfo )

//=============================================================================
// Main execution
//=============================================================================
StatusCode BuildMCTrackInfo::execute() {
  const bool isDebug   = msgLevel( MSG::DEBUG );
  const bool isVerbose = msgLevel( MSG::VERBOSE );

  if ( isDebug ) debug() << "==> Execute" << endmsg;

  LHCb::MCParticles* mcParts = get<LHCb::MCParticles>( LHCb::MCParticleLocation::Default );

  LHCb::MCProperty* trackInfo = new LHCb::MCProperty();
  put( trackInfo, LHCb::MCPropertyLocation::TrackInfo );

  //== The array size is bigger than MCParticle.size() as the MCParticles are
  //== compressed, uninteresting particles were removed at the end of Brunel.
  int highestKey = mcParts->size();
  for ( LHCb::MCParticles::const_iterator itP = mcParts->begin(); mcParts->end() != itP; itP++ ) {
    if ( highestKey < ( *itP )->key() ) highestKey = ( *itP )->key();
  }
  unsigned int sizePart = highestKey + 1;

  if ( isDebug ) debug() << "Highest MCParticle number " << highestKey << endmsg;

  std::vector<int> lastVelo( sizePart, -1 );
  std::vector<int> veloPix( sizePart, 0 );
  std::vector<int> station( sizePart, 0 );

  unsigned int MCNum;

  if ( m_withVP ) {
    LHCb::VPDigits const*   digits = get<LHCb::VPDigits>( LHCb::VPDigitLocation::Default );
    LHCb::LinksByKey const* lnks =
        get<LHCb::LinksByKey>( LHCb::LinksByKey::linkerName( LHCb::VPDigitLocation::Default ) );

    for ( const auto& digit : *digits ) {
      int module = digit->channelID().module();
      for ( const auto& part : LinkedTo<LHCb::MCParticle>( lnks ).range( digit->key() ) ) {
        if ( mcParts == part.parent() ) {
          MCNum = part.key();
          if ( veloPix.size() > MCNum ) {
            if ( module != lastVelo[MCNum] ) {
              // Count only once per module a given MCParticle
              lastVelo[MCNum] = module;
              veloPix[MCNum]++;
              if ( isVerbose ) {
                verbose() << "MC " << MCNum << " VP module " << module << " nbVP " << veloPix[MCNum] << endmsg;
              }
            }
          }
        }
      }
    }
  }

  if ( m_withUT ) { //== UT cluster -> particle associaton
    LHCb::UTDigits const*   UTDig = get<LHCb::UTDigits>( LHCb::UTDigitLocation::UTDigits );
    LHCb::LinksByKey const* lnks =
        get<LHCb::LinksByKey>( LHCb::LinksByKey::linkerName( LHCb::UTClusterLocation::UTClusters ) );
    for ( const auto& dig : *UTDig ) {
      int  sta = dig->channelID().station() - 1;   // 0-1 from 1-2
      int  lay = ( dig->channelID().layer() ) % 2; // New UTChannelID layer() is in [0,3]
      bool isX = ( 0 == lay ) || ( 3 == lay );
      for ( const auto& part : LinkedTo<LHCb::MCParticle>{lnks}.range( dig ) ) {
        if ( mcParts == part.parent() ) {
          MCNum = part.key();
          updateBit( station[MCNum], sta, isX );
          if ( isVerbose ) verbose() << "MC " << MCNum << " UT Sta " << sta << " lay " << lay << endmsg;
        }
      }
    }
  }

  if ( m_withFT ) { //=== Fibre Tracker
    using FTLiteClusters             = LHCb::FTLiteCluster::FTLiteClusters;
    FTLiteClusters const*   clusters = get<FTLiteClusters>( LHCb::FTLiteClusterLocation::Default );
    LHCb::LinksByKey const* lnks =
        get<LHCb::LinksByKey>( LHCb::LinksByKey::linkerName( LHCb::FTLiteClusterLocation::Default ) );

    for ( const auto& cluster : clusters->range() ) {
      LHCb::Detector::FTChannelID channelID = cluster.channelID();
      // Create station to be in the range 2-4
      int shiftedStation = channelID.globalStationIdx() + shiftT;
      for ( auto const& part : LinkedTo<LHCb::MCParticle>{lnks}.range( channelID ) ) {
        if ( mcParts == part.parent() ) {
          MCNum = part.key();
          updateBit( station[MCNum], shiftedStation, channelID.isX() );
          if ( isVerbose )
            verbose() << "MC " << MCNum << " FT Sta " << shiftedStation << " lay " << to_unsigned( channelID.layer() )
                      << endmsg;
        }
      }
    }
  }

  //=====================================================================
  // Now compute the acceptance, with MCHits
  //=====================================================================
  computeAcceptance( station );

  //== Build now the trackInfo tracks
  for ( auto const& part : *mcParts ) {
    MCNum    = part->key();
    int mask = station[MCNum];
    if ( m_withVP ) {
      m_multVPOrig += veloPix[MCNum];
      if ( 2 < veloPix[MCNum] ) mask |= MCTrackInfo::maskHasVelo;
      const auto multVP = std::min( veloPix[MCNum], MCTrackInfo::maskMultVelo >> MCTrackInfo::multVelo );
      m_multVP += multVP;
      mask |= ( multVP << MCTrackInfo::multVelo );

      m_hasVP += ( mask & MCTrackInfo::maskHasVelo ) != 0;
      m_accVP += ( mask & MCTrackInfo::maskAccVelo ) != 0;
    }
    if ( m_withUT ) {
      m_hasUT += ( mask & MCTrackInfo::maskHasUT ) != 0;
      m_accUT += ( mask & MCTrackInfo::maskAccUT ) != 0;
    }
    if ( m_withFT ) {
      m_hasT1 += ( mask & MCTrackInfo::maskHasT1 ) != 0;
      m_accT1 += ( mask & MCTrackInfo::maskAccT1 ) != 0;
      m_hasT2 += ( mask & MCTrackInfo::maskHasT2 ) != 0;
      m_accT2 += ( mask & MCTrackInfo::maskAccT2 ) != 0;
      m_hasT3 += ( mask & MCTrackInfo::maskHasT3 ) != 0;
      m_accT3 += ( mask & MCTrackInfo::maskAccT3 ) != 0;
    }

    if ( 0 != mask ) {
      trackInfo->setProperty( part, mask );
      if ( isDebug ) {
        debug() << format( "Track %4d mask %8x nPix %2d ", MCNum, mask, veloPix[MCNum] );
        if ( ( mask & MCTrackInfo::maskHasVelo ) != 0 ) debug() << " hasVelo ";
        if ( ( mask & MCTrackInfo::maskAccVelo ) != 0 ) debug() << " accVelo ";
        if ( ( mask & MCTrackInfo::maskHasUT ) != 0 ) debug() << " hasUT ";
        if ( ( mask & MCTrackInfo::maskAccUT ) != 0 ) debug() << " accUT ";
        if ( ( mask & MCTrackInfo::maskHasT ) != 0 ) debug() << " hasT ";
        if ( ( mask & MCTrackInfo::maskAccT ) != 0 ) debug() << " accT ";
        debug() << endmsg;
      }
    }
  }

  return StatusCode::SUCCESS;
}

//=========================================================================
//  Process the MC(Velo)Hits to get the 'acceptance'
//=========================================================================
void BuildMCTrackInfo::computeAcceptance( std::vector<int>& station ) {

  const bool isDebug = msgLevel( MSG::DEBUG );
  if ( m_withVP ) {
    std::vector<int> nVP( station.size(), 0 );
    LHCb::MCHits*    veloHits = get<LHCb::MCHits>( LHCb::MCHitLocation::VP );
    for ( LHCb::MCHits::const_iterator vHit = veloHits->begin(); veloHits->end() != vHit; vHit++ ) {
      unsigned int MCNum = ( *vHit )->mcParticle()->key();
      if ( station.size() <= MCNum ) continue;
      nVP[MCNum]++;
    }
    for ( unsigned int MCNum = 0; station.size() > MCNum; MCNum++ ) {
      if ( 2 < nVP[MCNum] ) station[MCNum] |= MCTrackInfo::maskAccVelo;
    }
  }

  if ( m_withUT ) { //== UT
    DetElementRef<DeUTDetector> utDet  = m_utDet.get();
    LHCb::MCHits*               utHits = get<LHCb::MCHits>( LHCb::MCHitLocation::UT );
    for ( LHCb::MCHits::const_iterator uHit = utHits->begin(); utHits->end() != uHit; uHit++ ) {
      unsigned int MCNum = ( *uHit )->mcParticle()->key();
      if ( station.size() <= MCNum ) continue;

      Gaudi::XYZPoint midPoint = ( *uHit )->midPoint();
#ifdef USE_DD4HEP
      auto utLay = utDet->findLayer( midPoint );
      if ( !utLay.isValid() ) {
#else
      auto utLayP = utDet->findLayer( midPoint );
      if ( !utLayP ) {
#endif
        if ( isDebug )
          debug() << format( "UT Hit not in any LAYER ! x %8.2f y%8.2f z%9.2f", midPoint.x(), midPoint.y(),
                             midPoint.z() )
                  << endmsg;
        continue;
      }
#ifndef USE_DD4HEP
      auto& utLay = *utLayP;
#endif
      int  sta = utLay.elementID().station() - 1;
      bool isX = LHCb::essentiallyZero( utLay.angle() );
      updateAccBit( station[MCNum], sta, isX );
    }
  }

  if ( m_withFT ) { //===Fibre Tracker
    DetElementRef<DeFT> ftDet  = m_ftDet.get();
    LHCb::MCHits*       ftHits = get<LHCb::MCHits>( LHCb::MCHitLocation::FT );
    for ( LHCb::MCHits::const_iterator fHit = ftHits->begin(); ftHits->end() != fHit; fHit++ ) {
      unsigned int MCNum = ( *fHit )->mcParticle()->key();
      if ( station.size() <= MCNum ) continue;
      Gaudi::XYZPoint midPoint = ( *fHit )->midPoint();
      const auto&     ftLay    = ftDet->findLayer( midPoint );
      const auto&     ftSta    = ftDet->findStation( midPoint );
      if ( !ftLay ) {
        if ( isDebug )
          debug() << format( "FT Hit not in any LAYER ! x %8.2f y%8.2f z%9.2f", midPoint.x(), midPoint.y(),
                             midPoint.z() )
                  << endmsg;
        continue;
      }
      int  shiftedStation = ftSta->stationIdx() + shiftT; // station 0-2 + 2, layerID 0-3 xuvx
      bool isX            = ( ftLay->layerID() % 4 == 0 || ftLay->layerID() % 4 == 3 ); // FIXME: hardcoded
      updateAccBit( station[MCNum], shiftedStation, isX );
    }
  }
}
//=============================================================================
