###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import appendPostConfigAction, ERROR
from Boole.Configuration import Boole
from Configurables import GaudiSequencer, OutputStream

boole = Boole()  # call to ensure Boole is setup

# kill xdigi stuff
xdigiPaths = [
    #added by Brunel:
    "/Event/Trigger",
    "/Event/Rich",
    "/Event/Calo",
    "/Event/Muon",
    "/Event/Other",
    #added by Boole
    "/Event/Link/Raw",
    "/Event/DAQ",
    "/Event/pSim/Rich/DigitSummaries",
    "/Event/MC/TrackInfo",
    "/Event/MC/Muon",
    "/Event/MC/DigiHeader",
    #not always there, depends on the DataType and format
    "/Event/Link/Trig",
    "/Event/MC/Rich/DigitSummaries",
    "/Event/Prev/DAQ",
    "/Event/PrevPrev/DAQ",
    "/Event/Next/DAQ",
    "/Event/NextNext/DAQ"
    '/Event/MC/DigiHeader',
    '/Event/MC/Muon',
    '/Event/MC/Muon/DigitsInfo',
    '/Event/Link/Raw/Velo/Clusters2MCHits',
    '/Event/Link/Raw/Velo/Clusters',
    '/Event/Link/Raw/Velo',
    '/Event/Link/Raw/TT/Clusters2MCHits',
    '/Event/Link/Raw/TT/Clusters',
    '/Event/Link/Raw/TT',
    '/Event/Link/Raw/IT/Clusters2MCHits',
    '/Event/Link/Raw/IT/Clusters',
    '/Event/Link/Raw/IT',
    '/Event/Link/Raw/OT/Times2MCHits',
    '/Event/Link/Raw/OT/Times',
    '/Event/Link/Raw/OT',
    '/Event/Link/Raw/Ecal/Digits',
    '/Event/Link/Raw/Ecal',
    '/Event/Link/Raw/Hcal/Digits',
    '/Event/Link/Raw/Hcal',
    '/Event/Link/Raw/Muon/Digits',
    '/Event/Link/Raw/Muon',
    '/Event/Link/Raw',
    '/Event/Trigger/RawEvent',
    '/Event/Calo/RawEvent',
    '/Event/Calo',
    '/Event/Muon/RawEvent',
    '/Event/Muon',
    '/Event/Rich/RawEvent',
    '/Event/Rich',
    '/Event/Other/RawEvent',
    '/Event/Other',
]

extraLoad = [
    '/Event/Link/MC/Rich/Hits2MCRichOpticalPhotons',
    '/Event/Link/MC/Particles2MCRichTracks'
]

from Configurables import (TESCheck, EventNodeKiller)
initBoole = GaudiSequencer("InitBooleSeq")
xdigiLoader = TESCheck("XDIGILoader")
xdigiLoader.Inputs = xdigiPaths + extraLoad
xdigiLoader.Stop = False  # If not MC do not expect all of the entries
xdigiLoader.OutputLevel = ERROR
xdigiKiller = EventNodeKiller("XDIGIKiller")
xdigiKiller.Nodes = xdigiPaths
xdigiHandler = GaudiSequencer("XDIGILoverHandler")
xdigiHandler.Members += [xdigiLoader, xdigiKiller]

xdigiHandler.IgnoreFilterPassed = True  # keep going
initBoole.Members += [xdigiHandler]


def patch():
    OutputStream("DigiWriter").ItemList += ["/Event/Link/MC#1"]


appendPostConfigAction(patch)
