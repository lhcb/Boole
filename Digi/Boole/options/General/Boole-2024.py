###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Default settings for all Boole tests
from Configurables import Boole, DDDBConf
from DDDB.CheckDD4Hep import UseDD4Hep

app = Boole()
app.DataType = "Upgrade"
app.EvtMax = 10
app.Outputs = ["DIGI", "MDF"]  # Test all output types
app.Monitors = ["FPE"]  # Add FPE checks
app.DisableTiming = True  # Do not print timing table

if UseDD4Hep:
    # For DD4hep builds we do not have geometry and condition tags yet
    DDDBConf(
        GeometryVersion='run3/2024.Q1.2-v00.00',
        ConditionsVersion='sim10/run3-ideal',
    )
else:
    app.DDDBtag = 'dddb-20240427'
    app.CondDBtag = 'sim10-2024.Q1.2-v1.1-md100'
