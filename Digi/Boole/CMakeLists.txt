###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Digi/Boole
----------
#]=======================================================================]

gaudi_install(PYTHON)
gaudi_generate_confuserdb()
lhcb_add_confuser_dependencies(
    Rich/RichDigiSys
)

lhcb_env(SET BOOLEOPTS ${CMAKE_CURRENT_SOURCE_DIR}/options)

gaudi_add_tests(QMTest)

# FIXME workaround for https://gitlab.cern.ch/gaudi/Gaudi/-/issues/216
# (both boole-reprocess-xdigi and its dependency validate against references)
set_property(TEST Boole.boole-reprocess-xdigi PROPERTY FIXTURES_REQUIRED)
set_property(TEST Boole.boole-dump-raw PROPERTY FIXTURES_REQUIRED)
set_property(TEST Boole.check-raw-data PROPERTY FIXTURES_REQUIRED)

if(BUILD_TESTING AND USE_DD4HEP)
    # Disable some tests that are not yet dd4hep ready
    set_property(
        TEST
           # these test are using condition Conditions/Calibration/VP/VPDigitisationParam/Thresholds
           Boole.boole-Muon
           Boole.boole-spillover
           Boole.boole-upgrade-baseline
           Boole.boole-run3
           Boole.boole-write-xdigi
           Boole.check-raw-data
        PROPERTY
           DISABLED TRUE
    )
endif()
