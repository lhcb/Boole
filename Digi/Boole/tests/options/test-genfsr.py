###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

from Configurables import GaudiSequencer
seqGenFSR = GaudiSequencer("GenFSRSeq")
seqGenFSR.Members += ["GenFSRMerge"]
seqGenFSR.Members += ["GenFSRLog"]

ApplicationMgr().TopAlg += [seqGenFSR]

from Configurables import LHCbApp, Boole
from Boole.Configuration import *
LHCbApp().DDDBtag = "dddb-20130929-1"
LHCbApp().CondDBtag = "sim-20130522-1-vc-md100"
LHCbApp(DataType="2012", Simulation=True)

from PRConfig import TestFileDB
TestFileDB.test_file_db["genFSR_2012_sim"].run()

# avoid stray output file from previous tests
import os
if os.path.exists('GeneratorLogFSR.xml'):
    os.remove('GeneratorLogFSR.xml')
