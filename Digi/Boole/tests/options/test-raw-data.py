###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Validate raw data source IDs and versions

Parses Boole stdout and finds banks from such messages:

    PrintHeader          INFO Run 16564960, Event 2951
    ...
    RawEventDump         INFO banks of type 63(VP) discovered in DAQ/RawEvent
    RawEventDump         INFO 104 banks of type 63: [size, source, version, magic]
    RawEventDump         INFO   [100, 6144, 4, cbcb] Data follows...

"""
import itertools
import re
import sys
import dataclasses
from collections import Counter


@dataclasses.dataclass(order=True)
class Bank:
    event: int
    bank_type: str
    size: int
    source: int
    version: int
    magic: int


def groupby(data, func):
    data = sorted(data, key=func)
    return [(k, list(g)) for k, g in itertools.groupby(data, func)]


def which_range(x, ranges, default=None):
    for i, (start, stop) in enumerate(ranges):
        if start <= x <= stop:
            return i
    return default


EXPECTED_VERSIONS = {
    "13(Muon)": 3,
    "16(ODIN)": 7,
    "63(VP)": 4,
    "64(FTCluster)": 8,
    "66(UT)": 4,
    "73(VPRetinaCluster)": 4,
    "77(Calo)": 5,
    "9(Rich)": 3,
    "15(DAQ)": 16,
}

# EDMS 2100937
EXPECTED_SOURCEID_RANGES = {
    "63(VP)": [(0x1000, 0x17FF), (0x1800, 0x1FFF)],
    "73(VPRetinaCluster)": [(0x1000, 0x17FF), (0x1800, 0x1FFF)],
    "9(Rich)": [(0x2000, 0x27FF), (0x4800, 0x4FFF)],
    # FIXME don't check UT until the encoding is ready
    # "66(UT)": [(0x2800, 0x2FFF), (0x3000, 0x37FF)],
    "64(FTCluster)": [(0x3800, 0x3FFF), (0x4000, 0x47FF)],
    "77(Calo)": [(0x5800, 0x5FFF), (0x6000, 0x67FF)],
    "13(Muon)": [(0x6800, 0x6FFF), (0x7000, 0x77FF)],
    "16(ODIN)": [(0x0001, 0x000A)],
    "84(Plume)": [(0x5000, 0x57FF)],
}

# Parse data into a list of Bank objects
bank_type = "UNKNOWN"
event = -1
all_banks = []
with open(sys.argv[1]) as f:
    for line in f:
        if m := re.search(r"PrintHeader .* Event ([0-9]+)", line):
            event = int(m.group(1))
        elif m := re.search(r" banks of type ([0-9]+\([A-Za-z]+\)) discovered",
                            line):
            bank_type = m.group(1)
        elif m := re.search(
                r" \[([0-9]+), ([0-9]+), ([0-9]+), ([a-z0-9]+)\] Data follows...",
                line):
            size, source, version, magic = m.groups()
            all_banks.append(
                Bank(event, bank_type, int(size), int(source), int(version),
                     int(magic, 16)))

assert all_banks, "The input is empty. Did the prerequisite test fail?"

# Test magic pattern
assert all(b.magic == 0xcbcb for b in all_banks), "Magic is 0xcbcb"

# Test that all events have the same structure
event_banks = [
    # event number and bank sizes vary from event to event
    list(sorted([dataclasses.replace(b, event=0, size=0) for b in banks]))
    for _, banks in groupby(all_banks, lambda b: b.event)
]
for banks in event_banks[1:]:
    if len(event_banks[0]) != len(banks):
        print("Number of banks differs")
    for a, b in zip(event_banks[0], banks):
        if a != b:
            print("Bank structure differs:", a, b)
    assert event_banks[0] == banks, "Bank structure differs"

# Test that raw data versions are as expected
for bank_type, banks in groupby(all_banks, lambda b: b.bank_type):
    version = EXPECTED_VERSIONS[bank_type]
    assert all(
        b.version == version
        for b in banks), f"Banks {bank_type} should be version {version}"

# Test that source IDs are not duplicated
for event, banks in groupby(all_banks, lambda b: b.event):
    # FIXME exclude UT banks
    banks = [b for b in banks if b.bank_type not in ["66(UT)"]]
    source_ids = Counter([b.source for b in banks])
    for source_id, count in source_ids.items():
        if count > 1:
            print(f"Source ID {source_id} appeared {count} times")
    assert all(
        c == 1 for c in source_ids.values()), ("Source IDs are not duplicated")

# Test that source IDs are in the expected ranges
for (event, bank_type), banks in groupby(all_banks,
                                         lambda b: (b.event, b.bank_type)):
    source_ids = list(sorted(set(b.source for b in banks)))
    assert len(source_ids) == len(banks), (
        f"{bank_type}: Source IDs are not duplicated")

    try:
        ranges = EXPECTED_SOURCEID_RANGES[bank_type]
    except KeyError:
        continue
    n_ranges_found = len(set(which_range(s, ranges) for s in source_ids))
    assert len(ranges) == n_ranges_found, (
        f"{bank_type}: Source IDs are not in expected ranges:" +
        f"expected {ranges} got {source_ids}")
