<?xml version="1.0" ?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
#######################################################
# Purpose: Check that raw data content does not change
#######################################################
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>gaudirun.py</text></argument>
  <argument name="timeout"><integer>1800</integer></argument>
   <argument name="prerequisites"><set>
    <tuple><text>boole-upgrade-baseline</text><enumeral>PASS</enumeral></tuple>
   </set></argument>
   <argument name="options"><text>
from Configurables import LHCbApp, IODataManager
from Configurables import LHCb__UnpackRawEvent, createODIN, PrintHeader
from Configurables import RawEventDump, RawBankSizeMonitor, ApplicationMgr

from GaudiConf import IOExtension
IOExtension().inputFiles(["Boole-10ev.mdf"])
LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"
IODataManager().DisablePFNWarning = True
ApplicationMgr().TopAlg += [
    LHCb__UnpackRawEvent(BankTypes=['ODIN'], RawBankLocations=["DAQ/RawBanks/ODIN"]),
    createODIN(RawBanks="DAQ/RawBanks/ODIN"),
    PrintHeader(),
    RawEventDump("RawEventDump", DumpData=True),
    RawBankSizeMonitor(),
    ]

try:
    os.remove("boole-dump-raw.stdout")
except:
    pass

</text></argument>
  <argument name="reference"><text>../refs/boole-dump-raw.ref</text></argument>
  <argument name="validator"><text>

from Boole.QMTest.BooleExclusions import preprocessor
validateWithReference(preproc = preprocessor)

with open("boole-dump-raw.stdout", "w") as f:
    f.write(stdout)

</text></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
</extension>
