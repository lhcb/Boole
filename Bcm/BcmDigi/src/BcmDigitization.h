/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef BCMDIGI_BCMDIGITIZATION_H
#define BCMDIGI_BCMDIGITIZATION_H 1

#include <string>

// From Gaudi
#include "Event/BcmDigit.h"
#include "Event/MCHit.h"
#include "GaudiAlg/GaudiAlgorithm.h"

class BcmDigitization : public GaudiAlgorithm {
public:
  // Constructor: A constructor of this form must be provided.
  BcmDigitization( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override;

private:
  std::string m_bcmHitsLocation;
  std::string m_bcmDigitLocation;
  std::string m_bcmMCDigitLocation;
  bool        m_bcmMCDigits;
};

#endif
