/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <vector>

#include "BcmDigitization.h"
#include "Event/BcmDigit.h"
#include "Event/MCBcmDigit.h"

DECLARE_COMPONENT( BcmDigitization )

BcmDigitization::BcmDigitization( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {
  declareProperty( "BcmHitsLocation", m_bcmHitsLocation = "MC/Bcm/Hits" );
  declareProperty( "BcmDigitLocation", m_bcmDigitLocation = LHCb::BcmDigitLocation::Default );
  declareProperty( "BcmMCDigitLocation", m_bcmMCDigitLocation = LHCb::MCBcmDigitLocation::Default );
  declareProperty( "MakeMCBcmDigits", m_bcmMCDigits = true );
}

StatusCode BcmDigitization::execute() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "starting the BCM Digitization algorithm " << endmsg;

  // Check existance of hit location
  const LHCb::MCHits* bcmMCHits = getIfExists<LHCb::MCHits>( m_bcmHitsLocation );
  if ( NULL == bcmMCHits )
    return Error( "There are no MCHits at " + m_bcmHitsLocation + " in TES!", StatusCode::SUCCESS );

  // Set up digi sensors
  LHCb::BcmDigits*   sensors   = new LHCb::BcmDigits;
  LHCb::MCBcmDigits* mcsensors = new LHCb::MCBcmDigits;
  for ( int i = 0; i < 2; i++ ) {
    for ( int j = 0; j < 8; j++ ) {
      LHCb::BcmDigit* sens = new LHCb::BcmDigit();
      sens->setStation( i );
      sens->setSensor( j );
      sensors->insert( sens, i * 8 + j );
      LHCb::MCBcmDigit* mcsens = new LHCb::MCBcmDigit();
      mcsensors->insert( mcsens, i * 8 + j );
    }
  }

  // Run over hits
  LHCb::MCHits::const_iterator i;
  int                          index = 0;
  for ( i = bcmMCHits->begin(); i < bcmMCHits->end(); i++ ) {
    const LHCb::MCParticle* origparticle = ( *i )->mcParticle();
    if ( origparticle ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Particle from which the hit originates (PDG code)" << origparticle->particleID().abspid() << endmsg;
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "EnDep " << ( *i )->energy() << " in sensor " << ( *i )->sensDetID() << endmsg;
      ( (LHCb::BcmDigit*)sensors->object( ( *i )->sensDetID() ) )
          ->setSignal( ( (LHCb::BcmDigit*)sensors->containedObject( ( *i )->sensDetID() ) )->signal() +
                       ( *i )->energy() );
      std::vector<int> keys = ( (LHCb::MCBcmDigit*)mcsensors->object( ( *i )->sensDetID() ) )->mcHitKeys();
      keys.push_back( index );
      ( (LHCb::MCBcmDigit*)mcsensors->object( ( *i )->sensDetID() ) )->setMCHitKeys( keys );
    } else {
      warning() << "Particle from which the hit originates is not defined " << endmsg;
    }
    index++;
  }
  put( evtSvc(), sensors, m_bcmDigitLocation );
  if ( m_bcmMCDigits ) {
    put( evtSvc(), mcsensors, m_bcmMCDigitLocation );
  } else
    delete mcsensors;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "End of the BCM Digitization" << endmsg;

  return StatusCode::SUCCESS;
}
