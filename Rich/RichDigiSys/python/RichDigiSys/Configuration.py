###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

## @package RichDigiSys
#  High level Configuration tools for RICH Digitisation
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   24/10/2008

__author__ = "Chris Jones <Christopher.Rob.Jones@cern.ch>"

from Gaudi.Configuration import log
from LHCbKernel.Configuration import *
from Configurables import (
    Rich__MC__Digi__Signal, Rich__MC__Digi__CopySummedDepositsToDigits,
    Rich__MC__Digi__MCRichDigitsToRawBufferAlg, Rich__MC__Digi__SummedDeposits)
from DDDB.CheckDD4Hep import UseDD4Hep

# ----------------------------------------------------------------------------------


## @class RichDigiSysConf
#  Configurable for RICH digitisation
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   24/10/2008
class RichDigiSysConf(LHCbConfigurableUser):

    ## Steering options
    __slots__ = {
        "UseSpillover": False,
        "UseLHCBackground": False,
        "RawDataFormatVersion": -1,
        "Sequencer": None,
        "OutputLevel": INFO,
        "DataType": "Upgrade",  # Upgrade
        "ResponseModel": "DetailedPMT",  # DetailedPMT / Copy
        "SpilloverModel": "PrevNext",  # None / Prev / PrevNext
        "GainMean": 0.9,
        "GainRms": 0.2,
        "Threshold": 0.125,
        "ReadParametersFromDB": True,
        "Sin": True,
        "SinNrOfTimeSlots": 3,
        "SinOccScaleToCurrentNu": True,
        "TimeCalib": [0., 0.],
        "TimeGate": True,
        "TimeGateWindowBegin": (18.00, 48.00),
        "TimeGateLookupTableR1": [],
        "TimeGateLookupTableR2": [],
        "TestRawFormatDecoding": False
    }

    def makeComponent(self, type, name):
        c = type(name)
        if self.isPropertySet("OutputLevel"):
            c.OutputLevel = self.getProp("OutputLevel")
        return c

    ## @brief Apply the configuration to the given GaudiSequencer
    #  @param sequence The GaudiSequencer to add the RICH digitisation to
    def applyConf(self):

        if not self.getProp("DataType") == "Upgrade":
            raise RuntimeError(
                "ERROR : RICH digitisation for Run3 (unknown DataType: '%s')" %
                self.getProp("DataType"))
        elif self.getProp("ResponseModel") == "DetailedPMT":
            if self.getProp("UseSpillover") == False:
                self.SpilloverModel = "None"

        sequence = self.getProp("Sequencer")

        # Process the raw hit signals.
        pdSignals = self.makeComponent(Rich__MC__Digi__Signal, "RichPDSignal")
        pdSignals.UseSpillover = self.getProp("UseSpillover")
        pdSignals.UseLHCBackground = self.getProp("UseLHCBackground")
        pdSignals.TimeCalib = self.getProp("TimeCalib")
        sequence.Members += [pdSignals]

        # deposit summation
        summedDeposits = self.makeComponent(Rich__MC__Digi__SummedDeposits,
                                            "RichSummedDeposits")
        sequence.Members += [summedDeposits]

        # PD response
        pdModel = self.getProp("ResponseModel")
        if pdModel == "Copy":
            response = self.makeComponent(
                Rich__MC__Digi__CopySummedDepositsToDigits, "RichPDResponse")
        elif pdModel == "DetailedPMT" and not UseDD4Hep:
            from Configurables import Rich__MC__Digi__DetailedFrontEndResponsePMT
            response = self.makeComponent(
                Rich__MC__Digi__DetailedFrontEndResponsePMT, "RichPDResponse")
            response.SpilloverModel = self.getProp("SpilloverModel")
            response.GainMean = self.getProp("GainMean")
            response.GainRms = self.getProp("GainRms")
            response.Threshold = self.getProp("Threshold")
            response.ReadParametersFromDB = self.getProp(
                "ReadParametersFromDB")
            response.Sin = self.getProp("Sin")
            response.SinNrOfTimeSlots = self.getProp("SinNrOfTimeSlots")
            response.SinOccScaleToCurrentNu = self.getProp(
                "SinOccScaleToCurrentNu")
            response.TimeGate = self.getProp("TimeGate")
            response.TimeGateWindowBegin = self.getProp("TimeGateWindowBegin")
            response.TimeGateLookupTableR1 = self.getProp(
                "TimeGateLookupTableR1")
            response.TimeGateLookupTableR2 = self.getProp(
                "TimeGateLookupTableR2")
        elif pdModel == "DetailedPMT" and UseDD4Hep:
            log.warning(
                "'DetailedPMT' response model not (yet) supported with DD4HEP. Will fallback to 'Copy'."
            )
            response = self.makeComponent(
                Rich__MC__Digi__CopySummedDepositsToDigits, "RichPDResponse")

        else:
            raise RuntimeError(
                "ERROR : Unknown PD Response model '%s'" % pdModel)
        sequence.Members += [response]

        # Fill the Raw Event
        rawEvtFill = self.makeComponent(
            Rich__MC__Digi__MCRichDigitsToRawBufferAlg, "RichFillRawBuffer")
        dataV = self.getProp("RawDataFormatVersion")
        if dataV > -1:
            rawEvtFill.DataVersion = dataV
        sequence.Members += [rawEvtFill]

        if self.getProp("TestRawFormatDecoding"):
            from Configurables import Rich__Future__RawBankDecoder as RichDecoder
            from Configurables import LHCb__UnpackRawEvent
            unpacker = LHCb__UnpackRawEvent(
                'UnpackRawEvent',
                BankTypes=['Rich'],
                RawEventLocation='/Event/DAQ/RawEvent',
                RawBankLocations=['/Event/DAQ/RawBanks/Rich'])

            testDecode = self.makeComponent(RichDecoder, "RichDecodeTest")
            sequence.Members += [unpacker, testDecode]
