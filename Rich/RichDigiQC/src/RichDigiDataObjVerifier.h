/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//------------------------------------------------------------------------------------
/** @file RichDigiDataObjVerifier.h
 *
 *  Header file for RICH Digitisation Quality Control algorithm : Rich::MC::Digi::DataObjVerifier
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2003-09-08
 */
//------------------------------------------------------------------------------------

#ifndef RICHDIGIQC_RICHDIGIDATOBJVERIFIER_H
#define RICHDIGIQC_RICHDIGIDATOBJVERIFIER_H 1

// base class
#include "RichKernel/RichAlgBase.h"

// Event model
#include "Event/MCRichDigit.h"
#include "Event/MCRichHit.h"
#include "Event/MCRichOpticalPhoton.h"

// Kernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSmartID.h"

namespace Rich {
  namespace MC {
    namespace Digi {

      /** @class DataObjVerifier RichDigiDataObjVerifier.h
       *
       *  Low level printout of Rich Digitisation objects
       *
       *  @author Chris Jones   (Christopher.Rob.Jones@cern.ch)
       *  @date   2003-09-08
       */

      class DataObjVerifier final : public Rich::AlgBase {

      public:
        /// Standard constructor
        DataObjVerifier( const std::string& name, ISvcLocator* pSvcLocator );

        StatusCode execute() override final; ///< Algorithm execution

      private: // methods
        /// Check the MCRichHits at the given location
        void checkHitsAt( const std::string& location ) const;

        /// Check the MCRichOpticalPhotons at the given location
        void checkPhotsAt( const std::string& location ) const;

      private: // data
        // job options
        bool m_bdMcDigits; ///< Flag to turn on/off the checking and printing of MCRichDigits
        bool m_bdMCHits;   ///< Flag to turn on/off the checking and printing of MCRichHits
        bool m_bdMCPhots;  ///< Flag to turn on/off the checking and printing of MCRichOpticalPhotons
      };

    } // namespace Digi
  }   // namespace MC
} // namespace Rich

#endif // RICHDIGIQC_RICHDIGIDATOBJVERIFIER_H
