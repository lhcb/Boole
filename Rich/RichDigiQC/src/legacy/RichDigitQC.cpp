/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/**
 *  Implementation file for RICH Digitisation Quality Control algorithm : RichDigitQC
 *
 *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   2003-09-08
 */

#include "RichKernel/RichHistoAlgBase.h"

#include "Event/MCRichDigit.h"

#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSmartID.h"

#include "RichUtils/RichHashMap.h"
#include "RichUtils/RichMap.h"
#include "RichUtils/RichPoissonEffFunctor.h"
#include "RichUtils/RichStatDivFunctor.h"

#include "RichDet/DeRichSystem.h"

#include "MCInterfaces/IRichMCTruthTool.h"
#include "RichInterfaces/IRichSmartIDTool.h"

#include "boost/format.hpp"
#include "boost/lexical_cast.hpp"

#include <sstream>

namespace Rich::MC::Digi {

  /** @class DigitQC RichDigitQC.h
   *
   *  Monitor for Rich digitisation and DAQ simulation
   *
   *  @author Chris Jones   (Christopher.Rob.Jones@cern.ch)
   *  @date   2003-09-08
   */
  class DigitQC final : public Rich::HistoAlgBase {

  public:
    DigitQC( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override final; // Algorithm initialization
    StatusCode execute() override final;    // Algorithm execution
    StatusCode finalize() override final;   // Algorithm finalization

  private:
    /// Returns the location of container for the MCRichHit associated to the given digit
    std::string mchitLocation( const LHCb::MCRichDigit* digit ) const;

    /// Pointer to RICH system detector element
    const DeRichSystem* m_richSys;

    ToolHandle<Rich::ISmartIDTool>     m_smartIDs{this, "RichSmartIDTool", "Rich::SmartIDTool/RichSmartIDTool"};
    ToolHandle<Rich::MC::IMCTruthTool> m_mcTool{this, "RichMCTruthTool", "Rich::MC::MCTruthTool/RichMCTruthTool"};

    Gaudi::Property<std::string> m_digitTDS{this, "InputDigits", LHCb::MCRichDigitLocation::Default,
                                            "Location of MCRichDigits in TES"};

    /// Number of events processed
    unsigned long long int m_evtC{0};

    /// Counter for hits in each HPD
    typedef Rich::HashMap<const LHCb::RichSmartID, unsigned int> HPDCounter;
    Rich::Map<Rich::DetectorType, HPDCounter> m_nHPDSignal; ///< Tally for HPD occupancy, in each RICH

    typedef Rich::HashMap<std::string, unsigned int> SpillCount;

    typedef Rich::Map<Rich::DetectorType, SpillCount> SpillDetCount;

    /// Number of digitised hits per RICH detector and event location
    SpillDetCount m_spillDigits;

    /// Number of signal digitised hits per RICH detector and event location
    SpillDetCount m_spillDigitsSignal;

    /// Number of raw MC hits hits per RICH detector and event location
    SpillDetCount m_totalSpills;

    /// Counter for hit tallies
    typedef std::map<Rich::DetectorType, unsigned int> HitTally;

    /// Number of digitised hits in each RICH
    HitTally m_allDigits;

    /// Number of rayleigh scattered hits in each RICH
    HitTally m_scattHits;

    /// Number of charged track hits in each RICH
    HitTally m_chrgTkHits;

    /// Number of gas quartz window CK hits in each RICH
    HitTally m_gasQCK;

    /// Number of HPD quartz window CK hits in each RICH
    HitTally m_hpdQCK;

    /// Number of nitrogen CK hits in each RICH
    HitTally m_nitroQCK;

    /// Number of nitrogen CK hits in each RICH
    HitTally m_aeroFiltQCK;

    /// Number of background hits in each RICH
    HitTally m_bkgHits;

    /// Number of charge shared hits in each RICH
    HitTally m_chrgShrHits;

    /// Number of HPD Reflection hits in each RICH
    HitTally m_hpdReflHits;

    /// Number of silicon backscatter hits in each RICH
    HitTally m_siBackScatt;

    /// Number of signal-induced noise hits in each RICH
    HitTally m_signalInducedNoiseHits;

    /// List of event locations to look for MCRichHits in
    typedef Rich::HashMap<std::string, bool> EventLocations;
    EventLocations                           m_evtLocs;
  };

  inline std::string DigitQC::mchitLocation( const LHCb::MCRichDigit* digit ) const {
    // Always just use the first hit, since this will always be signal if the digit has a signal contribution
    // In case of signal-induced noise digit, return proper information ( no associated hits )
    return ( digit->hits().empty() ? ( digit->history().signalInducedNoise() ? "Digit due to SIN" : "UNKNOWN" )
                                   : objectLocation( digit->hits().front().mcRichHit()->parent() ) );
  }

  DECLARE_COMPONENT( DigitQC )

} // namespace Rich::MC::Digi

// Standard constructor, initializes variables
Rich::MC::Digi::DigitQC::DigitQC( const std::string& name, ISvcLocator* pSvcLocator )
    : Rich::HistoAlgBase( name, pSvcLocator ), m_richSys( 0 ), m_evtC( 0 ) {
  setProperty( "HistoDir", "DIGI/DIGITQC" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  setProperty( "HistoOffSet", 10000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
}

// Initialisation
StatusCode Rich::MC::Digi::DigitQC::initialize() {
  return Rich::HistoAlgBase::initialize().andThen( [&] {
    // RichDet
    m_richSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );
    return StatusCode::SUCCESS;
  } );
}

// Main execution
StatusCode Rich::MC::Digi::DigitQC::execute() {
  // Locate MCRichDigits
  LHCb::MCRichDigits* richDigits = get<LHCb::MCRichDigits>( m_digitTDS );

  // temporary tally
  HPDCounter nHPDSignal[Rich::NRiches];

  // Loop over all digits
  Rich::Map<Rich::DetectorType, unsigned int> backs;
  SpillDetCount                               spills;
  for ( LHCb::MCRichDigits::const_iterator iDigit = richDigits->begin(); iDigit != richDigits->end(); ++iDigit ) {
    const LHCb::MCRichDigit* mcDig = *iDigit;
    // Get Rich ID
    const Rich::DetectorType rich = mcDig->key().rich();

    // Location of parent MCHit
    const std::string location = mchitLocation( mcDig );
    m_evtLocs[location]        = true;

    // count all hits
    ++m_allDigits[rich];
    ++( m_spillDigits[rich] )[location];

    // Check if digit is background
    if ( mcDig->history().isBackground() ) {
      //  count background hits
      ++backs[rich];
      ++m_bkgHits[rich];
      // tally up the different background sources
      if ( mcDig->history().scatteredHit() ) { ++m_scattHits[rich]; }
      if ( mcDig->history().chargedTrack() ) { ++m_chrgTkHits[rich]; }
      if ( mcDig->history().gasQuartzCK() ) { ++m_gasQCK[rich]; }
      if ( mcDig->history().hpdQuartzCK() ) { ++m_hpdQCK[rich]; }
      if ( mcDig->history().nitrogenCK() ) { ++m_nitroQCK[rich]; }
      if ( mcDig->history().aeroFilterCK() ) { ++m_aeroFiltQCK[rich]; }
      if ( mcDig->history().chargeShareHit() ) { ++m_chrgShrHits[rich]; }
      if ( mcDig->history().hpdReflection() ) { ++m_hpdReflHits[rich]; }
      if ( mcDig->history().hpdSiBackscatter() ) { ++m_siBackScatt[rich]; }
      if ( mcDig->history().signalInducedNoise() ) { ++m_signalInducedNoiseHits[rich]; }
    } else {
      // Count signal hits
      ++( nHPDSignal[rich] )[mcDig->key().pdID()];
      ++( m_spillDigitsSignal[rich] )[location];
      ++( spills[rich] )[location];
    }

    const auto RICH = Rich::text( rich );

    // plot occupancy per PMT copy number
    const auto pmtCopyNumberGlobal = mcDig->key().pdCol() * 16 + mcDig->key().pdNumInCol();
    const auto pmtCopyNumber       = ( rich == Rich::Rich1 ) ? pmtCopyNumberGlobal : pmtCopyNumberGlobal - 2112;
    const auto xRange              = ( rich == Rich::Rich1 ) ? 2112 : 2304;
    plot1D( pmtCopyNumber, RICH + " : Tot. occupancy per PMT_ID", 0, xRange, xRange );

    // plot channel occupancy (as XY digit map)
    Gaudi::XYZPoint hpdGlo;
    if ( m_smartIDs->globalPosition( mcDig->key().pixelID(), hpdGlo ) ) {
      const auto hpdLoc( m_smartIDs->globalToPDPanel( hpdGlo ) );
      plot2D( hpdLoc.x(), hpdLoc.y(), RICH + " : Tot. channel occupancy XY map", -825, 825, -825, 825, 1650, 1650 );
    }
  }

  // Get total number of hits in each event
  //------------------------------------------------------------------------------
  for ( EventLocations::const_iterator iC = m_evtLocs.begin(); iC != m_evtLocs.end(); ++iC ) {
    if ( exist<LHCb::MCRichHits>( iC->first ) ) {
      LHCb::MCRichHits* hits = get<LHCb::MCRichHits>( iC->first );
      for ( LHCb::MCRichHits::const_iterator iH = hits->begin(); iH != hits->end(); ++iH ) {
        if ( !( *iH )->isBackground() ) ++( m_totalSpills[( *iH )->rich()] )[iC->first];
      }
      if ( msgLevel( MSG::DEBUG ) ) { debug() << "Found " << hits->size() << " MCRichHits at " << iC->first << endmsg; }
    }
  }

  // count events
  ++m_evtC;

  // Various tallies and plots
  //------------------------------------------------------------------------------
  for ( unsigned int iRich = 0; iRich < Rich::NRiches; ++iRich ) {
    const Rich::DetectorType rich = (Rich::DetectorType)iRich;
    const std::string        RICH = Rich::text( rich );

    unsigned int totDet( 0 );
    for ( auto iHPD = nHPDSignal[rich].begin(); iHPD != nHPDSignal[rich].end(); ++iHPD ) {
      plot1D( ( *iHPD ).second, RICH + " : Average PMT occupancy (nHits>0)", 0, 150, 75 );
      ( m_nHPDSignal[rich] )[( *iHPD ).first] += ( *iHPD ).second;
      totDet += ( *iHPD ).second;
    }
    plot1D( totDet, RICH + " : Detector occupancy", 0, 15000, 150 );

    // Event printout
    //------------------------------------------------------------------------------
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << RICH + " : Total # digits = " << totDet << endmsg;
      for ( auto iC = spills[rich].begin(); iC != spills[rich].end(); ++iC ) {
        debug() << "      : " << iC->first << " " << iC->second << endmsg;
      }
      debug() << "      : # background " << backs[rich] << endmsg;
    }
  }

  return StatusCode::SUCCESS;
}

//  Finalize
StatusCode Rich::MC::Digi::DigitQC::finalize() {

  // Statistical calculators
  const Rich::StatDivFunctor    occ( "%8.2f +-%5.2f" );
  const Rich::PoissonEffFunctor eff( "%6.2f +-%5.2f" );

  info() << "===============================================================================================" << endmsg
         << "                            RICH Digitisation Simulation Summary" << endmsg;

  for ( unsigned int iRich = 0; iRich < Rich::NRiches; ++iRich ) {
    const Rich::DetectorType rich = (Rich::DetectorType)iRich;
    const std::string        RICH = Rich::text( rich );
    info() << "-----------------------------------------------------------------------------------------------"
           << endmsg;

    // Form final numbers
    unsigned int totDetSignal( 0 );

    debug() << " " << RICH << " : Individual HPD info :-" << endmsg;
    unsigned int            maxOcc( 0 ), minOcc( 999999 );
    Rich::DAQ::PDHardwareID maxHPD( 0 ), minHPD( 0 );
    for ( auto iHPD = m_nHPDSignal[rich].begin(); iHPD != m_nHPDSignal[rich].end(); ++iHPD ) {
      const auto hID( m_richSys->hardwareID( ( *iHPD ).first ) );
      totDetSignal += ( *iHPD ).second;
      // make sure that the choice of maxHPD and minHPD is independent of the iteration order
      if ( iHPD->second >= maxOcc ) {
        if ( iHPD->second > maxOcc || hID > maxHPD ) { maxHPD = hID; }
        maxOcc = iHPD->second;
      }
      if ( iHPD->second <= minOcc ) {
        if ( iHPD->second < minOcc || hID < minHPD ) { minHPD = hID; }
        minOcc = iHPD->second;
      }
    }

    info() << " " << RICH << " : Av. total  hit occupancy     " << occ( m_allDigits[rich], m_evtC ) << " hits/event"
           << endmsg;
    info() << "       : Av. signal hit occupancy     " << occ( totDetSignal, m_evtC ) << " hits/event" << endmsg;

    {
      for ( SpillCount::iterator iC = m_spillDigits[rich].begin(); iC != m_spillDigits[rich].end(); ++iC ) {
        std::string loc = iC->first;
        loc.resize( 28, ' ' );
        info() << "       :   " << loc << " " << eff( iC->second, m_allDigits[rich] ) << " % of total, "
               << eff( ( m_spillDigitsSignal[rich] )[iC->first], totDetSignal ) << " % signal eff." << endmsg;
      }
    }

    info() << "       : Av. HPD hit occupancy        " << occ( m_allDigits[rich], m_evtC * m_nHPDSignal[rich].size() )
           << " hits/event" << endmsg;
    info() << "       :   Min Av. HPD occ. hID=" << boost::format( "%6i" ) % minHPD.data() << occ( minOcc, m_evtC )
           << " hits/event" << endmsg;
    info() << "       :   Max Av. HPD occ. hID=" << boost::format( "%6i" ) % maxHPD.data() << occ( maxOcc, m_evtC )
           << " hits/event" << endmsg;

    info() << "       : % overall background hits      " << eff( m_bkgHits[rich], m_allDigits[rich] ) << " % " << endmsg
           << "       :   % rayleigh scattered hits    " << eff( m_scattHits[rich], m_allDigits[rich] ) << " % "
           << endmsg << "       :   % charged track on HPD hits  " << eff( m_chrgTkHits[rich], m_allDigits[rich] )
           << " % " << endmsg << "       :   % gas quartz window CK hits  " << eff( m_gasQCK[rich], m_allDigits[rich] )
           << " % " << endmsg << "       :   % hpd quartz window CK hits  " << eff( m_hpdQCK[rich], m_allDigits[rich] )
           << " % " << endmsg << "       :   % nitrogen CK hits           "
           << eff( m_nitroQCK[rich], m_allDigits[rich] ) << " % " << endmsg
           << "       :   % aerogel filter CK hits     " << eff( m_aeroFiltQCK[rich], m_allDigits[rich] ) << " % "
           << endmsg << "       :   % silicon back-scatter hits  " << eff( m_siBackScatt[rich], m_allDigits[rich] )
           << " % " << endmsg << "       :   % internal reflection hits   "
           << eff( m_hpdReflHits[rich], m_allDigits[rich] ) << " % " << endmsg
           << "       :   % silicon charge share hits  " << eff( m_chrgShrHits[rich], m_allDigits[rich] ) << " % "
           << endmsg << "       :   % singal-induced noise hits  "
           << eff( m_signalInducedNoiseHits[rich], m_allDigits[rich] ) << " % " << endmsg;
  }

  info() << "===============================================================================================" << endmsg;

  // finalize base class
  return Rich::HistoAlgBase::finalize();
}
