/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/MCRichDigitHistoryCode.h"
#include "Event/MCRichHit.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SmartRef.h"
#include "Kernel/RichSmartID.h"
#include <ostream>
#include <vector>

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_MCRichDeposit = 12410;

  // Namespace for locations in TDS
  namespace MCRichDepositLocation {
    inline const std::string Default = "MC/Rich/Deposits";
  }

  /** @class MCRichDeposit MCRichDeposit.h
   *
   * Energy deposited within a given pixel from a single MCRichHit
   *
   * @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *
   */

  class MCRichDeposit : public KeyedObject<int> {
  public:
    /// typedef for std::vector of MCRichDeposit
    typedef std::vector<MCRichDeposit*>       Vector;
    typedef std::vector<const MCRichDeposit*> ConstVector;

    /// typedef for KeyedContainer of MCRichDeposit
    typedef KeyedContainer<MCRichDeposit, Containers::HashMap> Container;

    /// Copy constructor. Creates a new MCRichDeposit object with the same information
    MCRichDeposit( const LHCb::MCRichDeposit* dep )
        : m_smartID( dep->smartID() )
        , m_energy( dep->energy() )
        , m_time( dep->time() )
        , m_history( dep->history() )
        , m_parentHit( dep->parentHit() ) {}

    /// Default Constructor
    MCRichDeposit() : m_smartID(), m_energy( 0 ), m_time( 0 ), m_history( 0 ) {}

    /// Default Destructor
    virtual ~MCRichDeposit() {}

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override;
    static const CLID& classID();

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve const  RichSmartID channel identifier
    const LHCb::RichSmartID& smartID() const;

    /// Update  RichSmartID channel identifier
    void setSmartID( const LHCb::RichSmartID& value );

    /// Retrieve const  Deposited energy
    double energy() const;

    /// Update  Deposited energy
    void setEnergy( double value );

    /// Retrieve const  Time of deposit
    double time() const;

    /// Update  Time of deposit
    void setTime( double value );

    /// Retrieve const  Bit-packed history information
    const LHCb::MCRichDigitHistoryCode& history() const;

    /// Update  Bit-packed history information
    void setHistory( const LHCb::MCRichDigitHistoryCode& value );

    /// Retrieve (const)  Parent MCRichHit
    const LHCb::MCRichHit* parentHit() const;

    /// Update  Parent MCRichHit
    void setParentHit( const SmartRef<LHCb::MCRichHit>& value );

    /// Update (pointer)  Parent MCRichHit
    void setParentHit( const LHCb::MCRichHit* value );

    friend std::ostream& operator<<( std::ostream& str, const MCRichDeposit& obj ) { return obj.fillStream( str ); }

  protected:
  private:
    LHCb::RichSmartID            m_smartID;   ///< RichSmartID channel identifier
    double                       m_energy;    ///< Deposited energy
    double                       m_time;      ///< Time of deposit
    LHCb::MCRichDigitHistoryCode m_history;   ///< Bit-packed history information
    SmartRef<LHCb::MCRichHit>    m_parentHit; ///< Parent MCRichHit

  }; // class MCRichDeposit

  /// Definition of Keyed Container for MCRichDeposit
  typedef KeyedContainer<MCRichDeposit, Containers::HashMap> MCRichDeposits;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline const CLID& LHCb::MCRichDeposit::clID() const { return LHCb::MCRichDeposit::classID(); }

inline const CLID& LHCb::MCRichDeposit::classID() { return CLID_MCRichDeposit; }

inline std::ostream& LHCb::MCRichDeposit::fillStream( std::ostream& s ) const {
  s << "{ "
    << "smartID :	" << m_smartID << std::endl
    << "energy :	" << (float)m_energy << std::endl
    << "time :	" << (float)m_time << std::endl
    << "history :	" << m_history << std::endl
    << " }";
  return s;
}

inline const LHCb::RichSmartID& LHCb::MCRichDeposit::smartID() const { return m_smartID; }

inline void LHCb::MCRichDeposit::setSmartID( const LHCb::RichSmartID& value ) { m_smartID = value; }

inline double LHCb::MCRichDeposit::energy() const { return m_energy; }

inline void LHCb::MCRichDeposit::setEnergy( double value ) { m_energy = value; }

inline double LHCb::MCRichDeposit::time() const { return m_time; }

inline void LHCb::MCRichDeposit::setTime( double value ) { m_time = value; }

inline const LHCb::MCRichDigitHistoryCode& LHCb::MCRichDeposit::history() const { return m_history; }

inline void LHCb::MCRichDeposit::setHistory( const LHCb::MCRichDigitHistoryCode& value ) { m_history = value; }

inline const LHCb::MCRichHit* LHCb::MCRichDeposit::parentHit() const { return m_parentHit; }

inline void LHCb::MCRichDeposit::setParentHit( const SmartRef<LHCb::MCRichHit>& value ) { m_parentHit = value; }

inline void LHCb::MCRichDeposit::setParentHit( const LHCb::MCRichHit* value ) { m_parentHit = value; }
