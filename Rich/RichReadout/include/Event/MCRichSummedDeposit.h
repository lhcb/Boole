/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/MCRichDeposit.h"
#include "Event/MCRichDigitHistoryCode.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SmartRefVector.h"
#include "Kernel/RichSmartID.h"
#include <ostream>
#include <vector>

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_MCRichSummedDeposit = 12409;

  // Namespace for locations in TDS
  namespace MCRichSummedDepositLocation {
    inline const std::string Default = "MC/Rich/SummedDeposits";
  }

  /** @class MCRichSummedDeposit MCRichSummedDeposit.h
   *
   * Combined energy of multiple deposits within one pixel
   *
   * @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *
   */

  class MCRichSummedDeposit : public KeyedObject<LHCb::RichSmartID> {
  public:
    /// typedef for std::vector of MCRichSummedDeposit
    typedef std::vector<MCRichSummedDeposit*>       Vector;
    typedef std::vector<const MCRichSummedDeposit*> ConstVector;

    /// typedef for KeyedContainer of MCRichSummedDeposit
    typedef KeyedContainer<MCRichSummedDeposit, Containers::HashMap> Container;

    /// Default Constructor
    MCRichSummedDeposit() : m_summedEnergy( 0 ), m_history( 0 ) {}

    /// Default Destructor
    virtual ~MCRichSummedDeposit() {}

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override;
    static const CLID& classID();

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Add to the summed energy
    void addEnergy( const double energy );

    /// Retrieve const  Combined energy of multiple deposits within a single pixel
    double summedEnergy() const;

    /// Update  Combined energy of multiple deposits within a single pixel
    void setSummedEnergy( double value );

    /// Retrieve const  Bit-packed history information
    const LHCb::MCRichDigitHistoryCode& history() const;

    /// Update  Bit-packed history information
    void setHistory( const LHCb::MCRichDigitHistoryCode& value );

    /// Retrieve (const)  MCRichDesposits contributing to this summed energy deposit
    const SmartRefVector<LHCb::MCRichDeposit>& deposits() const;

    /// Update  MCRichDesposits contributing to this summed energy deposit
    void setDeposits( const SmartRefVector<LHCb::MCRichDeposit>& value );

    /// Add to  MCRichDesposits contributing to this summed energy deposit
    void addToDeposits( const SmartRef<LHCb::MCRichDeposit>& value );

    /// Att to (pointer)  MCRichDesposits contributing to this summed energy deposit
    void addToDeposits( const LHCb::MCRichDeposit* value );

    /// Remove from  MCRichDesposits contributing to this summed energy deposit
    void removeFromDeposits( const SmartRef<LHCb::MCRichDeposit>& value );

    /// Clear  MCRichDesposits contributing to this summed energy deposit
    void clearDeposits();

    friend std::ostream& operator<<( std::ostream& str, const MCRichSummedDeposit& obj ) {
      return obj.fillStream( str );
    }

  protected:
  private:
    double                              m_summedEnergy; ///< Combined energy of multiple deposits within a single pixel
    LHCb::MCRichDigitHistoryCode        m_history;      ///< Bit-packed history information
    SmartRefVector<LHCb::MCRichDeposit> m_deposits;     ///< MCRichDesposits contributing to this summed energy deposit

  }; // class MCRichSummedDeposit

  /// Definition of Keyed Container for MCRichSummedDeposit
  typedef KeyedContainer<MCRichSummedDeposit, Containers::HashMap> MCRichSummedDeposits;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline const CLID& LHCb::MCRichSummedDeposit::clID() const { return LHCb::MCRichSummedDeposit::classID(); }

inline const CLID& LHCb::MCRichSummedDeposit::classID() { return CLID_MCRichSummedDeposit; }

inline std::ostream& LHCb::MCRichSummedDeposit::fillStream( std::ostream& s ) const {
  s << "{ "
    << "summedEnergy :	" << (float)m_summedEnergy << std::endl
    << "history :	" << m_history << std::endl
    << " }";
  return s;
}

inline double LHCb::MCRichSummedDeposit::summedEnergy() const { return m_summedEnergy; }

inline void LHCb::MCRichSummedDeposit::setSummedEnergy( double value ) { m_summedEnergy = value; }

inline const LHCb::MCRichDigitHistoryCode& LHCb::MCRichSummedDeposit::history() const { return m_history; }

inline void LHCb::MCRichSummedDeposit::setHistory( const LHCb::MCRichDigitHistoryCode& value ) { m_history = value; }

inline const SmartRefVector<LHCb::MCRichDeposit>& LHCb::MCRichSummedDeposit::deposits() const { return m_deposits; }

inline void LHCb::MCRichSummedDeposit::setDeposits( const SmartRefVector<LHCb::MCRichDeposit>& value ) {
  m_deposits = value;
}

inline void LHCb::MCRichSummedDeposit::addToDeposits( const SmartRef<LHCb::MCRichDeposit>& value ) {
  m_deposits.push_back( value );
}

inline void LHCb::MCRichSummedDeposit::addToDeposits( const LHCb::MCRichDeposit* value ) {
  m_deposits.push_back( value );
}

inline void LHCb::MCRichSummedDeposit::removeFromDeposits( const SmartRef<LHCb::MCRichDeposit>& value ) {
  auto i = std::remove( m_deposits.begin(), m_deposits.end(), value );
  m_deposits.erase( i, m_deposits.end() );
}

inline void LHCb::MCRichSummedDeposit::clearDeposits() { m_deposits.clear(); }

inline void LHCb::MCRichSummedDeposit::addEnergy( const double energy ) { m_summedEnergy += energy; }
