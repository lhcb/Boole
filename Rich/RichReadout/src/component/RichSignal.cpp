/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @file RichSignal.cpp
 *
 *  Implementation file for RICH digitisation algorithm : RichSignal
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @author Alex Howard   a.s.howard@ic.ac.uk
 *  @date   2003-11-06
 */

#include "RichKernel/RichAlgBase.h"

#include "Event/MCParticle.h"
#include "Event/MCRichDeposit.h"
#include "Event/MCRichHit.h"
#include "Kernel/ParticleID.h"
#include "LHCbAlgs/Transformer.h"

#include "MCInterfaces/IRichMCTruthTool.h"

#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/RndmGenerators.h"

#include <utility>

namespace {

  enum class EventType { Signal, PrevPrev, Prev, Next, NextNext };

} // namespace

namespace Rich::MC::Digi {

  /**
   *  Performs a simulation of the photon energy desposition
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @author Alex Howard   a.s.howard@ic.ac.uk
   *  @date   2003-11-06
   */
  class Signal : public LHCb::Algorithm::Transformer<LHCb::MCRichDeposits( LHCb::MCRichHits const& ),
                                                     Gaudi::Functional::Traits::BaseClass_t<Rich::AlgBase>> {

  public:
    Signal( const std::string& name, ISvcLocator* pSvcLocator );
    StatusCode           initialize() override;
    LHCb::MCRichDeposits operator()( LHCb::MCRichHits const& ) const override;

  private:
    /// Process the event at the given location, with the corresponding TOF offset
    void ProcessEvent( LHCb::MCRichHits const& hits, double tofOffset, EventType eventType,
                       LHCb::MCRichDeposits& mcDeposits ) const;
    void ProcessPotentialEvent( DataObjectReadHandle<LHCb::MCRichHits> const&, Gaudi::Accumulators::SummingCounter<>&,
                                double, EventType, LHCb::MCRichDeposits& ) const;

    // Handles to access MCRichHits other than Signal, as they are not necessary there
    DataObjectReadHandle<LHCb::MCRichHits> m_prevHits{this, "PrevLocation", "Prev/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_prevPrevHits{this, "PrevPrevLocation",
                                                          "PrevPrev/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_nextHits{this, "NextLocation", "Next/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_nextNextHits{this, "NextNextLocation",
                                                          "NextNext/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_lhcBkgHits{this, "LHCBackgroundLocation",
                                                        "LHCBackground/" + LHCb::MCRichHitLocation::Default};

    // Counters for the different potential types of Hits
    mutable std::optional<Gaudi::Accumulators::SummingCounter<>> m_signalHitsCounter;
    mutable std::optional<Gaudi::Accumulators::SummingCounter<>> m_prevPrevHitsCounter;
    mutable std::optional<Gaudi::Accumulators::SummingCounter<>> m_prevHitsCounter;
    mutable std::optional<Gaudi::Accumulators::SummingCounter<>> m_nextHitsCounter;
    mutable std::optional<Gaudi::Accumulators::SummingCounter<>> m_nextNextHitsCounter;
    mutable std::optional<Gaudi::Accumulators::SummingCounter<>> m_lhcBkgHitsCounter;

    ToolHandle<Rich::MC::IMCTruthTool> m_truth{this, "RichMCTruthTool", "Rich::MC::MCTruthTool/RichMCTruthTool"};

    Gaudi::Property<bool> m_doSpillover{this, "UseSpillover", false, "Flag to turn on the use of the spillover events"};
    Gaudi::Property<bool> m_doLHCBkg{this, "UseLHCBackground", false,
                                     "Flag to turn on the use of the LHC backgrounde events"};
    Gaudi::Property<std::vector<double>> m_timeShift{
        this, "TimeCalib", {0., 40.}, "Global time shift for each RICH, to get both to same calibrated point"};
  };

  DECLARE_COMPONENT( Signal )

} // namespace Rich::MC::Digi

using namespace Rich::MC::Digi;

Rich::MC::Digi::Signal::Signal( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, {"HitLocation", LHCb::MCRichHitLocation::Default},
                   {"DepositLocation", LHCb::MCRichDepositLocation::Default} ) {}

StatusCode Rich::MC::Digi::Signal::initialize() {
  return Transformer::initialize().andThen( [&] {
    m_signalHitsCounter.emplace( this, "Found MCRichHits at " + inputLocation<LHCb::MCRichHits>() );
    if ( m_doSpillover.value() ) {
      m_prevPrevHitsCounter.emplace( this, "Found MCRichHits at " + m_prevHits.objKey() );
      m_prevHitsCounter.emplace( this, "Found MCRichHits at " + m_prevPrevHits.objKey() );
      m_nextHitsCounter.emplace( this, "Found MCRichHits at " + m_nextHits.objKey() );
      m_nextNextHitsCounter.emplace( this, "Found MCRichHits at " + m_nextNextHits.objKey() );
    }
    if ( m_doLHCBkg.value() ) { m_lhcBkgHitsCounter.emplace( this, "Found MCRichHits at " + m_lhcBkgHits.objKey() ); }
  } );
}

LHCb::MCRichDeposits Rich::MC::Digi::Signal::operator()( LHCb::MCRichHits const& signalHits ) const {
  // Form new container of MCRichDeposits
  LHCb::MCRichDeposits mcDeposits{};

  // Process main event
  // must be done first (so that first associated hit is signal)
  *m_signalHitsCounter += signalHits.size();
  ProcessEvent( signalHits, 0, EventType::Signal, mcDeposits );

  // if requested, process spillover events
  if ( m_doSpillover.value() ) {
    ProcessPotentialEvent( m_prevPrevHits, *m_prevPrevHitsCounter, -50, EventType::PrevPrev, mcDeposits );
    ProcessPotentialEvent( m_prevHits, *m_prevHitsCounter, -25, EventType::Prev, mcDeposits );
    ProcessPotentialEvent( m_nextHits, *m_nextHitsCounter, 25, EventType::Next, mcDeposits );
    ProcessPotentialEvent( m_nextNextHits, *m_nextNextHitsCounter, 50, EventType::NextNext, mcDeposits );
  }

  // if requested, process LHC background
  if ( m_doLHCBkg.value() ) {
    ProcessPotentialEvent( m_lhcBkgHits, *m_lhcBkgHitsCounter, 0, EventType::Signal, mcDeposits );
  }
  return mcDeposits;
}

void Rich::MC::Digi::Signal::ProcessPotentialEvent( DataObjectReadHandle<LHCb::MCRichHits> const& handle,
                                                    Gaudi::Accumulators::SummingCounter<>& counter, double tofOffset,
                                                    EventType eventType, LHCb::MCRichDeposits& mcDeposits ) const {
  if ( handle.exist() ) {
    auto& hits = *handle.get();
    counter += hits.size();
    ProcessEvent( hits, tofOffset, eventType, mcDeposits );
  }
}

void Rich::MC::Digi::Signal::ProcessEvent( LHCb::MCRichHits const& hits, double tofOffset, EventType eventType,
                                           LHCb::MCRichDeposits& mcDeposits ) const {
  for ( const auto* hit : hits ) {
    // Get RichSmartID from MCRichHit (stripping sub-pixel info for the moment)
    const auto id = hit->sensDetID().pixelID();

    // Create a new deposit
    LHCb::MCRichDeposit* dep = new LHCb::MCRichDeposit();
    mcDeposits.insert( dep );
    dep->setSmartID( id );
    dep->setParentHit( hit );
    dep->setEnergy( hit->energy() );
    dep->setTime( tofOffset + hit->timeOfFlight() - m_timeShift[hit->rich()] );

    // get history from hit
    auto hist = hit->mcRichDigitHistoryCode();
    // add event type to history
    switch ( eventType ) {
    case EventType::Signal:
      hist.setSignalEvent( true );
      break;
    case EventType::Prev:
      hist.setPrevEvent( true );
      break;
    case EventType::PrevPrev:
      hist.setPrevPrevEvent( true );
      break;
    case EventType::Next:
      hist.setNextEvent( true );
      break;
    case EventType::NextNext:
      hist.setNextNextEvent( true );
    }
    dep->setHistory( hist );
  } // hit loop
}
