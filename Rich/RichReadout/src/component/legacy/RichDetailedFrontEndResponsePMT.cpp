/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSmartID.h"
#include "RichKernel/RichHistoAlgBase.h"

#ifdef USE_DD4HEP
#  include "Detector/Rich/DeRich.h"
#else
#  include "RichDet/DeRichPMT.h"
#  include "RichDet/DeRichSystem.h"
#endif

#include "RichInterfaces/IRichSmartIDTool.h"

#include "RichUtils/RichHashMap.h"
#include "RichUtils/RichMap.h"

#include "Event/GenHeader.h"
#include "Event/MCRichDeposit.h"
#include "Event/MCRichDigit.h"
#include "Event/MCRichHit.h"
#include "Event/MCRichSummedDeposit.h"

#include "Kernel/STLExtensions.h"
#include "LHCbAlgs/Transformer.h"

#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/StdArrayAsProperty.h"

#include <cmath>
#include <mutex>
#include <sstream>
#include <stdexcept>

#include <yaml-cpp/yaml.h>

namespace {
  // properties for the time gate (all times in ns)
  constexpr float                    TimeGateWindowRange = 25.0f;
  constexpr float                    TimeGateSlotWidth   = 3.125f;
  constexpr unsigned int             TimeGateNrOfSlots   = 8;
  typedef std::bitset<8>             TimeGateSlots;
  typedef std::vector<TimeGateSlots> TimeGateLookupTable;

  // SIN ratio given in the DB is measured for a certain time window range (SinRatioTimeRange); SIN hits are
  // added with probability based on this measured ratio in a number of required time slots (separately in each
  // time slot); SinNrOfTimeSlots corresponds to the number of time slots before the main time-gate window where
  // the SIN should be simulated (including the main time slot) - TimeGate simulation needs to be activated to
  // enable this (otherwise SIN will be simulated only for the main time slot)
  constexpr float SinRatioTimeRange{25.0f};

  // occupancies used for the SIN simulation are given in the DB for a reference nu (m_sinOccRefNu); they are
  // scaled accordingly (sinOccScaleFactor) to the current (run-time) nu
  constexpr float SinOccRefNu{7.60022f};

  // parameters for the monitoring histograms
  constexpr float HistInputSignalMax       = 5.0;
  constexpr float HistReadoutDelayMax      = 20.0;
  constexpr float HistTimeOverThresholdMax = 35.0;
  constexpr float HistTimeOfAcquisitionMax = 100.0;

  constexpr unsigned int m_nrOfBinsInHistogram1D = 100;
  constexpr unsigned int m_nrOfBinsInHistogram2D = 50;

  struct TimeInfoProperties {
    TimeInfoProperties( SmartIF<IRndmGenSvc> randSvc, float _pdPhotoelectronTimeOfFlight, float _pdTransitTimeMeanTypeR,
                        float _pdTransitTimeSpreadTypeR, float _pdTransitTimeMeanTypeH, float _pdTransitTimeSpreadTypeH,
                        float _timeInfoIndexIntervalInElectrons, unsigned int _readoutTimeInfoIndexMin,
                        unsigned int _readoutTimeInfoIndexMax )
        : pdPhotoelectronTimeOfFlight{_pdPhotoelectronTimeOfFlight}
        , timeInfoIndexIntervalInElectrons{_timeInfoIndexIntervalInElectrons}
        , readoutTimeInfoIndexMin{_readoutTimeInfoIndexMin}
        , readoutTimeInfoIndexMax{_readoutTimeInfoIndexMax} {
      pdTransitTimeTypeR = Rndm::Numbers( randSvc, Rndm::Gauss( _pdTransitTimeMeanTypeR, _pdTransitTimeSpreadTypeR ) );
      pdTransitTimeTypeH = Rndm::Numbers( randSvc, Rndm::Gauss( _pdTransitTimeMeanTypeH, _pdTransitTimeSpreadTypeH ) );
      gaussForGain       = Rndm::Numbers( randSvc, Rndm::Gauss( 0., 1. ) );
      flatForSinProb     = Rndm::Numbers( randSvc, Rndm::Flat( 0., 1. ) );
      flatForSinTime     = Rndm::Numbers( randSvc, Rndm::Flat( 0., SinRatioTimeRange ) );
      if ( !pdTransitTimeTypeR || !pdTransitTimeTypeH || !gaussForGain || !flatForSinProb || !flatForSinTime ) {
        throw std::runtime_error( "Failed to initialise random generators" );
      }
    }
    ~TimeInfoProperties() {
      pdTransitTimeTypeR.finalize().ignore();
      pdTransitTimeTypeH.finalize().ignore();
      gaussForGain.finalize().ignore();
      flatForSinProb.finalize().ignore();
      flatForSinTime.finalize().ignore();
    }
    // time of flight of a photoelectron in a PD (from photocathode to anode) in Gauss [ns]
    float        pdPhotoelectronTimeOfFlight{0.};
    float        timeInfoIndexIntervalInElectrons{0.};
    unsigned int readoutTimeInfoIndexMin{0};
    unsigned int readoutTimeInfoIndexMax{0};

#ifndef USE_DD4HEP
    std::vector<std::shared_ptr<Rich::TabulatedProperty1D>> readoutDelay{};
    std::vector<std::shared_ptr<Rich::TabulatedProperty1D>> readoutTimeOverThreshold{};
#endif
    Rndm::Numbers pdTransitTimeTypeR{};
    Rndm::Numbers pdTransitTimeTypeH{};
    Rndm::Numbers gaussForGain{};
    Rndm::Numbers flatForSinProb{};
    Rndm::Numbers flatForSinTime{};
  };

#ifdef USE_DD4HEP
  using DeRichSystem = LHCb::Detector::DeRich;
#endif

} // namespace

namespace Rich::MC::Digi {

  /**
   *  @author Marcin Kucharczyk, Mariusz Witek
   *  @date   2015-10-08
   */
  class DetailedFrontEndResponsePMT final
      : public LHCb::Algorithm::Transformer<
            LHCb::MCRichDigits( LHCb::MCRichSummedDeposits const&, LHCb::GenHeader const&, DeRichSystem const&,
                                LHCb::RichSmartID::Vector const&, TimeInfoProperties const& ),
            LHCb::DetDesc::usesBaseAndConditions<Rich::HistoAlgBase, DeRichSystem, LHCb::RichSmartID::Vector,
                                                 TimeInfoProperties>> {

  public:
    DetailedFrontEndResponsePMT( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       {{"MCRichSummedDepositsLocation", LHCb::MCRichSummedDepositLocation::Default},
                        {"GenHeaderLocation", LHCb::GenHeaderLocation::Default},
                        {"RichSystemLocation", DeRichLocations::RichSystem},
                        {"ReadoutChannelsLocation", name + "_ReadoutChannelsLocation"},
                        {"TimeInfoPropertiesLocation", name + "_TIPLocation"}},
                       {"MCRichDigitsLocation", LHCb::MCRichDigitLocation::Default} ) {
      setProperty( "HistoDir", "DIGI/PDRESPONSE" ).ignore();
    }

    StatusCode         initialize() override;
    LHCb::MCRichDigits operator()( LHCb::MCRichSummedDeposits const&, LHCb::GenHeader const&, DeRichSystem const&,
                                   LHCb::RichSmartID::Vector const&, TimeInfoProperties const& ) const override;

  private:
    class PMTChannel;
    typedef Rich::Map<LHCb::RichSmartID, PMTChannel> PMTChannelContainer;
    PMTChannelContainer Analog( LHCb::MCRichSummedDeposits const&, LHCb::GenHeader const&, DeRichSystem const&,
                                LHCb::RichSmartID::Vector const&, TimeInfoProperties const& ) const;
    LHCb::MCRichDigits  Digital( DeRichSystem const&, TimeInfoProperties const&, PMTChannelContainer& ) const;

    // get gain based on channel properties (gain mean/rms); require that gain is higher than certain value
    // (optionally)
    float getChannelGain( DeRichSystem const&, TimeInfoProperties const&, LHCb::RichSmartID const&,
                          float minValue = 0. ) const;
    // read data needed for time info simulation from the DB
    TimeInfoProperties readTimeInfoPropertiesFromDB( YAML::Node const&, IRndmGenSvc* ) const;
    // get time from signal arrival at the anode to it's acquisition at the readout output [ns]
    float getReadoutDelay( TimeInfoProperties const&, float, float ) const;
    // get time-over-threshold of the readout output signal after acquisition [ns]
    float getReadoutTimeOverThreshold( TimeInfoProperties const&, float, float ) const;
    // read the single photoelectron energy deposit from the DB ( must be the same number as used in Gauss to create
    // the deposit )
    StatusCode readPhotoelectronEnergyFromDB();

  private:
    ServiceHandle<IRndmGenSvc>     m_randSvc{this, "RndmGenSvc", "RndmGenSvc"};
    ToolHandle<Rich::ISmartIDTool> m_smartIDs{this, "RichSmartIDTool", "Rich::SmartIDTool/RichSmartIDTool"};

    Gaudi::Property<bool>        m_readParametersFromDB{this, "ReadParametersFromDB", false};
    Gaudi::Property<bool>        m_sin{this, "Sin", false};
    Gaudi::Property<std::string> m_spillover{this, "SpilloverModel", "None"};
    Gaudi::Property<float>       m_gainMean{this, "GainMean", 0};
    Gaudi::Property<float>       m_gainRms{this, "GainRms", 0};
    Gaudi::Property<float>       m_threshold{this, "Threshold", 0};

    // time gate is applied to 'raw' deposit times with t0 at the collision time (no offsets)
    // the beginning of the time-gate window and the lookup table are given separately for R1 / R2
    // if the lookup table is not provided, a 25 ns time window is used by default
    Gaudi::Property<bool>                     m_timeGate{this, "TimeGate", false};
    Gaudi::Property<std::array<float, 2>>     m_timeGateWindowBegin{this, "TimeGateWindowBegin", {0, 0}};
    Gaudi::Property<std::vector<std::string>> m_timeGateLookupTableR1{this, "TimeGateLookupTableR1", {}};
    Gaudi::Property<std::vector<std::string>> m_timeGateLookupTableR2{this, "TimeGateLookupTableR2", {}};

    // end of the time-gate window [ns] (based on the begin and range)
    std::array<float, 2> m_timeGateWindowEnd{0., 0.};

    // time-gate lookup tables (parsed to a proper format from a string from the Gaudi property)
    TimeGateLookupTable m_lookupTableR1{};
    TimeGateLookupTable m_lookupTableR2{};

    // SIN ratio given in the DB is measured for a certain time window range (SinRatioTimeRange); SIN hits are
    // added with probability based on this measured ratio in a number of required time slots (separately in each
    // time slot); SinNrOfTimeSlots corresponds to the number of time slots before the main time-gate window where
    // the SIN should be simulated (including the main time slot) - TimeGate simulation needs to be activated to
    // enable this (otherwise SIN will be simulated only for the main time slot)
    Gaudi::Property<unsigned int> m_sinNrOfTimeSlots{this, "SinNrOfTimeSlots", 0};

    // occupancies used for the SIN simulation are given in the DB for a reference nu (m_sinOccRefNu); they are
    // scaled accordingly (sinOccScaleFactor) to the current (run-time) nu
    Gaudi::Property<bool> m_sinOccScaleToCurrentNu{this, "SinOccScaleToCurrentNu", false};
    mutable float         m_current_sinOccScaleFactor{0.}; // only used to display values nly once in the log
    mutable std::mutex    m_sinOccScaleFactorMutex;

    // energy of a single photoelectron deposit from Gauss [MeV]
    float m_photoelectronEnergy{0.};

    // structure for deposits (not the same as RichDeposit, it is what is effectively seen by the Claro - can be a
    // SIN hit, or two deposits close in time which are seen as a single one)
    struct PMTDeposit {
      float inputSignal{0.};
      float timeOfAcquisition{0.};
      float timeOverThreshold{0.};
    };

    // helper class to process all deposits within a single PMT channel (including SIN)
    class PMTChannel {

    private:
      typedef std::vector<PMTDeposit> PMTDepositContainer;
      PMTDepositContainer             m_deposits{};
      TimeGateSlots                   m_outputTimeSampling{};
      bool                            m_hasSin{false};
      LHCb::MCRichSummedDeposit*      m_summedDeposit{nullptr};
      typedef std::vector<bool>       TimeGatePatternMatches;
      TimeGatePatternMatches          m_timeGatePatternMatches{};

    public:
      inline void addDeposit( const float inputSignal,       //
                              const float timeOfAcquisition, //
                              const float timeOverThreshold ) {
        PMTDeposit deposit;
        deposit.inputSignal       = inputSignal;
        deposit.timeOfAcquisition = timeOfAcquisition;
        deposit.timeOverThreshold = timeOverThreshold;
        m_deposits.push_back( deposit );
      }

      // add the given deposit (with a certain time-of-acquisition and time-over-threshold) to the Claro output
      // pattern
      inline void processDeposit( const float timeOfAcquisition, //
                                  const float timeOverThreshold, //
                                  const float timeGateMin,       //
                                  const float timeGateMax,       //
                                  const float timeGateBin ) {

        // check if the deposit has any overlap with the time-sampling window
        if ( isOverlapBetweenSignalAndTimeSampling( timeOfAcquisition, timeOverThreshold, timeGateMin, timeGateMax ) ) {

          const auto firstTimeSlotIndex = getTimeSlotIndex( timeOfAcquisition, timeGateMin, timeGateBin );

          const auto lastTimeSlotIndex =
              getTimeSlotIndex( timeOfAcquisition + timeOverThreshold, timeGateMin, timeGateBin );

          // set time slots status within the time-sampling window; time slots ordering is reversed wrt to the
          // 'bitset' format
          for ( auto i = firstTimeSlotIndex; i < lastTimeSlotIndex + 1; ++i ) {
            if ( i >= 0 && (std::size_t)i < m_outputTimeSampling.size() ) {
              m_outputTimeSampling.set( m_outputTimeSampling.size() - 1 - i );
            }
          }
        }
      }

      // apply a time-gate on the Claro output
      inline bool applyTimeGate( const TimeGateLookupTable& timeGateLookupTable );

      inline const TimeGateSlots&       getOutputTimeSampling() const { return m_outputTimeSampling; }
      inline const PMTDepositContainer& getDeposits() const { return m_deposits; }

      inline LHCb::MCRichSummedDeposit* getSummedDeposit() const { return m_summedDeposit; }
      inline void setSummedDeposit( LHCb::MCRichSummedDeposit* summedDeposit ) { m_summedDeposit = summedDeposit; }

      inline bool hasSin() const noexcept { return m_hasSin; }
      inline void setHasSin() { m_hasSin = true; }

      inline const TimeGatePatternMatches& getTimeGatePatternMatches() const { return m_timeGatePatternMatches; }

    private:
      // check if the given signal has any overlap (in time) with the time-sampling window
      inline bool isOverlapBetweenSignalAndTimeSampling( const float timeOfAcquisition, //
                                                         const float timeOverThreshold, //
                                                         const float timeGateMin,       //
                                                         const float timeGateMax ) const {
        return ( ( timeOfAcquisition <= timeGateMax ) && ( timeOfAcquisition + timeOverThreshold >= timeGateMin ) );
      }

      // get index of the time slot to which the given time corresponds
      inline int getTimeSlotIndex( const float time, const float timeGateMin, const float timeGateBin ) const {
        return std::floor( 0 != timeGateBin ? ( time - timeGateMin ) / timeGateBin : 0 );
      }

      // process a single line of the lookup table
      inline bool processLookupTableLine( const TimeGateSlots& line, const TimeGateSlots& outputTimeSampling ) const {
        return ( outputTimeSampling == line );
      }
    };

    // helper functions
    inline const DeRichPMT* getDePmtFromSmartId( DeRichSystem const& deRich, LHCb::RichSmartID const smartID ) const {
      return (DeRichPMT*)deRich.dePD( smartID );
    }
    inline float getChannelGainMean( DeRichSystem const& deRich, LHCb::RichSmartID const smartID ) const {
      return m_readParametersFromDB.value() ? getDePmtFromSmartId( deRich, smartID )->PmtChannelGainMean( smartID )
                                            : m_gainMean.value();
    }
    inline float getChannelGainRms( DeRichSystem const& deRich, LHCb::RichSmartID const smartID ) const {
      return m_readParametersFromDB.value() ? getDePmtFromSmartId( deRich, smartID )->PmtChannelGainRms( smartID )
                                            : m_gainRms.value();
    }
    inline float getChannelThreshold( DeRichSystem const& deRich, LHCb::RichSmartID const smartID ) const {
      return m_readParametersFromDB.value() ? getDePmtFromSmartId( deRich, smartID )->PmtChannelThreshold( smartID )
                                            : m_threshold.value();
    }
    inline float getChannelSinRatio( DeRichSystem const& deRich, LHCb::RichSmartID const smartID ) const {
      return m_readParametersFromDB.value() ? getDePmtFromSmartId( deRich, smartID )->PmtChannelSinRatio( smartID )
                                            : 0.;
    }
    inline float getPmtAverageOccupancy( DeRichSystem const& deRich, LHCb::RichSmartID const smartID,
                                         float sinOccScaleFactor ) const {
      return m_readParametersFromDB.value()
                 ? ( m_sinOccScaleToCurrentNu
                         ? getDePmtFromSmartId( deRich, smartID )->PmtAverageOccupancy() * sinOccScaleFactor
                         : getDePmtFromSmartId( deRich, smartID )->PmtAverageOccupancy() )
                 : 0.;
    }

    inline void updateHistoryWithSin( LHCb::MCRichDigit*& digit ) const {
      auto digitHistory = digit->history();
      digitHistory.setSignalInducedNoise( true );
      digit->setHistory( digitHistory );
    }

    // provide a valid index for time info input set (take the closest one from the DB)
    inline unsigned int validTimeInfoInputIndex( TimeInfoProperties const& tip, float thresholdInElectrons ) const {
      auto inputIndex = round( thresholdInElectrons / tip.timeInfoIndexIntervalInElectrons );
      if ( inputIndex < tip.readoutTimeInfoIndexMin ) {
        warning() << "No reliable time info for this threshold setting. The closest time info input will be used."
                  << endmsg;
        inputIndex = tip.readoutTimeInfoIndexMin;
      } else if ( inputIndex > tip.readoutTimeInfoIndexMax ) {
        warning() << "No reliable time info for this threshold setting. The closest time info input will be used."
                  << endmsg;
        inputIndex = tip.readoutTimeInfoIndexMax;
      }
      // offset the index ( should start from 0 )
      inputIndex -= tip.readoutTimeInfoIndexMin;
      return inputIndex;
    }

    // provide time of acquisition at the Claro output (taking all offsets into account)
    inline float timeOfAcquisition( TimeInfoProperties const& tip, float timeOfDeposit, float inputSignal,
                                    float threshold, bool isLargePMT ) const {
      const auto pdTransitTime = isLargePMT ? tip.pdTransitTimeTypeH() : tip.pdTransitTimeTypeR();
      return ( timeOfDeposit - tip.pdPhotoelectronTimeOfFlight + pdTransitTime +
               getReadoutDelay( tip, inputSignal, threshold ) );
    }

    // provide time of acquisition at the readout output for SIN hits ( random time within the time window of
    // interest )
    inline float timeOfAcquisitionForSin( TimeInfoProperties const& tip, const float timeWindowMin ) const {
      return timeWindowMin + tip.flatForSinTime();
    }

    // return a proper number if given timeGate property is different for R1/R2
    inline float timeGateParam( LHCb::RichSmartID const smartID, LHCb::span<float const> timeGateParamProperty ) const {
      const auto rich = smartID.rich();
      if ( rich == Rich::Rich1 ) {
        return timeGateParamProperty[0];
      } else if ( rich == Rich::Rich2 ) {
        return timeGateParamProperty[1];
      } else {
        warning() << "Given smartID not in R1/R2." << endmsg;
        return 0.;
      }
    }

    // parse & validate the patterns in a lookup table
    inline bool parseLookupTable( const std::vector<std::string>& lookupTableFrom,
                                  TimeGateLookupTable&            lookupTableTo ) const {
      bool isValid = true;
      for ( auto const& line : lookupTableFrom ) {
        if ( line.length() != TimeGateNrOfSlots ) {
          isValid = false;
          break;
        } else {
          lookupTableTo.emplace_back( line );
        }
      }
      return isValid;
    }
  };

  DECLARE_COMPONENT( DetailedFrontEndResponsePMT )

} // namespace Rich::MC::Digi

//-----------------------------------------------------------------------------
// Implementation file for class : RichDetailedFrontEndResponsePMT
// 2015-10-08 : Marcin Kucharczyk, Mariusz Witek
// 2017-07-22: Bartosz Malecki
//-----------------------------------------------------------------------------

StatusCode Rich::MC::Digi::DetailedFrontEndResponsePMT::initialize() {
  return Transformer::initialize().andThen( [&]() -> StatusCode {
    addConditionDerivation( {std::string{m_smartIDs->getPDPanelsPath()}}, inputLocation<LHCb::RichSmartID::Vector>(),
                            [&]( RichPDPanels const& panels ) { // unused but smartIDs depends on them
                              // Create a collection of all valid readout channels (pixels)
                              return m_smartIDs->readoutChannelList( panels );
                            } );

    addConditionDerivation(
        {DeRichLocations::ReadoutTimeInfoCondPath}, inputLocation<TimeInfoProperties>(),
        [&]( YAML::Node const& cond ) { return this->readTimeInfoPropertiesFromDB( cond, &*m_randSvc ); } );

    // load single photoelectron deposit energy from DB
    readPhotoelectronEnergyFromDB().orThrow( "Failed to read photoelectron deposit energy from the DB.",
                                             "DetailedFrontEndResponsePMT::initialize" );

    // validate input for time gate
    if ( m_timeGate.value() ) {
      // set the end of the time-gate window (based on the beginning and range)
      m_timeGateWindowEnd[0] = m_timeGateWindowBegin[0] + TimeGateWindowRange;
      m_timeGateWindowEnd[1] = m_timeGateWindowBegin[1] + TimeGateWindowRange;
      // parse and validate the lookup tables
      if ( !parseLookupTable( m_timeGateLookupTableR1.value(), m_lookupTableR1 ) ||
           !parseLookupTable( m_timeGateLookupTableR2.value(), m_lookupTableR2 ) ) {
        throw GaudiException(
            "DetailedFrontEndResponsePMT::initialize",
            fmt::format(
                "Time-gate configuration is not right (the number of characters in each lookup table pattern should "
                "match the number of slots: {})",
                TimeGateNrOfSlots ),
            StatusCode::FAILURE );
      }
    }

    if ( m_sin.value() ) {
      // check channel properties from DB are available if we simulate SIN
      if ( !m_readParametersFromDB.value() ) {
        throw GaudiException(
            "DetailedFrontEndResponsePMT::initialize",
            "Can't simulate SIN without access to the channel properties in the DB. Unsetting this option.",
            StatusCode::FAILURE );
      }
      // sanitise SIN time simulation settings
      if ( !m_timeGate.value() ) {
        m_sinNrOfTimeSlots.setValue( 1 );
        _ri_debug << "Time-gate simulation is not enabled. SIN will be simulated only in the main time slot." << endmsg;
      }
    }
    return StatusCode::SUCCESS;
  } );
}

LHCb::MCRichDigits                                                                                              //
Rich::MC::Digi::DetailedFrontEndResponsePMT::operator()( LHCb::MCRichSummedDeposits const& deposits,            //
                                                         LHCb::GenHeader const&            genHeader,           //
                                                         DeRichSystem const&               deRich,              //
                                                         LHCb::RichSmartID::Vector const&  readoutChannelsList, //
                                                         TimeInfoProperties const&         tip ) const {
  // Analog simulation
  PMTChannelContainer channels = Analog( deposits, genHeader, deRich, readoutChannelsList, tip );
  // Digital simulation
  return Digital( deRich, tip, channels );
}

Rich::MC::Digi::DetailedFrontEndResponsePMT::PMTChannelContainer                                            //
Rich::MC::Digi::DetailedFrontEndResponsePMT::Analog( LHCb::MCRichSummedDeposits const& deposits,            //
                                                     LHCb::GenHeader const&            genHeader,           //
                                                     DeRichSystem const&               deRich,              //
                                                     LHCb::RichSmartID::Vector const&  readoutChannelsList, //
                                                     TimeInfoProperties const&         tip ) const {
  PMTChannelContainer channels;
  // estimate factor of occupancy increase wrt to the one provided for a reference nu (to be tried once)
  float sinOccScaleFactor{0.};
  if ( m_sinOccScaleToCurrentNu ) {
    sinOccScaleFactor = static_cast<float>( genHeader.beamParameters()->nu() ) / SinOccRefNu;
    if ( m_current_sinOccScaleFactor != sinOccScaleFactor ) {
      std::scoped_lock l( m_sinOccScaleFactorMutex );
      if ( m_current_sinOccScaleFactor != sinOccScaleFactor ) {
        m_current_sinOccScaleFactor = sinOccScaleFactor;
        info() << "Occupancy for the SIN simulation will be scaled by a factor of " << std::fixed
               << std::setprecision( 4 ) << sinOccScaleFactor
               << " wrt the reference one provided for nu = " << std::fixed << std::setprecision( 5 ) << SinOccRefNu
               << "." << endmsg;
      }
    }
  }
  // ANALOG RESPONSE
  for ( auto const& summedDeposit : deposits ) {

    // for backward-compatibility with the spillover hits, don't process purely spillover summed deposits if the time
    // processing is not applied (it would add many irrelevant deposits) it correspond to the early spillover studies,
    // which assumed discarding hits from the main event if there was a hit in the same channel in the prev event (see
    // m_spillover) to be removed when the time-gate is optimised for the spillover studies with the full time procesing
    if ( !m_timeGate.value() ) {
      if ( !summedDeposit->history().signalEvent() ) continue;
    }

    // prepare a PMTChannel to process all deposits in the summed deposit
    PMTChannel channel{};
    channel.setSummedDeposit( summedDeposit );

    // values common for the channel
    const auto channelSmartID   = channel.getSummedDeposit()->key().pixelID();
    const auto channelThreshold = getChannelThreshold( deRich, channelSmartID );

    // process each deposit from the summed deposit
    for ( auto const& deposit : channel.getSummedDeposit()->deposits() ) {

      // equivalence of the given deposit energy in the number of initial photoelectrons (no gain simulation in Gauss)
      const int nrOfPhotoelectrons = (int)( deposit->energy() / m_photoelectronEnergy );
      // Claro input signal (number of electrons from the PMT [Me-])
      // in principle the gain value can be different for each photoelectron (but leave this 'averaged' approach, unless
      // a more detailed description is available - those gain values will not be independent of each other)
      const float inputSignal = nrOfPhotoelectrons * getChannelGain( deRich, tip, channelSmartID );

      // save only the deposits that will be visible at the Claro output (are above threshold)
      if ( inputSignal < channelThreshold ) continue;

      // time properties of the deposit
      const auto depositTimeOfAcquisition =
          m_timeGate.value()
              ? timeOfAcquisition( tip, deposit->time(), inputSignal, channelThreshold, channelSmartID.isLargePMT() )
              : 0.;
      const auto depositTimeOverThreshold =
          m_timeGate.value() ? getReadoutTimeOverThreshold( tip, inputSignal, channelThreshold ) : 0.;

      channel.addDeposit( inputSignal, depositTimeOfAcquisition, depositTimeOverThreshold );
    }

    channels.insert( std::pair<LHCb::RichSmartID, PMTChannel>( channelSmartID, channel ) );
  }

  // add SIN deposits
  if ( m_sin.value() ) {

    for ( auto const& readoutChannel : readoutChannelsList ) {

      // get SIN probability for the given channel
      const float sinProbInChannel = ( 1 - getPmtAverageOccupancy( deRich, readoutChannel, sinOccScaleFactor ) ) *
                                     ( 1 - exp( -getChannelSinRatio( deRich, readoutChannel ) *
                                                getPmtAverageOccupancy( deRich, readoutChannel, sinOccScaleFactor ) ) );

      for ( unsigned int i = 0; i < m_sinNrOfTimeSlots.value(); ++i ) {

        // add SIN with the givenl probability (all values are expected to be within 0-1 range)
        if ( tip.flatForSinProb() < sinProbInChannel ) {

          // get properties of the SIN deposit
          const auto channelThreshold = getChannelThreshold( deRich, readoutChannel );

          // make sure that the inputSignal will pass the threshold (assume that we add SIN with some probability, which
          // should not be reduced by the threshold)
          const auto inputSignal = getChannelGain( deRich, tip, readoutChannel, channelThreshold );
          // if ( inputSignal < channelThreshold ) continue; // remember to apply threshold if the SIN inputSignal will
          // not be anymore a random number above threshold value from the definition

          // time properties of the SIN deposit
          // TOA is a random number within time range of the given time slot
          const auto timeOfAcquisition =
              m_timeGate.value()
                  ? timeOfAcquisitionForSin( tip, timeGateParam( readoutChannel, m_timeGateWindowBegin.value() ) -
                                                      i * SinRatioTimeRange )
                  : 0.;
          const auto timeOverThreshold =
              m_timeGate.value() ? getReadoutTimeOverThreshold( tip, inputSignal, channelThreshold ) : 0.;

          // add the SIN deposit to the existing channel or create a new one (and get an iterator to it)
          const auto channelIteratorAndIfIsNew = channels.emplace( readoutChannel, PMTChannel() );
          channelIteratorAndIfIsNew.first->second.addDeposit( inputSignal, timeOfAcquisition, timeOverThreshold );
          channelIteratorAndIfIsNew.first->second.setHasSin();
        }
      }
    }
  }
  return channels;
}

LHCb::MCRichDigits Rich::MC::Digi::DetailedFrontEndResponsePMT::Digital( DeRichSystem const&       deRich,
                                                                         TimeInfoProperties const& tip,
                                                                         PMTChannelContainer&      channels ) const {
  LHCb::MCRichDigits mcRichDigits{};

  // Loop over all channels
  for ( auto& curChannel : channels ) {

    // process only channels with any deposit visible at the Claro output
    if ( curChannel.second.getDeposits().empty() ) continue;

    // simple spillover implementation (do not save a digit if there is any signal in this channel from prev/next event)
    if ( !m_spillover.value().compare( "PrevNext" ) ) {
      // check if summedDeposit exists (can be not the case for purely SIN channels)
      if ( curChannel.second.getSummedDeposit() != nullptr ) {
        if ( curChannel.second.getSummedDeposit()->history().prevEvent() ) continue;
        if ( curChannel.second.getSummedDeposit()->history().nextEvent() ) continue;
      }
    } else if ( !m_spillover.value().compare( "Prev" ) ) {
      // check if summedDeposit exists (can be not the case for purely SIN channels)
      if ( curChannel.second.getSummedDeposit() != nullptr ) {
        if ( curChannel.second.getSummedDeposit()->history().prevEvent() ) continue;
      }
    }

    // time processing
    if ( m_timeGate.value() ) {

      // values common for the channel
      const auto threshold          = getChannelThreshold( deRich, curChannel.first );
      const auto curTimeWindowBegin = timeGateParam( curChannel.first, m_timeGateWindowBegin.value() );
      const auto curTimeWindowEnd   = timeGateParam( curChannel.first, m_timeGateWindowEnd );
      const auto rich               = curChannel.first.rich();

      // check to make sure for the following choices on R1 / R2 lookup table / histograms (is this necessary?)
      if ( !( rich == Rich::Rich1 || rich == Rich::Rich2 ) ) {
        throw GaudiException( "DetailedFrontEndResponsePMT::Digital", "Unrecognized RICH detector",
                              StatusCode::FAILURE );
      }

      const auto& curLookupTable = ( rich == Rich::Rich1 ? m_lookupTableR1 : m_lookupTableR2 );

      // check all deposits in the given channel
      for ( auto const& deposit : curChannel.second.getDeposits() ) {

        if ( produceHistos() ) {
          const auto richAsText = Rich::text( rich );
          plot1D( deposit.inputSignal, richAsText + " : Input signal", richAsText + " : Claro input signal [Me-]", 0.,
                  HistInputSignalMax, m_nrOfBinsInHistogram1D );
          plot1D( deposit.timeOfAcquisition, richAsText + " : Time of acquisition",
                  richAsText + " : Deposit time of acquisition [ns]", 0., HistTimeOfAcquisitionMax,
                  m_nrOfBinsInHistogram1D );
          plot1D( deposit.timeOverThreshold, richAsText + " : Time over threshold",
                  richAsText + " : Deposit time over threshold [ns]", 0., HistTimeOverThresholdMax,
                  m_nrOfBinsInHistogram1D );
          plot2D( deposit.inputSignal, getReadoutDelay( tip, deposit.inputSignal, threshold ),
                  richAsText + " : Delay vs Input signal", richAsText + " : Readout delay [ns] vs Input signal [Me-]",
                  0., HistInputSignalMax, 0., HistReadoutDelayMax, m_nrOfBinsInHistogram2D, m_nrOfBinsInHistogram2D );
          plot2D( deposit.inputSignal, deposit.timeOverThreshold, richAsText + " : TOT vs Input signal",
                  richAsText + " : TOT [ns] vs Input signal [Me-]", 0., HistInputSignalMax, 0.,
                  HistTimeOverThresholdMax, m_nrOfBinsInHistogram2D, m_nrOfBinsInHistogram2D );
        }

        // add the contribution from the given deposit to the Claro output pattern
        curChannel.second.processDeposit( deposit.timeOfAcquisition, deposit.timeOverThreshold, curTimeWindowBegin,
                                          curTimeWindowEnd, TimeGateSlotWidth );
        _ri_debug << "Readout output pattern: " << curChannel.second.getOutputTimeSampling() << endmsg;
      }

      // apply a time gate on the output time-sampled signal
      const auto passesTimeGate = curChannel.second.applyTimeGate( curLookupTable );

      // print the time gate matching patterns for debugging
      if ( msgLevel( MSG::DEBUG ) ) {
        const auto timeGatePatternMatches = curChannel.second.getTimeGatePatternMatches();
        if ( timeGatePatternMatches.size() != curLookupTable.size() ) {
          debug() << "Lookup table size and number of results for compared patterns does not match. "
                  << "It will not be printed." << endmsg;
        } else {
          debug() << "Matched lookup table patterns:" << endmsg;
          for ( unsigned int i = 0; i < curLookupTable.size(); ++i ) {
            debug() << curLookupTable[i] << "\t" << timeGatePatternMatches[i] << endmsg;
          }
        }
      }

      if ( !passesTimeGate ) continue;
    }

    // Initial guess at implementation of adding time information to the hits
    // Get 'average' time for all deposits
    const auto& deps = curChannel.second.getDeposits();
    float       avDepTime{0};
    for ( auto const& deposit : deps ) { avDepTime += deposit.timeOfAcquisition; }
    if ( deps.size() > 0 ) { avDepTime /= deps.size(); }

    // Construct RichSmartID with time info
    LHCb::RichSmartID keyID( curChannel.first );
    keyID.setTime( avDepTime );

    // create the MCRichDigit
    auto newDigit = new LHCb::MCRichDigit();
    mcRichDigits.insert( newDigit, keyID );

    // check if summedDeposit exists (can be not the case for purely SIN channels)
    if ( curChannel.second.getSummedDeposit() != nullptr ) {
      const auto&                  deps = curChannel.second.getSummedDeposit()->deposits();
      LHCb::MCRichDigitHit::Vector hitVect;
      hitVect.reserve( deps.size() );
      for ( auto const& dep : deps ) { hitVect.emplace_back( *( dep->parentHit() ), dep->history() ); }
      newDigit->setHits( hitVect );
      newDigit->setHistory( curChannel.second.getSummedDeposit()->history() );
    }

    // add SIN history if necessary
    if ( curChannel.second.hasSin() ) { updateHistoryWithSin( newDigit ); }

    _ri_debug << newDigit->history() << " MCRichDigit" << endmsg;
  }

  return mcRichDigits;
}

//=============================================================================
// Return a valid gain for pixel
//=============================================================================
float Rich::MC::Digi::DetailedFrontEndResponsePMT::getChannelGain( DeRichSystem const&       deRich,
                                                                   TimeInfoProperties const& tip,
                                                                   LHCb::RichSmartID const&  smartID,
                                                                   float                     minValue ) const {

  const auto channelGainMean = getChannelGainMean( deRich, smartID );
  const auto channelGainRms  = getChannelGainRms( deRich, smartID );

  // try to get valid value (maxNrTries times)
  float     tmpGain;
  const int maxNrTries = 100;
  for ( int i = 0; i < maxNrTries; ++i ) {

    tmpGain = tip.gaussForGain() * channelGainRms + channelGainMean;

    if ( tmpGain > minValue )
      break;
    else {
      if ( i == 99 ) {
        tmpGain = 0.;
        warning() << "Check gain mean/sigma - no positive number in " << maxNrTries << " tries." << endmsg;
      }
    }
  }
  return tmpGain;
}

//=============================================================================
// Readout time info
//=============================================================================
TimeInfoProperties
Rich::MC::Digi::DetailedFrontEndResponsePMT::readTimeInfoPropertiesFromDB( YAML::Node const& cond,
                                                                           IRndmGenSvc*      randSvc ) const {
  TimeInfoProperties tip{randSvc,
                         cond["PDPhotoelectronTimeOfFlight"].as<float>(),
                         cond["PDTransitTimeMeanTypeR"].as<float>(),
                         cond["PDTransitTimeSpreadTypeR"].as<float>(),
                         cond["PDTransitTimeMeanTypeH"].as<float>(),
                         cond["PDTransitTimeSpreadTypeH"].as<float>(),
                         cond["TimeInfoIndexIntervalInElectrons"].as<float>(),
                         cond["ReadoutTimeInfo_IndexMin"].as<unsigned int>(),
                         cond["ReadoutTimeInfo_IndexMax"].as<unsigned int>()};

#ifndef USE_DD4HEP
  // FIXME tabulated properties have to be replaced by plain json arrays in DD4hep and this code adapted
  const auto readoutDelayPropBaseName = cond["ReadoutDelayCommonLoc"].as<std::string>();
  const auto readoutToTPropBaseName   = cond["ReadoutTimeOverThresholdCommonLoc"].as<std::string>();
  for ( unsigned int i = tip.readoutTimeInfoIndexMin; i < tip.readoutTimeInfoIndexMax + 1; ++i ) {
    SmartDataPtr<TabulatedProperty> readoutDelay( detSvc(), fmt::format( "{}{}", readoutDelayPropBaseName, i ) );
    SmartDataPtr<TabulatedProperty> readoutToT( detSvc(), fmt::format( "{}{}", readoutToTPropBaseName, i ) );
    tip.readoutDelay.push_back(
        std::make_shared<Rich::TabulatedProperty1D>( readoutDelay, false, gsl_interp_cspline ) );
    tip.readoutTimeOverThreshold.push_back(
        std::make_shared<Rich::TabulatedProperty1D>( readoutToT, false, gsl_interp_cspline ) );
  }
  // check validity of the interpolators
  for ( auto const& el : tip.readoutDelay ) {
    if ( !el->valid() ) {
      throw GaudiException(
          "DetailedFrontEndResponsePMT::readTimeInfoPropertiesFromDB",
          "Incorrect time info format. Check properties with readout delay/time-over-threshold in the DB.",
          StatusCode::FAILURE );
    }
  }
  for ( auto const& el : tip.readoutTimeOverThreshold ) {
    if ( !el->valid() ) {
      throw GaudiException(
          "DetailedFrontEndResponsePMT::readTimeInfoPropertiesFromDB",
          "Incorrect time info format. Check properties with readout delay/time-over-threshold in the DB.",
          StatusCode::FAILURE );
    }
  }
#endif
  return tip;
}

float Rich::MC::Digi::DetailedFrontEndResponsePMT::getReadoutDelay( TimeInfoProperties const& tip, float inputSignal,
                                                                    float threshold ) const {

  if ( tip.readoutDelay.empty() ) {
    return 0.;
  } else {
    const auto readoutTimeInfoIndex = validTimeInfoInputIndex( tip, threshold );
    if ( msgLevel( MSG::DEBUG ) ) {
      if ( !( tip.readoutDelay[readoutTimeInfoIndex]->withinInputRange( inputSignal ) ) )
        debug() << "No reliable time info for this input signal. Returned value will be from an extrapolation."
                << endmsg;
    }
    return tip.readoutDelay[readoutTimeInfoIndex]->value( inputSignal );
  }
}

float Rich::MC::Digi::DetailedFrontEndResponsePMT::getReadoutTimeOverThreshold( TimeInfoProperties const& tip,
                                                                                float                     inputSignal,
                                                                                float threshold ) const {

  if ( tip.readoutTimeOverThreshold.empty() ) {
    return 0.;
  } else {
    const auto readoutTimeInfoIndex = validTimeInfoInputIndex( tip, threshold );
    if ( msgLevel( MSG::DEBUG ) ) {
      if ( !( tip.readoutDelay[readoutTimeInfoIndex]->withinInputRange( inputSignal ) ) )
        debug() << "No reliable time info for this input signal. Returned value will be from an extrapolation."
                << endmsg;
    }
    return tip.readoutTimeOverThreshold[readoutTimeInfoIndex]->value( inputSignal );
  }
}

// apply a time-gate on the time-sampled readout output
inline bool Rich::MC::Digi::DetailedFrontEndResponsePMT::PMTChannel::applyTimeGate(
    const TimeGateLookupTable& timeGateLookupTable ) {

  auto passesTimeGate = false;

  const auto lookupTableSize = timeGateLookupTable.size();

  // ensure a clear vector of match results to the lookup patterns
  if ( !m_timeGatePatternMatches.empty() ) m_timeGatePatternMatches.clear();

  // use a standard 25 ns time gate by default
  if ( 0 == lookupTableSize ) {
    // check if any bit in the output time sampling is non-zero
    if ( m_outputTimeSampling.any() ) { passesTimeGate = true; }
  } else {
    // check if the output time-sampling pattern matches any line in the lookup table
    for ( auto const& line : timeGateLookupTable ) {

      const auto passedLine = processLookupTableLine( line, m_outputTimeSampling );
      m_timeGatePatternMatches.push_back( passedLine );

      if ( !passesTimeGate && passedLine ) { passesTimeGate = true; }
    }
  }
  return passesTimeGate;
}

//=============================================================================
// Global RICH properties
//=============================================================================
StatusCode Rich::MC::Digi::DetailedFrontEndResponsePMT::readPhotoelectronEnergyFromDB() {
#ifndef USE_DD4HEP
  SmartDataPtr<TabulatedProperty> photoelectronEnergyTab( detSvc(), DeRichLocations::PMTHighVoltageTabPropLoc );
  if ( !photoelectronEnergyTab ) {
    return Error( "No info on single photoelectron deposit energy found in the DB in the following location: " +
                  DeRichLocations::PMTHighVoltageTabPropLoc );
  } else {
    m_photoelectronEnergy = photoelectronEnergyTab->table().begin()->second;
  }
#endif
  if ( !( m_photoelectronEnergy > 0. ) ) {
    error() << "Invalid value of single photoelectron deposit energy ( must be a positive number ): "
            << m_photoelectronEnergy << endmsg;
    return StatusCode::FAILURE;
  } else {
    _ri_debug << "Initialized single photoelectron deposit energy: " << m_photoelectronEnergy << endmsg;
    return StatusCode::SUCCESS;
  }
}
