/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//===============================================================================
/** @file RichCopySummedDepositsToDigits.cpp
 *
 *  Implementation file for RICH digitisation algorithm : RichCopySummedDepositsToDigits
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   18/11/2011
 */
//===============================================================================

#include "RichCopySummedDepositsToDigits.h"

using namespace Rich::MC::Digi;

DECLARE_COMPONENT( CopySummedDepositsToDigits )

// Standard constructor, initializes variables
CopySummedDepositsToDigits::CopySummedDepositsToDigits( const std::string& name, ISvcLocator* pSvcLocator )
    : Rich::AlgBase( name, pSvcLocator ) {
  // job opts
  declareProperty( "MCRichSummedDepositsLocation",
                   m_mcRichSummedDepositsLocation = LHCb::MCRichSummedDepositLocation::Default );
  declareProperty( "MCRichDigitsLocation", m_mcRichDigitsLocation = LHCb::MCRichDigitLocation::Default );
}

StatusCode CopySummedDepositsToDigits::execute() {
  const auto* SummedDeposits = get<LHCb::MCRichSummedDeposits>( m_mcRichSummedDepositsLocation );

  _ri_debug << "Successfully located " << SummedDeposits->size() << " MCRichSummedDeposits at "
            << m_mcRichSummedDepositsLocation << endmsg;

  // make new mcrichdigits
  auto* mcRichDigits = new LHCb::MCRichDigits();
  put( mcRichDigits, m_mcRichDigitsLocation );

  for ( auto* SumDep : *SummedDeposits ) {

    // Make new MCRichDigit
    auto* newDigit = new LHCb::MCRichDigit();

    double                       summedEnergy = 0.0;
    float                        avDepTime    = 0.0;
    const auto&                  deps         = SumDep->deposits();
    LHCb::MCRichDigitHit::Vector hitVect;
    hitVect.reserve( deps.size() );
    for ( const auto& dep : deps ) {
      avDepTime += dep->time();
      summedEnergy += dep->energy();
      hitVect.emplace_back( LHCb::MCRichDigitHit( *( dep->parentHit() ), dep->history() ) );
    }
    newDigit->setHits( hitVect );

    if ( deps.size() > 0 ) { avDepTime /= deps.size(); }

    SumDep->setSummedEnergy( summedEnergy );

    // Store history info
    newDigit->setHistory( SumDep->history() );

    // Construct ID with time
    LHCb::RichSmartID keyID( SumDep->key() );
    keyID.setTime( avDepTime );

    // save the digit
    mcRichDigits->insert( newDigit, keyID );
  }

  _ri_debug << "Created " << mcRichDigits->size() << " MCRichDigits at " << m_mcRichDigitsLocation << endmsg;

  return StatusCode::SUCCESS;
}
