###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rich/RichReadout
----------------
#]=======================================================================]

gaudi_add_header_only_library(RichReadoutLib
    LINK
        Gaudi::GaudiKernel
        LHCb::LHCbKernel
        LHCb::MCEvent
)

if (NOT USE_DD4HEP)
  set(DetDescOnlyFiles
    src/component/legacy/RichDetailedFrontEndResponsePMT.cpp
  )
  # This is working around floating point exceptions when compiling RichDetailedFrontEndResponsePMT
  # with clang in sse mode. In that mode, the 2 calls to getTimeSlotIndex in processDeposit are
  # optimized into a single SSE division. Now the SSE vector register holds 4 floats, and the
  # remaining slots are filled with 0s for both operands, leading to a 'invalid' FPE.
  if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" )
    set_source_files_properties( src/component/legacy/RichDetailedFrontEndResponsePMT.cpp PROPERTIES COMPILE_OPTIONS -fno-slp-vectorize )
  endif()
endif()

gaudi_add_module(RichReadout
    SOURCES
        ${DetDescOnlyFiles}
        src/component/MCRichDigitsToRawBufferAlg.cpp
        src/component/RichCopySummedDepositsToDigits.cpp
        src/component/RichSignal.cpp
        src/component/RichSummedDeposits.cpp
    LINK
        RichReadoutLib
        Boost::headers
        Gaudi::GaudiKernel
        GSL::gsl
        LHCb::DAQEventLib
        LHCb::GenEvent
        LHCb::LHCbKernel
        LHCb::MCEvent
        LHCb::MCInterfaces
        LHCb::PartPropLib
        LHCb::RichDetLib
        LHCb::RichFutureDAQLib
        LHCb::RichInterfaces
        LHCb::RichKernelLib
        LHCb::RichUtils
        LHCb::RichFutureKernel
)
