###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
import glob
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    # Cambridge
    "/usera/jonesc/NFS/data/MC/Run3/NewSimBM/13104011/SIM/"
]

print "Data Files :-"
data = []
for path in searchPaths:
    files = sorted(glob.glob(path + "*.sim"))
    data += ["PFN:" + file for file in files]
    #for f in files: print f

IOHelper('ROOT').inputFiles(data, clear=True)
FileCatalog().Catalogs = ['xmlcatalog_file:out.xml']

from Configurables import LHCbApp
LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"

LHCbApp().DDDBtag = 'upgrade/master'
LHCbApp().CondDBtag = 'upgrade/jonrob/rich-update-cable-maps'

# export GITCONDDBPATH=/usera/jonesc/LHCbCMake/GitDB
